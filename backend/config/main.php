<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_backendUser', // unique for backend
                'path'=>'/backend/web'  // correct path for the backend app.
            ]
        ],
        'session' => [
            'name' => '_backendSessionId', // unique for backend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=theopsmg_teachsity',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
                'class' => 'yii\web\UrlManager',
                // Disable index.php
                // Disable r= routes
                'enablePrettyUrl' => true,
                'enableStrictParsing' => true,
                'showScriptName' => false,
              //  'suffix' => '.php',
                'rules' => array(
                    ''=>"site/dashboard",
                    'index'=>"site/dashboard",
                    '<_a:(auth|indextest|facebooktoken|linkedintoken)>'=>'site/<_a>',
                    '<controller:\w+>/<action>'=>'<controller>/<action>',
                    '<controller>/<id:\d+>/<action>' => '<controller>/<action>',

                ),
        ],
    ],
    'params' => $params,
];
