<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */

  //
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login',"auth","fbauth" ,'error','facebooktoken', 'linkedintoken','twittertoken',"googleplustoken"],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionDashboard(){
        
        return $this->render("dashboard");
    }

    public function actionLogin()
    {
        $this->layout="layout_login";
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionFacebooktoken(){
      $session = new \yii\web\Session;
      $session->open();
      FacebookSession::setDefaultApplication('1680785638822349','78bd0d1a1eac9152f182ad8cebb2a412');
      $scope = array('manage_pages');
      $helper = new FacebookRedirectLoginHelper(Url::base(true)."/index.php/site/fbauth");
      $loginUrl = $helper->getLoginUrl($scope);
      return $this->redirect($loginUrl);

      /*FacebookSession::setDefaultApplication('1680785638822349','78bd0d1a1eac9152f182ad8cebb2a412');
      $session = new FacebookSession($_GET['access_token']);

      try {
        $me = (new FacebookRequest(
          $session, 'GET', '/me'
        ))->execute()->getGraphObject(GraphUser::className());
        echo $me->getName();
      } catch (FacebookRequestException $e) {
        // The Graph API returned an error
      }*/

      return $this->render("facebook");

    }
    public function actionFbauth(){

        $session = new \yii\web\Session;
        $session->open();
        FacebookSession::setDefaultApplication('1680785638822349','78bd0d1a1eac9152f182ad8cebb2a412');
        $helper = new FacebookRedirectLoginHelper(Url::base(true)."/index.php/site/fbauth");
        try {
              $session = $helper->getSessionFromRedirect();
          } catch(FacebookRequestException $ex) {
              // When Facebook returns an error
          }
        if ($session) {
          //This will get page access_token that lasts for 2 hours days
          $session->getLongLivedSession();
          $request = new FacebookRequest($session, 'GET', '/741328445977404?fields=access_token');
          $response = $request->execute();
          $result = $response->getGraphObject()->asArray();
          $pageToken = $result['access_token'];
          $facebookSession = new FacebookSession($pageToken);

          Yii::$app->db->createCommand("truncate table facebooktoken")->execute();
          Yii::$app->db->createCommand()->insert('facebooktoken', [
                'token' => $pageToken
            ])->execute();
            echo "Token has been saved";

          //This will get user extended token that lasts for 60days
          /*  try {
                $user_profile = (new FacebookRequest(
                    $session, 'GET', '/me'
                ))->execute()->getGraphObject(GraphUser::className());
                echo "Name: " . $user_profile->getName();
            } catch(FacebookRequestException $e) {
                echo "Exception occured, code: " . $e->getCode();
                echo " with message: " . $e->getMessage();
            }
          /*  $longLivedSession = $session->getLongLivedSession();
          echo $longLivedSession->getToken();*/

        }
    }
    public function actionAuth(){
      if(Yii::$app->request->get("code")!=""){

        $code = Yii::$app->request->get("code");
        $client_secret ='agVLciPdN8j5tSlx';
        $grant_type = "authorization_code";
        $client_id ="75s20s0zytlbeh";
        $redirect_uri =Url::base(true)."/index.php/auth";

        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?'. http_build_query(['client_secret'=>$client_secret,'grant_type' =>$grant_type,'client_id' => $client_id,'code' => $code,'redirect_uri' =>$redirect_uri]);
        $context = stream_context_create(['http' => ['method' => 'POST']]);
        $response= json_decode(file_get_contents($url, false, $context),true);
        Yii::$app->db->createCommand("truncate table linkedintoken")->execute();
        Yii::$app->db->createCommand()->insert('linkedintoken', [
              'linkedin' => $response["access_token"],
              'expirationdate'=>gmdate("Y-m-d H:i:s",time()+$response["expires_in"])
          ])->execute();

          echo gmdate("Y,m,d H:i:s",time()+$response["expires_in"]);
      }
       //return $this->redirect($url);
    }
    public function actionLinkedintoken(){
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?'. http_build_query(['response_type' => 'code','client_id' => "75s20s0zytlbeh",'scope' => "rw_company_admin w_share",'state' => uniqid('', true),'redirect_uri' => Url::base(true)."/index.php/auth"]);
         return $this->redirect($url);
    }




}
