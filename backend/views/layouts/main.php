<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Materialism Angular Admin Theme">
    <meta name="author" content="Theme Guys - The Netherlands">
    <meta name="msapplication-TileColor" content="#9f00a7">
    <meta name="msapplication-TileImage" content="<?=Url::base()?>/assets/img/favicon/mstile-144x144.png">
    <meta name="msapplication-config" content="<?=Url::base()?>/assets/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=Url::base()?>/assets/img/favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="<?=Url::base()?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?=Url::base()?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="<?=Url::base()?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?=Url::base()?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?=Url::base()?>/assets/img/favicon/manifest.json">
    <link rel="shortcut icon" href="<?=Url::base()?>/assets/img/favicon/favicon.ico">
    <title>Teachsity Admin</title>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="<?=Url::base()?>/assets/css/vendors.min.css" rel="stylesheet" />
    <link href="<?=Url::base()?>/assets/css/styles.min.css" rel="stylesheet" />
    <script charset="utf-8" src="//maps.google.com/maps/api/js?sensor=true"></script>
    <script charset="utf-8" src="<?=Url::base()?>/assets/js/vendors.min.js"></script>
    <script charset="utf-8" src="<?=Url::base()?>/assets/js/app.min.js"></script>

</head>
<body scroll-spy="" id="top" class=" theme-template-dark theme-pink alert-open alert-with-mat-grow-top-right">
    <?php $this->beginBody() ?>
    <main>
      <aside class="sidebar fixed" style="width: 260px; left: 0px; ">
        <div class="brand-logo">
          <div id="logo">
            <div class="foot1"></div>
            <div class="foot2"></div>
            <div class="foot3"></div>
            <div class="foot4"></div>
          </div> Materialism </div>
        <div class="user-logged-in">
          <div class="content">
            <div class="user-name">Katsumoto <span class="text-muted f9">admin</span></div>
            <div class="user-email">last@samurai.jp</div>
            <div class="user-actions"> <a class="m-r-5" href="#">settings</a> <a href="#">logout</a> </div>
          </div>
        </div>
        <ul class="menu-links">
          <li icon="md md-blur-on"> <a href="index.html"><i class="md md-blur-on"></i>&nbsp;<span>Dashboard</span></a></li>
            <li> <a href="#"><i class="md md-my-library-books"></i>&nbsp;Courses</a>
          </li>
          <li> <a href="#"><i class="md md-account-circle"></i>&nbsp;Users</a>
          </li>
            <!--
                      <li> <a href="#" data-toggle="collapse" data-target="#employee" aria-expanded="false" aria-controls="Employee"><i class="md md-camera"></i>&nbsp;Courses</a>
                        <ul id="employee" class="collapse">
                          <li> <a href="#"><span>Trainings</span></a></li>
                        </ul>
                      </li>
            -->
          <li> <a href="#"><i class="md md-school"></i>&nbsp;Students</a>
          </li>
          <li> <a href="#"><i class="md md-people"></i>&nbsp;Tutors</a>
          </li>
          
         


        </ul>
      </aside>
      <div class="main-container">
        <?=$content?>
      </div>
    </main>


    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
