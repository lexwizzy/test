<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Teachsity - Login';
?>
<div class="card bordered z-depth-2" style="margin:0% auto; max-width:400px;">
       <div class="card-header">
         <div class="brand-logo" >
             <img width="100%" src="<?=Url::base()?>/assets/img/logo.gif"/>
       </div>
       <div class="card-content">
         <div class="m-b-30">
           <div class="card-title strong pink-text">Login</div>
           <p class="card-title-desc"> Welcome to Teachsity Admin. </p>
         </div>
          <?php
              $form =ActiveForm::begin()
           ?>
         <form class="form-floating">
           <div class="form-group">
             <label for="emailId" class="control-label">Email</label>
             <?=$form->field($model,"email")->textInput(["id"=>"emailId"])->label(false);?>
           <div class="form-group">
             <label for="inputPassword" class="control-label">Password</label>
             <?=$form->field($model,"password")->passwordInput(["id"=>"inputPassword"])->label(false);?>
           <div class="form-group">
             <div class="checkbox">
               <label>
                <?=$form->field($model,"rememberMe")->checkBox()->label("Remember me");?>
             </div>
           </div>
         </form>
       <?php ActiveForm::end();?>
       </div>
       <div class="card-action clearfix">
         <div class="pull-right">
           <button type="button" class="btn btn-link black-text">Forgot password</button>
           <button type="submit" class="btn btn-link black-text">Login</button>
         </div>
       </div>
     </div>
