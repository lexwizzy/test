<?php
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\web\View;

?>

<?php
if(Yii::$app->request->get("access_token")==""){

$fb=<<<JS
        $.ajaxSetup({ cache: true });
          $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
            FB.init({
              appId: '1680785638822349',
              version: 'v2.4',
            //  cookie  : true // or v2.0, v2.1, v2.2, v2.3
            });

          });
        window.onload=function(){
        FB.login(function (response) {
           if (response.authResponse) {
             console.log(response.authResponse.accessToken);
            // window.location="facebooktoken?access_token="+response.authResponse.accessToken;
           }
          }, {scope: 'email'});
      }

JS;
$this->registerJs($fb,View::POS_READY);
}
?>
<?= Html::a( 'Log in with <i class="fa pull-right fa-facebook fa-lg"></i> Facebook', ['site/auth', 'authclient'=> "facebook", ], ['class' => "cust-btn facebook "]) ?>
