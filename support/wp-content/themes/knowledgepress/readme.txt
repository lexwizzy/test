Theme Name:         KnowledgePress
Theme URI:          http://pressapps.co/
Description:        Responsive Knowledge base WordPress theme.
Author:             PressApps
Author URI:         http://pressapps.co/

License: GNU General Public License and Themeforest License
License URI: http://themeforest.net/licenses/regular_extended
