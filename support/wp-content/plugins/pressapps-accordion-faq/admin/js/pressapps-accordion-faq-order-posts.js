(function($) {
    "use strict";
    $("#the-list").sortable({
        items: "tr",
        opacity: .6,
        cursor: "move",
        axis: "y",
        update: function() {
            var order = $(this).sortable("serialize") + "&action=pafa_order_update_posts";
            $.post(ajaxurl, order, function(response) {});
        }
    });
})(jQuery);