<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://pressapps.co
 * @since      1.0.0
 *
 * @package    Pressapps_Accordion_Faq
 * @subpackage Pressapps_Accordion_Faq/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pressapps_Accordion_Faq
 * @subpackage Pressapps_Accordion_Faq/includes
 * @author     PressApps
 */
class Pressapps_Accordion_Faq_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
