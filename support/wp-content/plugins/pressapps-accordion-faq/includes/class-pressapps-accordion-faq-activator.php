<?php

/**
 * Fired during plugin activation
 *
 * @link       http://pressapps.co
 * @since      1.0.0
 *
 * @package    Pressapps_Accordion_Faq
 * @subpackage Pressapps_Accordion_Faq/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pressapps_Accordion_Faq
 * @subpackage Pressapps_Accordion_Faq/includes
 * @author     PressApps
 */
class Pressapps_Accordion_Faq_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
