(function($) {
    "use strict";
    $(".pafa-block-a").hide();
    $(".pafa-block").click(function() {
        var th = $(this).hasClass("pafa-block-open");
        var icon = $(".pressapps_faq_block").find("i." + icons.faq_open);
        $(".pafa-block").removeClass("pafa-block-open");
        icon.removeClass(icons.faq_open).addClass(icons.faq_close);
        $(".pafa-block-a").slideUp();
        if (th) {
            $(this).find(".pafa-block-a").first().slideUp();
            $(this).removeClass("pafa-block-open");
        } else {
            $(this).find(".pafa-block-a").first().slideDown();
            $(this).addClass("pafa-block-open");
            $(this).find("i").removeClass(icons.faq_close).addClass(icons.faq_open);
        }
    });
})(jQuery);