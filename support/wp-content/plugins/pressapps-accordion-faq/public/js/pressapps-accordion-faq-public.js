(function($) {
    "use strict";
    $(".pafa-accordion-a").hide();
    $(".pafa-accordion-q").click(function() {
        var th = $(this).hasClass("pafa-accordion-open");
        var icon = $(".pressapps_faq_accordion").find("i." + icons.faq_open);
        $(".pafa-accordion-q").removeClass("pafa-accordion-open");
        $(".pafa-accordion-a").slideUp();
        icon.removeClass(icons.faq_open).addClass(icons.faq_close);
        if (th) {
            $(this).parents(".pafa-accordion").first().find(".pafa-accordion-a").slideUp();
            $(this).removeClass("pafa-accordion-open");
        } else {
            $(this).parents(".pafa-accordion").first().find(".pafa-accordion-a").slideDown();
            $(this).addClass("pafa-accordion-open");
            $(this).find("i").removeClass(icons.faq_close).addClass(icons.faq_open);
        }
    });
})(jQuery);

(function($) {
    "use strict";
    jQuery(".pafa-list-cat a, .pafa-list-q a, .pafa-back-top").each(function() {
        var destination = "";
        jQuery(this).click(function() {
            var elementClicked = jQuery(this).attr("href");
            var elementOffset = $(this).parents(".pressapps_faq_list").find(elementClicked).offset();
            destination = elementOffset.top;
            jQuery("html,body").animate({
                scrollTop: destination - 30
            }, 500);
            return false;
        });
    });
})(jQuery);

(function($) {
    "use strict";
    $(".pafa-block-a").hide();
    $(".pafa-block").click(function() {
        var th = $(this).hasClass("pafa-block-open");
        var icon = $(".pressapps_faq_block").find("i." + icons.faq_open);
        $(".pafa-block").removeClass("pafa-block-open");
        icon.removeClass(icons.faq_open).addClass(icons.faq_close);
        $(".pafa-block-a").slideUp();
        if (th) {
            $(this).find(".pafa-block-a").first().slideUp();
            $(this).removeClass("pafa-block-open");
        } else {
            $(this).find(".pafa-block-a").first().slideDown();
            $(this).addClass("pafa-block-open");
            $(this).find("i").removeClass(icons.faq_close).addClass(icons.faq_open);
        }
    });
})(jQuery);