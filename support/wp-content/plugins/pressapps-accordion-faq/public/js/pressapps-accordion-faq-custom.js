(function($) {
    "use strict";
    jQuery(".faq-content").hide();
    jQuery(".faq .entry-title").click(function() {
        var th = jQuery(this).find("i." + icons.faq_close);
        var open_icon = jQuery("#pressapps_faq_accordion").find("i." + icons.faq_open);
        open_icon.removeClass(icons.faq_open).addClass(icons.faq_close);
        if (th.length) {
            th.removeClass(icons.faq_close).addClass(icons.faq_open);
            jQuery(".faq-content").slideUp();
            jQuery(this).parents(".faq").first().find(".faq-content").slideDown();
        } else {
            th = jQuery(this).find("i." + icons.faq_open);
            jQuery(this).parents(".faq").first().find(".faq-content").slideUp();
            th.removeClass(icons.faq_open).addClass(icons.faq_close);
        }
    });
})(jQuery);