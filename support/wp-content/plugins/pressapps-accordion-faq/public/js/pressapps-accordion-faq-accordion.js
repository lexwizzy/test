(function($) {
    "use strict";
    $(".pafa-accordion-a").hide();
    $(".pafa-accordion-q").click(function() {
        var th = $(this).hasClass("pafa-accordion-open");
        //var icon = jQuery("#pressapps_faq_accordion").find( 'i.' + icons.faq_open );
        var icon = $(".pressapps_faq_accordion").find("i." + icons.faq_open);
        $(".pafa-accordion-q").removeClass("pafa-accordion-open");
        $(".pafa-accordion-a").slideUp();
        icon.removeClass(icons.faq_open).addClass(icons.faq_close);
        if (th) {
            $(this).parents(".pafa-accordion").first().find(".pafa-accordion-a").slideUp();
            $(this).removeClass("pafa-accordion-open");
        } else {
            $(this).parents(".pafa-accordion").first().find(".pafa-accordion-a").slideDown();
            $(this).addClass("pafa-accordion-open");
            $(this).find("i").removeClass(icons.faq_close).addClass(icons.faq_open);
        }
    });
})(jQuery);