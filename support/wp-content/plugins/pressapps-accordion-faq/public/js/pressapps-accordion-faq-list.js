(function($) {
    "use strict";
    jQuery(".pafa-toc .pafa-section-heading a, .pafa-toc .entry-title a, .pafa-back-top").each(function() {
        var destination = "";
        jQuery(this).click(function() {
            var elementClicked = jQuery(this).attr("href");
            //var elementOffset = jQuery('body').find(elementClicked).offset();
            var elementOffset = $(this).parents(".pressapps_faq_list").find(elementClicked).offset();
            destination = elementOffset.top;
            jQuery("html,body").animate({
                scrollTop: destination - 30
            }, 500);
            return false;
        });
    });
})(jQuery);