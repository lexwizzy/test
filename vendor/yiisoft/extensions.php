<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable',
    ),
  ),
  'zelenin/yii2-summernote-widget' => 
  array (
    'name' => 'zelenin/yii2-summernote-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@Zelenin/yii/widgets/Summernote' => $vendorDir . '/zelenin/yii2-summernote-widget',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker',
    ),
  ),
  'karpoff/yii2-crop-image-upload' => 
  array (
    'name' => 'karpoff/yii2-crop-image-upload',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@karpoff/icrop' => $vendorDir . '/karpoff/yii2-crop-image-upload',
    ),
  ),
  'russ666/yii2-countdown' => 
  array (
    'name' => 'russ666/yii2-countdown',
    'version' => '0.1.0.0',
    'alias' => 
    array (
      '@russ666/widgets' => $vendorDir . '/russ666/yii2-countdown',
    ),
  ),
  'kartik-v/yii2-social' => 
  array (
    'name' => 'kartik-v/yii2-social',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/social' => $vendorDir . '/kartik-v/yii2-social',
    ),
  ),
  'yiisoft/yii2-sphinx' => 
  array (
    'name' => 'yiisoft/yii2-sphinx',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/sphinx' => $vendorDir . '/yiisoft/yii2-sphinx',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead',
    ),
  ),
  'imanilchaudhari/yii2-currency-converter' => 
  array (
    'name' => 'imanilchaudhari/yii2-currency-converter',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@imanilchaudhari/CurrencyConverter' => $vendorDir . '/imanilchaudhari/yii2-currency-converter',
    ),
  ),
  'scotthuangzl/yii2-google-chart' => 
  array (
    'name' => 'scotthuangzl/yii2-google-chart',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@scotthuangzl/googlechart' => $vendorDir . '/scotthuangzl/yii2-google-chart',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'bupy7/yii2-widget-cropbox' => 
  array (
    'name' => 'bupy7/yii2-widget-cropbox',
    'version' => '4.1.1.0',
    'alias' => 
    array (
      '@bupy7/cropbox' => $vendorDir . '/bupy7/yii2-widget-cropbox',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.3.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '1.7.4.0',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.1.1.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'marqu3s/yii2-summernote' => 
  array (
    'name' => 'marqu3s/yii2-summernote',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@marqu3s/summernote' => $vendorDir . '/marqu3s/yii2-summernote',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
);
