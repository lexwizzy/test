<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\jui\\' => array($vendorDir . '/yiisoft/yii2-jui'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\codeception\\' => array($vendorDir . '/yiisoft/yii2-codeception'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap'),
    'yii\\authclient\\' => array($vendorDir . '/yiisoft/yii2-authclient'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'scotthuangzl\\googlechart\\' => array($vendorDir . '/scotthuangzl/yii2-google-chart'),
    'russ666\\widgets\\' => array($vendorDir . '/russ666/yii2-countdown'),
    'marqu3s\\summernote\\' => array($vendorDir . '/marqu3s/yii2-summernote'),
    'kartik\\typeahead\\' => array($vendorDir . '/kartik-v/yii2-widget-typeahead'),
    'kartik\\rating\\' => array($vendorDir . '/kartik-v/yii2-widget-rating'),
    'kartik\\popover\\' => array($vendorDir . '/kartik-v/yii2-popover-x'),
    'kartik\\plugins\\popover\\' => array($vendorDir . '/kartik-v/bootstrap-popover-x'),
    'kartik\\plugins\\fileinput\\' => array($vendorDir . '/kartik-v/bootstrap-fileinput'),
    'kartik\\grid\\' => array($vendorDir . '/kartik-v/yii2-grid'),
    'kartik\\file\\' => array($vendorDir . '/kartik-v/yii2-widget-fileinput'),
    'kartik\\editable\\' => array($vendorDir . '/kartik-v/yii2-editable'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base'),
    'imanilchaudhari\\CurrencyConverter\\' => array($vendorDir . '/imanilchaudhari/yii2-currency-converter'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'bupy7\\cropbox\\' => array($vendorDir . '/bupy7/yii2-widget-cropbox'),
    'Razorpay\\Tests\\' => array($vendorDir . '/razorpay/razorpay/tests'),
    'Razorpay\\Api\\' => array($vendorDir . '/razorpay/razorpay/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'JmesPath\\' => array($vendorDir . '/mtdowling/jmespath.php/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Aws\\' => array($vendorDir . '/aws/aws-sdk-php/src'),
);
