<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      // 'css/style.css',
      // 'css/library/bootstrap.min.css'
        'css/library/font-awesome.min.css',
        'css/animates.css',
        'css/owl.carousel.css',
         'css/teachsity.css',
         'css/responsive.css',
         'css/jquery.Jcrop.css',
        // 'css/materialdesignicons.min.css',
        'css/bootstrap.min.css',
         'css/bootstrap-theme.min.css',

    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/owl.carousel.js',
        'js/library/jquery.appear.min.js',
        'js/library/perfect-scrollbar.min.js',
        'js/library/jquery.easing.min.js',
       'js/scripts.js',
       'js/library/jquery.magnific-popup.min.js',
        'js/library/jquery.Jcrop.min.js',
        'js/library/jquery.cookie.js',
        'js/photo.js',
        'js/common.js',



    ];
    public $depends = [
        'yii\web\YiiAsset',
      // 'yii\bootstrap\BootstrapAsset',
    ];
}
