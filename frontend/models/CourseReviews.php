<?php

namespace frontend\models;

use Yii;
use kartik\rating\StarRating;
/**
 * This is the model class for table "course_reviews".
 *
 * @property string $id
 * @property string $courseid
 * @property string Yii::$app->user->id
 * @property string $rating
 * @property string $title
 * @property string $comment
 * @property string $date
 */
class CourseReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'userid', 'rating', 'comment'], 'required'],
            [['courseid', 'userid', 'rating'], 'integer'],
            [['date'], 'safe'],
            //[['title'], 'string', 'max' => 70],
            [['comment'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'userid' => 'Userid',
            'rating' => 'Rating',
           // 'title' => 'Title',
            'comment' => 'Comment',
            'date' => 'Date',
        ];
    }
    public function totalReviews($courseid){
       return  Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid")->queryScalar();
    }

    public function getAverageRating($courseid){
        $one = Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid and rating =1")->queryScalar();
        $two = Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid and rating =2")->queryScalar();
        $three = Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid and rating =3")->queryScalar();
        $four = Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid and rating =4")->queryScalar();
        $five = Yii::$app->db->createCommand("select count(rating) from course_reviews where courseid=$courseid and rating =5")->queryScalar();

        $total= $one+$two+$three+$four+$five;
        if($total==0)
            $average= 0;
        else
            $average = ((1*$one)+(2*$two)+(3*$three)+(4*$four)+(5*$five))/$total;
            return round($average);
       /* switch(round($average)){
            case 0: return '<a href="#"></a><a href="#" ></a><a href="#" ></a><a href="#"></a><a href="#"></a>';
                    break;
            case 1: return '<a href="#" class="active"></a><a href="#" ></a><a href="#" ></a><a href="#"></a><a href="#"></a>';
                    break;
            case 2: return '<a href="#" class="active"></a><a href="#" class="active"></a><a href="#" ></a><a href="#"></a><a href="#"></a>';
                    break;
            case 3: return '<a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a><a href="#"></a><a href="#"></a>';
                    break;
            case 4: return '<a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a><a href="#"></a>';
                    break;
            case 5: return '<a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a><a href="#" class="active"></a>';
                    break;

        }*/
    }
    public function getCommentCount($courseid){
        return Yii::$app->db->createCommand("select count(comment) from course_reviews where courseid=$courseid")->queryScalar();
    }

    public function getCourseReviews($courseid){
       return CourseReviews::find()->where(['courseid'=>$courseid])->orderBy("date desc")->all();
    }
    public function designReviewBox($courseid){
        $r="";
        $firstname="";
        $lastname="";
        foreach(CourseReviews::getCourseReviews($courseid) as $review):
            $name=\frontend\models\User::getName($review->userid);
              if(!empty($name["firstname"]))
                $firstname=$name["firstname"];
              if(!empty($name["lastname"]))
                $lastname=$name["lastname"];

            //if(!empty($name["firstname"]) && !empty($name["lastname"])){
              $r.='<div class="review_box">
              <div class="review_box_left">
                  <img  class="small_profile_pix profile_pix" src="'.\frontend\models\User::getPhoto($review->userid,"black").'">
                </div>
              <div class="review_box_right">
                  <p class="review_box_name">'.$firstname." ".$lastname.'</p>
                    <p class="rate_star">'.StarRating::widget([
                                                 'name' => 'rating',
                                                 'value'=> $review->rating,
                                                 'pluginOptions' => [
                                                     'size' => 'xxs',
                                                     'stars' => 5,
                                                     'min' => 0,
                                                     'step' => 1,
                                                     'readonly'=>true,
                                                     'clearButton'=>'',
                                                     'max' => 5,
                                                     'showCaption' => false,

                                                     ],
                       ]).'</p>
                    <p><em>'.Utility::calculateTime(strtotime($review->date)).'</em></p>
                    <p class="review_txt">'.$review->comment.'</p>
                </div>
          </div>
          <div class="h_line"></div>';
          //  }

        endforeach;
        return $r;
    }
    public function designReviewBox2($courseid){
        $r="";
        foreach(CourseReviews::getCourseReviews($courseid) as $review):
            $name=\frontend\models\User::getName($review->userid);
            $r.='<li class="review"> <div class="body-review"> <div class="review-author"> <a href="#"> <img class="small_profile_pix profile_pix"  src="'.\frontend\models\User::getPhoto($review->userid,"black").'" alt="">  </div><div class="content-review"> <div class="row"><div class="col-md-6  text-left"><h4 class="sm black"> <a href="#">'.$name['firstname']." ".$name['lastname'].'</a> </h4></div> <div class="col-md-6 text-right">'.StarRating::widget([
                                            'name' => 'rating',
                                            'value'=> $review->rating,
                                            'pluginOptions' => [
                                                'size' => 'xxs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>true,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]).'</div></div><em>'.Utility::calculateTime(strtotime($review->date)).'</em> <p>'.$review->comment.'</p></div></div></li>';
        endforeach;
        return $r;
    }
}
