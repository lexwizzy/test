<?php

namespace frontend\models;

use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "course_announcement".
 *
 * @property string $id
 * @property string $courseid
 * @property string $userid
 * @property string $title
 * @property string $comment
 * @property string $date
 */
class CourseAnnouncement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_announcement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'userid', 'title'], 'required'],
            [['courseid', 'userid'], 'integer'],
            [['comment'], 'string'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'userid' => 'Userid',
            'title' => 'Title',
            'comment' => 'Comment',
            'date' => 'Date',
        ];
    }

    public function getCourseAnnouncement($courseid){
        $li="";
        $a= CourseAnnouncement::find()->where(["courseid"=>$courseid])->orderBy("date desc")->all();
        foreach($a as $announcement):
            $li.=CourseAnnouncement::designCourseLearnAnnouncement($announcement);
        endforeach;
        return $li;
    }

    public function designCourseLearnAnnouncement($announcement){
        $name = \frontend\models\User::getName($announcement->userid);
        return '<li>
                                        <div class="list-body">
                                        <span class="icon">
                                            <img src="'.Url::base().'/images/icon/speaker-2.png">
                                            </span>
                                            <div class="list-content">
                                                <h4 class="sm black bold">
                                                    <a href="#">'.$announcement->title.'</a>
                                                </h4>
                                                <p>'.$announcement->comment.'</p>
                                                <div class="author">By <a href="#">'.$name['firstname']." ".$name['lastname'].'</a></div>
                                                <em>'.Utility::calculateTime(strtotime($announcement->date)).'</em>
                                            </div>
                                        </div>
                                    </li>';
    }
}
