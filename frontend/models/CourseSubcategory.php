<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_subcategory".
 *
 * @property string $id
 * @property string $category
 * @property string $subcategory
 *
 * @property CourseCategory $category0
 */
class CourseSubcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_subcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'subcategory'], 'required'],
            [['category', 'subcategory'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'subcategory' => 'Subcategory',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory0()
    {
        return $this->hasOne(CourseCategory::className(), ['id' => 'category']);
    }
}
