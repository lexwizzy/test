<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "completed_quiz".
 *
 * @property string $id
 * @property string $userid
 * @property string $quizid
 * @property string $score
 */
class CompletedQuiz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'completed_quiz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'quizid', 'score','total'], 'required'],
            [['userid', 'quizid', 'score','total'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'quizid' => 'Quizid',
            'score' => 'Score',
        ];
    }
}
