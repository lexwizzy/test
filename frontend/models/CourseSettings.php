<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_setting".
 *
 * @property string $id
 * @property string $courseid
 * @property string $privacy
 * @property string $password
 * @property string $google_analytics
 * @property string $adwords_conversionid
 * @property string $adwords_label
 * @property string $gtm_container
 *
 * @property Courses $course
 */
class CourseSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid'], 'required'],
            [['courseid', 'adwords_conversionid','autopub'], 'integer'],
            [['privacy'], 'string', 'max' => 20],
            [['google_tag_manager'],'string','max'=>20],
            [['autopub'], 'integer'],
             ['autopub','default','value'=>"1"],
            [['password'], 'string', 'max' => 1000],
            ['password', 'required', 'when' => function($model) {
                return $model->privacy == 'private';
            }],
            [['google_analytics'], 'string', 'max' => 15],
            [['adwords_lpcl','adwords_cpcl','adwords_spcl'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'privacy' => 'Privacy',
            'password' => 'Password',
            'google_analytics' => 'Google Analytics',
            'adwords_conversionid' => 'Adwords Conversion Id',
            'adwords_lpcl'=>'Landing Page Conversion Label',
            'adwords_cpcl'=>'Checkout Page Conversion Label',
            'adwords_spcl'=>'Success Page Conversion Label'
            //'adwords_label' => 'Adwords Label',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }
}
