<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "quizselected_ans".
 *
 * @property string $userid
 * @property string $quizid
 * @property string $question
 * @property string $answers
 * @property string $date
 */
class QuizselectedAns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quizselected_ans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'quizid', 'question', 'answers'], 'required'],
            [['quizid', 'question'], 'integer'],
            [['answers'], 'string'],
            [['date'], 'safe'],
            [['userid'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'quizid' => 'Quizid',
            'question' => 'Question',
            'answers' => 'Answers',
            'date' => 'Date',
        ];
    }
}
