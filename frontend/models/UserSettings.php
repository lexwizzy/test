<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_setting".
 *
 * @property string Yii::$app->user->id
 * @property string $newmessage
 * @property string $course_progress
 * @property string $tas
 * @property string $tcr
 * @property string $ssfm
 * @property string $sml
 * @property string $sme
 * @property string $wmcr
 *
 * @property User $user
 */
class UserSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'required'],
            [['userid', 'newmessage', 'course_progress', 'tas', 'tcr', 'ssfm', 'sml', 'sme', 'wmcr'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'newmessage' => 'Newmessage',
            'course_progress' => 'Course Progress',
            'tas' => 'Tas',
            'tcr' => 'Tcr',
            'ssfm' => 'Ssfm',
            'sml' => 'Sml',
            'sme' => 'Sme',
            'wmcr' => 'Wmcr',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
