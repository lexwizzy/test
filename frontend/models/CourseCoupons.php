<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_coupons".
 *
 * @property integer $id
 * @property integer $courseid
 * @property string $coupon_code
 * @property string $discount
 * @property string $expirey_date
 * @property string $date
 *
 * @property Courses $course
 */
class CourseCoupons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_coupons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'coupon_code', 'discount', 'expirey_date'], 'required'],
            [['courseid'], 'integer'],
            [['discount'], 'number',"max"=>100],
            [['expirey_date', 'date'], 'safe'],
            [['coupon_code'], 'string', 'max' => 20],
            [['coupon_code'],'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'coupon_code' => 'Coupon Code',
            'discount' => 'Discount',
            'expirey_date' => 'Expirey Date',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    //calcute discount and sales price
    public function getSalesPrice($ori_price,$rate){
      $discount = ($ori_price*$rate)/100;
      $sale_price = $ori_price-$discount;
      return $sale_price;
    }
    public function getRate($coupon){
       $c = CourseCoupons::find()->where(["coupon_code"=>$coupon])->one();
       return $c->discount;
    }
    public function getDiscountAmount($ori_price,$rate){
      $discount = ($ori_price*$rate)/100;
      return $discount;
    }
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }
     public function getCoupons($courseid){
        $couponArray = array();
        $coupons=CourseCoupons::find()->where(['courseid'=>$courseid])->all();
        //return \yii\helpers\JSON::encode($coupons);
        return $coupons;

    }

    public function validCoupon($courseid){

    }
}
