<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for index "courses".
 *
 * @property integer $id
 * @property string $course_banner
 * @property string $course_desc
 * @property string $slug
 * @property string $course_title
 * @property integer $cid
 * @property integer $owner
 * @property integer $course_category
 * @property integer $status
 * @property string $createdate
 */
class CoursesIndex extends \yii\sphinx\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function indexName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id', 'cid', 'owner', 'course_category', 'status'], 'integer'],
            [['course_banner', 'course_desc', 'slug', 'course_title'], 'string'],
            [['createdate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_banner' => 'Course Banner',
            'course_desc' => 'Course Desc',
            'slug' => 'Slug',
            'course_title' => 'Course Title',
            'cid' => 'Cid',
            'owner' => 'Owner',
            'course_category' => 'Course Category',
            'status' => 'Status',
            'createdate' => 'Createdate',
        ];
    }

    //$query = new \yii\sphinx\Query;
    //print_r(CourseCategory::find()->all());

  /* $query->select("course_title,cid, WEIGHT()")->from("courses")

     ->match(new \yii\db\Expression("'@(course_title,course_desc)". Yii::$app->sphinx->escapeMatchValue($search_query)."'" ))
    // ->where("status = 1");
    ;
     $command = $query->createCommand();
     $rows= $command->queryAll();
     $q=count($rows).' Search results for "'.$search_query.'"';*/
}
