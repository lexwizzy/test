<?php

namespace frontend\models;

use Yii;
use frontend\models\Utility;
use yii\base\ErrorException;
use frontend\model\LessonContents;


/**
 * This is the model class for table "tc_uploaded_content".
 *
 * @property integer $id
 * @property string $filename
 * @property string $new_filename
 * @property string $type
 * @property string $size
 * @property string $dimension
 * @property integer $length
 * @property integer $no_of_pages
 * @property string uploaded_file
 * @property string $uploaded_date
 * @property integer $userid
 *
 * @property User $user
 */
class TcUploadedContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $uploaded_file;
    public $single_upload;
    public static function tableName()
    {
        return 'tc_uploaded_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'new_filename', 'type', 'size', 'userid'], 'required',"on"=>"withdetails"],
            [['length', 'userid','size','no_of_pages'], 'integer'],
            [['uploaded_file',],'required',"on"=>"justimage"],
            [['single_upload','filename', 'new_filename', 'type', 'size', 'userid'],'required',"on"=>"singleupload"],
            [['uploaded_date'], 'safe'],
            [['uploaded_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,mp4,bmp,gif,pdf,zip,rar,tar,ppt,pptx,avi,doc,docx,mp3', 'maxFiles' => 4,'checkExtensionByMimeType'=>false],
            [['single_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg,mp4,bmp,gif,pdf,zip,rar,tar,ppt,pptx,avi,doc,docx,mp3', 'checkExtensionByMimeType'=>false],
            [['filename',"thumbnail", 'new_filename'], 'string', 'max' => 500],
            [['type', 'dimension'], 'string', 'max' => 45]
        ];
    }
    public function singleUpload($userid){
        $time=time();
        $year =date("Y",$time);
        $month=date("m",$time);
        $path=Yii::getAlias("@aws_cc").'/'.$year.'/'.$month;
        $filename=Utility::file_newname($path,str_replace(" ","-",$this->single_upload->baseName));
        $pdfname=$filename;
        $filename=$filename.'.'.$this->single_upload->extension;
        $this->filename=$filename;
        $this->type=Utility::getFileType($this->single_upload->extension);
        $key = $time.".".$this->single_upload->extension;
        $this->new_filename=$key;
        $file_to_save=$path."/".$filename;
        $this->size=$this->single_upload->size;
        $this->userid=$userid;
       if($this->validate()){

         if(!file_exists($path)){
           mkdir($path,0755,true);
         }
         if(!file_exists($path."/thumbnail")){
           mkdir($path."/thumbnail",0755,true);
         }
         if($this->type=="video"){
              $this->length=LessonContent::getVideoLength($this->single_upload->tempName);
              $this->thumbnail=$this->single_upload->baseName.".png";
              if($this->save()){
                if($this->single_upload->saveAs($file_to_save)){
                  if(!file_exists($path."/thumbnail")){
                    mkdir($path."/thumbnail",0755,true);
                  }
                  LessonContent::createThumbnail($file_to_save,$path."/thumbnail/".$this->single_upload->baseName.".png");
                //  $transaction->commit();
                  $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/".$this->single_upload->baseName.".png' class='file-preview-other'>";
                }

                else{
                    return json_encode(["error"=>"An error occurred while uploading ".$this->single_upload->baseName.". Please try again."]);
                  }
              }
              else{
                $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
                return $li;
              }
          }
          else if($this->type=="doc"){

             $this->filename=$pdfname.".pdf";
              if($this->single_upload->extension!="pdf"){
                //$pdfname=str_replace(" ","-",$file->baseName);
                if($this->single_upload->saveAs($file_to_save)){
                  Utility::convert_to_pdf($path.'/'.$filename,$path.'/'.$pdfname.".pdf");

                  try{
                    unlink($path.'/'.$filename);
                    if(file_exists($path.'/'.$pdfname.".pdf")){
                      $this->no_of_pages=Utility::getPDFPageCount($path.'/'.$pdfname.".pdf");
                      Utility::convertPdfPageToPng($path.'/'.$pdfname.".pdf[0]",$path."/thumbnail/".$pdfname.".png");
                      $this->thumbnail=$pdfname.".png";
                      list($width,$height)=getimagesize($path."/thumbnail/".$this->thumbnail);
                      $this->dimension=$width.",".$height;
                      if($this->save()){
                          //  $transaction->commit();
                            $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/$pdfname.png' class='file-preview-other'>";
                      }
                      else{
                          try{
                            $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
                            unlink($path.'/'.$filename);
                            return $li;
                          }catch(ErrorException $e){
                            //do nothing
                          }

                        }
                    }
                    else{
                      return json_encode(["error"=>"Could not convert $pdfname into pdf"]);
                    }

                  }
                  catch(ErrorException $e){
                    // do nothing
                  }
                }else{
                  return json_encode(["error"=>"An error occurred while uploading ".$this->single_upload->baseName.". Please try again."]);
                }

              }
              else{

                  $this->no_of_pages=Utility::getPDFPageCount($this->single_upload->tempName);

                  Utility::convertPdfPageToPng($this->single_upload->tempName."[0]",$path."/thumbnail/".$pdfname.".png");
                 //return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                 list($width,$height)=getimagesize($path."/thumbnail/".$pdfname.".png");

                $this->dimension=$width.",".$height;
                $this->thumbnail=$pdfname.".png";
                if($this->save()){

                    if($this->single_upload->saveAs($file_to_save)){

                        //$transaction->commit();

                    $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/$pdfname.png' class='file-preview-other'>";

                    }else{
                      return json_encode(["error"=>"An error ocurred while saving your file"]);
                    }
                }
                else{

                    $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
                    return $li;
                }

              }
          }
          else if($this->type=="image"){
              list($width,$height)=getimagesize($this->single_upload->tempName);
              $this->dimension=$width.",".$height;
              if($this->save()){
                if($this->single_upload->saveAs($file_to_save)){
                  //$transaction->commit();
                  //$p1 = "<img style='max-width:209px' src='/upload/$year/$month/$filename' class='file-preview-other'>";
                }

                else{
                    return json_encode(["error"=>"An error occurred while uploading ".$this->single_upload->baseName.". Please try again."]);
                  }
              }
              else{
                $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
                return $li;
              }
          }
          else{
            if($this->save()){
              if($this->single_upload->saveAs($file_to_save)){
                //$transaction->commit();
                //$p1 = "<img style='max-width:150px' src='/images/unknown-file.png' class='file-preview-other'>";
              }

              else{
                  return json_encode(["error"=>"An error occurred while uploading ".$this->single_upload->baseName.". Please try again."]);
                }
            }
            else{
              $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
              return $li;
            }
          }


          return true;
       }
       else {
         $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
         return $li;
      }
    }
    public function multipleUpload($userid)
    {
      $p1="";
      $error_arr=array();
        if ($this->validate()) {
            foreach ($this->uploaded_file as $file) {
              $model = new TcUploadedContent();
              $model->scenario="withdetails";
              $time=time();
              $year =date("Y",$time);
              $month=date("m",$time);
              $path=Yii::getAlias("@aws_cc").'/'.$year.'/'.$month;
              if(!file_exists($path)){
                mkdir($path,0755,true);
              }
              if(!file_exists($path."/thumbnail")){
                mkdir($path."/thumbnail",0755,true);
              }
              //Yii::$app->end();
              $transaction= $model->getDb()->beginTransaction();
              $filename=Utility::file_newname($path,str_replace(" ","-",$file->baseName));
              $pdfname=$filename;

              $filename=$filename.'.'.$file->extension;
              $model->filename=$filename;
              $model->type=Utility::getFileType($file->extension);
              $key = $time.".".$file->extension;
              $model->new_filename=$key;
              $file_to_save=$path."/".$filename;
              $model->size=$file->size;
              $model->userid=$userid;
              if($model->type=="video"){
                   $model->length=LessonContent::getVideoLength($file->tempName);
                   $model->thumbnail=$file->baseName.".png";
                   if($model->save()){
                     if($file->saveAs($file_to_save)){
                       if(!file_exists($path."/thumbnail")){
                         mkdir($path."/thumbnail",0755,true);
                       }
                       LessonContent::createThumbnail($file_to_save,$path."/thumbnail/".$file->baseName.".png");
                       $transaction->commit();
                       $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/$file->baseName.png' class='file-preview-other'>";
                     }

                     else{
                         return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                       }
                   }
                   else{
                     $li=Utility::iterateErrors($model->activeAttributes(),$model->getErrors());
                     return $li;
                   }
               }
              else if($model->type=="doc"){
                $model->filename=$pdfname.".pdf";
                  if($file->extension!="pdf"){
                    //$pdfname=str_replace(" ","-",$file->baseName);
                    if($file->saveAs($file_to_save)){
                      Utility::convert_to_pdf($path.'/'.$filename,$path.'/'.$pdfname.".pdf");

                      try{
                        unlink($path.'/'.$filename);
                        if(file_exists($path.'/'.$pdfname.".pdf")){
                          $model->no_of_pages=Utility::getPDFPageCount($path.'/'.$pdfname.".pdf");
                          Utility::convertPdfPageToPng($path.'/'.$pdfname.".pdf[0]",$path."/thumbnail/".$pdfname.".png");
                          $model->thumbnail=$pdfname.".png";
                          list($width,$height)=getimagesize($path."/thumbnail/".$model->thumbnail);
                          $model->dimension=$width.",".$height;
                          if($model->save()){
                                $transaction->commit();
                                $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/$pdfname.png' class='file-preview-other'>";
                          }
                          else{
                              try{
                                $li=Utility::iterateErrors($model->activeAttributes(),$model->getErrors());
                                unlink($path.'/'.$filename);
                                return $li;
                              }catch(ErrorException $e){
                                //do nothing
                              }

                            }
                        }
                        else{
                          return json_encode(["error"=>"Could not convert $pdfname into pdf"]);
                        }

                      }
                      catch(ErrorException $e){
                        // do nothing
                      }
                    }else{
                      return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                    }

                  }
                  else{
                    $model->no_of_pages=Utility::getPDFPageCount($file->tempName);
                    Utility::convertPdfPageToPng($file->tempName."[0]",$path."/thumbnail/".$pdfname.".png");
                    //return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                    list($width,$height)=getimagesize($path."/thumbnail/".$pdfname.".png");

                    $model->dimension=$width.",".$height;
                    $model->thumbnail=$pdfname.".png";
                    if($model->save()){
                        if($file->saveAs($file_to_save)){
                          $transaction->commit();

                        //  return json_encode(["error"=>$path."/thumbnail/".$file->baseName.".png"]);
                          $p1 = "<img style='max-width:209px' src='/upload/$year/$month/thumbnail/$pdfname.png' class='file-preview-other'>";

                        }
                    }
                    else{
                        $li=Utility::iterateErrors($model->activeAttributes(),$model->getErrors());
                        return $li;
                    }

                  }


              }
              else if($model->type=="image"){
                  list($width,$height)=getimagesize($file->tempName);
                  $model->dimension=$width.",".$height;
                  if($model->save()){
                    if($file->saveAs($file_to_save)){
                      $transaction->commit();
                      $p1 = "<img style='max-width:209px' src='/upload/$year/$month/$filename' class='file-preview-other'>";
                    }

                    else{
                        return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                      }
                  }
                  else{
                    $li=Utility::iterateErrors($model->activeAttributes(),$model->getErrors());
                    return $li;
                  }
              }
              else{
                if($model->save()){
                  if($file->saveAs($file_to_save)){
                    $transaction->commit();
                    $p1 = "<img style='max-width:150px' src='/images/unknown-file.png' class='file-preview-other'>";
                  }

                  else{
                      return json_encode(["error"=>"An error occurred while uploading ".$file->baseName.". Please try again."]);
                    }
                }
                else{
                  $li=Utility::iterateErrors($model->activeAttributes(),$model->getErrors());
                  return $li;
                }
              }
                    //$p2 = ['caption' => "Animal-.jpg", 'width' => '120px', 'url' => $url, 'key' => $key];
                    //}
                return json_encode([
                        'initialPreview' => [$p1],
                        //'initialPreviewConfig' => $p2,
                        'append' => true
                ]);
            }
            //return true;
        } else {
          $li=Utility::iterateErrors($this->activeAttributes(),$this->getErrors());
          return $li;
        }
    }
    /**
     * @inheritdoc
     */

    public function getUserContent($userid){
      $content = TcUploadedContent::find()->where(["userid"=>$userid])->orderBy("uploaded_date desc")->all();
      return $content;
    }
    public function getModifiedContent($content){
      $content_array=array();
      $a=array();

      foreach($content as $c):

          $a['filename']=$c->filename;
          $a['id']=$c->id;
          $a['new_filename']=$c->new_filename;
          if(!empty($c->dimension)){
            $d=explode(",",$c->dimension);
            $a['dimension']=$d[0]." x ".$d[1];//explode(",",$c->dimension)[0]." x ".explode(",",$c->dimension)[0];
          }
          $a['length']=$c->length;
          $a['new_filename']=$c->new_filename;
          $a['no_of_pages']=$c->no_of_pages;
          $a['size']=LessonContent::formatBytes($c->size);
          $a['thumbnail']=TcUploadedContent::getUploaedContentThumbnail($c->uploaded_date,$c->filename,$c->thumbnail,$c->type);
          $a['type']=$c->type;
          $a['uploaded_date']=date("l",strtotime($c->uploaded_date))." ".date("d",strtotime($c->uploaded_date)).", ".date("Y",strtotime($c->uploaded_date));
          $a['userid']=$c->userid;
          $content_array[]=$a;
      endforeach;
      return $content_array;
    }
    public function getUploaedContentThumbnail($date,$name,$thumbnail,$type){
        if($type=="video" || $type=="doc"){
          return "/upload/".date("Y",strtotime($date))."/".date("m",strtotime($date))."/thumbnail/".$thumbnail;
        }
        else if($type=="image"){
          return "/upload/".date("Y",strtotime($date))."/".date("m",strtotime($date))."/".$name;
        }
        else{
          return "/images/unknown-file.png";
        }
    }
    public function getContentUrl($date,$name){
        $year = date("Y",strtotime($date));
        $month=date("m",strtotime($date));
        return "/upload/$year/$month/$name";
    }
    public function getUploadedContent(){

    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'new_filename' => 'New Filename',
            'type' => 'Type',
            'size' => 'Size',
            'dimension' => 'Dimension',
            'length' => 'Length',
            'uploaded_file'=>'Uploaded File',
            'uploaded_date' => 'Uploaded Date',
            'userid' => 'Userid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
