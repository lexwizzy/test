<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\TcCourseGroup;

/**
 * TcGroupSearch represents the model behind the search form about `frontend\models\TcCourseGroup`.
 */
class TcGroupSearch extends TcCourseGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'createdby'], 'integer'],
            [['name', 'createdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      // select tc_course_group.id,name,concat_ws(" ",firstname,lastname) As "createdby",
      //  count(groupid) as "noofusers", createdate from tc_course_group left join
      //   tc_group_user on tc_course_group.id =
      // groupid join user on user.id= tc_course_group.createdby   group by tc_course_group.id
      // $query = new Query();
      //   $query->select('tc_course_group.id,name,concat_ws(" ",`firstname`,`lastname`) As "createdby"
      //      ,count(groupid) as "noofusers", createdate ')
      //      ->from("tc_course_group")
      //     ->leftJoin('tc_Group_User','tc_course_group.id = groupid')
      //     ->leftJoin('user','user.id= tc_course_group.createdby')
      //     ->groupBy("tc_course_group.id");
        $query = TcCourseGroup::find()->where(["createdby"=>Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createdate' => $this->createdate,
            'createdby' => $this->createdby,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
    // $totalCount = Yii::$app->db->createCommand('select count(*) from tc_course_group ')
    //   ->queryScalar();;
    //
    //   $dataProvider = new SqlDataProvider([
    //     'sql' => 'select name,createdate,createdby from tc_course_group',
    //   //  'sql' => 'select tc_course_group.id,name,concat_ws(" ",firstname,lastname) As "createdby", count(groupid) as "noofusers", createdate from tc_course_group left join tc_group_user on tc_course_group.id =groupid join user on user.id= tc_course_group.createdby   group by tc_course_group.id ',
    //     // 'params' => [':publish' => 1],
    //     'totalCount' => $totalCount,
    //     //'sort' =>false, to remove the table header sorting
    //     'sort' => [
    //         'attributes' => [
    //             'name' => [
    //                 'asc' => ['name' => SORT_ASC],
    //                 'desc' => ['name' => SORT_DESC],
    //                 'default' => SORT_DESC,
    //                 'label' => 'Group name',
    //             ],
    //             'createdby' => [
    //                 'asc' => ['createdby' => SORT_ASC],
    //                 'desc' => ['createdby' => SORT_DESC],
    //                 'default' => SORT_DESC,
    //                 'label' => 'Created by',
    //             ],
    //             // 'noofusers'=>[
    //             //   'asc'=>['noofusers'=>SORT_ASC],
    //             //   'asc'=>['noofusers'=>SORT_DESC],
    //             //   'default'=>SORT_DESC,
    //             //   'label'=>'No of users'
    //             // ],
    //             'createdate'=>[
    //               'asc'=>['createdate'=>SORT_ASC],
    //               'asc'=>['createdate'=>SORT_ASC],
    //               "default"=>SORT_DESC,
    //               'label'=>"Created date"
    //             ]
    //         ],
    //     ],
    //     'pagination' => [
    //         'pageSize' => 10,
    //     ],
    //   ]);
    //
    //     return $dataProvider;
}
