<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "discussion_replies".
 *
 * @property string $id
 * @property string $discussion
 * @property string $userid
 * @property string $courseid
 * @property string $reply
 * @property string $date
 */
class DiscussionReplies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discussion_replies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discussion', 'userid', 'courseid'], 'required'],
            [['discussion', 'userid', 'courseid'], 'integer'],
            [['date'], 'safe'],
            [['reply'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discussion' => 'Discussion',
            'userid' => 'Userid',
            'courseid' => 'Courseid',
            'reply' => 'Reply',
            'date' => 'Date',
        ];
    }
    
    public function countReply($discussionid){
        return DiscussionReplies::find()->where(['discussion'=>$discussionid])->count();
    }
}
