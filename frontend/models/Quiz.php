<?php

namespace frontend\models;

use Yii;
use frontend\models\Question;

/**
 * This is the model class for table "quiz".
 *
 * @property string $q_id
 * @property string $q_name
 * @property string $q_description
 * @property string $partid
 * @property string $islimitedbytime
 * @property string $timelimit
 * @property string $counter
 */
class Quiz extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['q_name', 'partid'], 'required'],
            [['partid', 'timelimit', 'counter','courseid','outlineno'], 'integer'],
            [['q_name', 'q_description'], 'string', 'max' => 1000],
            [['islimitedbytime'], 'string', 'max' => 7]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'q_id' => 'Q ID',
            'q_name' => 'Quiz Name',
            'q_description' => 'Quiz Description',
            'partid' => 'Partid',
            'islimitedbytime' => 'Islimitedbytime',
            'timelimit' => 'Timelimit',
            'counter' => 'Counter',
        ];
    }
    public function setSavedAnswers($get_data,$userid){
      /*$ans["quizid"]=$quizid;*/
      $quizid= $get_data->get("quizid");
      $question=$get_data->get("questionid");
      $flag=false;
    //  $ans["type"]=$type;

      if(!QuizselectedAns::find()->where(["userid"=>$userid,"quizid"=>$quizid])->exists()){
          echo "yesy";

        $type=$get_data->get("type");
        if($type==1){
          foreach($get_data->get("checkbox") as $s=>$value):
              $selected[]=$value;
          endforeach;
        }
        else {
          $selected[]=$get_data->get("radio-ans");
        }
        $ans["quiz"]=$quizid;
        $q["question"]=$question;
        $q['type']=$type;
        $q["answer"]=$selected;
        $ans["answers"][]=$q;

        $model = new QuizselectedAns;
        $model->userid=$userid;
        $model->quizid=$quizid;
        $model->question=$question;
        $model->answers=json_encode($ans);

        if($model->save())
            echo "save";
          else
              print_r($model->getErrors());
      }else{
        $model = QuizselectedAns::find()->where(["userid"=>$userid,"quizid"=>$quizid])->one();
        $ans=json_decode($model->answers,true);
        $type=$get_data->get("type");
        if($type==1){
          foreach($get_data->get("checkbox") as $s=>$value):
              $selected[]=$value;
          endforeach;
        }
        else {
          $selected[]=$get_data->get("radio-ans");
        }

        $q["question"]=$question;
        $q["answer"]=$selected;
        $q['type']=$type;
       // print_r($test);
        foreach($ans["answers"] as $p=>$qi):
         if($qi["question"] == $question){
           //$q["answer"]=$selected;
           unset($ans["answers"][$p]);
         }
        endforeach;
        $ans["answers"][]=$q;
        $model->answers=json_encode($ans);
        $model->save();

      }

    }
    public function getSavedAnswers($userid,$quizid){
       $model = QuizselectedAns::find()->where(["userid"=>$userid,"quizid"=>$quizid])->one();
        if($model!=null)
            return $model->answers;
    }

    public function  getQuiz($model,$counter='',$outlineno=""){
       $id=mt_rand(100, 10000);
       $quiz["name"]=$model->q_name;
       $quiz["id"]=$model->q_id;
       $quiz["part_id"]=$model->partid;
       $quiz["outlineno"]=$outlineno;
       $quiz["counter"]=$counter;
       $quiz["limittime"]=$model->islimitedbytime;
       $quiz["idforlimittime"]=$id;
       $quiz['timelimit']=$model->timelimit;
       $quiz["description"]=$model->q_description;
       $quiz["questions"]=Question::getQuestions($model->q_id);

       return $quiz;

//         return '<li class="section-items quiz-li" data-section-no="'.$model->partid.'" data-type="quiz" data-outlineno="'.$outlineno.'">
//     <div class="dc-quizz-info dc-course-item" data-outline-no="'.$model->outlineno.'">
//         <div class="dc-content-title">
//             <h5 class="xsm black ">Quiz <span class="quiz_counter">'.$counter.'</span> : <span contenteditable=true class="quiz-title">'.$model->q_name.'</span></h5>
//             <div class="course-region-tool"> <a style="cursor:pointer" class="quiz-edit" title="edit"><i class="icon md-pencil"></i></a>  <a href="#" class="section-item-drag" title="drag"><i class="fa fa-arrows"></i></a>  <a style="cursor:pointer" data-quizno="'.$model->q_id.'" data-partid="'.$model->partid.'"
//                 class="delete-quiz" title="delete"><i class="icon md-recycle"></i></a>  <a style="cursor:pointer" class="save-quiz" data-quizno="'.$model->q_id.'" title="save"><i class="fa fa-save"></i></a>
//             </div>
//         </div>
//         <div class="quizz-body dc-item-body">
//             <div class="error"></div>
//             <div class="btn-add">
//                 <a href="#addquestion" data-quizno="'.$model->q_id.'" class="popup-with-zoom-anim"> <i class="icon md-plus"></i> Add Question</a>
//             </div>
//             <div class="form-item form-checkbox checkbox-style">
//                 <input type="checkbox" '.$model->islimitedbytime.' class="limittime" id="'.$id.'">
//                 <label for="'.$id.'"><i class="icon-checkbox icon md-check-1"></i>Limit time</label>
//             </div>
//             <div class="time">
//                 <div class="form-item">
//                     <input type="text" class="minutes" value="'.$model->timelimit.'">
//                     <label for="">mins</label>
//                 </div>
//             </div>
//             <div class="form-item">
//                 <input type="text" class="quiz-desc" placeholder="Short introduction ( optional)" style="width:100%" class="fullwidth" value="'.$model->q_description.'">
//             </div>
//             <div class="form-item-wrap hold-quiz">'.Question::getQuestions($model->q_id).'</div>
//         </div>
//     </div>
// </li>
// ';
    }

    public function getQuizCount($courseid){
        return Yii::$app->db->createCommand("SELECT COUNT(q_id)FROM quiz JOIN course_parts ON partid=part_id WHERE course_id=$courseid")->queryScalar();
    }

    public function getAllQuiz($section){
        $quiz= Quiz::find()->where(['partid'=>$section])->orderBy("counter asc")->all();
        return $quiz;
    }
    public function getNumberOfQuestion($quiz){
        $question = Question::find()->where(['quiz_id'=>$quiz])->count();
        return $question;
    }

}
