<?php

namespace frontend\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "question_type".
 *
 * @property string $id
 * @property string $type
 */
class QuestionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
        ];
    }
    
    public function getQuestionType(){
        $type =QuestionType::find()->all();
        $listdata = ArrayHelper::map($type,"id","type");
        return $listdata;
    }
}
