<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tc_course_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $createdate
 * @property integer $createdby
 *
 * @property User $createdby0
 * @property TcGroupUser[] $tcGroupUsers
 */
class TcCourseGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_course_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'createdby'], 'required'],
            [['createdate'], 'safe'],
            [['createdby'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'createdate' => 'Createdate',
            'createdby' => 'Createdby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(User::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTcGroupUsers()
    {
        return $this->hasMany(TcGroupUser::className(), ['groupid' => 'id']);
    }
}
