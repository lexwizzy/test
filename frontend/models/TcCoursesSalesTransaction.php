<?php

namespace frontend\models;

use Yii;
use yii\data\SqlDataProvider;
use frontend\models\CourseCoupons;

/**
 * This is the model class for table "tc_courses_sales_transaction".
 *
 * @property integer $id
 * @property integer $courseid
 * @property string $date
 * @property string $status
 * @property integer $userid
 * @property string $pp_payerid
 * @property string $pp_paymentid
 * @property string $payment_source
 *
 * @property Courses $course
 * @property User $user
 */
class TcCoursesSalesTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_courses_sales_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'status', 'userid', 'payment_source'], 'required'],
            [['courseid', 'userid'], 'integer'],
            [['date'], 'safe'],
            [['status'], 'string', 'max' => 45],
            [['coupon_code'],'string','max'=>20],
            [['pp_payerid', 'pp_paymentid', 'payment_source'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'date' => 'Date',
            'status' => 'Status',
            'userid' => 'Userid',
            'pp_payerid' => 'Pp Payerid',
            'pp_paymentid' => 'Pp Paymentid',
            'payment_source' => 'Source',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }
    public function getTransaction($userid){
      $connection = \Yii::$app->db;
      $count = $connection->createCommand("SELECT count(*) FROM `tc_courses_sales_transaction`
          join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and owner=:owner order by date asc",[':owner' => $userid])->queryScalar();

      $dataProvider =new SqlDataProvider([
                'sql' => "SELECT owner, coupon_code, tc_courses_sales_transaction.id as 'transactionid',date(date) as 'date',amount FROM `tc_courses_sales_transaction`
                    join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and  owner=:owner order by date asc",
                'params' => [':owner' => $userid],
                'totalCount' => $count,
                'pagination' => [
                    'pageSize' => 5,
                ],
                // 'sort' => [
                //     'attributes' => [
                //         'title',
                //         'view_count',
                //         'created_at',
                //     ],
                // ],
            ]);
      return $dataProvider;
    }
    public function getUserGross($userid){
      $connection = \Yii::$app->db;
      $amount=$connection->createCommand("SELECT sum(amount) FROM `tc_courses_sales_transaction`
         join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and  owner=:owner order by date asc",[':owner' => $userid])->queryScalar();
         return $amount;
    }
    public function getUserCurrentMonthNet($userid,$month){
        $connection = \Yii::$app->db;
        $amount=0;
        $model=$connection->createCommand("SELECT owner, amount, coupon_code FROM `tc_courses_sales_transaction`
           join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and  owner=:owner and month(date)=:month order by date asc",[':owner' => $userid,":month"=>$month])->queryAll();
               foreach($model as $m):
                 $amount+=TcCoursesSalesTransaction::doCommission($m["owner"],$m["coupon_code"],$m["amount"]);
               endforeach;
           return $amount;
    }
    public function getUserCurrentMonthGross($userid,$month){
      $connection = \Yii::$app->db;
      $amount=$connection->createCommand("SELECT sum(amount) FROM `tc_courses_sales_transaction`
         join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and  owner=:owner and month(date)=:month order by date asc",[':owner' => $userid,":month"=>$month])->queryScalar();
         return $amount;
    }
    public function getUserNet($userid){
        $connection = \Yii::$app->db;
        $amount=0;
        $model=$connection->createCommand("SELECT owner, amount, coupon_code FROM `tc_courses_sales_transaction`
           join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and owner=:owner order by date asc",[':owner' => $userid])->queryAll();
               foreach($model as $m):
                 $amount+=TcCoursesSalesTransaction::doCommission($m["owner"],$m["coupon_code"],$m["amount"]);
               endforeach;
           return $amount;
    }
    public function getUserCurrentMonthData($userid,$month){
      //this is used by monthly graph
      $connection = \Yii::$app->db;
       $data=array();
       $day_array=array();
       $t=array();
       $data[]=array('Month', 'Gross',"Net");
      //  $data[]=array('Jan', 7,3);
      //   $data[]=array('Feb', 8,5);
      //   $data[]=array('Mar', 12,7);
       $model=$connection->createCommand("SELECT day(date) as 'day' ,owner,  amount, coupon_code FROM `tc_courses_sales_transaction`
          join courses on courseid=course_id where tc_courses_sales_transaction.status ='completed' and  owner=:owner and month(date)=:month ",[':owner' => $userid,":month"=>$month])->queryAll();
          if($model!=null){
            foreach($model as $m):

              $net=TcCoursesSalesTransaction::doCommission($m["owner"],$m["coupon_code"],$m["amount"]);
              $gross= $m["amount"];
              $day=$m["day"];
              if(array_key_exists($day,$t)==0){
                $day_array[]=$day;
                $t[$day]=array($day,$gross,(int)$net);
              }

              else{

                $e = $t[$day];
                $e[1]=$e[1]+$gross;
                $e[2]=$e[2]+$net;
                $t[$day]=$e;
              }
                  // $data[]=array($m["day"],$m["amount"],round($net));
              //$data[]=array($m["day"],(int)$m["amount"],round($net));
            //$data[]=array(1,12,3);
            endforeach;
            foreach($day_array as $d):
              $data[]=$t[$d];
            endforeach;
          }
          else{
            $data[]=array(date("M",time()), 0,0);
          }

       return $data;
    }
    ///this function calculate the commission
    public function doCommission($userid,$coupon,$pricesold){
          $amount=0;
        //
        if(empty($coupon)){
          //this is an organic course sale

          $amount=(80*$pricesold)/100;
        }
        else{

          $rate=CourseCoupons::getRate($coupon);
          $salesprice=CourseCoupons::getSalesPrice($pricesold,$rate);
          $amount=(96*$salesprice)/100;//excluding 3% for
          //$amount=$pricesold;
        }
        return $amount;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
