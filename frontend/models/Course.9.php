<?php

namespace frontend\models;

use Yii;
use frontend\models\CourseReviews;
use frontend\models\Enrollment;
use yii\helpers\Url;
use yii\behaviors\SluggableBehavior;
use frontend\models\CourseViews;
/**
 * This is the model class for table "courses".
 *
 * @property string $course_id
 * @property string $course_title
 * @property string $course_category
 * @property string $course_desc
 * @property string $course_goal
 * @property string $skill_level
 * @property string $course_price
 * @property string $amount
 * @property string $language
 * @property string $course_privacy
 * @property string $course_banner
 * @property string $promo_video
 * @property string $course_tools
 * @property string $owner
 * @property string $createdate
 * @property string $status
 * @property string $date_published
 * @property CourseInstructors[] $courseInstructors
 * @property CourseParts[] $courseParts
 * @property CoursePrivacy $coursePrivacy
 * @property SkillLevel $skillLevel
 * @property User $owner0
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'course_title',
                'ensureUnique'=>true,
            ],
        ];
    }
    public function rules()
    {
        return [
            [['course_title','course_category'],'required','on'=>'isguest'],
            [['course_title'],'required','on'=>'edit_title'],
            [['course_goal','course_category','course_desc','course_subcategory','target_audience','language'],'required','on'=>'basicinfo'],
            [['course_privacy', 'owner'], 'integer'],
            [['createdate'], 'safe'],
            [['promo_video_mime'],'string'],
            [['course_title'], 'string', 'max' => 70],
            [['target_audience'],'string'],
            [['status'], 'integer'],
            [['date_published','promo_video_thumbnail'], 'string', ],
            [[ 'language', 'course_tools'], 'string', 'max' => 100],
            [['course_category','course_subcategory'],'integer'],
            [['amount','promo_video_length'], 'integer'],
            ['amount','default','value'=>""],
            ['amount', 'required', 'when' => function($model) {
                return $model->course_price == 'Paid';
            },'on'=>'basicinfo'],
            [['course_desc'], 'string'],
            [['course_goal'], 'string'],
            [['course_price'], 'string', 'max' => 20],
            [['course_banner', 'promo_video'], 'string', 'max' => 400],
            ['course_banner','file','extensions'=>'jpg,png','maxSize'=>1000000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate(){
        if($this->language =="" && $this->scenario=="basicinfo")
            $this->addError('language', 'Select your teaching language');

        return parent::beforeValidate();
    }
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course ID',
            'course_title' => 'Course Title',
            'course_category' => 'Course Category',
            'course_desc' => 'Course Desc',
            'course_goal' => 'Course Goal',
            'skill_level' => 'Skill Level',
            'course_price' => 'Course Price',
            'amount' => 'Amount',
            'language' => 'Language',
            'course_privacy' => 'Course Privacy',
            'course_banner' => 'Course Banner',
            'promo_video' => 'Promo Video',
            'course_tools' => 'Course Tools',
            'owner' => 'Owner',
            'createdate' => 'Createdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseInstructors()
    {
        return $this->hasMany(CourseInstructors::className(), ['course_id' => 'course_id']);
    }


    public function getCourseParts()
    {
        return $this->hasMany(CourseParts::className(), ['course_id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursePrivacy()
    {
        return $this->hasOne(CoursePrivacy::className(), ['id' => 'course_privacy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkillLevel()
    {
        return $this->hasOne(SkillLevel::className(), ['skill_level' => 'skill_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    public function getCourseBanner($courseid){
        $course=Course::find()->where(['course_id'=>$courseid])->one();
        if($course!==null){
            if($course->course_banner!="")
                return Yii::getAlias("@web")."/upload/coursebanners/".$course->course_banner;
            else
                return Url::base()."/images/feature/img-1.jpg";
        }
        else{
            return Url::base()."/images/feature/img-1.jpg";
        }


    }
    public function getCourseSameCategory($category,$courseid,$limit){
        $course= Course::find()->where(['and',["!=","course_id",$courseid],['course_category'=>$category]])->limit($limit)->all();

        if($course==null)
            $course= Course::getOtherCourseViewedByOthers($courseid);

        return $course;
    }
    public function getStatusString($id){
        return Yii::$app->db->createCommand("select status from course_status where id =$id")->queryScalar();
    }
    public function getTutorsCourse($id){
        $course_array=array();
        $counter=0;
        $course = Course::find()->where(['owner'=>$id])->all();
        foreach($course as $row):
                $name = User::getName($row->owner);
                $course_array[$counter]['courseid']=$row->course_id;
                 $course_array[$counter]['slug']=$row->slug;
                $course_array[$counter]['coursetitle']= $row->course_title;
                $course_array[$counter]['category']= CourseCategory::findOne([$row->course_category])->category;
                $course_array[$counter]['price']= $row->amount !=""? "$".$row->amount: "Free";
                $course_array[$counter]['coursebanner']=Course::getCourseBanner($row->course_id);
                $course_array[$counter]['tutorimage']=User::getPhoto($row->owner);
                $course_array[$counter]['name']=$name['firstname']." ".$name['lastname'];
                $course_array[$counter]['rating']=CourseReviews::getAverageRating($row->course_id);
                $course_array[$counter]['comment']=CourseReviews::getCommentCount($row->course_id);
                $course_array[$counter]['enrollment']=Enrollment::getEnrollmentCount($row->course_id);
                $course_array[$counter]['status']=Course::getStatusString($row->status);
                $counter++;
        endforeach;

        return $course_array;
    }
    public function getCourseAd($courseid){
        $course_array=array();
        $counter=0;
        $course = Course::find()->where(['course_id'=>$courseid])->one();
        $name = User::getName($course->owner);
        $course_array['courseid']=$course->course_id;
        $course_array['slug']=$course->slug;
        $course_array['coursetitle']= $course->course_title;
        $course_array['category']= CourseCategory::findOne([$course->course_category])->category;
        $course_array['price']= $course->amount !=""? "$".$course->amount: "Free";
        $course_array['coursebanner']=Course::getCourseBanner($course->course_id);
        $course_array['tutorimage']=User::getPhoto($course->owner);
        $course_array['name']=$name['firstname']." ".$name['lastname'];
        $course_array['rating']=CourseReviews::getAverageRating($course->course_id);
        $course_array['comment']=CourseReviews::getCommentCount($course->course_id);
        $course_array['enrollment']=Enrollment::getEnrollmentCount($course->course_id);



        return $course_array;
    }


    public function getTutorsOtherCourses($tutorid,$viewcourseid){
        $course = Course::find()->where("owner=$tutorid and course_id!=$viewcourseid limit 0,4")->all();
        return $course;
    }
    public function getOtherCourseViewedByOthers($courseid){

        //Get other users who has viewed this same course ($courseid)
        $cookie= Yii::$app->request->cookies->getValue("tsuser");
        $subquery = \frontend\models\CourseViews::find()->select('cookie')->where(['and',['courseid'=>$courseid],['!=',"cookie",$cookie]]);
        $mainquery= \frontend\models\CourseViews::find()->select("courseid")->where(['and',['!=','courseid',$courseid],["in",'cookie',$subquery]])->orderBy("date desc");
        return Course::find()->where(["in","course_id",$mainquery])->limit(4)->all();
    }
    //check if a user has viewed a course before
    public function isViewed($courseid,$cookie){
        if(CourseViews::find()->where(["cookie"=>$cookie,"courseid"=>$courseid])->exists())
            return false;
        else
            return true;
    }
    public function getPrice($courseid){
        $course= Course::find()->where(['course_id'=>$courseid])->one();
        $course->amount !=""?$amount =  "$".$course->amount: $amount= "Free";
        return $amount;
    }
    public function getRecommendedCourses(){
        $course = Yii::$app->db->createCommand("(SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN (SELECT (RAND() * (SELECT MAX(course_id) FROM courses)) AS id) AS r2 WHERE r1.course_id >= r2.id ORDER BY r1.course_id ASC LIMIT 1) ")->queryAll();
        return $course;
    }

    public function getNewlyPosted(){

    }
    public function getSections($courseid){
        $sections = CourseParts::find()->where(['course_id'=>$courseid])->orderBy("counter asc")->all();
        return $sections;
    }
    public function getUnits($sectionid){
        $units = CourseLessons::find()->where(['part_id'=>$sectionid])->orderBy("counter asc")->all();
        return $units;
    }

    public function createStudentCourseAd($coursead){
        $url =\yii\helpers\Url::toRoute(['/course/learn','slug'=>$coursead['slug']]);
        return ' <div class="mc-learning-item mc-item mc-item-2" data-link="'.$url.'"> <div class="image-heading"><a href="'.$url.'"> <img src="'.$coursead['coursebanner'].'" alt="'. $coursead['name'].'"></a> </div><div class="meta-categories"><a href="'.$url.'">'.$coursead['category'].'</a></div><div class="content-item"> <div class="image-author"> <img src="'.$coursead['tutorimage'].'" alt="'. $coursead['name'].'"> </div><h4><a href="'.$url.'" class="ellipsis-2line">'. $coursead['coursetitle'].'</a></h4> <div class="name-author"> By <a href="'.$url.'">'. $coursead['name'].'</a> </div></div><div class="ft-item"> <div class="percent-learn-bar"> <div class="percent-learn-run"></div></div><div class="percent-learn">100%<i class="fa fa-trophy"></i></div><a href="'.$url.'" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a></div></div>
                ';
    }
    public function createTutorsCourseAd($details){
         $url= \yii\helpers\Url::toRoute(['/course/studentpreview', 'slug'=>$details['slug']]);
         return '<div class="col-xs-6 col-sm-4 col-md-3"> <div class="mc-teaching-item mc-item mc-item-2 coursebox"> <div class="image-heading"><a href=""><img src="'.$details['coursebanner'].'" alt=""></a> </div><div class="meta-categories">'.$details['category'].'</div><div class="content-item"> <div class="image-author"> <img src="'.$details['tutorimage'].'" alt=""> </div><h4><a href="'.Url::base().'/course/basicinfo?course_id='.$details['courseid'] .'" class="ellipsis-2line title">'.$details['coursetitle'].'</a></h4> </div><div class="ft-item"> <div class="rating"> '.$details['rating'].'</div><div class="view-info"> <i class="icon md-users"></i>'.$details['enrollment'].' </div><div class="comment-info"> <i class="icon md-comment"></i> '.$details['comment'].'</div><div class="price">'.$details['price'].' </div></div><div class="edit-view"> <a href="'.Url::base().'/course/basicinfo?course_id='.$details['courseid'].' " class="edit">Edit</a> <a href="'.$url.'" class="view">View</a> <a class="view push-left">'.$details['status'].' </a> | <i data-toggle="modal" data-target="#announcementModal" data-courseid="'.$details['courseid'].'" title="Make  announcement to students enrolled in this course" class="fa fa-bullhorn announcement"></i></div></div></div>';

    }
    public function createGuestCourseAd($coursead,$class){

         $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
        return ' <a href="'.$url.'" > <div class="mc-item coursead '.$class.'" data-link="'.$url.'" style="max-width:270px"> <div class="image-heading"> <img src="'.$coursead['coursebanner'].'" alt=""> </div><div class="meta-categories"><a >'.$coursead['category'].'</a></div><div class="content-item"> <div class="image-author"> <img src="'.$coursead['tutorimage'].'" alt=""> </div><h4><a href="'.$url.'" class="ellipsis-2line">'. $coursead['coursetitle'].'</a></h4> <div class="name-author"> <a>By &nbsp;'. $coursead['name'].'</a> </div></div><div class="ft-item"> <div class="rating"> '. $coursead['rating'].' </div><div class="view-info"> <i class="icon md-users"></i> '.$coursead['enrollment'].' </div><div class="comment-info"> <i class="icon md-comment"></i> '. $coursead['comment'].' </div><div class="price"> '.$coursead['price'].' </div></div></div></a>';
    }

    public function createGuestCourseListAd($coursead){
         $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
        return '<a href="'.$url.'" ><div class="mc-item coursead  mc-item-2" data-link="'.$url.'"> <div class="image-heading"> <img src="'.$coursead['coursebanner'].'" alt=""> </div><div class="meta-categories"><a>'.$coursead['category'].'</a></div><div class="content-item"> <h4><a class="ellipsis-2line" href="'.$url.'">'.$coursead['coursetitle'].'</a></h4> </div><div class="ft-item"> <div class="rating"> '. $coursead['rating'].' </div><div class="price"> '.$coursead['price'].' </div></div></div></a>';
    }
    public function getCurrentProgress($courseid,$userid){
        $quizcount= Quiz::find()->where(["courseid"=>$courseid])->count();
        $lessoncount =CourseLessons::find()->where(["courseid"=>$courseid])->count();
        $completed = CourseLearnTracking::find()->where(["courseid"=>$courseid,"userid"=>$userid])->count();
        $total=$quizcount+$lessoncount;//Get the number of course contents
        $progress = (100/$total) * $completed;
        return $progress;
    }


}
