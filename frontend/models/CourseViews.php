<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_views".
 *
 * @property string $id
 * @property string $courseid
 * @property string $cookie
 * @property string $date
 */
class CourseViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_views';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'cookie'], 'required'],
            [['courseid'], 'integer'],
            [['date'], 'safe'],
            [['cookie'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'cookie' => 'Cookie',
            'date' => 'Date',
        ];
    }
}
