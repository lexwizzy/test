<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_learn_tracking".
 *
 * @property string $id
 * @property string $userid
 * @property string $courseid
 * @property string $section
 * @property string $lesson
 * @property string $lesson_type
 * @property string $date
 * @property string $status
 *
 * @property User $user
 * @property Courses $course
 * @property CourseParts $section0
 */
class CourseLearnTracking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_learn_tracking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'courseid', 'section', 'lesson', 'lesson_type'], 'required'],
            [['userid', 'courseid', 'section', 'lesson'], 'integer'],
            [['date'], 'safe'],
            [['lesson_type'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'courseid' => 'Courseid',
            'section' => 'Section',
            'lesson' => 'Lesson',
            'lesson_type' => 'Lesson Type',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection0()
    {
        return $this->hasOne(CourseParts::className(), ['part_id' => 'section']);
    }
}
