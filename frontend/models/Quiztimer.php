<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "quiztimer".
 *
 * @property string $id
 * @property string $userid
 * @property string $quizid
 * @property string $time
 * @property string $date
 */
class Quiztimer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiztimer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'quizid', 'time'], 'required'],
            [['userid', 'quizid', 'time'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'quizid' => 'Quizid',
            'time' => 'Time',
            'date' => 'Date',
        ];
    }
}
