<?php

namespace frontend\models;

use Yii;
use frontend\models\User;
/**
 * This is the model class for table "tc_lesson_content".
 *
 * @property integer $id
 * @property integer $contentid
 * @property integer $courseid
 * @property integer $partid
 * @property integer $lessonid
 * @property string $date
 *
 * @property TcUploadedContent $content
 * @property Courses $course
 * @property CourseLessons $lesson
 * @property CourseParts $part
 */
class TcLessonContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_lesson_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contentid', 'courseid', 'partid', 'lessonid','main'], 'required'],
            [['contentid', 'courseid', 'partid', 'lessonid'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contentid' => 'Contentid',
            'courseid' => 'Courseid',
            'partid' => 'Partid',
            'lessonid' => 'Lessonid',
            'date' => 'Date',
            'main'=>"Main Content"
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(TcUploadedContent::className(), ['id' => 'contentid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(CourseLessons::className(), ['lesson_id' => 'lessonid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPart()
    {
        return $this->hasOne(CourseParts::className(), ['part_id' => 'partid']);
    }
    public function getMainContent($lesson_no){
        $content=array();
        $lesson = TcLessonContent::find()->where(['and',['=','lessonid',$lesson_no],['=','main','Yes']])->one();
        $main_content = array();

        if($lesson !=null){
              $content_model=TcUploadedContent::findOne($lesson->contentid);
             // $content= '<!--Begin Contents info div--><div class="content-item-info">
             //                                            <div class="upload-thumb">';
             $main_content["owner"] = User::findByUserId($content_model->userid)->firstname;
             $main_content["content_no"]=$lesson->id;
             $main_content['raw_date']=$content_model->uploaded_date;
             $main_content["uploadtime"] = Yii::$app->formatter->asDatetime($content_model->uploaded_date, "php:d, M,Y");
             $main_content["content_defaultname"]=$content_model->filename;
             $main_content['main']=$lesson->main;
             if($content_model->type =="video"){
                 $main_content["type"]="video";
                 $main_content["video_length"]=gmdate("i:s",$content_model->length);
                 $main_content["content_size"] =  LessonContent::formatBytes($content_model->size);
                 $main_content["thumbnails"]=TcUploadedContent:: getUploaedContentThumbnail($content_model->uploaded_date,$content_model->filename,$content_model->thumbnail,$content_model->type);
             }
             else if($content_model->type=="doc"){

                 $main_content["type"]="pdf";
                 $main_content["page_no"]=$content_model->no_of_pages;
                $main_content["content_size"] =  LessonContent::formatBytes($content_model->size);
                    $main_content["thumbnails"]=TcUploadedContent:: getUploaedContentThumbnail($content_model->uploaded_date,$content_model->filename,$content_model->thumbnail,$content_model->type);
             }
             else{
              // $main_content["content_defaultname"]=$lesson->content_defaultname;
               $main_content["type"]="text";

             }

             ;
            return $main_content;
        }

    }
    public function getOtherContent($lesson_no){


        $others =array();
        $others_array = array();
       $other_content = TcLessonContent::find()->where(['and',['=','lessonid',$lesson_no],['!=','main','Yes']])->all();
       if($other_content !=null){

           foreach($other_content as $lesson):
               $content_model=TcUploadedContent::findOne($lesson->contentid);
               $others["owner"] = User::findByUserId($content_model->userid)->firstname;
               $others['main']=$lesson->main;
               $others["content_no"]=$lesson->id;
               $others["uploadtime"] = Yii::$app->formatter->asDatetime($content_model->uploaded_date, "php:d, M,Y");
               $others["content_defaultname"]=$content_model->filename;
               $others["content_size"] =  LessonContent::formatBytes($content_model->size);
               $others_array[]=$others;
           endforeach;

            return $others_array;

       }

   }
   //returns JSON
   public function getContents($lessonid){
     $result=array();
     $others_array=TcLessonContent::getOtherContent($lessonid);
     if(count($others_array)!=0){
       if(TcLessonContent::getMainContent($lessonid)!=null)
          array_unshift($others_array,TcLessonContent::getMainContent($lessonid));
     }

    else{
      if(TcLessonContent::getMainContent($lessonid)!=null)
        $others_array[]=TcLessonContent::getMainContent($lessonid);
    }


     //$result["content"]=$others_array;
     //$result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
    //  if($requesttype=="main"){
    //      $result["content"]=TcLessonContent::getMainContent($lessonid);
    //      $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
    //  }
    //  else{
    //    $result["content"]=TcLessonContent::getOtherContent($lessonid);
    //    $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
     //
    //  }
         return $others_array;
   }
}
