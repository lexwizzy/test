<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "iso_languages".
 *
 * @property string $id
 * @property string $language
 */
class IsoLanguages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'iso_languages';
    }
    
    public function getISOLanguage(){
        
        $array =ArrayHelper::map(IsoLanguages::find()->orderBy("language asc")->all(),"language",'language');
        return $array;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'string', 'max' => 3],
            [['language'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
        ];
    }
    
}
