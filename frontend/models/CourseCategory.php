<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_category".
 *
 * @property integer $id
 * @property string $category
 * @property string $icon
 * @property string image
 *
 * @property CourseSubcategory[] $courseSubcategories
 */
class CourseCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category'], 'required'],
            [['category'], 'string', 'max' => 100],
            [['icon','image'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Category',
            'icon' => 'Icon',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseSubcategories()
    {
        return $this->hasMany(CourseSubcategory::className(), ['category' => 'id']);
    }
    public function getCategory(){
        $category = CourseCategory::find()->orderBy("category asc")->all();
        $listData= ArrayHelper::map($category,"id","category");
        return $listData;
    }
    public function getSubCat($categoryid){
        $cat = CourseSubcategory::find()->joinWith("category0")->where(["course_subcategory.category"=>$categoryid])->all();
        return $cat;
    }
}
