<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_outline_order".
 *
 * @property string $id
 * @property string $itemid
 * @property string $type
 * @property string $courseid
 * @property string $date
 * @property string $counter
 */
class CourseOutlineOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_outline_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['itemid', 'courseid', 'counter','section'], 'integer'],
            [['type', 'courseid'], 'required'],
            [['date'], 'safe'],
            [['type'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itemid' => 'Itemid',
            'type' => 'Type',
            'courseid' => 'Courseid',
            'date' => 'Date',
            'counter' => 'Counter',
        ];
    }

    public function getSectionOutline($sectionid,$courseid){
        $mini_container ="";
        $lesson=null;
        $quiz=null;
        $lesson_array=array();
        $content_array=array();
        $quiz_array=array();
        $outline = CourseOutlineOrder::find()->where(['and',["section"=>$sectionid],['courseid'=>$courseid]])->orderBy("counter asc")->all();
                         $lesson_counter=0;
                         $quiz_counter=0;
                         $counter=0;
                         foreach($outline as $o):

                            if($o->type=="lesson"){
                                                  // $lesson = CourseLessons::find()->where(['part_id'=>$row->part_id])->orderBy('counter asc')->all();
                                    $lesson = CourseLessons::findOne([$o->itemid]);
                                          if($lesson!=null){
                                                                // foreach($lesson as $l):
                                                  $lesson_array[]= CourseLessons::getLesson($lesson,++$lesson_counter,$o->id);
                                                              //  endforeach;
                                          }
                                    else{
                                                              //  $mini_container.='';
                                    }
                                    $content_array[$counter]["content"]=$lesson_array;
                                    $content_array[$counter]["type"]="lesson";
                                    $counter++;
                            }
                         else{
                                   // $quiz= Quiz::find()->where(['partid'=>$row->part_id])->orderBy("counter asc")->all();
                                    $quiz= Quiz::findOne([$o->itemid]);
                                    if($quiz!=null){

                                       // foreach($quiz as $quiz_model):
                                            $quiz_array[]=Quiz::getQuiz($quiz,++$quiz_counter,$o->id);
                                       // endforeach;
                                    }else{
                                        // $mini_container.='';
                                    }
                                    $content_array[$counter]["content"]=$quiz_array;
                                    $content_array[$counter]["type"]="quiz";
                                    $counter++;
                            }
                            $quiz_array=array();
                            $lesson_array=array();
                          endforeach;
        //return an empty list
        // if($lesson==null && $quiz ==null){
        //     $mini_container="<li class='not-section-items'>&nbsp;</li>";
        // }
        return ["lessons"=>$lesson_array,"quiz"=>$quiz_array,"contents"=>$content_array];
    }
    public function updateOutline($id){
        $del = CourseOutlineOrder::findOne($id);
       // $lastrow = CourseOutlineOrder::find()->max()->one();
       // $lastid=$del->id;
        $flag=false;
        $section=$del->section;
        $courseid = $del->courseid;
        if($del->delete()){
            $flag = true;
            $d= CourseOutlineOrder::find()->where(['section'=>$section,'courseid'=>$courseid])->orderBy("counter asc")->all();
            $counter=0;
            foreach($d as $r):
                $c=CourseOutlineOrder::findOne([$r->id]);
                $c->counter=++$counter;
                $c->save();
            endforeach;
        }

           // CourseOutlineOrder::updateAllCounters(['counter'=>-1],['>','id',$lastid]);
       // $u= CourseOutlineOrder::find()->where(['>','id',$lastid])->all();

       /* foreach($u as $outline):


            if($outline->type=="section"){
               $s=CourseParts::find()->where(['part_id'=>$outline->itemid])->one();
               $s->outlineno=$outline->counter;
               if($s->save()){
                   $flag=1;
               }

            }
            else if($outline->type=="lesson"){
               $l=CourseLessons::find()->where(['lesson_id'=>$outline->itemid])->one();
               $l->outlineno=$outline->counter;
               if($l->save()){
                   $flag=1;
               }
            }
            else if($outline->type=="quiz"){
               $q=Quiz::find()->where(['q_id'=>$outline->itemid])->one();
               $q->outlineno=$outline->counter;
               if($q->save()){
                   $flag=1;

               }
            }

        endforeach;
        if($del ==$lastrow)
            $flag=true;*/
        return $flag;
    }
}
