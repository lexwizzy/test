<?php
namespace frontend\models;

use frontend\models\User;
use frontend\models\Course;
use frontend\models\CourseInstructors;
use yii\base\Model;
use app\models\CourseCategory;
use Yii;
use yii\db\Expression;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $email;
    public $password;
    public $roleid;
    public $course_title;
    public $coursecat;

    /**
     * @inheritdoc
     */


    public function rules()
    {
        return [
           // ['username', 'filter', 'filter' => 'trim'],
            [['email', 'firstname', 'lastname', 'roleid','password'], 'required','on'=>'student'],
            [['email','firstname','lastname','password','course_title','coursecat','roleid'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['email', 'firstname', 'roleid'], 'required',"on"=>"tutor"],
            [['email', 'firstname', 'roleid'], 'required',"on"=>"becometutor"],
            ['email', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This email has already been taken.'],
            ['email', 'string', 'min' => 2, 'max' => 100],
            ['course_title','string','min'=>2,'max'=>70],
            ['coursecat','string'],
            [['password'], 'string'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['roleid','integer'],


            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->email = $this->email;
            $user->created_at=new Expression("now()");
            $user->firstname = $this->firstname;
            $user->lastname=$this->lastname;
            $user->roleid=$this->roleid;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generatePasswordResetToken();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    public function tutorSignup()
    {

        if ($this->validate()) {
          if(Yii::$app->user->isGuest){

              $user = new User();
              $user->created_at=new Expression("now()");
              $user->email = $this->email;
              $user->firstname = $this->firstname;
              //$user->lastname=$this->lastname;
              $user->roleid=2;
              $user->setPassword($this->password);
              $user->generateAuthKey();
              $user->generatePasswordResetToken();
              $transaction=$user->getDb()->beginTransaction();
          }

            if ($user->save()) {

                $course= new Course(['scenario' => 'isguest']);
                $course->course_title=$this->course_title;

                $course->course_category=$this->coursecat;
                $course->owner = $user->getPrimaryKey();

                //CourseCategory::convertToInt($this->coursecat);
                if($course->save()){
                    $instructors=  new CourseInstructors();
                    $instructors->course_id=$course->getPrimaryKey();
                    $instructors->user_id=$user->getPrimaryKey();
                    if($instructors->save()){
                         $transaction->commit();
                            //if(!Yii::$app->session->isActive)
                                //    Yii::$app->session->open();

                        Yii::$app->session->set("new_course_slug",$course->slug);
                        return $user;
                    }
                    else
                        print_r($instructors->getErrors());



                }

            }

        }



        return null;
    }
}
