<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "question".
 *
 * @property string $question_id
 * @property string $quiz_id
 * @property string $title
 * @property string $type
 * @property string $description
 * @property string $counter
 *
 * @property Answers[] $answers
 * @property Quiz $quiz
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quiz_id', 'title', 'type', 'description'], 'required'],
            [['quiz_id','counter'], 'integer'],
            [['title'], 'string', 'max' => 200],
            [['type'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'question_id' => 'Question ID',
            'quiz_id' => 'Quiz ID',
            'title' => 'Title',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['questionid' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['q_id' => 'quiz_id']);
    }
    public function getQuestions($quizid){
      $question=array();
      $questions_array= array();
      $answers_array=array();
      $q = Question::find()
      ->joinWith("answers")
      ->where(['quiz_id'=>$quizid])
      //->with("answers")
      ->all();
           foreach($q as $row):
              $question["counter"]=$row->counter;
              $question["title"]=$row->title;
              $question["quiz_id"]=$row->quiz_id;
              $question["id"]=$row->question_id;
              $question["type"]=$row->type;
              $question["description"]=$row->description;
              $_answers=$row->getAnswers()->all();
              foreach($_answers as $a):
                  $answers["id"]=$a->id;
                  $answers["questionid"]=$a->questionid;
                  $answers["answer"]=$a->answer;
                  $answers["reason"]=$a->reason;
                  $answers["iscorrect"]=$a->iscorrect;
                  $answers_array[]=$answers;
              endforeach;
              $question["answers"]=$answers_array;
              $questions_array[]=$question;
              $answers_array=array();
              $answers=array();
          endforeach;
      return $questions_array;
    }
}
