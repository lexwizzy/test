<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tc_searchqueries".
 *
 * @property integer $id
 * @property string $query
 * @property string $date
 * @property string $page
 */
class TcSearchqueries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_searchqueries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'string'],
            [['query','page'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['date'], 'safe'],
            [['page'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'query' => 'Query',
            'date' => 'Date',
            'page' => 'Page',
        ];
    }
}
