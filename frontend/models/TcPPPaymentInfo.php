<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tc_pp_payment_info".
 *
 * @property integer $userid
 * @property string $email
 * @property string $address
 * @property string $city
 * @property integer $pincode
 * @property string $country
 *
 * @property User $user
 */
class TcPPPaymentInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_pp_payment_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'email', 'address'], 'required'],
            [['userid', 'pincode'], 'integer'],
            [['address'], 'string'],
            [['email', 'city', 'country'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'email' => 'Email',
            'address' => 'Address',
            'city' => 'City',
            'pincode' => 'Pincode',
            'country' => 'Country',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
