<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "answers".
 *
 * @property string $id
 * @property string $questionid
 * @property string $answer
 * @property string $reason
 * @property string $iscorrect
 *
 * @property Question $question
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questionid', 'answer'], 'required'],
            [['questionid'], 'integer'],
            [['answer', 'reason'], 'string', 'max' => 200],
            [['iscorrect'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questionid' => 'Questionid',
            'answer' => 'Answer',
            'reason' => 'Reason',
            'iscorrect' => 'Iscorrect',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['question_id' => 'questionid']);
    }
}
