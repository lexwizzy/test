<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Course;

/**
 * CourseSearch represents the model behind the search form about `frontend\models\Course`.
 */
class CourseSearch extends Course
{
    public $q;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'course_category', 'course_subcategory', 'skill_level', 'course_privacy', 'promo_video_length', 'owner', 'status'], 'integer'],
            [['course_title', 'course_desc', 'course_goal', 'target_audience', 'course_price', 'amount', 'language', 'course_banner', 'promo_video', 'promo_video_mime', 'promo_video_thumbnail', 'course_tools', 'createdate', 'date_published', 'slug', 'currency'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
     public function searchTutorCourse($params,$userid,$pagesize){
       $query = Course::find()->where(["owner"=>$userid]);

       if($params->get("sorter")=="oldest"){
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => $pagesize,
             ],
             'sort' => [
                'defaultOrder' => [
                    'createdate' => SORT_DESC,
                ]
            ]
         ]);
       }
       else if($params->get("sorter")=="title-asc"){
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => $pagesize,
             ],
             'sort' => [
                'defaultOrder' => [
                    'course_title' => SORT_ASC,
                ]
            ]
         ]);
       }
       else if($params->get("sorter")=="title-desc"){
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => $pagesize,
             ],
             'sort' => [
                'defaultOrder' => [
                    'course_title' => SORT_DESC,
                ]
            ]
         ]);

       }
       else if($params->get("sorter")=="newest"){
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => $pagesize,
             ],
             'sort' => [
                'defaultOrder' => [
                    'createdate' => SORT_DESC,
                ]
            ]
         ]);
       }
      else{
         $dataProvider = new ActiveDataProvider([
             'query' => $query,
             'pagination' => [
                 'pageSize' => $pagesize,
             ],
             'sort' => [
                'defaultOrder' => [
                    'createdate' => SORT_DESC,
                    'course_title' => SORT_ASC,
                ]
            ]
         ]);

       }
        if (!$this->validate()) {
           // uncomment the following line if you do not want to return any records when validation fails
           // $query->where('0=1');
           return $dataProvider;
       }

        $query->andFilterWhere([
            'owner'=>$userid
        ]);

        $query->andFilterWhere(['like', 'course_title', $params->get('q')]);

        //check if sorter is a number - this means user is sorting by status
        if(is_numeric($params->get("sorter")))
            $query->andFilterWhere(["status"=> $params->get("sorter")]);

       return $dataProvider;
     }
    public function search($params)
    {
        $query = Course::find()->where(["status"=>1]);//->where(['LIKE',"query",$params->get("q")]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
              //  'pageSize' => 3,
            ]
        ]);


      //  $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        $query->andFilterWhere([
          'course_price'=>$params->get("price")
        ])
        ->andFilterWhere(['skill_level'=>$params->get("level")])
        ->andFilterWhere(['language'=>$params->get("language")]);
        // ->andFilterWhere(['course_category'=>$params->get("categor")]);

        $query->andFilterWhere(['like', 'course_title', $params->get("q")])
            ->andFilterWhere(['like', 'course_desc', $params->get("q")]);


        return $dataProvider;
    }
}
