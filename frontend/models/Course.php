<?php

namespace frontend\models;

use Yii;
use frontend\models\CourseReviews;
use frontend\models\Enrollment;
use yii\helpers\Url;
use yii\behaviors\SluggableBehavior;
use frontend\models\CourseViews;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
//use yii\sphinx\ActiveDataProvider;
/**
 * This is the model class for table "courses".
 *
 * @property string $course_id
 * @property string $course_title
 * @property string $course_category
 * @property string $course_desc
 * @property string $course_goal
 * @property string $skill_level
 * @property string $course_price
 * @property string $amount
 * @property string $language
 * @property string $course_privacy
 * @property string $course_banner
 * @property string $promo_video
 * @property string $course_tools
 * @property string $owner
 * @property string $createdate
 * @property string $status
 * @property string $date_published
 * @property CourseInstructors[] $courseInstructors
 * @property CourseParts[] $courseParts
 * @property CoursePrivacy $coursePrivacy
 * @property SkillLevel $skillLevel
 * @property User $owner0
 * @property string currency
 */

class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'course_title',
                'ensureUnique'=>true,
            ],
        ];
    }
    public function rules()
    {
        return [
            [['course_title','course_category'],'required','on'=>'isguest'],
            [['course_title'],'required','on'=>'edit_title'],
            [['course_desc',"short_desc"],'required','on'=>"coursedesc"],
            [['short_desc'],'string','max'=>150],
            //[['query','page'],'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
            [['course_title','course_category','course_subcategory','language','skill_level'],'required','on'=>'basicinfo'],
            [['course_privacy', 'owner','promo_video','course_banner'], 'integer'],
            [['course_desc'],'string','max'=>600,'on'=>"coursedesc"],
            [['createdate'], 'safe'],
            [['promo_video_mime','currency'],'string'],
            [['course_title'], 'string', 'max' => 70],
            [['target_audience'],'string'],
            [['status'], 'integer'],
            [['date_published','promo_video_thumbnail'], 'string', ],
            [[ 'language', 'course_tools'], 'string', 'max' => 100],
            [['course_category','course_subcategory'],'integer'],
            [['amount','promo_video_length'], 'integer'],
            ['amount','default','value'=>""],
            ['amount', 'required', 'when' => function($model) {
                return $model->course_price == 'paid';
            },'on'=>'price-and-coupon'],
            [['course_desc'], 'string'],
            [['course_goal'], 'string'],
            [['course_price'], 'string', 'max' => 20],
            //[['course_banner'], 'string', 'max' => 400],
            ['course_banner','file','extensions'=>'jpg,png','maxSize'=>1000000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate(){
        if($this->language =="" && $this->scenario=="basicinfo")
            $this->addError('language', 'Select your teaching language');

        return parent::beforeValidate();
    }
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course ID',
            'course_title' => 'Course Title',
            'course_category' => 'Course Category',
            'course_desc' => 'Course Desc',
            'course_goal' => 'Course Goal',
            'skill_level' => 'Skill Level',
            'course_price' => 'Course Price',
            'amount' => 'Amount',
            'language' => 'Language',
            'course_privacy' => 'Course Privacy',
            'course_banner' => 'Course Banner',
            'promo_video' => 'Promo Video',
            'course_tools' => 'Course Tools',
            'owner' => 'Owner',
            'createdate' => 'Createdate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseInstructors()
    {
        return $this->hasMany(CourseInstructors::className(), ['course_id' => 'course_id']);
    }


    public function getCourseParts()
    {
        return $this->hasMany(CourseParts::className(), ['course_id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursePrivacy()
    {
        return $this->hasOne(CoursePrivacy::className(), ['id' => 'course_privacy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkillLevel()
    {
        return $this->hasOne(SkillLevel::className(), ['skill_level' => 'skill_level']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner0()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }

    public function getCourseBanner($coursebanner){
      $content = TcUploadedContent::findOne($coursebanner);
      if($content!=null){
        $path =TcUploadedContent::getUploaedContentThumbnail($content->uploaded_date,$content->filename,$content->thumbnail,$content->type);

        if(file_exists($path))
          return $path;
        else
          return $path;// Url::base()."/images/no-introduction-video.jpg";
      }
      else
        return Yii::getAlias("@web")."/upload/coursebanners/default.jpg";
    }
    public function getCoursePromoVideoThumbnail($course){
      $content = TcUploadedContent::findOne($course->promo_video);
      if($content!=null){
        $path =TcUploadedContent::getUploaedContentThumbnail($content->uploaded_date,$content->filename,$content->thumbnail,$content->type);
        if(file_exists($path))
          return $path;
        else
          return Url::base()."/images/no-introduction-video.jpg";
      }
      else
        return Url::base()."/images/no-introduction-video.jpg";

    }
    public function getCoursePromoVideo($course){
      $videopath="";
      $t=TcUploadedContent::findOne($course->promo_video);
      if($t!=null)
        $videopath=TcUploadedContent::getContentUrl($t->uploaded_date,$t->filename);
      return $videopath;
    }
    public function getCourseSameCategory($category,$courseid,$limit){
        $course= Course::find()->where(['and',["!=","course_id",$courseid],['course_category'=>$category]])->andWhere(["status"=>1])->limit($limit)->all();

        if($course==null)
            $course= Course::getOtherCourseViewedByOthers($courseid);

        return $course;
    }
    public function getStatusString($id){
        return Yii::$app->db->createCommand("select status from course_status where id =$id")->queryScalar();
    }
    public function getTutorsCourse($id){
        $course_array=array();
        $counter=0;
        $course = Course::find()->where(['owner'=>$id])->all();
        foreach($course as $row):
                $name = User::getName($row->owner);
                $course_array[$counter]['courseid']=$row->course_id;
                 $course_array[$counter]['slug']=$row->slug;
                $course_array[$counter]['coursetitle']= $row->course_title;
                $course_array[$counter]['category']= CourseCategory::findOne([$row->course_category])->category;
                $course_array[$counter]['price']= Course::coursePrice($row->amount,$row->currency);
                $course_array[$counter]['coursebanner']=Course::getCourseBanner($row->course_id);
                $course_array[$counter]['tutorimage']=User::getPhoto($row->owner);
                if($name!=null)
                  $course_array[$counter]['name']=$name['firstname']." ".$name['lastname'];

                $course_array[$counter]['rating']=CourseReviews::getAverageRating($row->course_id);
                $course_array[$counter]['comment']=CourseReviews::getCommentCount($row->course_id);
                $course_array[$counter]['enrollment']=Enrollment::getEnrollmentCount($row->course_id);
                $course_array[$counter]['status']=Course::getStatusString($row->status);
                $course_array[$counter]['slug']=$row->slug;
                $counter++;
        endforeach;

        return $course_array;
    }
   
    public function getCourseAd($courseid,$model=""){
        $course_array=array();
        $counter=0;
        if($model==null)
          $course = Course::find()->where(['course_id'=>$courseid])->one();
        else
          $course=$model;

        if($course->owner!="")
        $name = User::findOne([$course->owner]);
        $course_array['courseid']=$course->course_id;
        $course_array['slug']=$course->slug;
        $course_array['coursetitle']= $course->course_title;
        $course_array['category']= CourseCategory::findOne([$course->course_category])->category;
        $course_array['price']= Course::coursePrice($course->amount,$course->currency);
        $course_array['coursebanner']=Course::getCourseBanner($course->course_banner);
        $course_array['tutorimage']=User::getPhoto($course->owner);
        if($name!=null)
          $course_array['name']=$name->firstname." ".$name->lastname;
        else {
            $course_array['name']="";
        }
        $course_array['rating']=CourseReviews::getAverageRating($course->course_id);
        $course_array['comment']=CourseReviews::getCommentCount($course->course_id);
        $course_array['enrollment']=Enrollment::getEnrollmentCount($course->course_id);



        return $course_array;
    }


    public function getTutorsOtherCourses($tutorid,$viewcourseid){
        $course = Course::find()->where("owner=$tutorid and course_id!=$viewcourseid limit 0,4")->all();
        return $course;
    }
    public function getOtherCourseViewedByOthers($courseid){

        //Get other users who has viewed this same course ($courseid)
        $cookie= Yii::$app->request->cookies->getValue("tsuser");
        $subquery = \frontend\models\CourseViews::find()->select('cookie')->where(['and',['courseid'=>$courseid],['!=',"cookie",$cookie]]);
        $mainquery= \frontend\models\CourseViews::find()->select("courseid")->where(['and',['!=','courseid',$courseid],["in",'cookie',$subquery]])->orderBy("date desc");
        return Course::find()->where(["in","course_id",$mainquery])->andWhere(["status"=>1])->limit(4)->all();
    }
    //check if a user has viewed a course before
    public function isViewed($courseid,$cookie){
        if(CourseViews::find()->where(["cookie"=>$cookie,"courseid"=>$courseid])->exists())
            return false;
        else
            return true;
    }
     public function coursePrice($amount,$currency){
        if(empty($amount))
            $amount ="Free";
        else{
            $amount=$amount;
            $amount= "₹".round(Course::convertCurrency($currency,$amount));
        }
        return $amount;
    }
    public function coursePriceNoCurrencySymbol($amount,$currency){
        if(empty($amount))
            $amount ="Free";
        else{
            $amount=$amount;
            $amount= round(Course::convertCurrency($currency,$amount));
        }
        return $amount;
    }
    public function convertCurrency($currency,$amount,$toCurrency="INR"){
        $rate = Utility::converter($toCurrency,$currency);
        $price = $rate*$amount;
        return $price;
    }
    public function getPriceNoCurrency($course,$rate=0){
        
        if(empty($course->amount))
            $amount="Free";
        else{
          if($rate !=0)
            $course->amount=CourseCoupons::getSalesPrice($course->amount,$rate);

            $amount=Course::coursePriceNoCurrencySymbol($course->amount,$course->currency);
            
        }

        return $amount;
    }
    public function convertToPaise($rupeeAmount){
        return $rupeeAmount * 100;
    }
    public function getPrice($courseid,$rate=0){
        $course= Course::find()->where(['course_id'=>$courseid])->one();
        if(empty($course->amount))
            $amount="Free";
        else{
          if($rate !=0)
            $course->amount=CourseCoupons::getSalesPrice($course->amount,$rate);

            $amount=Course::coursePrice($course->amount,$course->currency);
        }

        return $amount;
    }
     public function getPriceByInstance($course,$rate=0){
        
        if(empty($course->amount))
            $amount="Free";
        else{
          if($rate !=0)
            $course->amount=CourseCoupons::getSalesPrice($course->amount,$rate);

            $amount=Course::coursePrice($course->amount,$course->currency);
        }

        return $amount;
    }
    public function getRecommendedCourses(){
      //(SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1)
        $course = Yii::$app->db->createCommand("(SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) UNION (SELECT course_id FROM courses AS r1 JOIN(SELECT(RAND() *(SELECT MAX(course_id) FROM courses)) AS id ) AS r2 WHERE r1.course_id >= r2.id  and status=1 ORDER BY r1.course_id ASC LIMIT 1) ")->queryAll();
        return $course;
    }

    public function getNewlyPosted(){

    }
    public function getSections($courseid){
        $sections = CourseParts::find()->where(['course_id'=>$courseid])->orderBy("counter asc")->all();
        return $sections;
    }
    public function getUnits($sectionid){
        $units = CourseLessons::find()->where(['part_id'=>$sectionid])->orderBy("counter asc")->all();
        return $units;
    }

    public function createStudentCourseAd($coursead,$counter=0){
        $url =\yii\helpers\Url::toRoute(['/course/learn','slug'=>$coursead['slug']]);
        $course_content_length="";
        $progress=Course::getCurrentProgress($coursead["courseid"],Yii::$app->user->id);
        if($progress==0)
          $progress_txt='<a title="Learn More" class="course-learn-more" href="'.$url.'"> Begin Course</a>';
        else
          $progress_txt=$progress."% Complete";

        $sec = LessonContent::getCourseVideoLength($coursead['courseid']);
        $pagecount=LessonContent::getCoursePptPages($coursead['courseid']);
        if($sec >= 3600){
            $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";
            $course_content_length ='<p class="hrs">'.$sec.' </p>';
        }
        else if($sec > 0 && $sec <3600){
            $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
            $course_content_length .='<p class="hrs">'.$sec.' </p>';
        }

        if(empty($pagecount)){
            $course_content_length .='<p class="hrs">'.$pagecount.' </p>';
        }
        return '<div class="course-box learning-course-box" data-courseid="'.$coursead['courseid'].'">
                                   <a href="'.$url.'">
                                        <div class="course-box-img">
                                            <img  class="course_banner" src="'.$coursead['coursebanner'].'" />
                                                <div class="course_black_box">
                                                  <img src="'.$coursead['tutorimage'].'"></p>
                                                    <p class="leatures">'.CourseLessons::getLessonCount($coursead['courseid']).' Lectures</p>
                                                    '.$course_content_length.'
                                                    <p class="hrs">'.$coursead['category'].'</p>
                                                </div>
                                        </div>
                                        <div class="course-ratings">
                                        		<div>'.\kartik\rating\StarRating::widget([
                                            'name' => 'rating'.$counter,
                                            'id'=>"rating".$counter,
                                            'value'=> \frontend\models\CourseReviews::getAverageRating($coursead['courseid']),
                                            'pluginOptions' => [
                                                'size' => 'xxs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>false,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]).'</div>
                                        </div>
                                        <div class="course-title">
                                            <p class="course_name ellipsis">'.$coursead['coursetitle'].'</p>
                                        </div>
                                        <div class="course-text">
                                            <p class="truncate2">By '.$coursead['name'].'</p>
                                        </div>
                                      <div class="percent-learn-bar"> <div class="percent-learn-run percent-learn-run-add" style="width: '.$progress.'%;"></div></div>
                                        <div class="course-bottom">
                                              '.$progress_txt.'

                                        </div>
                                        </a>
                                    </div> ';
        // return ' <div class="mc-learning-item mc-item mc-item-2" data-link="'.$url.'"> <div class="image-heading"><a href="'.$url.'"> <img src="'.$coursead['coursebanner'].'" alt="'. $coursead['name'].'"></a> </div><div class="meta-categories"><a href="'.$url.'">'.$coursead['category'].'</a></div><div class="content-item"> <div class="image-author"> <img src="'.$coursead['tutorimage'].'" alt="'. $coursead['name'].'"> </div><h4><a href="'.$url.'" class="ellipsis-2line">'. $coursead['coursetitle'].'</a></h4> <div class="name-author"> By <a href="'.$url.'">'. $coursead['name'].'</a> </div></div><div class="ft-item"> <div class="percent-learn-bar"> <div class="percent-learn-run"></div></div><div class="percent-learn">'.Course::getCurrentProgress($coursead["courseid"],Yii::$app->user->id).'%<i class="fa fa-trophy"></i></div><a href="'.$url.'" class="learnnow">Learn now<i class="fa fa-play-circle-o"></i></a></div></div>';
    }
    public function createTutorsCourseAd($coursead){
         $name = User::findOne([$coursead->owner]);
         $progress_txt="";
         $url= \yii\helpers\Url::toRoute(['/course/studentpreview', 'slug'=>$coursead['slug'],"role"=>2]);

         return '<div class="course-box learning-course-box">
                                    <a href="'.$url.'">
                                         <div class="course-box-img">
                                             <img  class="course_banner" src="'.Course::getCourseBanner($coursead['course_banner']).'" />
                                                 <div class="course_black_box">
                                                 <div class="edit_view">

                                                   <a href="'.Url::toRoute(["/course/target-and-requirements", "courseid"=>$coursead['course_id']]).'" title="Edit course">
                                                      <span>
                                                        <img src="'.Yii::getAlias("@web").'/images/icon/edit.png" alt="Edit Course">
                                                      </span>
                                                    </a>
                                                    &nbsp;

                                                   <a href="'.$url.'" title="Preview course as a student">
                                                    <span>
                                                      <img src="'.Yii::getAlias("@web").'/images/icon/view.png" alt="View Course">
                                                    </span>
                                                    </a>

                                                 </div>
                                                </div>
                                         </div>

                                         <div class="course-title">
                                             <p class="course_name ellipsis">'.$coursead['course_title'].'</p>
                                         </div>
                                         <div class="course-text">
                                             <p class="truncate2">'.Course::getStatusString($coursead['status']).
                                              '<span class="pointer announcement" data-courseid="'.$coursead['course_id'].'" style="float:right" title="Make  announcement to students enrolled in this course" data-target="#announcementModal" data-toggle="modal" >
                                              <img alt="announcement" src="'.Url::base().'/images/icon/speaker.png"</span>
                                             </p>
                                         </div>

                                         </a>
                                     </div> ';

        // return '<div class="col-xs-6 col-sm-4 col-md-3"> <div class="mc-teaching-item mc-item mc-item-2 coursebox"> <div class="image-heading"><a href=""><img src="'.$details['coursebanner'].'" alt=""></a> </div><div class="meta-categories">'.$details['category'].'</div><div class="content-item"> <div class="image-author"> <img src="'.$details['tutorimage'].'" alt=""> </div><h4><a href="'.Url::toRoute(["/course/basicinfo","slug"=>$details["slug"]]).'" class="ellipsis-2line title">'.$details['coursetitle'].'</a></h4> </div><div class="ft-item"> <div class="rating"> '.$details['rating'].'</div><div class="view-info"> <i class="icon md-users"></i>'.$details['enrollment'].' </div><div class="comment-info"> <i class="icon md-comment"></i> '.$details['comment'].'</div><div class="price">'.$details['price'].' </div></div><div class="edit-view"> <a href="'.Url::toRoute(["/course/basicinfo","slug"=>$details["slug"]]).' " class="edit">Edit</a> <a href="'.$url.'" class="view">View</a> <a class="view push-left">'.$details['status'].' </a> | <i data-toggle="modal" data-target="#announcementModal" data-courseid="'.$details['courseid'].'" title="Make  announcement to students enrolled in this course" class="fa fa-bullhorn announcement"></i></div></div></div>';

    }
    public function createGuestCourseAd($coursead,$class){
        $course_content_length="";
         $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
        $sec = LessonContent::getCourseVideoLength($coursead['courseid']);
        $pagecount=LessonContent::getCoursePptPages($coursead['courseid']);
        if($sec >= 3600){
            $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";
            $course_content_length ='<p class="hrs">'.$sec.' </p>';
        }
        else if($sec > 0 && $sec <3600){
            $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
            $course_content_length .='<p class="hrs">'.$sec.' </p>';
        }

        if(empty($pagecount)){
            $course_content_length .='<p class="hrs">'.$pagecount.' </p>';
        }
        return '<div class="item">
                	<div class="popular_box">
                     <a href="'.$url.'">
        				<div class="pop_img">
        					<img src="'.$coursead['coursebanner'].'" >
                			<div class="popular_black_box">
                            	<p><img src="'.$coursead['tutorimage'].'"></p>
                                <p class="leatures">'.CourseLessons::getLessonCount($coursead['courseid']).' Lectures</p>
                                '.$course_content_length.'
                                <p class="hrs">'.$coursead['category'].'</p>
                            </div>
            			</div>
            			<p class="course_name ellipsis">'. $coursead['coursetitle'].'</p>
           				<p class="name ellipsis_name">By</p>
            			<p class="course_tex truncate">'. $coursead['name'].'</p>
           				<div class="box_bottom">
            				<div class="user_icon_bg">
                			<p><a href="#"><span><img src="'.Url::base().'/images/user_icon.png" ></span> '.$coursead['enrollment'].'</a></p>
               				</div>
            			<div class="user_icon_bg">
                			<p><a href="#"><span><img src="'. Url::base().'/images/comment.png" ></span>'. $coursead['comment'].'</a></p>
               			 </div>
               				 <p class="free_txt"><a href="#">'.$coursead['price'].'</a><p>
           				</div>
                    </a>
      				</div>
                </div>';
    }
    public function createGuestOtherCourseAd($coursead,$rating_counter=1){
        $course_content_length="";
         $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
        $sec = LessonContent::getCourseVideoLength($coursead['courseid']);
        $pagecount=LessonContent::getCoursePptPages($coursead['courseid']);
        if($sec >= 3600){
            $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";
            $course_content_length ='<p class="hrs">'.$sec.' </p>';
        }
        else if($sec > 0 && $sec <3600){
            $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
            $course_content_length .='<p class="hrs">'.$sec.' </p>';
        }

        if(empty($pagecount)){
            $course_content_length .='<p class="hrs">'.$pagecount.' </p>';
        }
        return '<div class="course-box">
                                   <a href="'.$url.'">
                                        <div class="course-box-img">
                                            <img  class="course_banner" src="'.$coursead['coursebanner'].'" />
                                                <div class="course_black_box">
                                                  <img src="'.$coursead['tutorimage'].'"></p>
                                                    <p class="leatures">'.CourseLessons::getLessonCount($coursead['courseid']).' Lectures</p>
                                                    '.$course_content_length.'
                                                    <p class="hrs">'.$coursead['category'].'</p>
                                                </div>
                                        </div>
                                        <div class="course-ratings">
                                        		<div>'.\kartik\rating\StarRating::widget([
                                            'name' => 'rating',
                                            'value'=> \frontend\models\CourseReviews::getAverageRating($coursead['courseid']),
                                            'pluginOptions' => [
                                                'size' => 'xxs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>true,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]).'</div>
                                        </div>
                                        <div class="course-title">
                                            <p class="ellipsis2">'.$coursead['coursetitle'].'</p>
                                        </div>
                                        <div class="course-text">
                                            <p class="ellipsis2">By '.$coursead['name'].'</p>
                                        </div>
                                        <span class="course-border-line"></span>
                                        <div class="course-bottom">
                                            <a title="Learn More" class="course-learn-more" href="'.$url.'"> Learn More</a>
                                            <span class="course-price">'.$coursead['price'].'</span>

                                        </div>
                                        </a>
                                    </div> ';
//        return ' <a href="'.$url.'" > <div class="mc-item coursead '.$class.'" data-link="'.$url.'" style="max-width:270px"> <div class="image-heading"> <img src="'.$coursead['coursebanner'].'" alt=""> </div><div class="meta-categories"><a >'.$coursead['category'].'</a></div><div class="content-item"> <div class="image-author"> <img src="'.$coursead['tutorimage'].'" alt=""> </div><h4><a href="'.$url.'" class="ellipsis-2line">'. $coursead['coursetitle'].'</a></h4> <div class="name-author"> <a>By &nbsp;'. $coursead['name'].'</a> </div></div><div class="ft-item"> <div class="rating"> '. $coursead['rating'].' </div><div class="view-info"> <i class="icon md-users"></i> '.$coursead['enrollment'].' </div><div class="comment-info"> <i class="icon md-comment"></i> '. $coursead['comment'].' </div><div class="price"> '.$coursead['price'].' </div></div></div></a>';
    }

    public function createGuestCourseListAd($coursead){

         $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
        return '

                    <div class="mc-item coursead  mc-item-2" data-link="'.$url.'">
                        <div class="image-heading"> <img src="'.$coursead['coursebanner'].'" alt=""> </div>

                        <div class="meta-categories">

                            <a>'.$coursead['category'].'</a>

                        </div>
                        <h4 class="list_title"><a class="ellipsis-2line" href="'.$url.'">'.$coursead['coursetitle'].'</a></h4>
                         <div class="price"> '.$coursead['price'].' </div>

                    <!--<div class="content-item">
                      <h4><a class="ellipsis-2line" href="'.$url.'">'.$coursead['coursetitle'].'</a></h4>
                    </div>

                     <div class="ft-item">
                        <div class="rating"> '. $coursead['rating'].' </div>
                         <div class="price"> '.$coursead['price'].' </div>
                     </div>-->

                    </div>

        ';
    }
    public function createCourseDetailListAd($coursead){
      $url= \yii\helpers\Url::toRoute(['course/details', 'slug'=>$coursead['slug']]);
      return '<div class="right_rating_list">
          <div class="rate_img">
              <img src="'.$coursead['coursebanner'].'">
            </div>
            <div class="rate_star_bg">
              <p class="rate_title ellipsis2"><a style=" color:inherit" href="'.$url.'">'.$coursead['coursetitle'].'</a></p>
              <div class="rating">'
              .\kartik\rating\StarRating::widget([
              'name' => 'rating',
              'value'=> $coursead['rating'],
              'pluginOptions' => [
                  'size' => 'xxs',
                  'stars' => 5,
                  'min' => 0,
                  'step' => 1,
                  'readonly'=>true,
                  'clearButton'=>'',
                  'max' => 5,
                  'showCaption' => false,

                  ],
              ]).'</div>
                <p class="rate_amount">'.$coursead['price'].'</p>
            </div>
        </div>';
    }
    public function getCurrentProgress($courseid,$userid){
        $progress=0;
        $quizcount= Quiz::find()->where(["courseid"=>$courseid])->count();
        $lessoncount =CourseLessons::find()->where(["courseid"=>$courseid])->count();
        $completed = CourseLearnTracking::find()->where(["courseid"=>$courseid,"userid"=>$userid])->count();
        $total=$quizcount+$lessoncount;//Get the number of course contents
        if($total!=0)
            $progress = (100/$total) * $completed;

        return ceil($progress);
    }

    public function doSearch($filter,$q=""){
      //$query="";
      $count=0;

        if($filter=="search"){
          //only query
//          $query = CoursesIndex::find()
//            ->match(new \yii\db\Expression("'@(course_title,course_desc)". Yii::$app->sphinx->escapeMatchValue($q)."'" ))
//            ->where(["status"=>1]);
//            $countQuery = clone $query;
//            $count = $countQuery->count();
//            $q= $count.' Search results for "'.$q.'"';

            $query =Course::find();
            $countQuery =clone $query;
            $count =$countQuery->count();
            $q=$count.' Search result for "'.$q.'"';
        }
        else if($filter=="category"){
          //query and  category search
          $query = CoursesIndex::find()
          //  ->match(new \yii\db\Expression("'@(course_title,course_desc)". Yii::$app->sphinx->escapeMatchValue($q)."'" ))
            ->where(["course_category"=>$q,"status"=>1]);
            $countQuery = clone $query;
            $count = $countQuery->count();
            $q= "Showing courses in '".CourseCategory::findOne([$q])->category."' category";

        }
        else if($filter=="skill_level"){
            //skill level search
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 9,
            ],
        ]);



//        $pages = new Pagination([
//            'totalCount' => $count,
//            'pageSize'=>9]);
//        $query = $query->offset($pages->offset)
//        ->limit($pages->limit)
//        ->all();

        return ["model"=>$query,"pages"=>$dataProvider,"search_term"=>$q];
    }
    public function promoVideoThumbnail($courseid){
        $course = Course::findOne(["course_id"=>$courseid]);
        if(!empty($course->promo_video_thumbnail)){
          return "<img src='/upload/promovideos/thumbnails/".$course->promo_video_thumbnail."' />";
        }
        else {
          return "Drag and drop video here.";
        }
    }

}
