<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\CourseLessons;
use frontend\models\Quiz;

/**
 * This is the model class for table "course_parts".
 *
 * @property string $part_id
 * @property string $part_name
 * @property string $achievement
 * @property string $course_id
 * @property string $counter
 *
 * @property Courses $course
 */
class CourseParts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_parts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['part_name'], 'required','message'=>'Section title cannot be empty'],
            [['course_id','counter','outlineno'], 'integer'],
            [['part_name'], 'string', 'max' => 70],
            [['achievement'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'part_id' => Yii::t('app', 'Part ID'),
            'counter' => Yii::t('app', 'Part ID'),
            'part_name' => Yii::t('app', 'Section Name'),
            'achievement' => Yii::t('app', 'What a student be able to do at the end of this section'),
            'course_id' => Yii::t('app', 'Course ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'course_id']);
    }
    // public function getSectionContents($sectionid,$courseid){
    //   $section_contents=array();
    //   $section=array();
    //   $section_contents=CourseOutlineOrder::getSectionOutline($sectionid,$courseid);
    //   $section["lessons"]=$section_contents["lessons"];
    //   $section["quiz"]=$section_contents["quiz"];
    //   return \yii\helpers\Json::encode($section);
    // }
    public function getOutline($course_id){
        $section_array=array();
            $counter =0;
                     $parts = CourseParts::find()->where(['course_id'=>$course_id])->orderBy("counter asc")->all();
                     if($parts!=null){

                    foreach($parts as $row):
                        ++$counter;
                        $section["part_id"]=$row->part_id;
                        $section["part_name"]=$row->part_name;
                        $section["achievement"]=$row->achievement;
                        $section["rowcounter"]= $row->counter;
                        $section["counter"]=$counter;
                        $section_contents=CourseOutlineOrder::getSectionOutline($row->part_id,$course_id);
                        $section["lessons"]=$section_contents["lessons"];
                        $section["quiz"]=$section_contents["quiz"];
                        $section["contents"]=$section_contents["contents"];

                        $section_array[]=$section;

                  endforeach;
                     }
        return \yii\helpers\Json::encode($section_array);

    }
    public function getPartCount($course_id){
        return CourseParts::find()->where(['course_id'=>$course_id])->count();
    }
}
