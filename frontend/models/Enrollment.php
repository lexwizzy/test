<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "enrollment".
 *
 * @property integer $id
 * @property integer $courseid
 * @property integer $userid
 * @property string $date
 * @property integer $progress
 * @property integer $status
 *
 * @property Courses $course
 * @property User $user
 * @property EnrollmentStatus $status0
 */
class Enrollment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'enrollment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'userid'], 'required'],
            [['courseid', 'userid', 'progress', 'status'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'userid' => 'Userid',
            'date' => 'Date',
            'progress' => 'Progress',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(EnrollmentStatus::className(), ['id' => 'status']);
    }
    public function getEnrollmentCount($courseid){
        return Yii::$app->db->createCommand("select count(courseid) from enrollment where courseid = $courseid")->queryScalar();
    }

    public function getCourseEnrollments($courseid){
        $enrollments= Enrollment::find()->where(['courseid'=>$courseid])->all();
        return $enrollments;
    }
    public function designLearnEnrollment($courseid){
        $li="";
        $enrollment = Enrollment::getCourseEnrollments($courseid);
        foreach($enrollment as $e):
        $name = \frontend\models\User::getName($e->userid);
        $li.='<div class="Students-section">
            <div class="left-side-section-student">
              <img class="small_profile_pix profile_pix" src="'.\frontend\models\User::getPhoto($e->userid,"black").'" alt=""/>
              </div>
              <div class="right-side-section-student">
                <p class="discuss-name">'.$name['firstname']." ".$name['lastname'].'</p>
                  <!-- <p class="student-text">Hanoi, Vietnam</p>-->

              </div>
          </div>';
          //  $li.=' <li><div class="image"><img src="'.\frontend\models\User::getPhoto($e->userid).'" alt=""></div><div class="list-body"><cite class="xsm"><a href="#">'.$name['firstname']." ".$name['lastname'].'</a></cite><span class="address">Hanoi, Vietnam</span></div></li>';
        endforeach;
        return $li;
    }
    public function getMyEnrollments($userid){
        $enroll = Enrollment::find()->where(['userid'=>$userid])->orderBy("date desc")->all();
        if($enroll == null)
            return 0;
        else
            return $enroll;
    }

    public function checkIfEnrolled($userid,$courseid){
      if(Enrollment::find()->where(["userid"=>$userid,"courseid"=>$courseid])->exists()){
          return true;
      }
      else
      return false;
    }

    public function UpdateStatus($userid, $courseid){
        if(Course::getCurrentProgress($courseid,$userid)==100){
          $e= Enrollment::find()->where(["userid"=>$userid,"courseid"=>$courseid])->one();
          $e->status=1;
          $e->progress=Course::getCurrentProgress($courseid,$userid);
          $e->save();
        }
        else{
          $e= Enrollment::find()->where(["userid"=>$userid,"courseid"=>$courseid])->one();
          $e->status=2;
          $e->progress=Course::getCurrentProgress($courseid,$userid);
          $e->save();
        }
    }
}
