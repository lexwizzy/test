<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tc_premium_tutors".
 *
 * @property integer $userid
 * @property string $datetime
 * @property integer $step
 * @property integer $agreement
 *
 * @property User $user
 */
class TcPremiumTutors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_premium_tutors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'required'],
            [['agreement'],'required','requiredValue'=>1,'message'=>"You must accept Teachsity Premium Tutors terms"],
            [['userid', 'step', 'agreement'], 'integer'],
            [['datetime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'datetime' => 'Datetime',
            'step' => 'Step',
            'agreement' => 'Agreement',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
