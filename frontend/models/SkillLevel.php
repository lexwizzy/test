<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "skill_level".
 *
 * @property string $id
 * @property string $skill_level
 *
 * @property Courses[] $courses
 */
class SkillLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'skill_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['skill_level'], 'required'],
            [['skill_level'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skill_level' => 'Skill Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['skill_level' => 'id']);
    }
    public function getSkillLevel($id){
        return SkillLevel::findOne([$id])->skill_level;
    }
}
