<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;
use frontend\models\CourseLessons;
use frontend\models\User;
use yii\i18n\Formatter;

/**
 * This is the model class for table "lesson_content".
 *
 * @property string $content_no
 * @property string $lesson_no
 * @property string $content_defaultname
 * @property string $content_editedname
 * @property string $content_format
 * @property string $content_mime
 * @property string $content_link
 * @property string $content_basefoldername
 * @property string $date
 * @property string $uploaded_by
 * @property string $main
 */
class LessonContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_no', 'content_format',], 'required'],
            [['content_link', 'content_defaultname',], 'required','on'=>'external'],
            [['content_defaultname', 'text_content'], 'required','on'=>'text','message'=>'This field is required'],
            [['lesson_no', 'uploaded_by','video_length','no_of_pages'], 'integer'],
            [['date'], 'safe'],
            [['thumbnail'],'string'],
            [['size'],'integer'],
            [['content_defaultname'], 'string', 'max' => 200],
            [['content_editedname'], 'string', 'max' => 400],
            [['content_format'], 'string', 'max' => 100],
            [['content_link'], 'string', 'max' => 1000],
            [['content_link'],'url'],
            [['content_mime'],'string','max'=>400],
            [['text_content'],'string'],
            [['content_basefoldername'],'string','max'=>1000],
            [['main'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content_no' => 'Content No',
            'lesson_no' => 'Lesson No',
            'content_defaultname' => 'Title',
            'content_editedname' => 'Content Editedname',
            'content_format' => 'Content Format',
            'content_link' => 'Content Link',
            'date' => 'Date',
            'uploaded_by' => 'Uploaded By',
            'main' => 'Main',
        ];
    }
    // public function getContents($lesson_no){
    //     $content="";
    //     $content.=LessonContent::getMainContent($lesson_no);
    //     $content.=LessonContent::getOtherContent($lesson_no);
    //     return $content;
    // }
    // public function getMainContent($lesson_no){
    //     $content=array();
    //     $lesson = LessonContent::find()->where(['and',['=','lesson_no',$lesson_no],['=','main','Yes']])->one();
    //     $main_content = array();
    //
    //     if($lesson !=null){
    //
    //          // $content= '<!--Begin Contents info div--><div class="content-item-info">
    //          //                                            <div class="upload-thumb">';
    //          $main_content["owner"] = User::findByUserId($lesson->uploaded_by)->firstname;
    //          $main_content["content_no"]=$lesson->content_no;
    //          $main_content["uploadtime"] = Yii::$app->formatter->asDatetime($lesson->date, "php:d, M,Y");
    //          $main_content["content_defaultname"]=$lesson->content_defaultname;
    //
    //          if($lesson->video_length !=0){
    //              $main_content["type"]="video";
    //              $main_content["video_length"]=gmdate("i:s",$lesson->video_length);
    //              $main_content["content_size"] =  LessonContent::formatBytes($lesson->size);
    //              $main_content["thumbnails"]=Url::base().'/upload/coursevideos/thumbnails/'.$lesson->thumbnail;
    //          }
    //          else if($lesson->content_format=="pdf"){
    //
    //              $main_content["type"]="pdf";
    //              $main_content["page_no"]=$lesson->no_of_pages;
    //             $main_content["content_size"] =  LessonContent::formatBytes($lesson->size);
    //              $main_content["thumbnails"]=Url::base().'/upload/coursepresentations/thumbnails/'.$lesson->thumbnail;
    //          }
    //          else{
    //           // $main_content["content_defaultname"]=$lesson->content_defaultname;
    //            $main_content["type"]="text";
    //
    //          }
    //
    //          $content[]=$main_content;
    //         return $content;
    //     }
    //
    // }
    //  public function getOtherContent($lesson_no){
    //
    //
    //      $others =array();
    //      $others_array = array();
    //     $other_content = LessonContent::find()->where(['and',['=','lesson_no',$lesson_no],['!=','main','Yes']])->all();
    //     if($other_content !=null){
    //         foreach($other_content as $lesson):
    //             $others["owner"] = User::findByUserId($lesson->uploaded_by)->firstname;
    //             $others["content_no"]=$lesson->content_no;
    //             $others["uploadtime"] = Yii::$app->formatter->asDatetime($lesson->date, "php:d, M,Y");
    //             $others["content_defaultname"]=$lesson->content_defaultname;
    //             $others["content_size"] =  LessonContent::formatBytes($lesson->size);
    //             $others_array[]=$others;
    //         endforeach;
    //
    //
    //
    //         //
    //         //     $others .='<!--Begin Contents info div--><div class="content-item-info">
    //         //                                             <div class="upload-thumb">';
    //         //  if(!empty($lesson->video_thumbnail))
    //         //    $others.='<img src="'.Url::base().'/upload/coursevideos/thumbnails/'.$lesson->video_thumbnail.'" alt="">';
    //
    //         //                 $others.='</div>
    //         //                                             <div class="upload-cnt">
    //         //                                                 <div class="title-file">
    //         //                                                     <span class="name-file">'.$lesson->content_defaultname.'</span>
    //
    //         //                                                 </div>
    //         //                                                 <div class="upload-meta">
    //         //                                                     <span>Uploaded by '.User::findByUserId($lesson->uploaded_by)->firstname.' . '.Yii::$app->formatter->asDatetime($lesson->date, "php:d, M,Y").'</span>
    //         //                                                 </div>
    //         //                                                 <div class="upload-meta"><i class="fa fa-file icon">  '.LessonContent::formatBytes($lesson->size).'</i>
    //         //                                         </div>
    //         //                                                 <div class="upload-footer">
    //         //                                                    <!-- <a href="#">Edit</a>-->
    //         //                                                     <!--<a href="#">Preview</a>-->
    //         //                                                     <a style="cursor:pointer" class="delete-content" data-contentno="'.$lesson->content_no.'">Delete</a>
    //         //                                                 </div>
    //         //                                             </div>
    //         //                                         </div><!--End Contents info div-->';
    //         // endforeach;
    //          return $others_array;
    //
    //     }
    //
    // }
    // public function getContentThumbanail($content_id){
    //
    // }

    public function getVideoLength($file){
      //install awk package
       //$ffmpeg_path = Yii::getAlias("@vendor").'/ffmpeg/bin/';
       $xyz = shell_exec("/home/teachsity_user/bin/ffmpeg -i \"{$file}\" 2>&1 | grep \"Duration\"| cut -d ' ' -f 4 | sed s/,// | sed 's@\..*@@g' | awk '{ split($1, A, \":\"); split(A[3], B, \".\"); print 3600*A[1] + 60*A[2] + B[1] }'");
      //  $search='/Duration: (.*?),/';
       //
      //  preg_match($search, $xyz, $matches);
      //  $explode = explode(':', $matches[1]);
       return $xyz;//round($explode[2]);
   }
    public function convertSeconds($second,$format="H:i:s"){
       return gmdate($format,$second);
    }


    public function createThumbnail($videofile,$destination){
         //$destination = Yii::getAlias("@webroot")."/upload/tdseste.png";
        // $ffmpeg_path = Yii::getAlias("@vendor").'/ffmpeg/bin/';
        return  shell_exec("/home/teachsity_user/bin/ffmpeg -i \"{$videofile}\" -ss 00:00:3.435 -vframes 1 ".$destination);
    }
    public function formatBytes($bytes, $decimals = 2) {
      $sz = 'BKMGTP';
      $factor = floor((strlen($bytes) - 1) / 3);
      return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    public function getCourseVideoLength($courseid){
        $query = new \yii\db\Query;
        $query	->select(['sum(video_length)'])
                ->from('lesson_content')
                ->join("join",'course_lessons','lesson_id = lesson_no')
                ->join("join","courses","course_id = courseid")
                ->where(["content_basefoldername"=>"coursevideos","courseid"=>$courseid])
                ->groupBy("courseid");
        $command = $query->createCommand();
        $sec = $command->queryScalar();

        return $sec;
    }
    public function getCoursePptPages($courseid){
        $pages=0;
        $query = new \yii\db\Query;
        $query	->select(['sum(no_of_pages)'])
                ->from('lesson_content')
                ->join("join",'course_lessons','lesson_id = lesson_no')
                ->join("join","courses","course_id = courseid")
                ->where(["content_basefoldername"=>"coursepresentations","courseid"=>$courseid])
                ->groupBy("courseid");
        $command = $query->createCommand();
        $pages = $command->queryScalar();
    }
}
