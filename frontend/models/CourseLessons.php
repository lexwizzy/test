<?php

namespace frontend\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\LessonContent;
use frontend\models\TcLessonContent;
/**
 * This is the model class for table "course_lessons".
 *
 * @property string $lesson_id
 * @property string $lesson_title
 * @property string $lesson_description
 * @property string $part_id
 * @property string $counter
 */
class CourseLessons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_lessons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_title', 'lesson_description', 'part_id'], 'required'],
            [['part_id', 'counter','courseid','outlineno'], 'integer'],
            [['lesson_title'], 'string', 'max' => 70],
            [['lesson_description'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lesson_id' => Yii::t('app', 'Lesson ID'),
            'lesson_title' => Yii::t('app', 'Lesson Title'),
            'lesson_description' => Yii::t('app', 'Lesson Description'),
            'part_id' => Yii::t('app', 'Part ID'),
            'counter' => Yii::t('app', 'Counter'),
        ];
    }
    public function mainContentExist($lessonid){
        if(!TcLessonContent::find()->where(['lessonid'=>$lessonid,"main"=>"Yes"])->exists()){
          //,["type"=>"text","text"=>"Text"]
            return[["type"=>"video","text"=>"Video"],["type"=>"ppt","text"=>"Presentation"],["type"=>"downloadables","text"=>"Resources"],["type"=>"library","text"=>"Upload from library"]];
        }
        else{
            return[["type"=>"downloadables","text"=>"Resources"],["type"=>"library","text"=>"Upload from library"]];
        }
    }
    public function getLesson($model,$counter='',$outlineno=''){
         $lesson["outlineno"] = $outlineno;
         $lesson["lesson_id"]=$model->lesson_id;
         $lesson["part_id"]=$model->part_id;
         $lesson["lesson_counter"]=$counter;
         $lesson["lesson_title"]=$model->lesson_title;
         $lesson["lesson_description"]=htmlentities($model->lesson_description);
         $lesson['lesson_content'] = TcLessonContent::getContents($model->lesson_id);
        // $lesson["othercontent"] = TcLessonContent::getOtherContent($model->lesson_id);
         $lesson["content_dropdown"]=CourseLessons::mainContentExist($model->lesson_id);

        return $lesson;

    }
    // public function getLesson($model,$counter='',$outlineno=''){
    //      $lesson["outlineno"] = $outlineno;
    //      $lesson["lesson_id"]=$model->lesson_id;
    //      $lesson["part_id"]=$model->part_id;
    //      $lesson["lesson_counter"]=$counter;
    //      $lesson["lesson_title"]=$model->lesson_title;
    //      $lesson["lesson_description"]=htmlentities($model->lesson_description);
    //      $lesson['main_content'] = LessonContent::getMainContent($model->lesson_id);
    //      $lesson["othercontent"] = LessonContent::getOtherContent($model->lesson_id);
    //      $lesson["content_dropdown"]=CourseLessons::mainContentExist($model->lesson_id);
    //
    //     return $lesson;
    //
    // }

    public function getLessonCount($courseid){
        return Yii::$app->db->createCommand("SELECT COUNT(lesson_id)FROM course_lessons JOIN course_parts ON course_lessons.part_id=course_parts.part_id WHERE course_id=$courseid")->queryScalar();
    }

    public function getUnitMainContent($unitid){
      if(TcLessonContent::find()->where(['lessonid'=>$unitid,'main'=>'Yes'])->exists()){
          $content=TcLessonContent::getMainContent($unitid);//TcLessonContent::find()->where(['lessonid'=>$unitid,'main'=>'Yes'])->one();
          return $content;
      }

    }

    public function getContentType($contentid){
        if(LessonContent::find()->where(['content_no'=>$contentid])->exists()){
            $content=LessonContent::find()->where(['content_no'=>$contentid])->one();

            if(strpos(strtolower($content->content_mime),"video")!==false){
                return 0;
            }
            else if(strpos(strtolower($content->content_mime),"powerpoint")!==false){
                return 1;
            }
            else if(strpos(strtolower($content->content_mime),"pdf")!==false){
                return 2;
            }
            else
                return 3;
         }

    }

}
