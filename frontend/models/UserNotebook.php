<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_notebook".
 *
 * @property string $userid
 * @property string $note
 * @property string $date
 */
class UserNotebook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notebook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'required'],
            [['userid'], 'integer'],
            [['note'], 'string'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => 'Userid',
            'note' => 'Note',
            'date' => 'Date',
        ];
    }
}
