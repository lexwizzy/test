<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "flag_abuse".
 *
 * @property string $id
 * @property string $courseid
 * @property string $reason
 * @property string $userid
 * @property string $comment
 * @property string $date
 */
class FlagAbuse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flag_abuse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['courseid', 'reason', 'userid'], 'required'],
            [['courseid', 'reason', 'userid'], 'integer'],
            [['comment'], 'string'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'courseid' => 'Courseid',
            'reason' => 'Reason',
            'userid' => 'Userid',
            'comment' => 'Comment',
            'date' => 'Date',
        ];
    }
}
