<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tc_group_user".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $dateadded
 * @property integer $groupid
 *
 * @property TcCourseGroup $group
 * @property User $user
 */
class TcGroupUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tc_group_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'groupid'], 'required'],
            [['userid', 'groupid'], 'integer'],
            [['dateadded'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'dateadded' => 'Dateadded',
            'groupid' => 'Groupid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TcCourseGroup::className(), ['id' => 'groupid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
