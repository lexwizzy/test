<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "discussions".
 *
 * @property string $id
 * @property string $userid
 * @property string $courseid
 * @property string $topic
 * @property resource $comment
 * @property string $date
 *
 * @property User $user
 * @property Courses $course
 */
class Discussions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discussions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'courseid', 'topic', 'comment'], 'required'],
            [['userid', 'courseid'], 'integer'],
            [['comment'], 'string'],
            [['date'], 'safe'],
            [['topic'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'courseid' => 'Courseid',
            'topic' => 'Topic',
            'comment' => 'Comment',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'courseid']);
    }
    public function getDiscussions($courseid){
        $discussion = Discussions::find()->where(["courseid"=>$courseid])->all();
        return $discussion;
    }

    public function designDiscussion($discussion){
            $name = \frontend\models\User::getName($discussion->userid);
            return '<div class="discuss-section">
                        	<div class="left-side-section-discuss">
                            	  <img class="small_profile_pix profile_pix" src="'.\frontend\models\User::getPhoto($discussion->userid,"black").'" alt="">
                            </div>
                            <div class="right-side-section-discuss">
                            	<p class="discuss-name">'.$name['firstname']." ".$name['lastname'].'</p>
                                <p class="discuss-title-name">'.$discussion->topic.'</p>
                                <p class="discuss-text">'.$discussion->comment.'</p>
                                <p class="month-discuss">'.Utility::calculateTime(strtotime($discussion->date)).' | <span class="pointer count-reply"><i class="fa fa-reply "></i> '.DiscussionReplies::countReply($discussion->id).'  replies</span></p>

                                <ul class="replies" style="display:none">
                                    '.Discussions::getReply($discussion->id).'
                                    <li class="last-child">
                                        <form action="savereply" onsubmit="return false;" class="replyform">
                                            <input type="hidden" name="DiscussionReplies[discussion]" value="'.$discussion->id.'"/>
                                            <input type="hidden" name="DiscussionReplies[courseid]" value="'.$discussion->courseid.'"/>
                                            <input type="hidden" class="userid" name="DiscussionReplies[userid]" value/>
                                            <textarea name="DiscussionReplies[reply]" class="form-control"></textarea>
                                        <div>

                                            <button type="submit" class=" course-learn-more review-input-box save-reply" data-color="blue" data-style="expand-right" data-size="xs">Reply</button>

                                        </div>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="clear"></div>
                          <hr/>


                                    ';


    }
     public function displayDiscussion($courseid){
        $discussion = Discussions::getDiscussions($courseid);
        $r="";
        foreach($discussion as $d){
            $r.=Discussions::designDiscussion($d);
        }
        return $r;
    }

    public function getReply($discussionid){
        $li="";

        $reply= DiscussionReplies::find()->where(['discussion'=>$discussionid])->all();
        if($reply == null){
            $li="<li class='text-center'><p> Be the first to reply</p><li>";
        }else{
             foreach($reply as $r):
              $li.=Discussions::designReply($r);
            endforeach;
        }

            return $li;

    }
    public function designReply($reply){
        $name = \frontend\models\User::getName($reply->userid);
            $li='<li>
                                                <div>
                                                  <div class="left-side-section-discuss">
                                                    <img class="image small_profile_pix profile_pix" src="'.\frontend\models\User::getPhoto($reply->userid,"black").'" alt="">
                                                    </div>
                                                    <cite class="discuss-name">'.$name['firstname']." ".$name['lastname'].'</cite>
                                                    <p>'.Utility::calculateTime(strtotime($reply->date)).'</p>
                                                </div>
                                                <p>
                                                    '.$reply->reply.'
                                                </p>
                                            </li>';
        return $li;
    }
    public function getCount($courseid){
        return Discussions::find()->where(['courseid'=>$courseid])->count();
    }
}
