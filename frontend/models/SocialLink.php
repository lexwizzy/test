<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "social_link".
 *
 * @property string Yii::$app->user->id
 * @property string $linkedin
 * @property string $github
 * @property string $twitter
 * @property string $google
 * @property string $facebook
 *
 * @property User $user
 */
class SocialLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['linkedin', 'github', 'twitter', 'google', 'facebook'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userid' => Yii::t('app', 'Userid'),
            'linkedin' => Yii::t('app', 'Linkedin'),
            'github' => Yii::t('app', 'Github'),
            'twitter' => Yii::t('app', 'Twitter'),
            'google' => Yii::t('app', 'Google'),
            'facebook' => Yii::t('app', 'Facebook'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
    public function checkLink($userid){
       return  SocialLink::find()->where([
            'userid'=>$userid,
        ])->exists();
    }
    public function getLinks($userid){
        $link_array=array();
        $link= SocialLink::findOne(['userid'=>$userid]);
        if($link !=null){
             $link_array['facebook']=$link->facebook;
            $link_array['google']=$link->google;
            $link_array['twitter']=$link->twitter;
            $link_array['github']=$link->github;
            $link_array['linkedin']=$link->linkedin;
        }else{
            $link_array['facebook']="#";
            $link_array['google']="#";
            $link_array['twitter']="#";
            $link_array['github']="#";
            $link_array['linkedin']="#";
        }


        return $link_array;

    }
    public function saveLink($client,$check_user){
      $link="#";
        $attributes = $client->getUserAttributes();
        if($client->getId()=="linkedin"){
                                $new_social_link= new SocialLink([
                                'userid'=>$check_user->id,
                                "linkedin"=>$attributes['public-profile-url']
                                    ]);
                            }
                            else if($client->getId()=="twitter"){
                                $new_social_link= new SocialLink([
                                'userid'=>$check_user->id,
                                'twitter'=>$attributes['public-profile-url'],
                                    ]);
                            }
                            else if($client->getId()=="facebook"){
                                if(!empty($attributes['link']))
                                  $link=$attributes['link'];
                                $new_social_link= new SocialLink([
                                'userid'=>$check_user->id,
                                'facebook'=>$link,
                                    ]);
                            }

                            else if($client->getId()=="google"){
                                $new_social_link= new SocialLink([
                                'userid'=>$check_user->id,
                                'google'=>$attributes['public-profile-url'],
                                    ]);
                            }

                            $new_social_link->save();
    }
    public function updateLink($client,$check_user){
        $attributes = $client->getUserAttributes();
        $update_social_link=SocialLink::findOne(
                          [
                              'userid'=>$check_user->id
                          ]);
                          if($client->getId()=="linkedin")
                            $update_social_link->linkedin=$attributes['public-profile-url'];
                          else if($client->getId()=="twitter")
                            $update_social_link->twitter=$attributes['public-profile-url'];
                          else if($client->getId()=="facebook")
                            $update_social_link->facebook=$attributes['public-profile-url'];
                          else if($client->getId()=="google")
                            $update_social_link->google=$attributes['public-profile-url'];
                          $update_social_link->update();
    }
}
