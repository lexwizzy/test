<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "course_instructors".
 *
 * @property string $course_id
 * @property string $user_id
 *
 * @property Courses $course
 * @property User $user
 */
class CourseInstructors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_instructors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'course_id', 'user_id'], 'required'],
            [[ 'course_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => Yii::t('app', 'Course ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Courses::className(), ['course_id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
