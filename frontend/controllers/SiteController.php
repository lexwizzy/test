<?php
namespace frontend\controllers;

use Yii;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\Auth;
use frontend\models\SocialLink;
use frontend\models\User;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\db\Expression;
use frontend\models\Course;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout ="main";


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','learn','tutorsignup'],
                'rules' => [
                    [
                        'actions' => ['signup','tutorsignup','learn','forgotpassword','testscript','dashboard'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','testscript'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'logout' => ['post'],
                ],
            ],
            [
               'class' => 'yii\filters\HttpCache',
               'only' => ['index'],
               'cacheControlHeader' => 'public, max-age=3600',
              'lastModified' => function ($action, $params) {

                if(Yii::$app->user->isGuest){
                  $q = new \yii\db\Query();
                   return strtotime($q->from('courses')->max('createdate'));
                }
                else {
                  return time();
                }

              },
           ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }
    public function actionAuth(){

    }

    public function onAuthSuccess($client)
    {
      $fb_lastname="";
      $fb_lastname="";
      $gender="";
      $bio="";
      $profile_px="";
      $link="";
        $attributes = $client->getUserAttributes();
      //   print_r($client->getUserAttributes());
      // Yii::$app->end();
        /* @var $auth Auth */
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();
       // print_r($attributes['picture']['data']['url']);
       // Yii::$app->end();
        if (Yii::$app->user->isGuest) {


            if ($auth) { // login
                $user = $auth->user;

                Yii::$app->user->login($user);
                 LoginForm::setSession($user);
            } else { // signup
                $check_user=User::findOne(['email' => $attributes['email']]);
                if (isset($attributes['email']) && $check_user) {
                    /*Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()]),
                    ]);*/
                    $auth = new Auth([
                            'user_id' => $check_user->id,
                            'source' => $client->getId(),
                            'source_id' => (string)$attributes['id'],
                        ]);
                      if(SocialLink::checkLink($check_user->id)){
                          SocialLink::updateLink($client,$check_user);
                        }
                        else{
                            SocialLink::saveLink($client,$check_user);
                        }
                        if ($auth->save()) {
                           // $transaction->commit();

                            $user = $auth->user;
                            Yii::$app->user->login($user);
                            LoginForm::setSession($user);
                        }


                        Yii::$app->user->login($user);
                        LoginForm::setSession($user);
                    //  echo "Sign up";
                //Yii::$app->end();
                } else {
                    $password = Yii::$app->security->generateRandomString(6);

                    if($client->getId()=="linkedin"){
                         $user = new User([
                        //'username' => $attributes['login'],
                            'email' => $attributes['email'],
                            'password' => $password,
                            'firstname'=>$attributes['first-name'],
                            'lastname'=>$attributes['last-name'],
                             'photo'=>$attributes['picture-url'],
                             'aboutme'=>$attributes['summary'],
                              'created_at'=>new Expression("now()")
                        ]);
                    }
                    else if($client->getId()=="facebook"){
                         if(empty($attributes['first_name'])){
                           $fb_firstname = explode(" ",$attributes["name"])[0];
                           $fb_lastname=explode(" ",$attributes["name"])[1];
                         }
                         if(!empty($attributes['gender'])){
                           $gender = $attributes['gender'];
                         }
                         if(!empty($attributes['bio'])){
                           $bio = $attributes['bio'];
                         }
                         if(!empty($attributes['picture'])){
                           $profile_px=$attributes['picture']['data']['url'];
                         }
                        $user = new User([
                        //'username' => $attributes['login'],
                            'email' => $attributes['email'],
                            'password' => $password,
                            'firstname'=>$fb_firstname,
                            'lastname'=>$fb_lastname,
                            'gender'=>$gender,
                            'aboutme'=>$bio,
                            'photo'=>$profile_px,
                            'created_at'=>new Expression("now()")
                        ]);
                    }

                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    $transaction = $user->getDb()->beginTransaction();
                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $client->getId(),
                            'source_id' => (string)$attributes['id'],
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            //Save social links
                             if(SocialLink::checkLink($user->id)){
                              SocialLink::updateLink($client,$user);
                            }
                            else{
                                SocialLink::saveLink($client,$user);
                            }
                            Yii::$app->user->login($user);
                             LoginForm::setSession($user);
                        } else {
                            print_r($auth->getErrors());
                        }

                    } else {
                        print_r($user->getErrors());
                    }
                }
            }
        } else {
            // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }
    }
      public function actionIndex()
    {

        $this->layout="home_page";
        $cookie= Yii::$app->response->cookies;
        if(!Yii::$app->request->cookies->has("tsuser"))
            $cookie->add(new \yii\web\Cookie(['name'=>"tsuser",'value'=>Yii::$app->security->generateRandomString(),'expire'=>3600 * 1000 * 24 * 365]));
        if(!Yii::$app->user->isGuest){
            return $this->redirect(Url::toRoute("/courses"));
          // if(Yii::$app->session->has("role")){
          //     if(Yii::$app->session->get('role')==1)
          //       return $this->redirect("student/dashboard");
          //   else if(Yii::$app->session->get('role')==2)
          //        return $this->redirect("tutors/dashboard");
          // }
          // else{
          //     $role =User::findOne([Yii::$app->user->id])->roleid;
          //     Yii::$app->session->set("role",$role);
          //     return $this->redirect(Url::toRoute("/site/index"));
          // }


        }
        else{
           $course = Course::find()->where(["status"=>1])->orderBy("createdate desc")->limit(8)->all();
           return $this->render('index',["course"=>$course]);
        }

    }

    public function actionDashboard(){
      if(Yii::$app->session->has("role")){
          if(Yii::$app->session->get('role')==1)
            return $this->redirect(Url::toRoute("/student/dashboard"));
        else if(Yii::$app->session->get('role')==2)
             return $this->redirect(Url::toRoute("/tutors/dashboard"));
      }
      else{
          $role =User::findOne([Yii::$app->user->id])->roleid;
          Yii::$app->session->set("role",$role);
          return $this->redirect(Url::toRoute("/site/index"));
      }
    }
    public function actionIndextest()
    {
        $this->layout="home_page";

        /*if(!Yii::$app->user->isGuest){
            return $this->redirect("student");
        }
        else*/
        return $this->render('index_1');
    }
    public function actionLogin()
    {
        $this->layout="main";
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(Url::toRoute("/courses"));
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
           return $this->redirect(Yii::$app->user->getReturnUrl());
            if($model->getUser()->roleid==2)
                return $this->redirect("tutors/dashboard");
            else
                return $this->goBack();
        } else {
          if(Yii::$app->request->get("ajax")==1){
            if($model->hasErrors()){
              return $this->render('_login', [
                  'model' => $model
              ]);
            }
            return $this->renderAjax('_login', [
                'model' => $model
            ]);
          }
          else{
            return $this->render('_login', [
                'model' => $model
            ]);
          }

        }

    }
    public function actionTestscript(){
        echo Yii::getAlias("@app");
        echo "<br>";
        echo Yii::getAlias("@vendor");
        echo "<br>";
        echo Yii::getAlias("@webroot");
        echo "<br>";
        echo Yii::getAlias('@webroot').'\upload\coursepresentations\1444133081.ppt';
        echo Yii::getAlias("@webroot")."/upload/teste123.png";
        echo "<br>";
        //echo Yii::getAlias("@webroot")."/upload/coursepresentations/1442636957.pdf";
        //echo \frontend\models\Utility::getPDFPageCount(Yii::getAlias("@webroot")."/upload/coursepresentations/1442636957.pdf");
        echo \frontend\models\Utility::convert_to_pdf(Yii::getAlias('@webroot')."/upload/coursepresentations/1440825631.pptx",Yii::getAlias('@webroot')."/upload/coursepresentations/tester.pdf");
        //echo \frontend\models\Utility::convertPdfPageToPng(Yii::getAlias("@webroot")."/upload/coursepresentations/1442636957.pdf[0]",Yii::getAlias('@webroot')."/upload/coursepresentations/thumbnails/test1123.jpg");
      //$unoconv->transcode('C:\convertTest\Handover.docx', 'pdf', 'document.pdf'); //
    //  print_r( \frontend\models\LessonContent::getVideoLength(Yii::getAlias("@webroot")."/upload/coursevideos/1452241602.mp4"));
      // \frontend\models\LessonContent::createThumbnail(Yii::getAlias("@webroot")."/upload/promovideos/1445412825.mp4",Yii::getAlias("@webroot")."/upload/teste123.png");
       //echo  \frontend\models\LessonContent::getVideoLength(Yii::getAlias("@webroot")."/upload/promovideos/1442878471Adobephotoshoptool2.mp4");
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        $model->scenario ="student";
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                     LoginForm::setSession($user);

                  // \Yii::$app->mailer->compose(['html' => 'signupComplete-html'], ['user' => $user])
                  //  ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                  //  ->setTo($user->email)
                  //  ->setSubject('Welcome to ' . \Yii::$app->name)
                  //  ->send();
                    return $this->redirect("/student/dashboard");
                }
            }
        }
        if(Yii::$app->request->get("ajax")==1){
          if($model->hasErrors()){
              return $this->render('_signup', [
                  'model' => $model
              ]);
            }
          return $this->renderAjax('_signup', [
              'model' => $model
          ]);
        }
        else{
          return $this->render('_signup', [
              'model' => $model
          ]);
        }
    }
    /*public function actionTutorSignup(){

        $model=new SignupForm();

        if($model->load(Yii::$app->request->post())){
            if($user = $model->tutorsignup()){
                if(Yii::$app->getUser()->login($user)){
                      LoginForm::setSession($user);
                    return $this->goHome();
                }
            }
        }
          return $this->render('tutor-signup', [
            'model' => $model,
        ]);
    }*/
    public function actionForgotpassword()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

              //  return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

          //  return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    public function actionLearn(){
        return $this->render("helplearn");
    }
}
