<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TcCourseGroup;
use frontend\models\TcGroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * GroupController implements the CRUD actions for TcCourseGroup model.
 */
class GroupController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TcCourseGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new TcGroupSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //
        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    public function actionAddUser(){

      $g_user = new \frontend\models\TcGroupUser;
      $params= Yii::$app->request->post();
      $user = \frontend\models\User::find()->where(["email"=>$params['useremail']])->one();

      $g= \frontend\models\TcCourseGroup::find()->where(['id'=>$params['group']])->one();
      if($user==null){
          Yii::$app->session->setFlash("error","The email address you entered does not exist.");
          return $this->redirect(Url::previous());
      }
      else if($g == null){
        Yii::$app->session->setFlash("error","The group you selected does not exist.");
        return $this->redirect(Url::previous());
      }
      else if(\frontend\models\TcGroupUser::find()->where(["userid"=>$user->id,"groupid"=>$g->id])->exists()){
        Yii::$app->session->setFlash("error",$user->email.' is already added to this group "'.$g->name.'"');
        return $this->redirect(Url::previous());
      }
      $g_user->groupid=$params["group"];
      $g_user->userid= $user->id;
      $g_user->save();
      Yii::$app->session->setFlash("success",$params['useremail']." was successfully added to ".$g->name);
      return $this->redirect(Url::previous());

    }

    /**
     * Displays a single TcCourseGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->request->get('delete')){
            $guser= Yii::$app->request->get("guser");
            $del=\frontend\models\TcGroupUser::find()->where(["id"=>$guser])->one();
            $del->delete();
        }
      $query = \frontend\models\TcGroupUser::find()->where(["groupid"=>$id]);
      $dataProvider = new \yii\data\ActiveDataProvider([
          'query' => $query,
          // 'pagination' => [
          //     'pageSize' => 2,
          // ],
      ]);
        return $this->renderAjax('_user', [
            'model' => $this->findModel($id),
            'dataProvider'=>$dataProvider
        ]);
    }

    /**
     * Creates a new TcCourseGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TcCourseGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          Yii::$app->session->setFlash("success","Your last operation was successful.");
            return $this->redirect(Url::previous());
        } else {
          Yii::$app->session->setFlash("error","Your last operation was unsuccessful.");
          return $this->redirect(Url::previous());
            // return $this->render('create', [
            //     'model' => $model,
            // ]);
        }
    }

    /**
     * Updates an existing TcCourseGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {

      if (Yii::$app->request->post('hasEditable')) {
        $model = $this->findModel(Yii::$app->request->post('editableKey'));
        $post = [];
        $posted = current($_POST['TcCourseGroup']);
        $post['TcCourseGroup'] = $posted;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($model->load($post) && $model->save()) {
            return ['output'=>$model->name, 'message'=>''];
        } else {
            return ['output'=>'', 'message'=>''];
        }
      }

    }

    /**
     * Deletes an existing TcCourseGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash("success","Your last operation was successful.");
          return $this->redirect(Url::previous());
    }

    /**
     * Finds the TcCourseGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TcCourseGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TcCourseGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
