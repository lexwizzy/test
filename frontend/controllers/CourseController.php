<?php

namespace frontend\controllers;

use Yii;
use yii\web\Response;
use frontend\models\Course;
use frontend\models\CourseInstructors;
use frontend\models\CourseParts;
use frontend\models\CourseLessons;
use frontend\models\LessonContent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use frontend\models\User;
use frontend\models\Quiz;
use frontend\models\Question;
use frontend\models\Answers;
use frontend\models\CourseCoupons;
use frontend\models\CourseSettings;
use frontend\models\CourseCategory;
use frontend\models\Utility;
use frontend\models\CourseOutlineOrder;
use Foolz\SphinxQL\SphinxQL;
use Foolz\SphinxQL\Connection;
use yii\data\Pagination;
use frontend\models\CoursesIndex;
use yii\base\ErrorException;
use frontend\models\TcLessonContent;
use frontend\models\TcUploadedContent;

/**
 * CourseController implements the CRUD actions for Course model.
 */

class CourseController extends Controller
{
    public $defaultAction ="checkowner";

    public function behaviors()
    {

        return [
             'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                   [
                        'actions' => ['index','apply-coupon','checkowner','details','coursecategory','search','login','autosuggestjson','language'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['content-from-library','quiz-result','questions-display','question-modal','apply-coupon','under-the-hood','privacy','price-and-coupons','description','savegoals','getgoals','saveaudience','getaudience','saverequirements','getrequirements','target-and-requirements','addlogotovideo','language','outlinejson','login','search','autosuggestjson','lessoncomplete','finish-quiz','saveanswers','getsavedanswers','createcourse','basicinfo','savename','savereview','flagabuse','index','subscribe','learn','getsubcategory','coursecategory','coursebanner','details','promovideo','deletebanner','contents','preview','studentreport','publish','unpublish','submitforreview','settings','saveparttitle','editpart','deletepart','newunit','editunit','deleteunit','uploadnewunitvideo','uploadnewunitpresentation','downloadables','getcontents','externallinks','textcontent','deleteunitcontent','newquiz','deletequiz','editquiz','newquestion','deletequestion','updatequestion','getquestion','deletecoupon','delete','savediscussion','getdiscussions','savereply','getlastreply','saveannouncement','classroom','studentpreview','learning','saveusernote','sectionorder','sectionitemsorder','classroomnavigation','quizintro','quizquestion','savetimer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Course models.
     * @return mixed
     */
     function checkOwner($loginuserid,$ownerid){
        if($loginuserid != $ownerid){
            return $this->redirect(Url::toRoute("/tutors/dashboard"));
        }

    }
    public function actionLogin(){
      return $this->redirect(Url::toRoute("/site/login"));
    }
    public function actionCreatecourse(){

        $course= new Course(['scenario' => 'isguest']);
        if($course->load(Yii::$app->request->post())){

            $transaction = $course->getDb()->beginTransaction();
            $course->owner = Yii::$app->user->id;
            $course->skill_level=1;

            if($course->save()){
                $instructors=  new CourseInstructors();
                $instructors->course_id=$course->getPrimaryKey();
                $instructors->user_id=Yii::$app->user->id;
                print_r($instructors);
                if($instructors->save())
                    {
                        $transaction->commit();
                        return $this->redirect(Url::toRoute(["basicinfo",'slug'=>$course->slug]));
                    }
            }

            print_r($course->getErrors());
        }



    }
    public function actionDetails($slug){
        Url::remember();
        $processed_amount=0;
        $coupon="";
        if(!Course::find()->where(["slug"=>$slug])->exists()){
          return $this->redirect("/site");
        }
        $course=Course::find()->where(["slug"=>$slug])->one();

        $processed_amount =Course::getPrice($course->course_id);//
        $login =  new \frontend\models\LoginForm();
        $couponForm="<form  method='get' id='coupon-form'><input type='hidden' name='". Yii::$app->request->csrfParam."' value='".Yii::$app->request->csrfToken."' /><input type='text' name='coupon_code' placeholder='Enter coupon' class='form-control'/><button id='apply-coupon' type='submit' class='btn btn-success'>Apply</button></form>";
        Yii::$app->user->returnUrl=Url::current();
        //redirect user to learn page if already enrolled.
         if(!Yii::$app->user->isGuest && \frontend\models\Enrollment::find()->where(['courseid'=>$course->course_id,"userid"=>Yii::$app->user->id])->exists()){
           return $this->redirect(Url::toRoute(["learn","slug"=>$course->slug]));
         }
         //apply coupon code
         if(Yii::$app->request->get("coupon_code")){
           $code = Yii::$app->request->get("coupon_code");
           $courseid =$course->course_id;// Yii::$app->request->get("courseid");

           if(CourseCoupons::find()->where(["courseid"=>$courseid,"coupon_code"=>$code])->exists()){
             $coupon=CourseCoupons::find()->where(["courseid"=>$courseid,"coupon_code"=>$code])->one();
             if(strtotime("today") >=strtotime($coupon->expirey_date)){
                     Yii::$app->session->setFlash("error","The applied coupon has expired.");
                     //return $this->redirect(Url::previous());
               }
            else{
              $processed_amount =Course::getPrice($course->course_id,$coupon->discount);//
              $coupon=$code;
            }
           }
           else{
             Yii::$app->session->setFlash("error",'Sorry this coupon does not exist');
            // return $this->redirect(Url::previous());
           }
         }
        // if(Course::getPrice($course->course_id)=="Free"){
        //     $buttontext="Start Learning Now";
        //     $button= '<a href="'.Url::toRoute(["/course/subscribe","slug"=>$course->slug]).'" class="mc-btn btn-style-1">'.$buttontext.'</a>';
        //
        // }
        // else{
        //     $buttontext = "Take This Courses";
        //     $button= '<a href="'.Url::toRoute(["/payment/checkout","id"=>$course->course_id]).'" class="mc-btn btn-style-1">'.$buttontext.'</a>';
        //
        // }
        /*if(!Yii::$app->request->cookies->has("tsuser")){
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name'=>"tsuser",'value'=>Yii::$app->security->generateRandomString(),'expire'=>3600 * 1000 * 24 * 365]));
        }*/
        //if it is not an instructor viewing his own course--preview
        if(Yii::$app->request->get("role")!=2 && Course::isViewed($course->course_id,Yii::$app->request->cookies->getValue("tsuser"))){
             $view = new \frontend\models\CourseViews;
                $view->courseid= $course->course_id;
                $cookie=Yii::$app->request->cookies;
                $view->cookie=$cookie->getValue("tsuser");

                $view->save();
        }

        return $this->render("coursedetails",array('coupon'=>$coupon,'processed_amount'=>$processed_amount,'couponForm'=>$couponForm,"course"=>$course,'login'=>$login,"role"=>1));


    }
    public function actionApplyCoupon(){
      $code = Yii::$app->request->post("coupon_code");
      $courseid = Yii::$app->request->post("courseid");

      if(CourseCoupons::find()->where(["courseid"=>$courseid,"coupon_code"=>$code])->exists()){
        $coupon=CourseCoupons::find()->where(["courseid"=>$courseid,"coupon_code"=>$code])->one();
        if(strtotime("today") >=strtotime($coupon->expirey_date)){
                Yii::$app->session->setFlash("error","The applied coupon has expired.");
                return $this->redirect(Url::previous());
          }
      }
      else{
        Yii::$app->session->setFlash("error",'Sorry this coupon does not exist');
        return $this->redirect(Url::previous());
      }
    }
    public function actionStudentpreview($slug){
        $role=0;
        $coupon="";
        $course=Course::find()->where(["slug"=>$slug])->one();
        $couponForm="<form  method='get' id='coupon-form'><input type='hidden' name='". Yii::$app->request->csrfParam."' value='".Yii::$app->request->csrfToken."' /><input type='text' name='coupon_code' placeholder='Enter coupon' class='form-control'/><button id='apply-coupon' type='submit' class='btn btn-success'>Apply</button></form>";
        $processed_amount =Course::getPrice($course->course_id);//
        $login =  new \frontend\models\LoginForm();
        Yii::$app->user->returnUrl=Url::current();
        if(Course::getPrice($course->course_id)=="Free"){
            $buttontext="Start Learning Now";
            if($role==2)// check if an instructor is previewing his course.
              $button= '<button class="mc-btn btn-style-1">'.$buttontext.'</button>';
            else{
                  $button ='<a href="'.Url::toRoute(["/course/subscribe","slug"=>$course->slug]).'" class="mc-btn btn-style-1">'.$buttontext.'</a>';
                }
        }
        else{
            $buttontext = "Take This Courses";
          //  $button= '<button class="mc-btn btn-style-1">'.$buttontext.'</button>';
            if($role==2)// check if an instructor is previewing his course.
              $button= '<button class="mc-btn btn-style-1">'.$buttontext.'</button>';
            else{

                $button= '<a href="'.Url::toRoute(["/payment/checkout","slug"=>$course->slug]).'" class="mc-btn btn-style-1">'.$buttontext.'</a>';
              }
        }
        /*if(!Yii::$app->request->cookies->has("tsuser")){
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name'=>"tsuser",'value'=>Yii::$app->security->generateRandomString(),'expire'=>3600 * 1000 * 24 * 365]));
        }*/


        return $this->render("coursedetails",array("couponForm"=>$couponForm,"coupon"=>$coupon,'processed_amount'=>$processed_amount,"course"=>$course,'login'=>$login,"buttontext"=>$buttontext,'role'=>2,"button"=>$button));


    }

    public function actionTargetAndRequirements($courseid){
      $this->layout="course_edit_layout";
      $course = Course::findOne($courseid);
      $this->view->params["course"]=$course;
      return $this->render("target-and-requirements",["course"=>$course]);
    }
    public function actionSavegoals(){
      $goal= Yii::$app->request->get("goal");
       $course = Course::findOne(Yii::$app->request->get("courseid"));
       $course->course_goal= $goal;

       if($course->save())
          echo 1;
        else
          echo 2;
    }
    public function actionGetgoals(){
      Yii::$app->response->format=Response::FORMAT_JSON;
      $course=Course::findOne(Yii::$app->request->get("courseid"));
        if(!empty($course->course_goal))
          return [$course->course_goal];
        else
          return '{"goals":[]}';
    }

    public function actionSaveaudience(){
      $audience= Yii::$app->request->get("audience");
       $course = Course::findOne(Yii::$app->request->get("courseid"));
       $course->target_audience= $audience;

       if($course->save())
          echo 1;
        else
          echo 2;
    }
    public function actionGetaudience(){
      Yii::$app->response->format=Response::FORMAT_JSON;
      $course=Course::findOne(Yii::$app->request->get("courseid"));
        if(!empty($course->target_audience))
          return [$course->target_audience];
        else
          return '{"audience":[]}';
    }

    public function actionSaverequirements(){
      $requirement= Yii::$app->request->get("requirement");
       $course = Course::findOne(Yii::$app->request->get("courseid"));
       $course->course_tools= $requirement;

       if($course->save())
          echo 1;
        else
          echo 2;
    }
    public function actionGetrequirements(){
      Yii::$app->response->format=Response::FORMAT_JSON;
      $course=Course::findOne(Yii::$app->request->get("courseid"));
        if(!empty($course->course_tools))
          return [$course->course_tools];
        else
          return '{"requirement":[]}';
    }

    public function actionDescription(){
      $courseid=Yii::$app->request->get("courseid");
      $this->layout="course_edit_layout";
      $course = Course::findOne($courseid);


      if($course->load(Yii::$app->request->post())){

         if($course->save()){
           Yii::$app->session->setFlash("success","Your last operation was successfull");
         }
      }
      $this->view->params["course"]=$course;
      return $this->render("description",["course"=>$course]);

    }
    public function actionBasicinfo($slug){
      $this->layout="course_edit_layout";

            $course=Course::find()->where(["slug"=>$slug])->one();
            $course->scenario="basicinfo";
            $this->view->params["course"]=$course;
            $course_id=$course->course_id;
            $this->checkOwner(Yii::$app->user->id,$course->owner);

        if($course->load(Yii::$app->request->post())){
            //if($course->language!="")
             //   $course->language = implode(",",$course->language);
            // if(!User::isPremium(Yii::$app->user->id) && $course->course_price=="Paid"){
            //        Yii::$app->session->setFlash("error","Apply for premium tutor to start charging for your courses.");
            //        return $this->redirect(array('basicinfo','slug'=>$course->slug));
            // }
            if($course->validate() && $course->save()){
                 Yii::$app->session->setFlash("success","Your last operation was successfull");
                //return $this->redirect(array('contents','slug'=>$course->slug));
            }


        }

        return $this->render("basic-info",array("course"=>$course));
    }

    public function actionPriceAndCoupons($courseid){
      Url::remember();
      $this->layout ="course_edit_layout";
      $course=Course::findOne($courseid);
      $course->scenario="price-and-coupon";
      $coupon= new CourseCoupons;
      $this->view->params["course"]=$course;
      if($course->load(Yii::$app->request->post())){
          if($course->course_price=="free")
            $course->amount="";
        if(!User::isPremium(Yii::$app->user->id) && $course->course_price=="paid"){
            Yii::$app->session->setFlash("premium-tutor-error","Apply for premium tutor to start charging for your courses.");
            return $this->render("price-and-coupon",["course"=>$course,"coupon"=>$coupon]);
          }    //  return $this->redirect(array('basicinfo','slug'=>$course->slug));
          if($course->validate() && $course->save()){
                   Yii::$app->session->setFlash("success","Your last operation was successful");
          }
          else{
                Yii::$app->session->setFlash("error",implode(" ",$course->getErrors()["amount"]));
           }

      }
      if($coupon->load(Yii::$app->request->post())){
        if(strtotime("today") >=strtotime($coupon->expirey_date)){
                Yii::$app->session->setFlash("error","Your coupon expiration must be greater than today.");
              return $this->render("price-and-coupon",["course"=>$course,"coupon"=>$coupon]);
           }
          $coupon->courseid=$courseid;
          if(!empty($course->amount)){
              if($coupon->save()){
                  Yii::$app->session->setFlash("success","Operation was done successfully");
                  //return $this->redirect(array("price-and-coupons",'slug'=>$course->slug));
              }


          }
          else{
                  Yii::$app->session->setFlash("error","You cannot create coupon for a free course");
                  ///return $this->redirect(array("settings",'slug'=>$course->slug));
          }

      }
      return $this->render("price-and-coupon",["course"=>$course,"coupon"=>$coupon]);
    }

    public function actionUnderTheHood($courseid){
      $this->layout ="course_edit_layout";
      $course=Course::findOne($courseid);
      $this->view->params["course"]=$course;
      $setting= CourseSettings::find()->where(["courseid"=>$courseid])->one();
      if($setting == null){
        $setting= new CourseSettings;
        $setting->courseid=$courseid;
      }
      if($setting->load(Yii::$app->request->post())){
        if($setting->save()){
          Yii::$app->session->setFlash("success","Your last operation was successful");
        }
        else {

          Yii::$app->session->setFlash("error","We were unable to complete your last request.");
        }
      }
      return $this->render("under-the-hood",["course"=>$course,"setting"=>$setting]);
    }
     public function actionContents($slug){
            Url::remember();
            Yii::$app->user->returnUrl=Url::current();
            $this->layout="course_edit_layout";
            $course=Course::find()->where(["slug"=>$slug])->one();
            $this->view->params["course"]=$course;
            $this->checkOwner(Yii::$app->user->id,$course->owner);
          //  $parts_all= CourseParts::find()->where(['course_id'=>$course_id])->all();
            $part = new CourseParts;
            $lesson = new CourseLessons;
            $external= new LessonContent;
            $external->scenario="external";
            $textcontent= new LessonContent();
            $textcontent->scenario="text";

            $quiz = new Quiz();
            $question = new Question;

            return $this->render('contents',array("course"=>$course,'part'=>$part,'lesson'=>$lesson,'external'=>$external,"quiz"=>$quiz,'question'=>$question,"text"=>$textcontent));
    }
    public function actionOutlinejson($slug){
        $course=Course::find()->where(["slug"=>$slug])->one();
        echo CourseParts::getOutline($course->course_id);
        Yii::$app->end();

    }
    //save section title
    public function actionSaveparttitle(){
        $part= new CourseParts();
       if($part->load(Yii::$app->request->post())){
           $transaction=$part->getDb()->beginTransaction();
           $lastcounter=CourseParts::find()->where(['course_id'=>$part->course_id])->orderBy("counter desc")->limit(1)->one();
           if($part->save() && $part->validate()){

               if($lastcounter==null){
                   $part->updateCounters(['counter' => 1]);
                   $transaction->commit();
               }
               else{
                      $part->updateCounters(['counter' => $lastcounter->counter+1]);
                      $transaction->commit();
                }


               return $this->redirect(Url::previous());
            }
       }
    }
    public function actionEditpart(){
        $part = CourseParts::findOne(['part_id'=>Yii::$app->request->post('sectionid')]);
        if(Yii::$app->request->post('sectiontitle') !="")
           $part->part_name=Yii::$app->request->post('sectiontitle');
         else
             $part->achievement=Yii::$app->request->post('sectionacheive');

            $part->save();
    }
    public function actionDeletepart(){
      $partid = Yii::$app->request->post("partid");
        if(Yii::$app->request->isPost){
            $model=CourseParts::findOne($partid);
            //$transaction=$model->getDb()->beginTransaction();
            $model->delete();
           // CourseParts::updateAllCounters(["counter"=>-1],['>','part_id',$partid]);
           // CourseParts::updateAllCounters(["counter"=>-1],['>','part_id',$partid]);

            //if(CourseOutlineOrder::updateOutline("section",$partid))
              //  $transaction->commit();

            return $this->redirect(Url::previous());

        }

    }
    //Units
    public function actionNewunit($part_id){
        $lesson_array=array();
        $model= new CourseLessons();

        if($model->load(Yii::$app->request->post())){
            $transaction=$model->getDb()->beginTransaction();
            $model->part_id=$part_id;

            $lc=CourseLessons::find()->where(['part_id'=>$part_id])->orderBy("lesson_id desc")->limit(1)->one();


            if($model->validate() && $model->save()){

                if($lc  == null)
                   $model->updateCounters(['counter'=>1]);
                else
                    $model->updateCounters(['counter'=>$lc->counter+1]);

                $lastOutlineCounter = CourseOutlineOrder::find()->where(['and',['section'=>$part_id],["courseid"=>$model->courseid]])->orderBy("id desc")->limit(1)->one();
                $outline = new CourseOutlineOrder;
                $outline->itemid=$model->getPrimaryKey();
                $outline->type="lesson";
                $outline->section=$part_id;
                $outline->courseid=$model->courseid;
                if($outline->save()){
                   if($lastOutlineCounter  == null){
                        $outline->updateCounters(['counter'=>1]);
                     }
                     else {
                         $outline->updateCounters(['counter'=>$lastOutlineCounter->counter+1]);
                                                 // $transaction->commit();
                    }
                    $model->outlineno=$outline->getPrimarykey();
                       if($model->save()){
                           $transaction->commit();
                           $lesson_array["content"][]=CourseLessons::getLesson($model,2,$model->outlineno);
                           $lesson_array["type"]="lesson";
                           echo \yii\helpers\Json::encode($lesson_array);//2 id a junk value
                       }
                       else
                          echo 0;

               }


            }
            else{

                echo  "<div class='alert alert-danger'> An error occurred while performing operation. Please try again.</div>";
            }

        }


    }
    public function actionEditunit(){
        $unit_cont="";
        $unit = CourseLessons::find()->where(['lesson_id'=>Yii::$app->request->post("lesson_id")])->one();

        if($unit->load(Yii::$app->request->post()) && $unit->save()){
            $allUnit=CourseLessons::find()->where(['part_id'=>$unit->part_id])->orderBy("counter asc")->all();

               echo \yii\helpers\Json::encode(CourseOutlineOrder::getSectionOutline($unit->part_id,$unit->courseid));
               //
               // endforeach;
                  //  echo    echo \yii\helpers\Json::encode(CourseLessons::getLesson($unit,2,$unit->outlineno));//2 id a junk value
        }

    }
    public function actionDeleteunit(){
        $unit_cont="";
        $lid=Yii::$app->request->post('lid');
        if(Yii::$app->request->isPost){
            $delete = CourseLessons::findOne([$lid]);
            $transaction = $delete->getDb()->beginTransaction();
            $lpart =$delete->part_id;
            $courseid= $delete->courseid;
            if($delete->delete()){
                 $allUnit=CourseLessons::find()->where(['part_id'=>$lpart])->orderBy("counter asc")->all();
                 CourseLessons::updateAllCounters(["counter"=>-1],['and',['>','lesson_id',$lid],['=','part_id',$lpart]]);
                if(CourseOutlineOrder::updateOutline($delete->outlineno)){
                    $transaction->commit();
                    echo 1;
                   // foreach($allUnit as $row):
                        //$unit_cont=CourseOutlineOrder::getSectionOutline($lpart,$courseid);

                   // endforeach;
                        //echo \yii\helpers\Json::encode($unit_cont);
                }


            }
            else
                 echo  "<div class='alert alert-danger'> An error occurred while performing operation. Please try again.</div>";
           // return $this->redirect($redirect);

        }

    }
    //End Units

    //Start Unit contents( videos, ppt, pdf etc)

    //add content to lesson from library
    public function actionContentFromLibrary($lessonid,$contentid){
      $ismain="not-main";
      if(Yii::$app->request->isPost){
          if(!empty($contentid) && !empty($lessonid)){
            $model=new TcLessonContent();
            $model->contentid=$contentid;
            $l=  CourseLessons::findOne($lessonid);
            $model->courseid=$l->courseid;
            $model->partid=$l->part_id;
            $model->lessonid=$lessonid;
            if(TcLessonContent::find()->where(['and',['=','lessonid',$lessonid],['=','main','Yes']])->exists())
            {
                $model->main="No";
            }
            else{
                $model->main="Yes";
                $ismain="main";
            }

            if($model->save()){
              $result["content"]=TcLessonContent::getContents($lessonid);
              $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
              echo \yii\helpers\Json::encode($result);
            }
            else
              echo 2;
          }

      }

    }
    public function actionUploadnewunitvideo(){

      $userid=Yii::$app->user->id;
       $ismain="not-main";
       if (Yii::$app->request->isPost) {
         $lessonid=Yii::$app->request->post("lesson");
         $model = new TcUploadedContent();
         $transaction= $model->getDb()->beginTransaction();
         $model->scenario="singleupload";
         $model->single_upload = UploadedFile::getInstance($model, 'single_upload');

         $res=$model->singleUpload($userid);
         if(is_array($res) && array_key_exists("error",json_decode($res,true))){
               print_r($res);
         }
         else{
             $cont=new TcLessonContent();
             $cont->contentid=$model->getPrimaryKey();
             $l=  CourseLessons::findOne($lessonid);
             $cont->courseid=$l->courseid;
             $cont->partid=$l->part_id;
             $cont->lessonid=$lessonid;
             if(TcLessonContent::find()->where(['and',['=','lessonid',$lessonid],['=','main','Yes']])->exists())
             {
                 $cont->main="No";
             }
             else{
                 $cont->main="Yes";
                 $ismain="main";
             }

             if($cont->save()){
               $transaction->commit();
               $result["content"]=TcLessonContent::getContents($lessonid);
               $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
               echo \yii\helpers\Json::encode($result);
             }
             else
               echo 2;
         }

       }
    }
    public function actionGetcontents(){
      $result=array();
      $lessonid=Yii::$app->request->get("lessonid");
      $result["content"]=TcLessonContent::getContents($lessonid);
      $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
      echo \yii\helpers\Json::encode($result);
    }
    public function actionUploadnewunitpresentation(){
      $userid=Yii::$app->user->id;
       $ismain="not-main";
       if (Yii::$app->request->isPost) {
         $lessonid=Yii::$app->request->post("lesson");
         $model = new TcUploadedContent();


         $transaction= $model->getDb()->beginTransaction();
         $model->scenario="singleupload";

         $model->single_upload = UploadedFile::getInstance($model, 'single_upload');


         $res=$model->singleUpload($userid);
         
         if(is_array($res) && array_key_exists("error",json_decode($res,true))){
               print_r($res);
         }
         else{

             $cont=new TcLessonContent();
             $cont->contentid=$model->getPrimaryKey();
             $l=  CourseLessons::findOne($lessonid);
             $cont->courseid=$l->courseid;
             $cont->partid=$l->part_id;
             $cont->lessonid=$lessonid;
             if(TcLessonContent::find()->where(['and',['=','lessonid',$lessonid],['=','main','Yes']])->exists())
             {
                 $cont->main="No";
             }
             else{
                 $cont->main="Yes";
                 $ismain="main";
             }

             if($cont->save()){
               $transaction->commit();
               $result["content"]=TcLessonContent::getContents($lessonid);
               $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
               echo \yii\helpers\Json::encode($result);
             }
             else
               echo 2;
         }

       }
    }

    public function actionDownloadables(){
      $userid=Yii::$app->user->id;
        $ismain="not-main";
        if (Yii::$app->request->isPost) {
          $lessonid=Yii::$app->request->post("lesson");
          $model = new TcUploadedContent();
          $transaction= $model->getDb()->beginTransaction();
          $model->scenario="singleupload";
          $model->single_upload = UploadedFile::getInstance($model, 'single_upload');

          $res=$model->singleUpload($userid);
          if(is_array($res) && array_key_exists("error",json_decode($res,true))){
                print_r($res);
          }
          else{
              $cont=new TcLessonContent();
              $cont->contentid=$model->getPrimaryKey();
              $l=  CourseLessons::findOne($lessonid);
              $cont->courseid=$l->courseid;
              $cont->partid=$l->part_id;
              $cont->lessonid=$lessonid;
              $cont->main="No";

              if($cont->save()){
                $transaction->commit();
                $result["content"]=TcLessonContent::getContents($lessonid);
                $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);//get dropdown options
                echo \yii\helpers\Json::encode($result);
              }
              else
                echo 2;
          }

        }
    }
     public function actionTextcontent(){

        $model= new LessonContent;
        $model->lesson_no=Yii::$app->request->post('lesson_no');
        $model->main="Yes";

        $model->uploaded_by=Yii::$app->user->id;

        if($model->load(Yii::$app->request->post())){

            $model->content_format="text";
            $model->content_mime="Yes";

            if($model->save()){
                echo json_encode(array("status"=>1,"lessonid"=>$model->lesson_no));
            }
        }
    }
    public function actionExternallinks(){

        $model= new LessonContent;
        $model->lesson_no=Yii::$app->request->post('lesson_no');
        $model->main="Yes";
        $model->uploaded_by=Yii::$app->user->id;
        if($model->load(Yii::$app->request->post())){

            $model->content_format="link";
            $model->content_mime="link";
            if($model->save()){
               echo json_encode(array("status"=>1,"lessonid"=>$model->lesson_no));
            }
        }
    }

    public function actionDeleteunitcontent(){
            if(Yii::$app->request->post()){

              $delete = TcLessonContent::findOne(['id'=>Yii::$app->request->post('contentno')]);
              $lessonid=$delete->lessonid;
              $delete->delete();
              //$transaction->commit();
              $result["dropdown"]=\frontend\models\CourseLessons::mainContentExist($lessonid);
              echo \yii\helpers\Json::encode($result);
           }
        //else

    }
    //End Unit contents

    //Begin Quuiz

    public function actionNewquiz(){
        $model= new Quiz;
        if($model->load(Yii::$app->request->post())){
            $lc=Quiz::find()->where(['partid'=>$model->partid])->orderBy("q_id desc")->limit(1)->one();
             $transaction =$model->getDb()->beginTransaction();
            if($model->save()){
                if($lc == null)
                    $model->updateCounters(['counter'=>1]);
                else
                    $model->updateCounters(['counter'=>$lc->counter+1]);

                 $lastOutlineCounter = CourseOutlineOrder::find()->where(["courseid"=>$model->courseid])->orderBy("id desc")->limit(1)->one();
                $outline = new CourseOutlineOrder;
                $outline->itemid=$model->getPrimaryKey();
                $outline->type="quiz";
                $outline->section=$model->partid;
                $outline->courseid=$model->courseid;
                if($outline->save()){
                   if($lastOutlineCounter  == null){
                        $outline->updateCounters(['counter'=>1]);
                   }
                    else {
                         $outline->updateCounters(['counter'=>$lastOutlineCounter->counter+1]);
                                                 // $transaction->commit();
                    }
                    $model->outlineno =$outline->getPrimaryKey();
                       if($model->save()){
                           $transaction->commit();
                           $quiz_array["content"][]=Quiz::getQuiz($model,2,$model->outlineno);// 2 is just a junk value
                           $quiz_array["type"]="quiz";
                           echo \yii\helpers\Json::encode($quiz_array);
                       }
                }

            }
            else
                echo "<div class='alert alert-danger'> An error occurred while  saving quiz. Please try again.</div>";
        }
    }

    public function actionEditquiz(){
      // print_r(Yii::$app->request->post());
      // Yii::$app->end();
        if(Yii::$app->request->isPost){
            $quizno= Yii::$app->request->post("quizno");
            $quizname= Yii::$app->request->post("quizname");
            $quizdesc= Yii::$app->request->post('quizdesc');
            $islimited =Yii::$app->request->post("islimited");
            $minutes =Yii::$app->request->post("minutes");
            $quizmodel= Quiz::find()->where(['q_id'=>$quizno])->one();
            $quizmodel->q_name=$quizname;
            $quizmodel->q_description=$quizdesc;
            $quizmodel->islimitedbytime=$islimited;
            $quizmodel->timelimit=$minutes;

            if($quizmodel->save()){
                echo 1;//Yii::$app->request->post('quizdesc');//json_encode(print_r($quizmodel));
            }
            else
                echo print_r($quizmodel->getErrors());
         }

    }
    public function actionDeletequiz(){
        if(Yii::$app->request->post()){
            $partid= Yii::$app->request->post("partid");
            $quizno=Yii::$app->request->post('quizno');
            $delete =Quiz::findOne(['q_id'=>$quizno]);
            $transaction = $delete->getDb()->beginTransaction();
            $quiz_cont='';
            $courseid=$delete->courseid;
            $outlineno= $delete->outlineno;
            if($delete->delete()){

                Quiz::updateAllCounters(["counter"=>-1],['and',['>','q_id',$quizno],['=','partid',$partid]]);
                $allQuiz=Quiz::find()->where(['partid'=>$partid])->orderBy("counter asc")->all();

                if(CourseOutlineOrder::updateOutline($outlineno)){
                    $transaction->commit();
                    echo 1;
                }

            }
            else{
                echo 2;
            }

        }

    }

    public function actionQuestionModal(){
      $question = new Question;
      return $this->renderAjax("_addquestions",["question"=>$question]);
    }
    public function actionNewquestion(){
    $question = new Question;
      //  print_r(Yii::$app->request->post());
      //  Yii::$app->end();
        if($question->load(Yii::$app->request->post())){
            $question->quiz_id=Yii::$app->request->post("quizid");
            $lc=Question::find()->where(['quiz_id'=>$question->quiz_id])->orderBy("question_id desc")->limit(1)->one();

            if($question->save()){
                if($lc == null)
                    $question->updateCounters(['counter'=>1]);
                else
                    $question->updateCounters(['counter'=>$lc->counter+1]);
                switch ($question->type){
                    case 1:    $answer = Yii::$app->request->post("answer-field-multiple");
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-multiple")[$i];
                                for($j=0; $j < count(Yii::$app->request->post("checkedanswers")); $j++){
                                    if(Yii::$app->request->post("checkedanswers")[$j] == $i){
                                        $ans->iscorrect= "true";
                                    }
                                }
                                $ans->save();
                             }
                             break;
                    case 2:  $answer = Yii::$app->request->post("answer-field-radio");
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-single")[$i];
                                if(Yii::$app->request->post("radioselected") == $i){
                                        $ans->iscorrect= "true";
                                }

                                $ans->save();
                             }
                             break;
                    case 3:  $answer = array('true','false');

                               for($i=0;$i<count($answer); $i++){

                                    $ans = new Answers;
                                    $ans->questionid = $question->getPrimaryKey();
                                    $ans->answer = $answer[$i];
                                    $ans->reason =Yii::$app->request->post("reason-truefalse")[$i];
                                    if(Yii::$app->request->post("optradio") == $answer[$i]){
                                            $ans->iscorrect= "true";
                                    }

                                    $ans->save();
                             }
                             break;
                }
                echo \yii\helpers\Json::encode(Question::getQuestions($question->quiz_id));
            }
            else
                echo "error";

        }
    }

    public function actionDeletequestion(){

        $question_cont="";
        $questionid=Yii::$app->request->post("questionid");
        $quizid=Yii::$app->request->post("quizid");

        if(Yii::$app->request->isPost){

            $delete = Question::findOne(['question_id'=>$questionid]);

            if($delete->delete()){

                Question::updateAllCounters(["counter"=>-1],['and',['>','question_id',$questionid],['=','quiz_id',$quizid]]);
                $allQuestion=Question::find()->where(['quiz_id'=>$quizid])->orderBy("counter asc")->all();
                // foreach($allQuestion as $row):
                //     $question_cont.= Question::getQuestions($row->quiz_id);
                //
                // endforeach;
                //     echo $question_cont;
                echo 1;
            }
            else
                echo "error";
        }
    }
    // public function actionGetquestion($id){
    //     $question = \yii\helpers\ArrayHelper::toArray(Question::findOne(['question_id'=>$id]));
    //     $answer = \yii\helpers\ArrayHelper::toArray(Answers::find()->where(['questionid'=>$id])->all());
    //     $merged = \yii\helpers\ArrayHelper::merge($question,$answer);
    //     echo \yii\helpers\Json::encode($merged);
    //
    // }
    public function actionUpdatequestion(){
        $question = Question::find()->where(['question_id'=>Yii::$app->request->post("question-id")])->one();

        if($question->load(Yii::$app->request->post())){

            if($question->save()){

                switch ($question->type){
                    case 1:    $answer = Yii::$app->request->post("answer-field-multiple");
                               Answers::deleteAll(['questionid'=>$question->question_id]);
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-multiple")[$i];
                                for($j=0; $j < count(Yii::$app->request->post("checkedanswers")); $j++){
                                    if(Yii::$app->request->post("checkedanswers")[$j] == $i){
                                        $ans->iscorrect= "true";
                                    }
                                }
                                $ans->save();
                             }
                             break;
                    case 2:  $answer = Yii::$app->request->post("answer-field-radio");
                              Answers::deleteAll(['questionid'=>$question->question_id]);
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-single")[$i];
                                if(Yii::$app->request->post("radioselected") == $i){
                                        $ans->iscorrect= "true";
                                }

                                $ans->save();
                             }
                             break;
                    case 3:  $answer = array('true','false');

                                Answers::deleteAll(['questionid'=>$question->question_id]);

                               for($i=0;$i<count($answer); $i++){

                                    $ans = new Answers;
                                    $ans->questionid = $question->getPrimaryKey();
                                    $ans->answer = $answer[$i];
                                    $ans->reason =Yii::$app->request->post("reason-truefalse")[$i];
                                    if(Yii::$app->request->post("optradio") == $answer[$i]){
                                            $ans->iscorrect= "true";
                                    }

                                    $ans->save();
                             }
                             break;
                }
                  echo \yii\helpers\Json::encode(Question::getQuestions($question->quiz_id));
            }
            else
                echo "error";
        }
    }

    //End Quiz





    public function actionDeletebanner(){
        if(Yii::$app->request->post()){
            $model= Course::find()->where(['course_id'=>$_POST['courseid']])->one();

            $path = Yii::getAlias('@aws_cc').'/coursebanners/'.$model->course_banner;
            $transaction = $model->getDb()->beginTransaction();
            $oldbanner=$model->course_banner;
            $model->course_banner="";
                   if($model->save()){
                       if(file_exists($oldbanner) && !empty($oldbanner))
                           unlink($oldbanner);
                       $transaction->commit();
                   }
                   else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                   }
            $key=122;
            // $p1 = "<img src='http://localhost/teachsity/frontend/assets/coursebanners/".$key."' class='file-preview-image'>";
             echo json_encode([
               // 'initialPreview' => [$p1],
                'append' => true
             ]);
        }
    }
    public function actionCoursebanner(){
        if(Yii::$app->request->isGet){
           $this->layout="course_edit_layout";

             $courseid=Yii::$app->request->get("courseid");
             $course = Course::findOne($courseid);
             $bannerpath=Course::getCourseBanner($course->course_banner);
             $course->scenario="basicinfo";
             $this->view->params["course"]=$course;
             return $this->render("course-banner",["course"=>$course,"bannerpath"=>$bannerpath]);
        }
        $userid=Yii::$app->user->id;
        if (Yii::$app->request->isPost) {
          $course=Course::find()->where(['course_id'=>$_POST['courseid']])->one();
          $model = new TcUploadedContent();
          $transaction= $model->getDb()->beginTransaction();
          $model->scenario="singleupload";
          $model->single_upload = UploadedFile::getInstance($model, 'single_upload');

          $res=$model->singleUpload($userid);
          if(is_array($res) && array_key_exists("error",json_decode($res,true))){
                print_r($res);
          }
          else{
              $course->course_banner=$model->getPrimaryKey();
              if($course->save()){
                $transaction->commit();
                return $this->redirect(["course-banner","courseid"=>$course->course_id]);
              }
              else
                print_r($course->getErrors());
          }

        }
    }
    public function actionPromovideo(){
      if(Yii::$app->request->isGet){
         $this->layout="course_edit_layout";
         $videopath="";
           $courseid=Yii::$app->request->get("courseid");
           $course = Course::findOne($courseid);
           $course->scenario="basicinfo";
            $videopath=$videopath=Course::getCoursePromoVideo($course);

           $this->view->params["course"]=$course;
           return $this->render("intro-video",["course"=>$course,"videopath"=>$videopath]);
      }
      $userid=Yii::$app->user->id;
      if (Yii::$app->request->isPost) {
        $course=Course::find()->where(['course_id'=>$_POST['courseid']])->one();
        $model = new TcUploadedContent();
        $transaction= $model->getDb()->beginTransaction();
        $model->scenario="singleupload";
        $model->single_upload = UploadedFile::getInstance($model, 'single_upload');

        $res=$model->singleUpload($userid);
        if(is_array($res) && array_key_exists("error",json_decode($res,true))){
              print_r($res);
        }
        else{
            $course->promo_video=$model->getPrimaryKey();
            if($course->save()){
              $transaction->commit();
              return $this->redirect(["intro-video","courseid"=>$course->course_id]);
            }
            else
              print_r($course->getErrors());
        }

      }
    }
    public function actionSavename($id,$newtitle){
        $course=Course::find()->where(["course_id"=>$id])->one();
        $course->course_title=$newtitle;
       /// $course->scenario="edit_title";
        if($course->save())
            return $this->redirect(array("basicinfo",'slug'=>$course->slug));
        else{
            print_r ($course->getErrors());
        }



    }
    public function actionSettings($slug){
      $this->layout= "course_edit_layout";
         $course= Course::find()->where(["slug"=>$slug])->one();
         $this->checkOwner(Yii::$app->user->id,$course->owner);
         $this->view->params["course"]=$course;
         $courseid = $course->course_id;
         $setting= CourseSettings::find()->where(["courseid"=>$courseid])->one();
         if($setting == null)
             $setting= new CourseSettings;

        //saving form
       // print_r(Yii::$app->request->post());

        if($setting->load(Yii::$app->request->post())){
            $setting->courseid=$courseid;
            if($setting->save()){
                Yii::$app->session->setFlash("success","Course settings was updated successfully");
                return $this->redirect(array("settings",'slug'=>$course->slug));
            }
        }
        // if($coupon->load(Yii::$app->request->post())){
        //     $coupon->courseid=$courseid;
        //     if(!empty($course->amount)){
        //         if(strtotime("today") >=strtotime($coupon->expirey_date)){
        //              Yii::$app->session->setFlash("error","Your coupon expiration must be greater than today.");
        //              return $this->redirect(array("settings",'slug'=>$course->slug));
        //         }
        //         if($coupon->save()){
        //             Yii::$app->session->setFlash("success","Operation was done successfully");
        //             return $this->redirect(array("settings",'slug'=>$course->slug));
        //         }
        //     }
        //     else{
        //             Yii::$app->session->setFlash("error","You cannot create coupon for a free course");
        //             return $this->redirect(array("settings",'slug'=>$course->slug));
        //     }
        //
        // }
         return $this->render("settings",array("course"=>$course,"setting"=>$setting));
    }
    public function actionPrivacy($slug){
          Url::remember();
          $this->layout= "course_edit_layout";
         $course= Course::find()->where(["slug"=>$slug])->one();
         $this->checkOwner(Yii::$app->user->id,$course->owner);
         $this->view->params["course"]=$course;
         $courseid = $course->course_id;
         $searchModel = new \frontend\models\TcGroupSearch();
         $group=new \frontend\models\TcCourseGroup;
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


         $setting= CourseSettings::find()->where(["courseid"=>$courseid])->one();
         if($setting == null)
             $setting= new CourseSettings;

         if($setting->load(Yii::$app->request->post())){
           $setting->courseid=$courseid;
           if($setting->save()){
               Yii::$app->session->setFlash("success","Your last operation was successful");
               return $this->redirect(array("privacy",'slug'=>$course->slug));
           }
         }

         return $this->render("privacy",["settings"=>$setting,'searchModel' => $searchModel,
         'dataProvider' => $dataProvider,"group"=>$group]);
    }

    public function actionDeletecoupon($courseid,$redirect){
        if(Yii::$app->request->post()){
            if(CourseCoupons::findOne(['id'=>$courseid])->delete()){
                $this->redirect($redirect);
                Yii::$app->session->setFlash("success","Operation was done successfully");
            }

        }
    }
    public function actionPreview($slug){
        $course= Course::find()->where(["slug"=>$slug])->one();
        $this->checkOwner(Yii::$app->user->id,$course->owner);
        $course_ad=Course::getCourseAd($course->course_id);
        return $this->render("preview",array("course"=>$course,"coursead"=>$course_ad));
    }
    public function actionStudentreport($slug){
        $course= Course::find()->where(["slug"=>$slug])->one();
        $this->checkOwner(Yii::$app->user->id,$course->owner);
        return $this->render('studentreport',array("course"=>$course));
    }

    public function actionSubmitforreview(){
        if(Yii::$app->request->isPost){
             $model = Course::find()->where(['course_id'=>Yii::$app->request->post("courseid")])->one();
            $model->status=3;
            $model->save();
             return $this->render("success",array("courseid"=>$model->course_id));
        }

    }
    public function actionPublish($courseid){
        $course= Course::find()->where(["course_id"=>$courseid])->one();
        $this->checkOwner(Yii::$app->user->id,$course->owner);
        if($course->status==4){
            $course->status =1;
            $course->save();
            Yii::$app->session->setFlash("success","Your course has been published");
            return $this->redirect(array("under-the-hood",'courseid'=>$course->course_id));
        }
        else{
            Yii::$app->session->setFlash("error","You cannot publish this course. Please submit this course for review.");
            return $this->redirect(array("under-the-hood",'courseid'=>$course->course_id));
        }
    }
     public function actionUnpublish($courseid){
        $course= Course::find()->where(["course_id"=>$courseid])->one();
        $this->checkOwner(Yii::$app->user->id,$course->owner);
        if($course->status==1){
            $course->status =2;
            $course->save();
            Yii::$app->session->setFlash("success","Your course has been unpublished");
            return $this->redirect(array("under-the-hood",'courseid'=>$course->course_id));
        }
        else{
            Yii::$app->session->setFlash("error","An error occurred while performing the last operation.");
            return $this->redirect(array("under-the-hood",'courseid'=>$course->course_id));
        }
    }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Course::find(),
        ]);

        return $this->render('courses', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSubscribe($slug){

       $course= Course::findOne(['slug'=>$slug]);
       $courseid=$course->course_id;
       if(!\frontend\models\Enrollment::find()->where(['courseid'=>$courseid,"userid"=>Yii::$app->user->id])->exists()){
           $enroll = new \frontend\models\Enrollment;
           $enroll->courseid=$courseid;
           $enroll->userid=Yii::$app->user->id;

           if($enroll->save()){
               Yii::$app->session->setFlash("success","success");
               return $this->render("subscribe",array("course"=>$course));
           }
            else{
                Yii::$app->session->setFlash("error","An error occurred while trying to enroll you for this course.");
                return $this->render("subscribe",array("course"=>$course));
            }
       }
        else{
                //Yii::$app->session->setFlash("error","You have already enrolled for this course.");
                return $this->redirect(Url::toRoute(["learn","slug"=>$course->slug]));
            }


    }

    //Course Dashboard
   public function actionLearn($slug){
      $course = Course::find()->where(["slug"=>$slug])->one();
      $courseid=$course->course_id;
      $userid=Yii::$app->user->id;
      if(\frontend\models\Enrollment::checkIfEnrolled(Yii::$app->user->id,$courseid)){
        $flag =new \frontend\models\FlagAbuse;
        $discussion= new \frontend\models\Discussions;
         if(\frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->exists())
              $rating =  \frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->one();
        else
            $rating = new \frontend\models\CourseReviews;
        $progress = Course::getCurrentProgress($course->course_id,$userid);

        return $this->render("learn",array("progress"=>$progress,"course"=>$course,'rating'=>$rating,'flag'=>$flag,'discussion'=>$discussion));

      }
      else{
           Yii::$app->session->setFlash("error","You have not enrolled for this course");
           return $this->redirect(Url::toRoute(["/course/details","slug"=>$slug]));
      }

  }
    public function actionSavereview(){
        $userid= Yii::$app->request->get("CourseReviews")['userid'];
        $courseid= Yii::$app->request->get("CourseReviews")['courseid'];

        if(\frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->exists())
              $review = \frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->one();
        else
            $review = new \frontend\models\CourseReviews;

        if($review->load(Yii::$app->request->get())){

            if($review->save()){
                echo 1;
            }
            else
                echo 0;
        }
    }
    public function actionFlagabuse(){
        $userid= Yii::$app->request->get("FlagAbuse")['userid'];
        $courseid= Yii::$app->request->get("FlagAbuse")['courseid'];
         $flag =new \frontend\models\FlagAbuse;


         if($flag->load(Yii::$app->request->get())){
             if($flag->save())
                 echo 1;
             else
                 echo 0;
         }


    }
    public function actionSavediscussion(){
        $userid= Yii::$app->request->post("Discussions")['userid'];
        $courseid= Yii::$app->request->post("Discussions")['courseid'];

        $discussion = new \frontend\models\Discussions;

        if($discussion->load(Yii::$app->request->post())){

            if($discussion->save()){
                echo $discussion->getPrimaryKey();
            }
            else
                echo 0;
        }
    }
    public function actionGetdiscussions(){
        $d=  \frontend\models\Discussions::findOne([Yii::$app->request->get("discussionid")]);
        echo \frontend\models\Discussions::designDiscussion($d);
    }

    public function actionSavereply(){
        $reply =new  \frontend\models\DiscussionReplies;
        if($reply->load(Yii::$app->request->get())){
            if($reply->save())
                echo $reply->getPrimaryKey();
            else
                echo 0;
        }
    }
    public function actionGetlastreply(){
        $d=   \frontend\models\DiscussionReplies::findOne([Yii::$app->request->get("replyid")]);
        echo  \frontend\models\Discussions::designReply($d);
    }
    public function actionSaveannouncement(){
        $a = new \frontend\models\CourseAnnouncement;
        if($a->load(Yii::$app->request->post())){
            if($a->save()){
                Yii::$app->session->setFlash("success"," You have successfully made an announcement");
                return  $this->redirect(Yii::$app->request->post("returnUrl"));
            }
            else{
                Yii::$app->session->setFlash("error"," We were unable to complete your request at this time.");
                return  $this->redirect(Yii::$app->request->post("returnUrl"));
            }
        }
    }

    public function actionLearning($lessonid,$courseid,$isquiz=false){
          $type="video";
          $this->layout="learning";
          $course=Course::findOne([$courseid]);
          //if(CourseLessons::find()->where(["lesson_id"=>$lessonid])->exists())
            $lesson = CourseLessons::findOne([$lessonid]);
          // else{
          //     return $this->redirect(["course_completed","courseid"=>$courseid]);
          // }
          $content = CourseLessons::getUnitMainContent($lessonid);
          if($content!=null){
            // if($content->content_mime=="txt"){
            //
            //   }
            $thumbnail=$content['thumbnails'];
            $path=TcUploadedContent::getContentUrl($content['raw_date'],$content['content_defaultname']);
            if($content['type']=="pdf"){

                return $this->redirect(Yii::getAlias("@web")."/js/library/pdfjs/web/viewer.html?file=".Yii::getAlias("@web").$path);
            }
            return $this->render("learning",array("thumbnail"=>$thumbnail,"course"=>$course,"lesson"=>$lesson,'content'=>$content,"type"=>$type,"path"=>$path));
          }
    }
    public function actionClassroom($slug){
      $course=Course::find()->where(["slug"=>$slug])->one();
      $courseid = $course->course_id;
        if(\frontend\models\Enrollment::checkIfEnrolled(Yii::$app->user->id,$courseid)){
          $this->layout="classroom";
          $discussion= new \frontend\models\Discussions;


          $note = \frontend\models\UserNotebook::findOne([Yii::$app->user->id]);
          if($note==null)
              $note="Start writing here";
          else
              $note=$note->note;

          return $this->render("classroom",array("course"=>$course,'discussion'=>$discussion,'note'=>$note));

        }
        else{
          Yii::$app->session->setFlash("error","You have been enrolled for this course.");
          return $this->redirect(Url::toRoute(["/course/details","slug"=>$slug]));
        }
      }

    public function actionClassroomnavigation(){
        $itemid= Yii::$app->request->get("itemid");
        $type= Yii::$app->request->get("type");
        $prev_array=array();
        $next_array=array();
        if($type=="lesson"){
            $lesson = CourseLessons::findOne([$itemid]);
            $courseid=$lesson->courseid;
            $outlineno=$lesson->outlineno;

        }
        else{
            $quiz = Quiz::findOne([$itemid]);
            $courseid=$quiz->courseid;
            $outlineno=$quiz->outlineno;
        }


        $outline_model = CourseOutlineOrder::findOne([$outlineno]);

        $current['itemid'] =$itemid;
        $current['type']=$type;
        $current['section']=$outline_model->section;

        $max_outline = CourseOutlineOrder::find()->where(['and',["section"=>$outline_model->section],["courseid"=>$courseid]])->orderBy("counter desc")->one();
        $section_model= CourseParts::findOne([$outline_model->section]);
        $max_section_counter =CourseParts::find()->where(['course_id'=>$courseid])->orderBy("counter desc")->limit(1)->one();
        //checking previous

        if($outline_model->counter-1 !=0)
        {
            $prev = CourseOutlineOrder::find()->where("section=$outline_model->section and courseid =$courseid and counter < $outline_model->counter")->orderBy("counter desc")->one();
            $prev_array["itemid"]=$prev->itemid;
            $prev_array["type"]=$prev->type;
            $prev_array["section"]=$outline_model->section;
        }
        else{
            if($section_model->counter !=1){

                $s = CourseParts::find()->where("course_id =$courseid and counter < $section_model->counter")->orderBy("counter desc")->limit(1)->one();
              //  echo $s->part_id;
                $prev = CourseOutlineOrder::find()->where("section=".$s->part_id ." and courseid =$courseid ")->orderBy("counter desc")->one();
                $prev_array["itemid"]=$prev->itemid;
                $prev_array["type"]=$prev->type;
                $prev_array["section"]=$s->part_id;
            }
        }


        // Get next

        if($outline_model->counter != $max_outline->counter)
        {
            $next = CourseOutlineOrder::find()->where("section=$outline_model->section and courseid =$courseid and counter > $outline_model->counter")->orderBy("counter asc")->one();
            $next_array["itemid"]=$next->itemid;
            $next_array["type"]=$next->type;
            $next_array["section"]=$outline_model->section;
        }else{
            if($section_model->counter != $max_section_counter->counter){

                $s = CourseParts::find()->where("course_id =$courseid and counter > $section_model->counter")->orderBy("counter asc")->limit(1)->one();
                $next = CourseOutlineOrder::find()->where("section=$s->part_id and courseid =$courseid and counter = 1 ")->orderBy("counter asc")->one();
               // print_r($next);
              //  Yii::$app->end();
                $next_array["itemid"]=$next->itemid;
                $next_array["type"]=$next->type;
                $next_array["section"]=$s->part_id;
            }
        }
        $navigation["prev"]=$prev_array;
        $navigation["next"]=$next_array;
        $navigation["current"]=$current;
        echo json_encode($navigation);

    }
    public function actionSaveusernote(){
      $userid=Yii::$app->request->get("userid");
      if(\frontend\models\UserNotebook::find()->where(["userid"=>$userid])->exists()){
        $note =\frontend\models\UserNotebook::find()->where(["userid"=>$userid])->one();
        $note->note = Yii::$app->request->get("note");
        $note->userid= $userid;
      }else{
        $note = new \frontend\models\UserNotebook;
        $note->note = Yii::$app->request->get("note");
        $note->userid= Yii::$app->request->get("userid");
      }

        if($note->save())
            echo 1;
        else
            echo 0;
    }
    public function actionQuizintro(){

        $this->layout="classroom";
        $userid=Yii::$app->user->id;

        $quiz = Quiz::findOne([Yii::$app->request->get("quiz")]);
        $completed=\frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$quiz->q_id])->one();
        if(\frontend\models\QuizselectedAns::find()->where(["and",["quizid"=>$quiz->q_id],["userid"=>$userid]])->exists()){
          return $this->redirect(["questions-display","quizid"=>$quiz->q_id]);
        }
        else if(\frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$quiz->q_id])->one() !=null){
            return $this->redirect(["quiz-result","quizid"=>$quiz->q_id,"userid"=>Yii::$app->user->id]);
        }
        // $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quiz->q_id,"userid"=>Yii::$app->user->id]);
        // if($timer->exists())
        //     $timelimit = "+".$timer->one()->time."m";
        // else{
        //     $timer =new \frontend\models\Quiztimer;
        //     $timer->quizid=$quiz->q_id;
        //     $timer->userid =Yii::$app->user->id;
        //     $timer->time=$quiz->timelimit;
        //     $timelimit="+".$quiz->timelimit."m";//this will tell the countdown that the time is in minutes
        //     $timer->save();
        // }
        $timelimit=$quiz->timelimit;
        return $this->render("quizintro",array("quiz"=>$quiz,"timelimit"=>$timelimit,"completed"=>$completed));
    }
    public function actionQuizResult(){
      $this->layout="classroom";
      $userid=Yii::$app->request->get("userid");
      $quiz = Quiz::findOne([Yii::$app->request->get("quizid")]);
      $completed=\frontend\models\CompletedQuiz::find()->where(['userid'=>$userid,"quizid"=>$quiz->q_id])->one();
        return $this->render("quiz-result",array("quiz"=>$quiz,"completed"=>$completed));
    }
    public function actionSavetimer(){
        $seconds = Yii::$app->request->get("seconds");
        $minutes = Yii::$app->request->get("minutes");
        $hour = Yii::$app->request->get("hour");
        $quizid = Yii::$app->request->get("quizid");
        $userid = Yii::$app->request->get("userid");
        $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quizid,"userid"=>$userid])->one();

        if(Yii::$app->request->get("delete")=="true"){
            if($timer->delete())
                echo 1;
        }
        else{
          //Conversion fomular
          //minutes = hours x 60
          //second= hours x 3,600
          //minutes = seconds ÷ 60
          //second= minute x second
          //minutes = milliseconds ÷ 60,000

          //check if time is remaining just seconds
          //  if($hour==0 && $minutes==0){
          //    $time= "+".$seconds."s";
          //  }
          //  else{
          //    $hour=$hour*60;
          //    $seconds=$seconds/60;
          //    $minutes=$hour+$seconds+$minutes;
          //    $milliseconds=$minutes*60000;
          //    $time=(int)$minutes;//($minutes * 60) + $seconds;
          //    $time= "+".$time."m";
          //  }
           $hour=$hour*3600;
           $minutes=$minutes*60;
           $seconds=$hour+$seconds+$minutes;
           $milliseconds=$minutes*60000;
           $time=$seconds;//($minutes * 60) + $seconds;
           //$time= "+".$time."m";
            $timer->time=$time;
            $timer->save();
            echo $time;
            //echo date("H:i:s", time()+3600000);//$time;
        }

    }
    public function actionQuizquestion(){
        $question_array=array();
        $answer_array=array();
        $quizid=Yii::$app->request->get("quizid");
        $counter = Yii::$app->request->get("counter");
        if(Yii::$app->request->get("resume")==true){
              if(Yii::$app->request->cookies->has("quizid")){
                   $cookies=  Yii::$app->request->cookies;
                   $quizid= $cookies->getValue("quizid");
                   $counter = $cookies->getValue("counter");

              }else{
                echo 0;
                Yii::$app->end();
              }

         }
        $quiz = Quiz::findOne([$quizid]);
        $question= Question::find()->where(['quiz_id'=>$quiz->q_id,'counter'=>$counter])->orderBy("counter asc")->limit(1)->one();
        $question_array['description']=$question->description;
        $question_array["questionid"]=$question->question_id;
        $question_array['title']=$question->title;
        $question_array['type']=$question->type;
        if($question->type==1){
             $question_array["multiple"]="multiple";
        }
        else if($question->type==2){
             $question_array["single"]="single";
        }
        else {
              $question_array["truefalse"]="truefalse";
        }

        $question_array['counter']=$question->counter;
        $a =Answers::find()->where(["questionid"=>$question->question_id])->all();
        if($a!=null){
                    foreach($a as $answer):
                       $a_array["id"]=$answer->id;
                       $a_array["reason"]=$answer->reason;
                       $a_array["answer"]=$answer->answer;
                       $answer_array[]=$a_array;
                   endforeach;
        }
         //select the last question;
        $lq = Question::find()->where(['quiz_id'=>$quiz->q_id])->orderBy("counter desc")->limit(1)->one();
        $lastquestion =$lq->counter;
        //check if question is the last.
        if($counter == $lastquestion)
          $question_array['last']=true;
        $question_array['answers']=$answer_array;

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            "name"=>"quizid",
            "value"=>$quiz->q_id
          ]));
        $cookies->add(new \yii\web\Cookie([
              "name"=>"counter",
              "value"=>$question->counter
            ]));
        $cookies->add(new \yii\web\Cookie([
                "name"=>"question",
                "value"=>$question->question_id
        ]));

        echo json_encode($question_array);


    }

    public function actionQuestionsDisplay(){
      $this->layout="learning";
      $userid=Yii::$app->user->id;
      $retake = Yii::$app->request->get("retake");
      $quiz = Quiz::findOne([Yii::$app->request->get("quizid")]);
      $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quiz->q_id,"userid"=>Yii::$app->user->id]);

      if($retake!="true" && \frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$quiz->q_id])->one() !=null){
          return $this->redirect(["quiz-result","quizid"=>$quiz->q_id,"userid"=>$userid]);
      }
      if($timer->exists())
          $timelimit = $timer->one()->time;
      else{
          $timer =new \frontend\models\Quiztimer;
          $timer->quizid=$quiz->q_id;
          $timer->userid =Yii::$app->user->id;
          // $timer->time="+".$quiz->timelimit."m";
          // $timelimit="+".$quiz->timelimit."m";
          $timer->time=$quiz->timelimit*60;
          $timelimit=$quiz->timelimit*60;
          // date("H:i:s", time())
          $timer->save();
      }
      return $this->render("_question",array("quiz"=>$quiz,"timelimit"=>$timelimit));

    }
    public function actionSaveanswers(){
      Quiz::setSavedAnswers(Yii::$app->request,Yii::$app->request->get("userid"));
       //echo $answers;
    }
    public function actionGetsavedanswers($quizid){
       $answers = Quiz::getSavedAnswers(Yii::$app->request->get("userid"),$quizid);
       echo $answers;

    }
    public function actionFinishQuiz(){
      $this->layout="learning";
      // {"quiz":"9","answers":{
      // "0":{"question":"29","type":"2","answer":["91"]},
      // "1":{"question":"30","answer":["96"],"type":"2"},
      // "2":{"question":"31","answer":["98"],"type":"2"},
      // "4":{"question":"32","answer":["104"],"type":"2"},
      // "5":{"question":"33","answer":["106","107"],"type":"1"}}}
      $correctans =0;
      $check_correct=array();
      $check_wrong=array();
      $all_user_checked_ans=array();//for checkbox scenario

      $flag=false;
      $failed =0;
      $quizid = Yii::$app->request->get("quiz");
      $userid = Yii::$app->request->get("userid");
      $answers = Quiz::getSavedAnswers($userid,$quizid);
    //  echo \yii\helpers\Json::encode($answers);
      //Yii::$app->end();
      $totalquestion = Question::find()->where(["quiz_id"=>$quizid])->orderBy("counter desc")->limit(1)->one();
      $quizmodel=Quiz::findOne([$quizid]);
      $answers= json_decode($answers,true);

      if(count($answers["answers"])!=0){
        foreach($answers["answers"] as $p=>$q):
        // echo json_encode($q);
        //print_r($q);
          $question= Question::findOne([$q["question"]]);
          $r["question"]=$question->title;
          $r["counter"]=$question->counter;
          if($q['type']==1){
            //check is the users answer is more than thec correct ans
            if( !$a=Answers::find()->where(["and",['questionid'=>$q["question"]],["iscorrect"=>"true"]])->count() < count($q['answer'])){
              $a=Answers::find()->where(["and",['questionid'=>$q["question"]],["iscorrect"=>"true"]])->all();
              //check if the amount of checked answered matches with the user selection;
              foreach ($a as $key) {
              //search the answer array to check if a correct ans exist adn return index
                $index= array_search($key->id,$q["answer"]);
                if($index!==false){
                  $flag=true;
                  $check_correct[]=$key->id;
                }
                else{
                  $r['class']="fail";
                  $check_wrong[]=$key->id;
                  //break;
                }
              }
              if($flag){
                $r['class']="correct";
                ++$correctans;
              }
              else{
                  $r["iscorrect"]="false";
                  ++$failed;
              }

            }else{
              foreach($q["answer"] as $a):
                  $check_wrong[]=$a;
              endforeach;
              $r["iscorrect"]="false";
              $r['class']="fail";
              ++$failed;

            }
            foreach($q["answer"] as $a):
                $all_user_checked_ans[]=$a;//store the the student checked ans
            endforeach;
            //This multi dimensional array stores all the user selected answers. Not teh questions.
            $r["correct_ans"]=$check_correct;
            $r["user_marked_ans"]=$all_user_checked_ans;
            $r["wrong_ans"]=$check_wrong;//
            //get all the students  selected ans.
            $check_correct=array();
            $check_wrong=array();
          }
          else{// this is for radio button questions
            $a=Answers::find()->where(["and",["id"=>$q['answer'][0]],["iscorrect"=>"true"]])->one();
          //  echo $a->answer."<br/>";
            if($a!=null){
                  $r["iscorrect"]="true";
                  $r['class']="correct";
                  $check_correct[]=$q['answer'][0];
                  ++$correctans;
              }
              else{
                $r["iscorrect"]="false";
                $r['class']="fail";
                $check_wrong[]=$q['answer'][0];
                ++$failed;
              }
              $r["correct_ans"]=$check_correct;
              $r["user_marked_ans"]=array($q['answer'][0]);
              $r["wrong_ans"]=$check_wrong;//
              $check_correct=array();
              $check_wrong=array();
          }
          $result["answers"][]=$r;
        endforeach;
      }else {

        //here we get all the failed questions
        $failedquestion = Question::find()->where(["quiz_id"=>$quizid])->all();
        foreach($failedquestion as $qu):
          $r["question"]=$qu->title;
          $r["counter"]=$qu->counter;
          $r["iscorrect"]="false";
          $r['class']="fail";
          $r["user_marked_ans"]=[];
          $result["answers"][]=$r;
        endforeach;
        $correctans=0;
        $failed=0;


      }

      $result["correct"]=$correctans;
      $result["failed"]=$failed;
      $result["total"]=$totalquestion->counter;

      ///Do some house cleaning
      $cookies =Yii::$app->response->cookies;
      $cookies->remove("quizid");
      $cookies->remove("counter");
      $cookies->remove("question");

      if( \frontend\models\QuizselectedAns::find()->where(["and",["quizid"=>$quizid],["userid"=>$userid]])->exists())
            \frontend\models\QuizselectedAns::find()->where(["and",["quizid"=>$quizid],["userid"=>$userid]])->one()->delete();
        $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quizid,"userid"=>$userid]);
        if($timer->exists()){
          $timer->one()->delete();
        }

      if(!\frontend\models\CompletedQuiz::find()->where(["userid"=>$userid,"quizid"=>$quizid])->exists()){
        $complete= new \frontend\models\CompletedQuiz;
        $transaction = $complete->getDb()->beginTransaction();
        $complete->userid=$userid;
        $complete->quizid =$quizid;
        $complete->score=$correctans;
        $complete->total=$totalquestion->counter;
        $complete->save();
      }else{
        $complete=\frontend\models\CompletedQuiz::find()->where(["userid"=>$userid,"quizid"=>$quizid])->one();
        $complete->score=$correctans;
        $complete->total=$totalquestion->counter;
        $complete->save();
      }

      if(!\frontend\models\CourseLearnTracking::find()->where(["userid"=>$userid,"courseid"=>$quizmodel->courseid,"section"=>$quizmodel->partid,"lesson"=>$quizid,"lesson_type"=>"quiz"])->exists()){
        $track =new \frontend\models\CourseLearnTracking;
        $track->userid=$userid;
        $track->courseid=$quizmodel->courseid;
        $track->section=$quizmodel->partid;
        $track->lesson=$quizid;
        $track->lesson_type="quiz";
        $track->status="Completed";
        if($track->save())
                $transaction->commit();
      }




      return $this->render("finish-quiz",["result"=>$result,"quiz"=>$quizmodel]);
  }


    public function actionLessoncomplete(){
      $userid=Yii::$app->request->get("userid");
      $courseid=Yii::$app->request->get("courseid");
      $section=Yii::$app->request->get("sectionid");
      $lesson =Yii::$app->request->get("lessonid");
      $type=Yii::$app->request->get("type");
      $status =Yii::$app->request->get("status");

      if(!\frontend\models\CourseLearnTracking::find()->where(["userid"=>$userid,"courseid"=>$courseid,"section"=>$section,"lesson"=>$lesson,"lesson_type"=>$type])->exists()){
        $track =new \frontend\models\CourseLearnTracking;
        $track->userid=$userid;
        $track->courseid=$courseid;
        $track->section=$section;
        $track->lesson=$lesson;
        $track->lesson_type=$type;
        $track->status=$status;
        if($track->save()){
          echo 1;
        }
        else {
            echo 0;
        }
      }
      else {
          echo "already exist";
        }
    }
    /**
     * Displays a single Course model.
     * @param string $id
     * @return mixed
     */
    public function actionSectionorder(){
        if(Yii::$app->request->isPost){
            $counter=0;
           foreach(Yii::$app->request->post("neworder") as $order):
                $s= CourseParts::findOne([$order['sectionid']]);
                $s->counter=++$counter;
                $s->save();

            endforeach;
        }

    }
    public function actionSectionitemsorder(){
        if(Yii::$app->request->isPost){
            $counter=0;

           foreach(Yii::$app->request->post("neworder") as $order):
                $s= CourseOutlineOrder::findOne([$order['outlineno']]);
               // $transaction = $s->getDb()->beginTransaction();
                $s->counter=++$counter;
                if(Yii::$app->request->post('swapsection')==1)
                    $s->section = Yii::$app->request->post('sectionid');
                $s->save();
                if($s->type=="lesson"){
                    $l=CourseLessons::findOne([$s->itemid]);
                   $l->part_id=Yii::$app->request->post('sectionid');
                    $l->save();
                         // $transaction.commit();

                }
                /*else{
                    $q=Quiz::findOne([$s->itemid]);
                    $q->partid=$order['sectionid'];
                    if($q->save())
                        $transaction->commit();
                }*/
            endforeach;
        }
    }
    public function actionCoursecategory($category){
      // $raw_category =str_replace("-"," ",str_replace("&","and",strtolower($category)));
      try{
          $raw_category =str_replace("&","and",strtolower($category));//does nothing
          //$raw_category = Utility::cleanString(\yii\helpers\HtmlPurifier::process($category));
          // echo $raw_category;
          // Yii::$app->end();
          if(trim($raw_category)=="")
                $raw_category ="Teachnology";
          $category = CourseCategory::find()->where(["category"=>$raw_category])->one()->id;
          $query = Course::find()->where(["course_category"=>$category,"status"=>1]);
          $dataProvider = new ActiveDataProvider([
              'query' => $query,
              'pagination' => [
                //'pageSize' => 1,
              ]
          ]);
          // $result = Course::doSearch("category",$category);
          // $model = $result["model"];
          // $search_term =$result["search_term"];
          // $pages =$result["pages"];

          $savequery = new \frontend\models\TcSearchqueries;
          $savequery->query =$category;
          $savequery->page =Yii::$app->request->get("page");
          $savequery->save();

          //return $this->render("search",["rows"=>$model,"search_term"=>$search_term,"pages"=>$pages]);
          return $this->render("categories",["q"=>$raw_category,"pages"=>$dataProvider]);
            //echo Url::toRoute(["/course/coursecategory","slug"=>"adobe-photoshop-tool"]);
      }
        catch(\yii\base\ErrorException $e){
            return $this->redirect(["index"]);
        }
      
    }
    public function actionSearch(){
        $search_query = \yii\helpers\HtmlPurifier::process(Yii::$app->request->get("q"));
        $page=Yii::$app->request->get("page");
        $search_query = Utility::cleanString($search_query);
        if(trim($search_query)=="")
            $search_query="web";
        $searchModel = new \frontend\models\CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request);



//        $result = Course::doSearch("search",$search_query);
//        $model = $result["model"];
//        $search_term =$result["search_term"];
//        $pages =$result["pages"];

       $savequery = new \frontend\models\TcSearchqueries;
       $savequery->query =$search_query;
       $savequery->page =Yii::$app->request->get("page");
       $savequery->save();

         return $this->render("search",["page"=>$page,"q"=>$search_query,"search_term"=>$search_query,"pages"=>$dataProvider,"searchModel"=>$searchModel]);
    }
    public function actionAutosuggestjson($q){
      // Course::find()
      //   ->where('username LIKE :query')
      //   ->addParams([':query'=>'%admin%'])
      //   ->all();

      //   $query = new \yii\sphinx\Query;
      //   $query->from('searchqueries')
      //   ->match(new \yii\db\Expression(':match', ['match' => '@(query) ' . Yii::$app->sphinx->escapeMatchValue($q)]))
      //   ->limit(10);
      //  // build and execute the query
      //   $command = $query->createCommand();
      //   // $command->sql returns the actual SQL
      //   $data = $command->queryAll();
        $query = new  \yii\db\Query();

        $query->select('query')
            ->from('tc_searchqueries')
            ->where(['LIKE',"query",$q])
            ->orderBy('query')
            ->groupBy('query');
            $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['query']];
        }
        echo \yii\helpers\Json::encode($out);
    }
    public function actionGetsubcategory($category){
        $sub=CourseCategory::getSubCat($category);
        echo \yii\helpers\Json::encode($sub);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            if($this->findModel($id)->delete())
                Yii::$app->session->setFlash("success","Operation was performed successfully");
            else
                Yii::$app->session->setFlash("error","An error occurred while performing the operation. Please try again");

            return $this->redirect(Url::base().'/tutors/dashboard');


    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddlogotovideo(){
       // echo Yii::getAlias("@webroot")."/upload/promovideos/1444391521.mp4";
     echo Utility::addLogo(Yii::getAlias("@webroot")."/upload/promovideos/1444391521.mp4",Yii::getAlias("@webroot")."/upload/promovideos/1444391521-1.mp4",Yii::getAlias("@webroot")."/images/logo@200trans.png");
    }

    public function actionLanguage($language){

    }
}
