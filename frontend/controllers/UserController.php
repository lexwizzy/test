<?php

namespace frontend\controllers;

use Yii;
use frontend\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\SocialLink;
use yii\db\Expression;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\Url;
use frontend\models\UserSettings;
use frontend\models\Utility;
use frontend\models\TcUploadedContent;
use yii\base\ErrorException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','learn','login'],
                'rules' => [
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['delete-content','load-library','file-upload','profile','photo','settings','account','login','premiumtutor'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];

    }

    /**
     * Lists all User models.
     * @return mixed
     */
     public function actionDeleteContent($id){
      if(Yii::$app->request->isPost){
         if(TcUploadedContent::find()->where(["id"=>$id])->exists()){
            $c= TcUploadedContent::find()->where(["id"=>$id])->one();
            $year =date("Y",strtotime($c->uploaded_date));
            $month=date("m",strtotime($c->uploaded_date));
            $path=Yii::getAlias("@aws_cc").'/'.$year.'/'.$month."/".$c->filename;
            try{
              if(unlink($path)){
                 if($c->delete())
                  echo json_encode(["status"=>"success"]);
              }
            }
            catch(ErrorException $e){

            }

         }
         else
            echo json_encode(["status"=>"error"]);
      }
    }
     public function actionLoadLibrary($userid){
        $content_array=array();
        $a=array();
        $content = TcUploadedContent::getUserContent($userid);
        $content_array=TcUploadedContent::getModifiedContent($content);
        echo \yii\helpers\Json::encode($content_array);
    }
    public function actionFileUpload(){
        $userid=Yii::$app->user->id;

        if (Yii::$app->request->isPost) {
          $model = new TcUploadedContent();
          $model->scenario="justimage";
          $model->uploaded_file = UploadedFile::getInstances($model, 'uploaded_file');

          print_r($model->multipleUpload($userid));
        }


    }
    public function photoCropping($user){
        if(Yii::$app->request->isPost && Yii::$app->request->post("crop")==""){
            $user->load(Yii::$app->request->post());
            $user->photo= UploadedFile::getInstance($user, 'photo');
            if($user->validate()){
                $name=time().".".$user->photo->extension;
                $path=Yii::getAlias("@webroot")."/upload/temp/".$name;
                list($width, $height)=getimagesize($user->photo->tempName);
                  Utility::compress($user->photo->tempName,$path,50);
                 $response = array(
                        "status" => 1,
                        "width"=>$width,
                        "height"=>$height,
                        "path" =>Url::base()."/upload/temp/".$name,
                        "partialpath"=>"upload/temp/".$name,
                        "filename"=>$name
                      );

                echo json_encode($response);
            }else{
                foreach($user->getErrors()['photo'] as $error ):
                 echo $error;
                endforeach;
            }
            //return true;
            Yii::$app->end();
        }
        else if(Yii::$app->request->isPost && Yii::$app->request->post("crop")!=""){

            $targ_w = $targ_h = 200;
            $jpeg_quality = 90;

            $src = Yii::$app->request->post("path");
            $exploded = explode('.',$src);
            $ext = $exploded[count($exploded) - 1];
            $mime_type = mime_content_type($src);
            if ($mime_type == 'image/jpeg')
                $img_r=imagecreatefromjpeg($src);
            else if ($mime_type == 'image/png')
                $img_r=imagecreatefrompng($src);
            else if ($mime_type == 'image/gif')
                $img_r=imagecreatefromgif($src);
            else if ($mime_type == 'image/bmp')
                $img_r=imagecreatefrombmp($src);
            else
                return 0;
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
            $targ_w,$targ_h,$_POST['w'],$_POST['h']);
                //saving image
            $path = "upload/user/profile/photo/".Yii::$app->request->post("filename");
            $user->photo=Yii::$app->request->post("filename");
            $transaction = $user->getDb()->beginTransaction();
            if($user->save()){
                if(imagejpeg($dst_r, $path, $jpeg_quality)){
                    $transaction->commit();
                    return true;
                }
            }

        }
        return false;
    }
    public function actionPhoto(){
        $this->layout="main_with_sidebar";
        $this->view->params['elementclass_ph']="active";
        $user=User::findOne(['id'=>Yii::$app->user->id]);
        $this->photoCropping($user);

        return $this->render("photo",array("user"=>$user));
    }
     public function actionProfile(){
         $this->view->params['elementclass_pro']="active";
         $this->layout="main_with_sidebar";
        $user = User::findOne(['id'=>Yii::$app->user->id]);
        $social = SocialLink::findOne(['userid'=>Yii::$app->user->id]);
        if($social == null)
            $social = new SocialLink;
        if($user->load(Yii::$app->request->post()) && $social->load(Yii::$app->request->post())){
            $user->updated_at=new Expression("now()");
            $social->userid=Yii::$app->user->id;
            if($user->save() && $social->save()){
                Yii::$app->session->set("fname",$user->firstname);
                Yii::$app->session->set("lname",$user->lastname);
                Yii::$app->session->setFlash("success","Your profile was updated successfully");
            }
            else{
                Yii::$app->session->setFlash("error","An error occcurred while performing the last operation");
            }
            return $this->redirect(array("profile"));

        }
        return $this->render("profile",array("user"=>$user,'link'=>$social));
    }

    public function actionSettings(){
        $this->layout="main_with_sidebar";
        $this->view->params['elementclass_se']="active";
        $user=User::findOne(['id'=>Yii::$app->user->id]);
        $setting= UserSettings::findOne(['userid'=>Yii::$app->user->id]);
        if($setting == null){
            $setting= new UserSettings;
            $setting->userid=$user->id;
        }

        if($setting->load(Yii::$app->request->post())){
            if($setting->save()){
                Yii::$app->session->setFlash("success","Your profile was updated successfully");
            }
            else{
                Yii::$app->session->setFlash("error","An error occcurred while performing the last operation");
            }
            return $this->redirect(array("settings"));
        }
        return $this->render("settings",array("user"=>$user,"setting"=>$setting));
    }
   public function actionAccount(){
       $this->layout="main_with_sidebar";
        $this->view->params['elementclass_ac']="active";
        $user=User::findOne(['id'=>Yii::$app->user->id]);
       //set default scenario
       $user->scenario="changeemail";
       if($user->load(Yii::$app->request->post())){

           $scenario=Yii::$app->request->post("scenario");
           $user->scenario=$scenario;
           //$user->update_profile_pwd =Yii::$app->security->generatePasswordHash(Yii::$app->request->post("update_profile_pwd"));

           if($user::validatePasswordFromController($user->update_profile_pwd,$user->password)){

               if($user->scenario=="changepwd")
                   $user->password = Yii::$app->security->generatePasswordHash($user->new_password);

                 if($user->save()){
                    Yii::$app->session->setFlash("success","Your profile was updated successfully");
                     Yii::$app->session->set("email",$user->email);
                   // Yii::$app->user->logout();
                    //return $this->redirect(array("site/login"));
                }
                else{
                    Yii::$app->session->setFlash("error","An error occcurred while performing the last operation");
                }
           }else{
               Yii::$app->session->setFlash("error","The password you entered is incorrect");
           }

            //return $this->redirect(array("account"));
       }
       return $this->render("account",array("user"=>$user));

   }

   public function actionPremiumTutor(){
        $this->view->params['elementclass_pre']="active";
        $this->layout="main_with_sidebar";
        $userid =Yii::$app->user->id;
        $parameters=array();
        //$view_name="_terms_marketing";

        $p = \frontend\models\TcPremiumTutors::findOne([$userid]);
        if($p == null)
            $p = new \frontend\models\TcPremiumTutors;
        $payment = \frontend\models\TcPPPaymentInfo::findOne([$userid]);
        if($payment ==null)
            $payment = new \frontend\models\TcPPPaymentInfo;


        if($p->step == 2){
            $parameters["action_url"]="premium-tutor";
            $view_name ="_photo_upload";
        }
        else if($p->step == 3){
            $parameters["model"]=$p;
            $view_name="_terms_marketing";
        }
        else if($p->step == 4){
            $view_name ="payment_info";
            $parameters["model"]=$payment;
        }

        else
            $view_name ="_profile_form";


        $user = User::findOne(['id'=>$userid]);
        $user->scenario = "premiumtutor";
        $social = SocialLink::findOne(['userid'=>$userid]);
        if($social == null)
            $social = new SocialLink;
        
        $parameters["user"]=$user;
        $parameters["link"]=$social;

        if($user->load(Yii::$app->request->post()) && $social->load(Yii::$app->request->post())){
            $user->updated_at=new Expression("now()");
            $social->userid=$userid;
            if($user->save() && $social->save()){
                if(empty($user->photo)){
                    $p->userid=$userid;
                    $p->step=2;
                    $p->agreement=1;
                    $p->save();
                    $view_name = "_photo_upload";
                    $parameters["user"]= $user;
                    $parameters["action_url"]="premium-tutor";//url where the cropped image will be submitted
                }else{
                    $p->userid=$userid;
                    $p->step=3;
                    $p->agreement=1;
                    $p->save();
                    $view_name = "_terms_marketing";
                    $parameters["model"]=$p;//url where the cropped image will be submitted
                }

            }
            else{
                Yii::$app->session->setFlash("error","An error occcurred while performing the last operation");
            }
            //return $this->redirect(array("user/premium-tutor"));

        }
         else if($view_name =="_photo_upload" && $this->photoCropping($user) ){
             $view_name = "_terms_marketing";
             $parameters["model"]=$p;
             $p->step=3;
             $p->save();

        }
        else if( $p->load(Yii::$app->request->post())){

                $p->step=4;
                if($p->save()){
                     $view_name = "payment_info";
                     $parameters["model"]=$payment;
                }
                else{
                   // $view_name = "_payment_info_form";
                    $parameters["model"]=$p;
                    Yii::$app->session->setFlash("error","You must accept the Teachsity Premium Tutors Agreement.");
                }
        }
        else if($payment->load(Yii::$app->request->post())){
            $payment->userid=$userid;
            if($payment->save()){
                $parameters["model"]=$payment;
                $user->roleid=2;
                $user->save();
                Yii::$app->session->setFlash("success","Congratulations! You have successfully applied for Teachsity Premium Tutor.");
            }
        }

        // else{
        //     print_r(Yii::$app->request->post());
        //     Yii::$app->end();
        // }
        // if($this->photoCropping($user)){
        //
        // }

        return $this->render("premiumtutor",array("view_name"=>$view_name,"parameters"=>$parameters));
   }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new User();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
