<?php

namespace frontend\controllers;

use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use frontend\models\Course;
use yii\helpers\Url;
use yii;
use frontend\models\CourseCoupons;
use Razorpay\Api\Api;

class PaymentController extends \yii\web\Controller
{
    const RAZOR_API_KEY ="rzp_test_2IKJLNW0vMjOww";
    const RAZOR_API_SECRET ="Fb2Xv7883qPTLmUdn6fMRakS";
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    /*[
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['make-paypal-payment','make-payment','checkout','payment-cancel','payment-success','test','razor-pay-capture'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'razor-pay-capture' => ['post'],
                    
                ],
            ],
        ];
    }
    public function actionCheckout($id){
      //  Url::remember();
        $course = Course::findOne([$id]);
        Yii::$app->session["pcid"]=$course;
        $coupon = Yii::$app->request->get("coupon");
        $original_amount=Course::getPriceByInstance($course);
        $total_amount= $original_amount;
        $discount=0;
        $rate=0;
        $totalAmountWithNoCurrency = Course::getPriceNoCurrency($course);
        $paiseAmount = Course::convertToPaise($totalAmountWithNoCurrency);
        

        if(!empty($coupon)){
          if(CourseCoupons::find()->where(["courseid"=>$id,"coupon_code"=>$coupon])->exists()){
            $_coupon=CourseCoupons::find()->where(["courseid"=>$id,"coupon_code"=>$coupon])->one();
            if(strtotime("today") >=strtotime($_coupon->expirey_date)){
                    Yii::$app->session->setFlash("error","The applied coupon has expired.");
                  //  return $this->redirect(Url::previous());

              }
           else{
              $totalAmountWithNoCurrency = Course::getPriceNoCurrency($course,$_coupon->discount);
              $paiseAmount = Course::convertToPaise($totalAmountWithNoCurrency);
              $total_amount = Course::getPriceByInstance($course,$_coupon->discount);//()
              $discount=CourseCoupons::getDiscountAmount($course->amount,$_coupon->discount);//
              $rate =$_coupon->discount;
           }
          }
          else{
            Yii::$app->session->setFlash("error",'Sorry this coupon does not exist');
          //  return $this->redirect(Url::previous());
          }
        }
        
        // save the actual paise amount is session to enable capturing thesame value
        $session = Yii::$app->session;
        $session->set("paiseAmount",$paiseAmount);;
        
        
        return $this->render("checkout",["course"=>$course,"rate"=>$rate,"discount"=>$discount,"coupon"=>$coupon,"total_amount"=>$total_amount,"amount"=>$original_amount,"totalAmountWithNoCurrency"=>$totalAmountWithNoCurrency,"paiseAmount"=>$paiseAmount]);
    }
    
    public function actionRazorPayCapture(){
        $session = Yii::$app->session;
        $coursePrice = $session->get("paiseAmount");
        $course = $session->get("pcid");
        $coupon = Yii::$app->request->post("coupon");
        
        $paymentId = Yii::$app->request->post("paymentId");
//        $coursePrice = Yii::$app->request->post()->get("coursePrice");
        $api = new Api(PaymentController::RAZOR_API_KEY, PaymentController::RAZOR_API_SECRET);
        
        $payment = $api->payment->fetch($paymentId);
        $payment->capture(array('amount' =>$coursePrice ));
        
        $session->remove("paiseAmount");
        if ($payment->error_code ==null && $payment->status =="captured"){
                if(!\frontend\models\Enrollment::find()->where(['courseid'=>$course->course_id,"userid"=>Yii::$app->user->id])->exists()){
                   $enroll = new \frontend\models\Enrollment;
                   $enroll->courseid=$course->course_id;
                   $enroll->userid=Yii::$app->user->id;

                   if(!$enroll->save()){
                      Yii::$app->session->setFlash("error","An error occurred while trying to enroll you for \"".$course->course_title."\"");
                       //$this->redirect(Url::toRoute(["learn","slug"=>$course->slug]));
                   }
               }
               $payment= new \frontend\models\TcCoursesSalesTransaction();
               $payment->payment_source="razorpay";
               $payment->courseid=$course->course_id;
               $payment->status ="captured";
               $payment->userid=Yii::$app->user->id;
               $payment->coupon_code=$coupon;
               $payment->save();
               return 1;
        }
        else{
            Yii::$app->response->statusCode = 500;
            return 0;
        }
        // To get the payment detail
        
    }
    public function actionMakePaypalPayment($tid){
      $course = Course::findOne([$tid]);
      $code=Yii::$app->request->get("coupon");
      $amount = Course::getPrice($course->course_id);//
      //check coupon validity;
      if(!empty($code))
      {
        if(CourseCoupons::find()->where(["courseid"=>$course->course_id,"coupon_code"=>$code])->exists()){
          $coupon=CourseCoupons::find()->where(["courseid"=>$course->course_id,"coupon_code"=>$code])->one();
          if(strtotime("today") >=strtotime($coupon->expirey_date)){
                  Yii::$app->session->setFlash("error","The applied coupon has expired.");
                  return $this->redirect(Url::previous());
            }
         else{
            $amount = Course::getPrice($course->course_id,$coupon->discount);//

         }
        }
        else{
          Yii::$app->session->setFlash("error",'Sorry this coupon does not exist');
          return $this->redirect(Url::previous());
        }
      }
      //Yi::$app->session["cc"]=$code;//store coupon code
      //https://api.sandbox.paypal.com/v1
      //store courseid in session to be used after payment process;
      // $access_token_request= shell_exec('curl -v https://api.sandbox.paypal.com/v1/oauth2/token -H "Accept: application/json" -H "Accept-Language: en_US" -u "AQ-MjLoafAKkrENBkvyPgMubFNVN5-1YmIwuM9ejZNLjxaPPR0O_xZudSdBsQ7NMLt5B-flFZT6CeL3M:EIbrdd7mVC6MB-GfYxnH_l8p-3OZ4dIuPusObqU__ZIIfsGN1TR4tSLtoX_aIMfjNGUem35RmtcHlX4W" -d "grant_type=client_credentials"');
      $access_token_request= shell_exec('curl -v https://api.paypal.com/v1/oauth2/token -H "Accept: application/json" -H "Accept-Language: en_US" -u "AagLuWa5Eq6snwST6fLHPekvoJ8n5Vti88tMNPOTf7hyEDhoWsgvqiL7zAVZGinoabxtCRnnUmeeTmoI:EIrz-_j6CndyDB9HcJI82b19odxuGSXqJaOCrnQLDOaF3rHGuszXyFyi2lQNYzQtXqR6pgSGENcu4VIT" -d "grant_type=client_credentials"');
      $access_token_request =json_decode($access_token_request,true);
      Yii::$app->session["pp_access_token"]=$access_token_request["access_token"];//save access_token to session;

      $payment_request= shell_exec('curl -v https://api.paypal.com/v1/payments/payment \
       -H "Content-Type: application/json" \
       -H "Authorization: Bearer '.$access_token_request["access_token"].'" \
       -d \'{
         "intent":"sale",
         "redirect_urls":{
           "return_url":"http://teachsity.com/payment/payment-success",
           "cancel_url":"http://teachsity.com/payment/payment-cancel"
         },
         "payer":{
           "payment_method":"paypal"
         },
         "transactions":[
           {
             "amount":{
               "total":"'.$course->amount.'",
               "currency":"INR"
             },
              "description": "Course payment - '.$course->course_title.' on teachsity.com."
           }
         ]
       }\'');


       $payment_request = json_decode($payment_request,true);

       if($payment_request["state"]=="created"){
           $payment= new \frontend\models\TcCoursesSalesTransaction();
           $payment->payment_source="paypal";
           $payment->courseid=$course->course_id;
           $payment->status ="created";
           $payment->userid=Yii::$app->user->id;
           $payment->coupon_code=$code;
           $payment->save();
           Yii::$app->session["transactionid"]=$payment->getPrimaryKey();

           $links=$payment_request["links"];//get links

           if($links[1]["rel"]=="approval_url"){
             $approval_url =$links[1]["href"];//get approval url to redirect user.
             return $this->redirect($approval_url);
           }
       }
       else {
         Yii::$app->session->setFlash("error","We were unable to intiate a transaction with paypal. Please try again.");
         return $this->redirect(array("payment-cancel"));
       }
    }

        public function actionPaymentSuccess(){
         
          $course= Yii::$app->session["pcid"];
          Yii::$app->session["pcid"]="";
          //check if this is paypal payment.
          if(Yii::$app->request->get("paymentId") && Yii::$app->request->get("PayerID") ){
            $transactionid = Yii::$app->session["transactionid"];
            $trans = \frontend\models\TcCoursesSalesTransaction::findOne([$transactionid]);
            $payer_id =Yii::$app->request->get("PayerID");
            $paymentId = Yii::$app->request->get("paymentId");
            $trans->status="approved";


            $accesstoken = Yii::$app->session["pp_access_token"];
            $execute_payment = shell_exec('curl -v https://api.sandbox.paypal.com/v1/payments/payment/PAY-6RV70583SB702805EKEYSZ6Y/execute/ \
            -H \'Content-Type: application/json\' \
            -H \'Authorization: Bearer $accesstoken\' \
            -d \'{ "payer_id" : "'.$payer_id.'" }\'');


            if(empty($execute_payment)){
              $trans->status="completed";
              
              $courseid =$course->course_id;

               if(!\frontend\models\Enrollment::find()->where(['courseid'=>$courseid,"userid"=>Yii::$app->user->id])->exists()){
                   $enroll = new \frontend\models\Enrollment;
                   $enroll->courseid=$courseid;
                   $enroll->userid=Yii::$app->user->id;

                   if(!$enroll->save()){
                      Yii::$app->session->setFlash("error","An error occurred while trying to enroll you for \"".$course->course_title."\"");
                       //$this->redirect(Url::toRoute(["learn","slug"=>$course->slug]));
                   }
                   else
                      Yii::$app->session->setFlash("success","You have been enrolled for ".$course->course_title);
               }

              $trans->pp_payerid=$payer_id;
              $trans->pp_paymentid=$paymentId;
              $trans->save();
              return $this->render("payment-success",["course"=>$course]);
            }
            else{
                $execute_payment=json_decode($execute_payment,true);
                if($execute_payment["status"]==404){
                  Yii::$app->session->setFlash("error","We were unable to intiate a transaction. Please try again.");
                  return $this->redirect(array("payment-cancel"));
                }
                else{
                  Yii::$app->session->setFlash("error","We were unable to intiate a transaction . Please try again.");
                  return $this->redirect(array("payment-cancel"));
                }
            }

          }
            
          else if(Yii::$app->request->get("pm")=="rzpy"){//pm=payment method
              
               return $this->render("payment-success",["course"=>$course]); 
          }

        }

        public function actionPaymentCancel(){
          Yii::$app->session["pp_access_token"]="";
          Yii::$app->session["pcid"]="";
          return $this->render("payment-cancel");
        }
    
    

}
