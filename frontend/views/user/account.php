<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\User;
 use frontend\models\CourseCoupons;

$class="active";
?>
<style>
  #upload-file{

    border: 1px solid #ccc;
    padding: 0.5em;
    border-radius: 2px;

    }
    .jcrop-holder{
        margin-left:8em;
        margin-right:8em;
    }
</style>

    <!-- END / BANNER CREATE COURSE -->

    <!-- CREATE COURSE CONTENT -->
                 <?php $this->beginBlock('center'); ?>
                    <div class="create-course-content " >
                         <?php if(Yii::$app->session->hasFlash("error")):?>
                         <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
                        <?php elseif(Yii::$app->session->hasFlash("success")):?>
                            <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
                        <?php endif?>
                        <!-- Course goal-->
                        <div class="main-content-title text-center" >
                            <h2 class="sm black bold">Account</h2>
                            <p>Change your email address and password.</p>
                        </div>
                <div class="content-setting">

                  <div class="row">

                    <div class="col-md-8 col-md-offset-2">
                        <div class="setting-box">
                            <h4 class="sm black bold">Change Email Address</h4>
                            <ul>
                                 <li>
                                     <?php $form = ActiveForm::begin()?>
                                    <input type="hidden" name="scenario"  value="changeemail"/>
                                    <div class="col-md-12 top-margin" >
                                        <?=$form->field($user,"email")->textInput(['placeholder'=>"Email",'autocomplete'=>"off"])->label(false);?>
                                    </div>
                                    <div class="col-md-12">
                                        <?=$form->field($user,"update_profile_pwd")->passwordInput(['placeholder'=>"Enter your password to change email",'value'=>"",'autocomplete'=>"off"])->label(false);?>
                                    </div>
                                      <div class="text-center">
                                        <input type="submit" value="Change Email Address" class="btn btn-success">
                                     </div>
                                     <?php ActiveForm::end()?>
                                 </li>
                            </ul>

                        </div>

                       <div class="setting-box">
                            <h4 class="sm black bold">Change Password</h4>
                            <ul>
                                 <li>
                                     <?php $form = ActiveForm::begin()?>
                                    <input type="hidden" name="scenario"  value="changepwd"/>

                                    <div class="col-md-12 top-margin">
                                        <?=$form->field($user,"update_profile_pwd")->passwordInput(['placeholder'=>"Enter old password ",'value'=>""])->label(false);?>
                                    </div>
                                     <div class="col-md-12">
                                        <?=$form->field($user,"new_password")->passwordInput(['placeholder'=>"Enter new password",'value'=>""])->label(false);?>
                                    </div>
                                     <div class="col-md-12">
                                        <?=$form->field($user,"new_password_repeat")->passwordInput(['placeholder'=>"Confirm new password",'value'=>""])->label(false);?>
                                    </div>
                                      <div class="text-center">
                                        <input type="submit" value="Change Password" class="btn btn-success">
                                     </div>
                                     <?php ActiveForm::end()?>
                                 </li>
                            </ul>

                        </div>



                    </div>
                </div>

            </div>


                        </div>

                  <?php $this->endBlock(); ?>




    <!-- END / CREATE COURSE CONTENT -->

   <!-- <script type="text/javascript" src="https://apis.google.com/js/api.js"></script>-->
<?php
    $this->registerJsFile( Url::base().'/js/photo.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
    $this->registerJS("var userid=".json_encode(Yii::$app->user->id),View::POS_HEAD);
?>
