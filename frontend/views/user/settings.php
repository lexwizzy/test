<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\User;
 use frontend\models\CourseCoupons;

$class="active";
?>
<style>
  #upload-file{

    border: 1px solid #ccc;
    padding: 0.5em;
    border-radius: 2px;

    }
    .jcrop-holder{
        margin-left:8em;
        margin-right:8em;
    }
</style>

    <!-- END / BANNER CREATE COURSE -->

    <!-- CREATE COURSE CONTENT -->
                 <?php $this->beginBlock('center'); ?>

                    <div class="create-course-content  " >
                         <?php if(Yii::$app->session->hasFlash("error")):?>
                         <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
                        <?php elseif(Yii::$app->session->hasFlash("success")):?>
                            <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
                        <?php endif?>
                        <!-- Course goal-->
                        <div class="main-content-title text-center" >
                            <h2 class="sm black bold">Setting</h2>
                            <p>Email notification settings.</p>
                        </div>
                        <div class="content-setting">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="notification-setting setting-box">
                            <?php $form = ActiveForm::begin()?>
                            <ul>
                                <li>
                                     <?= $form->field($setting, 'course_progress', [
                                                        'template' => "{input}{error} <label for='course_progress'>Course progress<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"course_progress","class"=>"inputcheck"],false)->label(false)?>

                                  </li>
                                <li>
                                    <?= $form->field($setting, 'newmessage', [
                                                        'template' => "{input}{error} <label for='newmessage'>New message<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"newmessage","class"=>"inputcheck"],false)->label(false)?>


                                </li>
                                <li>
                                     <?= $form->field($setting, 'tas', [
                                                        'template' => "{input}{error} <label for='tas'>Teachsity Special Announcement<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"tas","class"=>"inputcheck"],false)->label(false)?>

                                </li>
                                <li>
                                     <?= $form->field($setting, 'tcr', [
                                                        'template' => "{input}{error} <label for='tcr'>Teachsity course recommendation<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"tcr","class"=>"inputcheck"],false)->label(false)?>

                                </li>
                                <li>
                                     <?= $form->field($setting, 'ssfm', [
                                                        'template' => "{input}{error} <label for='ssfm'>New follower<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"ssfm","class"=>"inputcheck"],false)->label(false)?>

                                </li>
                                <li>
                                    <?= $form->field($setting, 'wmcr', [
                                                        'template' => "{input}{error} <label for='wmcr'>New course review<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"wmcr","class"=>"inputcheck"],false)->label(false)?>
                                </li>
                                <li>
                                    <?= $form->field($setting, 'sml', [
                                                        'template' => "{input}{error} <label for='sml'>Show my location<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"sml","class"=>"inputcheck"],false)->label(false)?>
                                </li>
                                <li>
                                    <?= $form->field($setting, 'sme', [
                                                        'template' => "{input}{error} <label for='sme'>Show my email<i class='icon icon_check fa fa-check'></i></label>",
                                         ])->checkbox(["id"=>"sme","class"=>"inputcheck"],false)->label(false)?>
                                </li>

                            </ul>
                        </div>


                             <div class="input-save text-center">
                                <input type="submit" value="Save changes" class="btn btn-success">
                             </div>
                            <?php ActiveForm::end()?>

                    </div>
                </div>

            </div>


                        </div>

                    </div>
                  <?php $this->endBlock(); ?>



    <!-- END / CREATE COURSE CONTENT -->

   <!-- <script type="text/javascript" src="https://apis.google.com/js/api.js"></script>-->
<?php
    $this->registerJsFile( Url::base().'/js/photo.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
    $this->registerJS("var userid=".json_encode(Yii::$app->user->id),View::POS_HEAD);
?>
