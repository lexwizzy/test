

    <!-- END / BANNER CREATE COURSE -->

    <!-- CREATE COURSE CONTENT -->
                <?php $this->beginBlock('center'); ?>
                    <div class="create-course-content text-center " >
                         <?php if(Yii::$app->session->hasFlash("error")):?>
                         <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
                        <?php elseif(Yii::$app->session->hasFlash("success")):?>
                            <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
                        <?php endif?>
                        <!-- Course goal-->
                        <div class="main-content-title text-center">
                            <h2 class="sm black bold">Photo</h2>
                            <p>An image is worth a thousand words</p>
                        </div>


                        <div class="course-banner create-item profile-photo-center">
                            <div class="row  text-center">
                                <?= $this->render("_photo_upload",["user"=>$user,"action_url"=>"photo"])?>
                            </div>



                            </div>
                        <!-- COURSE BANNER -->



                        <!-- PROMO VIDEO -->

                        </div>

                      <?php $this->endBlock(); ?>


    <!-- END / CREATE COURSE CONTENT -->

   <!-- <script type="text/javascript" src="https://apis.google.com/js/api.js"></script>-->
