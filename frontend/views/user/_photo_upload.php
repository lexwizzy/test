<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\User;
 use frontend\models\CourseCoupons;
?>
<style>
  #upload-file{

    border: 1px solid #ccc;
    padding: 0.5em;
    border-radius: 2px;

    }
    /*.jcrop-holder{
        margin-left:8em;
        margin-right:8em;
    }*/
    .edit-profile-pic-div{
    /* padding: 20%; */
    padding-left: 10%;
    padding-right: 10%;
    margin-left: 23%;
    margin-right: 20%;
    background: #AAAFAE;
    border: 10px solid #fff;
    und: #AFAAAA;
    /* max-width: 300px; */
    /* z-index: -1; */

}

.edit-profile-pic-div .img-div{
  padding: 20px;
}

.group-input {
    max-width: 64%;
    margin-left: 19%;
    margin-top: 20px;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    text-align: right;
    opacity: 0;
    background: 0 0;
    cursor: inherit;
    display: block;
}


.input-group-btn > .btn + .btn {
    margin-left: -1px;
}
.input-group-btn > .btn + .btn {
    margin-left: -1px;
}
.input-group-btn > .btn + .btn {
    margin-left: -1px;
}
.input-group-btn > .btn + .btn {
    margin-left: -1px;
}
.input-group-btn > .btn {
    position: relative;
}
.input-group-btn > .btn {
    position: relative;
}
.input-group-btn > .btn {
    position: relative;
}
.input-group-btn > .btn {
    position: relative;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
#file-button {
    border-width: 1px;
    border-radius: 3px;
    font-size: 13px;
    font-weight: 600;
    padding: 10px 18px;
    transition: background-color 0.15s linear, border 0.15s linear;
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857;
    border-radius: 0;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.row{
  display: block
}
.edit-profile-pic-div  {
/* padding: 20%; */
/* padding-left: 10%; */
/* padding-right: 10%; */
max-height: 335px;
margin-left: 19%;
margin-right: 20%;
margin-bottom: 40px;
background: #AAAFAE;
border: 10px solid #fff;
und: #AFAAAA;
/* max-width: 300px; */
/* z-index: -1; */
width: 64%;
}
.upload-the-file {
    width: 64%;
    margin-left: 19%;
}
#file-button{
  height: 34px;
}
.upload-progress{
  margin-top: 15px;
}
.progress-bar{
  background: #ef4132!important;
}
</style>
<!-- edit-profile-pic-div {
/* padding: 20%; */
/* padding-left: 10%; */
/* padding-right: 10%; */
margin-left: 19%;
margin-right: 20%;
background: #AAAFAE;
border: 10px solid #fff;
und: #AFAAAA;
/* max-width: 300px; */
/* z-index: -1; */
width: 64%;
} -->
                        <?php if($this->context->action->id !="photo"):?>
                          <div class="col-md-12">
                            <ul class="progress-indicator">
                             <!--  <li class="completed"> <span class="bubble"></span> Personal Info. </li> -->
                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i> Personal Info. </li>
                              <li class="completed"> <span class="bubble"></span>Profile Picture. </li>
                              <li > <span class="bubble"></span> Marketing Service. </li>
                              <li> <span class="bubble"></span> Payment Info. </li>
                            </ul>
                          </div>
                        <?php endif;?>
<div class="alert alert-success hidden"></div>
                                <div class="alert alert-danger hidden"></div>
                                    <div class="col-md-12 edit-profile-pic-div" style="">
                                        <div class="img-div">
                                            <img height="200"  width="200" class="img-responseive profile_pix" id="profile-pix" src="<?=User::getPhoto(Yii::$app->user->id)?>"/>
                                        </div>
                                           <p>Maximum photo size is 200 x 200.</p>
                                    </div>
                                <div class="upload-the-file" >
                                    <div class="input-group ">
                                       <div tabindex="500" class="form-control file-caption  kv-fileinput-caption">
                                       <div class="file-caption-name" title="">Upload .jpg, png, gif</div>
                                    </div>

                                       <div class="input-group-btn">
                                           <div tabindex="500" id="file-button" class="btn btn-success btn-file">Browse
                                              <div class="field-signupform-cv">
                                                  <?php $form=ActiveForm::begin(['id'=>'photoform','action'=>'javascript:;','options'=> ['enctype' => 'multipart/form-data','accept-charset'=>"utf-8"]]);?>
                                                  <?= $form->field($user, 'photo')->fileInput()->label(false) ?>
                                                  <?php ActiveForm::end();?>
                                                </div>
                                               </div>
                                        </div>

                                        <!-- <p class="help-block help-block-error"></p> -->
                                   </div>
                                   <div class="progress upload-progress hidden" >
                                       <div class="progress-bar progress-bar-striped active" role="progressbar"
                                      aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                        0%
                                      </div>
                                    </div>
                                    <div  style="margin-top:10px">
                                     <form action="<?=$action_url?>" method="post" class="coords" onsubmit="return checkCoords();">

                                         <input type="hidden" size="4" id="x1" name="crop"  value="true"/>
                                         <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                         <input type="hidden" size="4" id="path" name="path" />
                                         <input type="hidden" size="4" id="filename" name="filename" />
                                        <input type="hidden" id="x" name="x" />
                                        <input type="hidden" id="y" name="y" />
                                        <input type="hidden" id="w" name="w" />
                                        <input type="hidden" id="h" name="h" />
                                          <input type="submit" class="btn btn-success" id="savephoto" disabled value="Save"/>
                                    </form>

                                </div>
                                 </div>






                <?php
    $this->registerJsFile( Url::base().'/js/photo.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
    $this->registerJS("var userid=".json_encode(Yii::$app->user->id),View::POS_HEAD);
?>
