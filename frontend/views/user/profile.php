<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
?>
<style>
.file-drop-zone{
    height:238px;
    padding:0px;
}
.file-preview {
    width:270px;
}
.file-drop-zone-title {
    color: #aaa;
    font-size: 26px;
    padding: 30px 5px;
}
.bootstrap-tagsinput{
    width:500px;
}
.bootstrap-tagsinput span {
    display: inline !important;
    color:#fff !important;
    font-size:11px !important;
}
.bootstrap-tagsinput input{
    border:none !important;
    width:6em !important;
}
#amount-item .form-group {
    margin-bottom:0 !important;
}
#amount-item .help-block{
    margin:0;
}

</style>
    <!-- END / BANNER CREATE COURSE bootstrap-formhelpers.min.css-->

    <!-- CREATE COURSE CONTENT -->
<link  rel="stylesheet" href="<?=Url::base()?>/css/bootstrap-formhelpers.min.css" type="text/css"/>

                 <?php $this->beginBlock('center'); ?>


                    <div class="create-course-content text-center " >
                         <?php if(Yii::$app->session->hasFlash("error")):?>
                         <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
                        <?php elseif(Yii::$app->session->hasFlash("success")):?>
                            <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
                        <?php endif?>
                        <!-- Course goal-->
                        <div class="main-content-title">
                            <h2 class="sm black bold">Profile Info</h2>
                            <p>Tell people more about you.</p>
                        </div>


                        <div class="course-banner create-item profile-center-div ">
                            <div class="col-md-12  text-center">
                                <?= $this->render("_profile_form",["link"=>$link,"user"=>$user]);?>
                        </div>
                        <!-- COURSE BANNER -->



                        <!-- PROMO VIDEO -->

                </div>

            </div>
        <?php $this->endBlock(); ?>



    <!-- END / CREATE COURSE CONTENT -->
