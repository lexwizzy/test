<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
?>
                        <?php $form=ActiveForm::begin()?>

                 <?php if($this->context->action->id !="profile" && !\frontend\models\TcPPPaymentInfo::find()->where(["userid"=>Yii::$app->user->id])->exists()):?>
                      <div class="col-md-12 ">
                                            <ul class="progress-indicator">
                                             <!--  <li class="completed"> <span class="bubble"></span> Personal Info. </li> -->
                                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i> Personal Info. </li>
                                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i>Profile Picture. </li>
                                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i> Marketing Service. </li>
                                              <li class="completed"> <span class="bubble"></span> Payment Info. </li>
                                            </ul>
                                        </div>
                    <?php endif;?>
                            <div class="col-md-12 field ">
                                        <?= $form->field($model,'email')->Input("email",['placeholder'=>'Email'])->label(false)?>
                            </div>

                            <div class="col-md-12 field">
                                    <?= $form->field($model,'address')->textArea(['placeholder'=>'Address as in your paypal account'])->label(false)?>
                            </div>
                            <div class="col-md-12 field ">
                                        <?= $form->field($model,'city')->textInput(['placeholder'=>'City'])->label(false)?>
                            </div>

                             <div class="col-md-12 field" style="text-align:left">
                                <?=$form->field($model,"country")->dropDownList([],['prompt'=>'Select country','data-country'=>$model->country,'class'=>"form-control bfh-countries"])->label(false)?>

                            </div>
                           <div style="padding-top:20px;">
                                           <input type="submit" value="Save" class=" btn btn-success">
                                      </div>

                     <?php ActiveForm::end()?>
<?php
     $this->registerJsFile( Url::base().'/js/bootstrap-formhelpers.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
?>
