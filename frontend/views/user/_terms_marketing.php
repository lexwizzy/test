<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\User;
 use frontend\models\CourseCoupons;
?>
<style>
strong {
   font-family: FuturaStd-Heavy;
}.icon_check {
    width: 16px;
    height: 16px;
    border: 2px solid #d4d4d4;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    float: left;
    margin-top: 1sspx;
    margin-right: 12px;
    font-size: 12px;
    color: transparent;
    -webkit-transition: all 0.3s ease 0s;
    -moz-transition: all 0.3s ease 0s;
    -ms-transition: all 0.3s ease 0s;
    -o-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
}
input:checked ~ label .icon_check {
    color: #ef4132;
    border-color: #ef4132;
}
</style>
  <?php $form=ActiveForm::begin();?>
<div class=" col-md-12">
 <ul class="progress-indicator">
                             <!--  <li class="completed"> <span class="bubble"></span> Personal Info. </li> -->
                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i> Personal Info. </li>
                              <li class="completed"> <span class="bubble"></span><i class="fa fa-check-circle fa-2x"></i> Profile Picture. </li>
                              <li class="completed" > <span class="bubble"></span> Marketing Service. </li>
                              <li> <span class="bubble"></span> Payment Info. </li>
                            </ul>
                          </div>
                                <div class=" col-md-12" style="padding-left:15%; padding-right:15%; min-height:500px" >


                                           <div class="marketing_terms" style="height:400px; overflow:scroll; border:1px solid #bbb; padding:10px; text-align:left ">
                                             <strong>Tutors Terms & Conditions (Required)</strong>

Key Definitions

<strong>"Base Rate"</strong> means the course price set by the Instructor.<br/>

<strong>"Base Currency"</strong> means the currency of the Base Price.<br/>

<strong/>"Base Exchange Rate"</strong> means a system-wide rate used by the Company for foreign currency

conversion excluding any fee or mark-up by the Company. The rate is established using one or more

third parties such as Open Exchange Rates and is fixed periodically (e.g. forthnightly) to prevent

daily price fluctuations. Accordingly, the Base Exchange Rate may not be identical to the applicable

market rate in effect at the specific time a foreign currency conversion is processed.<br/>

<strong>"Sale Rate"</strong> means the actual sale price for the Course. When the Sale Currency is different from the

Base Currency, the Company will determine the Sale Price based on the applicable Base Exchange

Rate and Cost Adjustment Factor.<br/>

<strong>"Sale Currency"</strong> means the currency of the sale. This is determined by the country of origin of the

User purchasing the Course.<br/>
<strong>"Cost Adjustment Factor"</strong> means applicable local taxes and other fees associated with currency

conversions. In regions that use a common currency, e.g., the EU, the Cost Adjustment Factor uses

a weighted average of country specific tax rates to ensure the same prices to end customers across

the region.<br/>

<strong>"Net Amount"</strong> means the amount actually received from Students for Your Course, less any refunds

paid, applicable sales or other taxes (VAT in EU), and any amounts paid in connection with

Marketing Programs that You participate in.<br/>

<strong>Pricing</strong>

As a tutor, You will be solely responsible for deciding the Base Price You charge Students for Your

Course(s). You agree to charge only for Your own Submitted Content. The Company will handle

billing and other fee interaction with Users. When the Sale Currency is different than the Base

Currency, Teachsity will decide the Sale Rate according to the most recent Base Exchange Rate

and applicable Cost Adjustment Factor.<br/>

<p>If You choose to participate in any of the Company's Marketing Programs, the fee You receive from

the Company will be in accordance with the terms of the particular Marketing Program that applies to

the sale of Your Course. Otherwise You will receive the following:</p>
<ul>

<li>Generally, the Company will pay You fifty percent (50%) of the Net Amount received for Your

Course. The Net Amount equals the amount actually received from Students for Your Course, less

any refunds paid, applicable sales or other taxes (if any), applicable fees for mobile application

sales from either the Apple App Store or Google Play, and any amounts paid in connection with

Marketing Programs that You participate in. The Net Amount will typically be based on the Base Rate, but Company reserves the right to increase or decrease the Base Rate in connection with

the Company's marketing and promotional efforts (including through Marketing Programs).</li>
<li>You will be able to increase Your portion of the fee by promoting Your Courses by using a

coupon code that You have created on Teachsity in accordance with the instructions found

at www.Teachsity.com/support. For those Users that input Your coupon code at the time

they enroll in Your Course, You will receive one-hundred percent (100%) of the Net

Amount, less a three percent (3%) administrative and handling fee.</li>
</ul>

<p>You authorize the Company to perform the appropriate calculations, deduct and retain the

transaction fee, and pay You the Net Amount as indicated above.</p>
<p>
Marketing Programs include, but are not limited to, Company's Deals Program and Marketing Boost

Program. You acknowledge that the amounts paid for Marketing Programs are not fixed, and the

Company has the sole discretion to decide those amounts and which Courses to offer as part of

Marketing Programs. Further, the Company does not guarantee any minimum level of success in

connection with any Marketing Programs, and its selection of Courses to include is not an

endorsement of those Courses, or of You. As part of Your participation in Marketing Programs,

You give Us permission to share Your Course, and information about You and the Course

with Teachsity employees and selected partners, for which you will not receive

compensation. If You do not wish to participate in Marketing Programs, log into Your account and

opt out of them.</p>
<p>

Users are entitled to refunds pursuant to Our general Terms of Service and You agree that

Company may deduct such refunds from subsequent amounts owed to You. Company will issue

remit to You any amounts remaining after the foregoing deductions and adjustments, in US

dollars, via check or PayPal. Payment will be made within forty-five (45) days of the end of the

month in which the fee for a Course was received. You are responsible for providing Company

with all identifying and tax information necessary for the payment of amounts due.</p>
                                            </div>
                                            <p></p>
                                            <p></p>
                                            <div class="form-item form-checkbox checkbox-style">
                                            <?= $form->field($model, 'agreement', [
                                                        'template' => "{error}{input}
                                                        <label for='agreement'>
                                                        <i class='icon icon_check fa fa-check'></i>
                                                        I have read and agree to the Teachsity Premium Tutors Terms & Conditions.</label>",
                                         ])->checkbox(["id"=>"agreement"],false)->label(false)?>

                                           <!--  <input type="hidden" name="terms" value="yes"/>

                                                    <input type="checkbox"  name="TcPremiumTutors[agreement]" id="accept">
                                                    <label for="accept">
                                                        <i class="icon-checkbox icon md-check-1"></i>
                                                        I have read and agree to the Teachsity Premium Tutors Terms & Conditions.
                                                    </label> -->
                                            </div>
                                        <div style="padding-top:20px;">
                                           <input type="submit" value="Save" class="btn btn-success">
                                      </div>

                                        <?php ActiveForm::end();?>


                                  </div>

<?php

    //$this->registerJS(" $('.marketing_terms').perfectScrollbar();",View::POS_END);
?>
