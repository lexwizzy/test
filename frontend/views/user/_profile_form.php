<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\User;
 use frontend\models\CourseCoupons;
 use marqu3s\summernote\Summernote;
 $this->registerJs('var state='.json_encode($user->state),View::POS_HEAD);

?>

            <?php $form=ActiveForm::begin()?>
             <?php if($this->context->action->id !="profile"):?>
              <div class="col-md-12  ">
                            <ul class="progress-indicator">
                             <!--  <li class="completed"> <span class="bubble"></span> Personal Info. </li> -->
                              <li class="completed"> <span class="bubble"></span></i> Personal Info. </li>
                              <li> <span class="bubble"></span>Profile Picture. </li>
                              <li > <span class="bubble"></span> Marketing Service. </li>
                              <li> <span class="bubble"></span> Payment Info. </li>
                            </ul>
                        </div>
               <?php endif;?>


                          <div class="col-md-12 field ">
                                     <?= $form->field($user,'firstname')->textInput(['placeholder'=>'First name'])->label(false)?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($user,'lastname')->textInput(['placeholder'=>'Last name'])->label(false)?>
                            </div>
                             <div class="col-md-12 field" style="text-align: left;padding-left: 15%;">
                            <div class="form-item form-radio radio-style">
                                    <?=$form->field($user, 'gender')->radioList(["1"=> 'Male', "2" => 'Female'],['item' =>                                                  function($index, $label, $name, $checked, $value) {
                                                if($checked)
                                                    $checked="checked";
                                    $return = '<input type="radio"  '.$checked.' id="'.$label.'" name="' .$name. '" value="'. $value.'" separator="">';
                                    $return .= '<label for="'.$label.'"><i class="icon-radio"></i> '.$label.'</label>';
                                    return $return;
                                    }])->label(false)?>


                                </div>
                            </div>
                             <div class="col-md-12 field">
                                <?= $form->field($user,'dob')->widget(yii\jui\DatePicker::className(),['dateFormat'=>'yyyy-MM-dd','clientOptions' => ['dateFormat'=>'yy,M d'],'options' => ['class' => 'form-control','placeholder'=>'Date of birth']])->label(false) ?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($user,'headline')->textInput(['placeholder'=>'Enter your headline'])->label(false)?>
                            </div>
                            <div class="col-md-12 field" style="text-align:left">
                                <?=$form->field($user,"country")->dropDownList([],['prompt'=>'Select country','data-country'=>$user->country,'class'=>"form-control bfh-countries",'id'=>'countries_states'])->label(false)?>

                            </div>
                            <div class="col-md-12 field" style="text-align:left">
                                <?=$form->field($user,"state")->dropDownList([],['options' => [$user->state => ['selected ' => true]],'class'=>"form-control bfh-states","data-country"=>"countries_states"])->label(false)?>

                            </div>
                            <div class="col-md-12 field" style="text-align:left">
                                <?= $form->field($user, 'aboutme')->widget(Summernote::className(),
                                 ['clientOptions' => ["height"=>"200",'toolbar'=>[['style',['bold','italic','underline']]]]])->label(false) ?>
                            </div>
                            
                           <div class="col-md-12 field text-left" ><h4 class="sm black bold">Links</h4></div>
                            <div class="col-md-12 field">
                                    <?= $form->field($link,'linkedin')->textInput(['placeholder'=>'Enter LinkedIn Link'])->label(false)?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($link,'github')->textInput(['placeholder'=>'Enter Github Link'])->label(false)?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($link,'twitter')->textInput(['placeholder'=>'Enter Twitter Link'])->label(false)?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($link,'google')->textInput(['placeholder'=>'Enter Google+ Link'])->label(false)?>
                            </div>
                            <div class="col-md-12 field">
                                    <?= $form->field($link,'facebook')->textInput(['placeholder'=>'Enter Facebook Link'])->label(false)?>
                            </div>
                            <div>
                                 <input type="submit" value="Save" class=" btn btn-success">
                            </div>

                            
                           

            <?php ActiveForm::end()?>

<?php
$this->registerJsFile(Url::base()."/js/library/jquery.magnific-popup.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile( Url::base().'/js/bootstrap-formhelpers.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
    $this->registerJsFile( Url::base().'/js/profile.js',['depends' => [\yii\web\JqueryAsset::className()]] );
?>
