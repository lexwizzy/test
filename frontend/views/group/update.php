<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\TcCourseGroup */

$this->title = 'Update Tc Course Group: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tc Course Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tc-course-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
