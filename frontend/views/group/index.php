<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TcGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tc Course Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tc-course-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tc Course Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'name',
            'no of users'
            'created date',
            'createdby',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
