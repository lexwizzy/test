<?php
  use frontend\models\User;
  use yii\widgets\ListView;
  use yii\widgets\Pjax;
  use yii\helpers\Url;


 ?>



 <div class="modal-content">
 <div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
     <h4 class="modal-title" id="myModalLabel">Group Users<span id="group-name"></span></h4>
 </div>
     <div class="modal-body" >
         <div class="row">

             <div class="col-md-12">
               <?php Pjax::begin(["id"=>"pjax_tutors",'enablePushState' => false]); ?>
                 <?=
                     ListView::widget([
                     'dataProvider' => $dataProvider,
                     'itemOptions'=>[
                       "tag"=>"li",
                       'class'=>"_items"
                     ],
                     'options' => [
                         'tag' => 'div',
                         'class' => 'view-user-ul',
                         'id' => '',
                             ],
                     'itemView'=>function ($model) {
                        $name =User::getName($model->userid);
                        $url = Url::toRoute(["/group/view","id"=>$model->groupid,"guser"=>$model->id,"delete"=>"yes"]);
                        return $name["firstname"]." ".$name["lastname"]."<a style='float:right' data-guserid='".$model->id."' href='".$url."'><span class='fa fa-trash'></span></a>";
                     },
                         'layout' => "{summary}{items}\n {pager}",
                     ]);


                   ?>
                 <?php Pjax::end(); ?>

             </div>

         </div>

        <div class="modal-footer">
          Remove a user from this group by clicking the red trash button next to the user name
         </div>
     </div>
   </div>
