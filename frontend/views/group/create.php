<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TcCourseGroup */

$this->title = 'Create Tc Course Group';
$this->params['breadcrumbs'][] = ['label' => 'Tc Course Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tc-course-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
