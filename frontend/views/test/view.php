<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Course */

$this->title = $model->course_id;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->course_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->course_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'course_id',
            'course_title',
            'course_category',
            'course_desc:ntext',
            'course_subcategory',
            'course_goal:ntext',
            'target_audience:ntext',
            'skill_level',
            'course_price',
            'amount',
            'language',
            'course_privacy',
            'course_banner',
            'promo_video',
            'promo_video_length',
            'promo_video_mime',
            'promo_video_thumbnail',
            'course_tools',
            'owner',
            'createdate',
            'status',
            'date_published',
            'slug',
            'currency',
        ],
    ]) ?>

</div>
