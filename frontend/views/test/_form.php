<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'course_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'course_category')->textInput() ?>

    <?= $form->field($model, 'course_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'course_subcategory')->textInput() ?>

    <?= $form->field($model, 'course_goal')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'target_audience')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'course_price')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'course_privacy')->textInput() ?>

    <?= $form->field($model, 'course_banner')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'promo_video')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'promo_video_length')->textInput() ?>

    <?= $form->field($model, 'promo_video_mime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'promo_video_thumbnail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'course_tools')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'owner')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'date_published')->textInput() ?>

    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
