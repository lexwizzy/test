<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CourseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'course_id') ?>

    <?= $form->field($model, 'course_title') ?>

    <?= $form->field($model, 'course_category') ?>

    <?= $form->field($model, 'course_desc') ?>

    <?= $form->field($model, 'course_subcategory') ?>

    <?php // echo $form->field($model, 'course_goal') ?>

    <?php // echo $form->field($model, 'target_audience') ?>

    <?php // echo $form->field($model, 'skill_level') ?>

    <?php // echo $form->field($model, 'course_price') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'language') ?>

    <?php // echo $form->field($model, 'course_privacy') ?>

    <?php // echo $form->field($model, 'course_banner') ?>

    <?php // echo $form->field($model, 'promo_video') ?>

    <?php // echo $form->field($model, 'promo_video_length') ?>

    <?php // echo $form->field($model, 'promo_video_mime') ?>

    <?php // echo $form->field($model, 'promo_video_thumbnail') ?>

    <?php // echo $form->field($model, 'course_tools') ?>

    <?php // echo $form->field($model, 'owner') ?>

    <?php // echo $form->field($model, 'createdate') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'date_published') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
