<?php
  use yii\helpers\Url;

 ?>
<!-- SUB BANNER -->

<style type="text/css">


    .course-top .container {
        padding-bottom: 10em;
        padding-top: 2em;
    }
</style>

    <!-- COURSE -->
            <div class="row pay-ment-success" >

                <div class="col-md-8  col-md-offset-2" style="padding-top: 1.5em;">

                    <div class="tabs-page" style="padding:20px">
                    <div class="alert alert-success"><h4 class="text-center title"> <i class="fa fa-check-circle fa-2x" style="color:green"></i> &nbsp;Your payment was successfull!</h4></div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="purchase-table">
                            <?php if(Yii::$app->session->hasFlash("error")):?>
                               <h4 class="text-center black bold">  <?=Yii::$app->session->getFlash("error");?> </h4>
                            <?php else:?>
                                <h4 class="text-center black bold">  <?=Yii::$app->session->getFlash("success");?> </h4>
                            <?php endif;?>
                            </div>
                            <div class="payment-methods row">
                                 <div class="col-md-12 text-center">
                                            <a class="btn btn-success"  href="<?=Url::toRoute(["/course/learn","slug"=>$course->slug])?>"> Start Learning</a>
                                 </div>
                            </div>
                         </div>


                        </div>
                    </div>
                </div>

    <!-- END / COURSE TOP -->
