<?php
  use yii\helpers\Url;

 ?>
<!-- SUB BANNER -->

<style type="text/css">


	.course-top .container {
    	padding-bottom: 10em;
    	padding-top: 2em;
	}
</style>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<?php
$script=<<<EOD

var options = {
    "key": "rzp_test_2IKJLNW0vMjOww",
    "amount": "$paiseAmount", // 2000 paise = INR 20
    "name": "Teachsity",
    "description": "Course purchase:$course->course_title",
    "image": "http://104.131.190.147/images/logo-with-text.gif",
    "handler": function (response){
         
        $.ajax({
            method:"post",
            data:{"paymentId":response.razorpay_payment_id,"coupon":"$coupon"},
            url:"/payment/razor-pay-capture",
            beforeSend:function(){
                $(".pop_loader").show();
            },
            success:function(data){
                console.log(data)
                $(".pop_loader").hide();
                 window.location.href= "/payment/payment-success?pm=rzpy"
            },
            error:function(){
                $(".pop_loader").hide();
                window.location.href= "/payment/payment-cancel"
            }
        })
    },
    "prefill": {
        "name": "Harshil Mathur",
        "email": "harshil@razorpay.com"
    },
    "notes": {
        "address": "Hello World"
    },
    "theme": {
        "color": "#ccc"
    }
};
var rzp1 = new Razorpay(options);



    $("#rzp-button1").click(function(e){
        rzp1.open();
        e.preventDefault();
    });

EOD;

$this->registerJs($script);
?>

    <!-- COURSE -->
    <section class="course-top">
        <div class="container">
            <div class="row">
              <?php if(Yii::$app->session->hasFlash("error")):?>
                      <div class="col-md-12">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <div class="alert alert-danger text-center">  <?=Yii::$app->session->getFlash("error");?> </div>
                       </div>
               <?php endif;?>
                <div class="col-md-8  col-md-offset-2" style="padding-top: 1.5em;">

                    <div class="tabs-page">
                    <h5 class="text-center black bold">Review Purchase</h5>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="purchase-table">
                            	<table class="table table-strip">
                            		<tr>
                            			<td colspan="3"><?=$course->course_title?></td>
                            			<td><?=$amount?></td>
                            		</tr>
                                <?php if(!empty($coupon)):?>
                                  <tr>
                              			<td colspan="3"><?=$rate?>% discount with coupon (<?=$coupon?>)</td>
                              			<td>$<?=$discount?></td>
                              		</tr>
                                <?php endif?>
                            		<!-- <tr>
                            			<td colspan="3">Coupon 449823</td>
                            			<td>$19</td>
                            		</tr> -->
                            		<tr>
                            			<td colspan="3">Total</td>
                            			<td><?=$total_amount?></td>
                            		</tr>
                            	</table>
                            </div>
                            <div class="payment-methods row">
															<div class="col-md-6 text-center">
																			<a class="btn btn-sky"  href="<?=Url::toRoute(["make-paypal-payment","tid"=>$course->course_id,"coupon"=>$coupon])?>"> Pay with Paypal</a>
															</div>

                                                <div class="col-md-6 text-center">
                                                    <button id="rzp-button1">Pay</button>
                                                                    
                                                </div>

                            </div>
                         </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END / COURSE TOP -->
