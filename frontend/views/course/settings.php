<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\bootstrap\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\CourseCoupons;
 use yii\bootstrap\Button;

$this->registerJs('var courseid = '.json_encode($course->course_id),View::POS_BEGIN, 'my-options');
?>
<div class="main-content-title row  text-center"><h3 class="title">Google  Tracking</h3></div>
    <div class="main-content-body row text-center">
      <!-- <h5>Use Google tracking to track your course performace.</h5> -->
        <?php $form=ActiveForm::begin(["layout"=>"horizontal",'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ]])?>
             <?= $form->field($setting,"google_analytics")->textInput(["rel"=>"txtTooltip",'title'=>'Google Analytics is a free tool offerred by Google which you can yse to track your courses. You can track which email campaign, referrals, and even search engine lead a student to your course landing page. Teachsity allows the use of Google Analytics to track your course sales.
<p>Create your google analytics account on <a href="http://www.google.com/analytics">http://www.google.com/analytics</a></p>','data-html'=>"true",'data-placement'=>"right"])?>
             <?= $form->field($setting,"google_tag_manager")->textInput(["rel"=>"txtTooltip",'title'=>'Google Tag Manager is a container where you can setup different tracking codes from third parties. You can track which email campaign, referrals, and even search engine lead a student to your course landing page.
<p>Create your google analytics account on <a href="http://www.google.com/tagmanager">http://www.google.com/tagmanager</a></p>','data-html'=>"true",'data-placement'=>"right"])?>
             <?= $form->field($setting,"adwords_conversionid")->textInput(["rel"=>"txtTooltip",'title'=>'Google Adwords is a Google Advertising tool where you can create and manage your advertising campaign. To track the performance of your campaigns with respect to your courses on Teachsity, paste the conversion ID and conversion label given to you by Google after creating a conversion on Adwords.

<p>Create your Google Adwords account on <a href="http://www.google.com/adwords">http://www.google.com/adwords</a></p>','data-html'=>"true",'data-placement'=>"right"])?>
             <?= $form->field($setting,"adwords_lpcl")->textInput(['autocomplete'=>"off"])?>
             <?= $form->field($setting,"adwords_spcl")->textInput(['autocomplete'=>"off"])?>
             <?=Button::widget(["label"=>"Save","options"=>["class"=>"btn btn-success","type"=>"submit"]])?>
        <?php ActiveForm::end()?>
    </div>
