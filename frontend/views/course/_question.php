<?php
    use frontend\models\User;
    use yii\helpers\Url;
    use yii\web\View;
    use yii\widgets\ActiveForm;

    $this->registerJs('var userid='.json_encode(Yii::$app->user->id),View::POS_HEAD);
    //$this->registerJs('var courseid='.json_encode($course->course_id),View::POS_HEAD);
    $this->registerJs('var baseUrl='.json_encode(Url::base()),View::POS_HEAD);
    $this->registerJs('var quizid='.json_encode($quiz->q_id),View::POS_HEAD);
    $this->registerJs('var timer_counter2='.json_encode($timelimit),View::POS_HEAD);
      // 
      // $this->registerJs('var timer_counter1='.json_encode(date("Y/m/d H:i:s", time())),View::POS_HEAD);
    $this->registerJs('var timer_counter='.json_encode($timelimit),View::POS_HEAD);

?>
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/jquery.countdown.css">
<script type="text/x-handlebars-template" id="questions-template">

                            <h4 class="title">Question {{counter}} - {{title}}</h4>
                            <p>{{description}}</p>
                            <div class="answer">
                                <!-- <h4 class="title">Answer</h4> -->
                                <form method="get" id="ans-form" onsubmit="return false" action="saveanswers">
                                <ul class="answer-list">

                                  {{#each answers}}
                                    <li>
                                        {{#if ../multiple }}
                                        <input type="checkbox" name="checkbox[]" value="{{id}}" id="checkbox-{{id}}">
                                        <label for="checkbox-{{id}}">
                                            <i class="icon icon_check fa fa-check"></i>
                                            {{answer}}
                                        </label>
                                        {{else}}

                                          <input type="radio" name="radio-ans" value="{{id}}" id="radio-{{id}}">
                                          <label for="radio-{{id}}">
                                              <i class="icon icon_radio"></i>
                                              {{answer}}
                                          </label>
                                        {{/if}}
                                    </li>
                                {{/each}}

                                </ul>
                              </form>
                            </div>
                            <a  id="previous" class="btn btn-default pointer" data-questiontype="{{type}}" data-questionid="{{questionid}}" data-counter="{{counter}}" data-quizid="<?=$quiz->q_id?>">Previous Question</a>
                            {{#if last}}
                              <a id="finish" class="btn btn-default pointer" data-questiontype="{{type}}" data-questionid="{{questionid}}" data-counter="{{counter}}" data-quizid="<?=$quiz->q_id?>">Finish</a>
                            {{else}}
                                <a  id="next" class="btn btn-default pointer"  data-questiontype="{{type}}"  data-questionid="{{questionid}}" data-counter="{{counter}}" data-quizid="<?=$quiz->q_id?>">Next question</a>
                            {{/if}}

                            <?php  if($quiz->islimitedbytime!="No"):?>
                              <!-- <div style="padding: 15px;font-size: 20px;" class="text-right"><span>Remaining Time &nbsp;&nbsp;<i class="fa icon fa-clock-o"></i>&nbsp;&nbsp;</span><span id="clock"></span></div> -->
                            <?php endif;?>


</script>
<script type="text/x-handlebars-template" id="result-template">
     <h4 class="sm">You got {{correct}}/{{total}} Questions right</h4>
                            <table class="table-question">
                                <tbody>
                                  {{#each answers}}
                                    <tr>
                                        <td><i class="icon {{class}}"></i></td>
                                        <td class="td-quest">

                                            {{counter}}. {{question}}

                                        </td>
                                    </tr>
                                    {{/each}}
                                </tbody>
                            </table>
</script>
<section id="quizz-intro-section" class=" learn-section" style="min-height: 947px;">
        <div class="container">
            <div class="title-ct">
                <h3 class="title"><strong>Quizz <?=$quiz->counter?>: </strong><?=$quiz->q_name?> </h3>
                <!--<div class="tt-right">
                    <a href="#" class="skip"><i class="icon md-arrow-right"></i>Skip quizz</a>
                </div>-->
            </div>

            <div class="question-content-wrap">
              <?php  if($quiz->islimitedbytime!="No"):?>
                <div style="padding: 15px;font-size: 20px; padding-bottom:0" class="text-right"><span>Remaining Time &nbsp;&nbsp;<i class="fa icon fa-clock-o"></i>&nbsp;&nbsp;</span><span id="clock"></span></div>
              <?php endif;?>
                <div class="question-content">
                  <div class="text-center">  <i class="fa fa-circle-o-notch fa-spin fa-2x"></i></div>
                </div>
                <?php  //if($quiz->islimitedbytime!="No"):?>
                  <!-- <div style="padding: 15px;font-size: 20px;" class="text-right"><span>Remaining Time &nbsp;&nbsp;<i class="fa icon fa-clock-o"></i>&nbsp;&nbsp;</span><span id="clock"></span></div> -->
                <?php //endif;?>
            </div>
</section>

<?php $this->registerJsFile( Url::base().'/js/library/handlebars.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php //$this->registerJsFile( Url::base().'/js/library/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/jquery.plugin.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/quiz.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
