<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use frontend\models\Course;
use kartik\popover\PopoverX;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');

$content='<ul class="category-ul">';
    foreach(\frontend\models\CourseCategory::find()->with("courseSubcategories")->each() as $c):
     $content.='<li><a href="'.
      Url::toRoute([
        '/course/coursecategory',
        'category' => str_replace(" ","-",strtolower($c->category))]).'">
        <i class="'.$c->icon.'"></i>&nbsp;&nbsp;'
        .str_replace("and","&",$c->category).'</a></li>';

    endforeach;
$content.="</ul>";
$template = '<div><a style="line-height:30px" href="'.Url::home(true).'course/search?q={{value}}&page=searchpage">{{value}}</a></div>';
?>
<style>

    .mc-item{

    }
    .full-width-course-div .coursebox{
       width:270px;
    }

</style>
<div class="full-width-course-div">

<section class="page-control">
        <div class="container">
           <div class="page-info row">
             <div class="col-md-2">
             <?=

             PopoverX::widget([
              // 'header' => 'Hello world',
               'placement' => PopoverX::ALIGN_BOTTOM,
               'content' => $content,
            //   'footer' => Html::button('Submit', ['class'=>'btn btn-sm btn-primary']),
               'toggleButton' => ['label'=>'<i class="fa fa-bars"></i>&nbsp;&nbsp; Categories',"tag"=>"a","style"=>"color:#333", 'class'=>''],
            ]);
             ?>
             </div>
              <div class="col-md-8" style="padding:10px">
                <form action="<?=Url::toRoute("/course/search")?>">
                  <div class="input-group">
                      <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                      <input type="hidden" name="page" value="searchpage" />
                      <?=
                             kartik\typeahead\Typeahead::widget([
                                'name' => 'q',
                                'options' => ['placeholder' => 'Find a course on nearly anything.'],
                                'pluginOptions' => ['highlight'=>true],
                                'dataset' => [
                                    [
                                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                        'display' => 'value',
                                        //'prefetch' => Url::home("true") . '/courses/autosuggestjson',
                                        'remote' => [
                                            'url' => Url::toRoute(['course/autosuggestjson']) . '?q=%QUERY',
                                            'wildcard' => '%QUERY'
                                        ],
                                        'limit'=>10,
                                          'templates' => [
                                               // 'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                                'suggestion' => new \yii\web\JsExpression("Handlebars.compile('{$template}')")
                                            ]
                                    ]
                                ]
                            ]);
                        ?>
                  <!--  <input type="text" name="q" id="search_page_search_bar" class="form-control" placeholder="What do you want to learn?">-->
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </form>

              </div>

            </div>
            <!-- <div class="page-view">

                <div class="mc-select">
                    <select class="select" name="" id="all-categories" style="z-index: 10; opacity: 0;">
                        <option value="">All level</option>
                        <option value="">2</option>
                    </select><span class="select">All level</span><i class="fa fa-angle-down"></i>
                </div>
            </div> -->
        </div>
    </section>
<section id="categories-content" class="categories-content">
        <div class="container">

            <div class="row">
                <div class="col-md-9 col-md-push-3 ">

                    <div class="content grid">
                <?=
                    ListView::widget([
                    'dataProvider' => $pages,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'row',
                        'id' => 'list-wrapper',
                            ],
                    'itemView'=>"_search_list_item",
                        'layout' => "{summary}\n{items}\n{pager}",
                    ]);


                ?>
                    </div>
                </div>
              </div.
                <!-- SIDEBAR CATEGORIES -->
                <div class="col-md-3 col-md-pull-9">
                    <aside class="sidebar-categories">
                        <div class="inner">

                            <!-- WIDGET TOP -->
                            <div class="widget">
                                <ul class="list-style-block">
                                    <li class="current"><a href="#">Featured</a></li>
                                    <li><a href="#">Free</a></li>
                                    <li><a href="#">Paid</a></li>
                                </ul>
                            </div>
                            <!-- END / WIDGET TOP -->

                            <!-- WIDGET CATEGORIES -->
                            <div class="widget widget_categories">
                                <ul class="list-style-block">
                                  <li><a href="#">English</a></li>
                                  <li><a href="#">French</a></li>
                                  <li><a href="#">Spanish</a></li>
                                  <li><a href="#">Italian</a></li>
                                  <li><a href="#">Chinese</a></li>
                                  <li><a href="#">Hindi </a></li>
                                </ul>
                            </div>
                            <!-- END / WIDGET CATEGORIES -->
                            <!-- WIDGET CATEGORIES -->
                            <div class="widget">
                                <ul class="list-style-block">
                                  <?php foreach(\frontend\models\SkillLevel::find()->all() as $skill):?>
                                    <li><a href="#"><?= $skill->skill_level?></a></li>
                                  <?php endforeach;?>
                                </ul>
                            </div>
                            <!-- END / WIDGET CATEGORIES -->
                            <!-- BANNER ADS -->
                            <div class="mc-banner">
                                <a href="#"><img src="<?=Url::base()?>/images/banner-ads-1.jpg" alt=""></a>
                            </div>
                            <!-- END / BANNER ADS -->

                            <!-- BANNER ADS -->
                            <div class="mc-banner">
                                <a href="#"><img src="<?=Url::base()?>/images/banner-ads-2.jpg" alt=""></a>
                            </div>
                            <!-- END / BANNER ADS -->
                        </div>
                    </aside>
                </div>
                <!-- END / SIDEBAR CATEGORIES -->

            </div>
        </div>
    </section>
</div>
