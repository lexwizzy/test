<?php
    use frontend\models\User;
    use yii\helpers\Url;
    use yii\web\View;
    use yii\widgets\ActiveForm;

    // $this->registerJs('var userid='.json_encode(Yii::$app->user->id),View::POS_HEAD);
    // //$this->registerJs('var courseid='.json_encode($course->course_id),View::POS_HEAD);
    // $this->registerJs('var baseUrl='.json_encode(Url::base()),View::POS_HEAD);
    // $this->registerJs('var quizid='.json_encode($quiz->q_id),View::POS_HEAD);
    // $this->registerJs('var timer_counter='.json_encode(date("Y/m/d H:i:s", time()+$timelimit)),View::POS_HEAD);
$script=<<<EOD
$("#start-quiz").click(function(){
    //console.log(parent.document.getElementById("displayboard"))
    //parent.document.getElementById("displayboard").src=baseUrl+"/course/questions-display?quizid="+$quiz->q_id+"#counter=1";
    //setandreload(baseUrl+"/course/questions-display?quizid="+$quiz->q_id+"#counter=1");
    window.location.href=baseUrl+"/course/questions-display?quizid="+$quiz->q_id+"#counter=1";
    // quizid=$(this).attr("data-quizid");
    // $.get(baseUrl+'/course/quizquestion',{'quizid':quizid,"counter":1},function(data){
    //     source   = $("#questions-template").html();
    //     var template = Handlebars.compile(source);
    //     data=JSON.parse(data);
    //     var html    = template(data);
    //     $(".question-content").html(html);
    //      $('#clock').countdown("resume")
    //
    // })

})
EOD;

$this->registerJs($script);
?>
<section id="quizz-intro-section" class=" learn-section" style="min-height: 947px;">
        <div class="container">


            <div class="title-ct">
                <h3 class="title"><strong>Quizz <?=$quiz->counter?>: </strong><?=$quiz->q_name?> </h3>
                <!--<div class="tt-right">
                    <a href="#" class="skip"><i class="icon md-arrow-right"></i>Skip quizz</a>
                </div>-->
            </div>
            <div class="question-content-wrap">
                <div class="row">
                    <div class="col-md-12" >
                        <div class="question-content">
                       <h4 class="title" id ="quizheader" class="md">Introduction</h4>
                            <p><?= $quiz->q_description?></p>
                            <div class="form-action">
                               <div class="text-center"><input type="submit" value="Start Quizz" class="btn btn-primary" id="start-quiz"></div>
                               <div class="text-center" style="padding-top:10px"><span class="total-time"><strong>Total time : <?= $quiz->islimitedbytime=="No"?"No time limit ": $quiz->timelimit." minutes"?> </strong></span></div>
                            </div>


                        </div>
                        <?php  if($quiz->islimitedbytime!="No"):?>
                          <!-- <div style="padding: 15px;font-size: 20px;" class="text-right"><span>Remaining Time &nbsp;&nbsp;<i class="fa icon fa-clock-o"></i>&nbsp;&nbsp;</span><span id="clock"></span></div> -->
                        <?php endif;?>
                    </div>
                </div>
            </div>



        </div>

    </section>

<?php //$this->registerJsFile( Url::base().'/js/library/handlebars.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php //$this->registerJsFile( Url::base().'/js/library/jquery.countdown.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php //$this->registerJsFile( Url::base().'/js/quiz.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
