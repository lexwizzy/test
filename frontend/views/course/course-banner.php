<?php
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\BootstrapAsset;
use frontend\models\Course;


$this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);
$this->registerJs('var baseUrl = '.json_encode(Url::base()),View::POS_BEGIN, 'my-options');
 ?>
<style>
#video-display{
  width: 100%;
  height: 100%;
  background: #bbb;
  padding: 10px;
}
#promo_video{
  width: 100%;
}
#video-text-content{
    background: #fff;
    padding: 20px;
    color: #333;
}
#file-input-upload-div{
  margin-top: 20px;
}
.no-video{
    /* min-width: 30%; */
    min-height: 200px;
    background: #eee;
    padding:80px;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/fileinput.min.css">
<div class="main-content-title row  text-center"><h3 class="title">Course Banner</h3></div>
    <div class="main-content-body row text-center">

      <div class="col-md-8 col-md-offset-2 col-md-offset-right-2">
            <div id="video-display">
              <div id="banner-image">
                      <img class="img-responsive" src="<?=$bannerpath?>"  alt="<?=$course->course_title?>"/>
              </div>

              <div id="video-text-content">
                  Upload a good course banner as per Teachsity specification to increase a chance of student enrolling for this course.

                  <p><a href="http://www.support.teachsity.com">See format specification for course banner</a></p>
              </div>

            </div>
      </div>

      <div id="file-input-upload-div" class="col-md-8 col-md-offset-2 col-md-offset-right-2">
        <input type="file" class="file-loading" id="course-image"  name="TcUploadedContent[single_upload]">
        <div class="form-item">
              <span>maximum image size is 1GB. Dimesion 480x270. File types 'jpg','png','gif'</span>
        </div>
      </div>
    </div>

<?php
$this->registerJsFile( Url::base().'/js/fileinput.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
$script=<<<EOD
$("#course-image").fileinput({
    previewFileType:"image",
    showCaption:true,
    showPreview :false,
    showRemove : true,
    showUpload: true,
    overwriteInitial:true,
    minImageWidth: 480,
    maxImageWidth: 480,
    maxImageHeight: 270,
    browseClass:"btn btn-danger",
    minImageHeight: 270,
    browseLabel: "Pick Image",
    browseIcon: "<i class=\"fa fa-picture-o\"></i> ",
    uploadUrl: baseUrl+"/course/coursebanner", // server upload action,
    deleteUrl:baseUrl+'/course/deletebanner',
    deleteExtraData: function() {
        var obj = {};

          obj['courseid']=$course->course_id;
        return obj;
    },
    uploadExtraData:function() {
        var obj = {};

          obj['courseid']=$course->course_id;
        return obj;
    },
    allowedFileExtensions:['jpg','png','gif'],
    removeClass: "btn btn-danger",
    removeLabel: " Delete",
    removeIcon: "<i class=\"fa fa-trash\"></i>",
    uploadClass: "btn btn-info",
    uploadLabel: "Upload",
    uploadIcon: "<i class=\"icon md-upload\"></i>",

});
EOD;
$this->registerJS($script);
 ?>
