<?php
    use frontend\models\User;
    use frontend\models\CourseLessons;
    use frontend\models\Course;
    use yii\helpers\Url;
    use yii\web\View;
    use yii\helpers\ArrayHelper;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\BootstrapAsset;
    use marqu3s\summernote\Summernote;
    use frontend\models\LessonContent;
    use frontend\models\Enrollment;

    $sec = LessonContent::getCourseVideoLength($course->course_id);
    $pagecount=LessonContent::getCoursePptPages($course->course_id);
    $counter=1;
    if($sec >= 3600){
        $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";

    }
    else if($sec > 0 && $sec <3600){
        $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
    }
    $course_content_length ='<p class="total"><a  class="pointer"><span><img src="'.Url::base().'/images/icon/video.png"></span> Total Video</a></p>
                          <p class="totale_right">'.$sec.'</p>';
    $name= User::getName($course->owner);
  //  $this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);
    $this->registerJs('var userid='.json_encode(Yii::$app->user->id),View::POS_HEAD);
    $this->registerJs('var courseid='.json_encode($course->course_id),View::POS_HEAD);
    $this->registerJs('var baseUrl='.json_encode(Url::base()),View::POS_HEAD);
?>
<style>
  .content_bg{
    margin-top: 0px;
    background: #000;
  }
  .wrap{
    max-width: none !important;
  }
</style>

<div class="top_row">

    <div class="top-nav">
        <div class="land_top_left">
            <a href="<?=Url::toRoute(["/course/learn","slug"=>$course->slug])?>" class="pointer"><img src="<?=Url::base()?>/images/back_button.jpg"></a>
        </div>
        <ul class="top-nav-list">
            <li class="prev-course"><a style="cursor:pointer" id="previous"><p class="close_icon"><img src="<?=Url::base()?>/images/icon/tab_arrow_l.png"></p></a></li>
            <li class="next-course"><a style="cursor:pointer" id="next"><p class="close_icon"><img src="<?=Url::base()?>/images/icon/tab_arrow_r.png"></p></a></li>


            <li class="outline-learn">
                <a class="pointer">
                <p class="fa_icon"><img class="icon1" src="<?=Url::base()?>/images/icon/menu_dot.png"></p>
                <p class="fa_icon2">Menu</p>
                </a>
                <div class="list-item-body outline-learn-body ps-container ps-active-y">
                <div class="right_slide_bg">
		<!-- Accordion begin -->
    <!-- Accordion begin -->
		<div class="outline_accordion">

			<!-- Section  -->
      <?php foreach(Course::getSections($course->course_id) as $section):?>
			<div class="accordion_in acc_active <?= $counter==1 ? " acc_active":""?>">
        <?php ++$counter?>
				<div class="acc_head">Section <?=$section->counter?>: <?=$section->part_name?></div>
				<div class="acc_content">
                  <ul class="modifi_list">
                  <?php $outline = \frontend\models\CourseOutlineOrder::find()->where(['and',["section"=>$section->part_id],['courseid'=>$course->course_id]])->orderBy("counter asc")->all();?>
                   <?php foreach($outline as $order):?>
                     <?php $units= CourseLessons::findOne([$order->itemid]);?>
                    <?php if($order->type=="lesson"):?>
                      <?php
                          $main_content= CourseLessons::getUnitMainContent($units->lesson_id);
                          if($main_content!=null){
                            if($main_content["type"]=="video"){
                               $d=$main_content["video_length"];
                               $icon="lesson_icon2.png";
                            }else if($main_content["type"]=="pdf" ){
                              $d=$main_content["page_no"]." Pages";
                              $icon="lesson_icon.png";
                            }
                            else {
                              $icon="lesson_icon.png";
                            }
                             //  $d=  ?gmdate("i:s",$main_content->video_length):$main_content->no_of_pages." Pages";
                           }
                       ?>
                        <li class="section-li-item" data-section="<?= $section->part_id?>" data-itemid="<?= $units->lesson_id?>" data-type="lesson">
                          <a data-outlineno="<?=$units->outlineno?>" data-type="lesson" data-itemid="<?= $units->lesson_id?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/lesson/".$units->lesson_id])?>" class="section-item">
                            <p class="circle_active ">
                          <p class="circle_active">
                            <img src="<?=Url::base()?>/images/icon/<?=\frontend\models\CourseLearnTracking::find()->where(["lesson_type"=>"lesson","courseid"=>$course->course_id,'userid'=>Yii::$app->user->id,"section"=>$units->part_id,"lesson"=>$units->lesson_id])->one() !=null? "circle_red.png": "circle_white.png"?> ">
                           <span class="circle_img"><img src="<?=Url::base()?>/images/icon/<?=$icon?>"></p>
                            Lecture <?=$order->counter?>: <?=$units->lesson_title?>
                         <span class="right_txt"><?=$d?></span></a></li>
                    <?php else :?>
                      <?php $q = \frontend\models\Quiz::findOne([$order->itemid]);?>
                       <li class="section-li-item"  data-section="<?= $section->part_id?>" data-itemid="<?= $q->q_id?>" data-type="quiz" >
                         <a  class="section-item" data-outlineno="<?=$q->outlineno?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/quiz/".$q->q_id])?>" data-section="<?= $section->part_id?>" data-itemid="<?= $q->q_id?>" data-type="quiz">
                           <p class="circle_active section-item">
                         <p class="circle_active">
                            <img src="<?=Url::base()?>/images/icon/<?=\frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$q->q_id])->one() !=null? "circle_red.png": "circle_white.png"?>">
                             <span class="circle_img"><img src="<?=Url::base()?>/images/icon/quiz_icon.png"></p>
                               Quiz <?=$q->counter?>: <?=$q->q_name?>
                            <span class="right_txt"><?=\frontend\models\Quiz::getNumberOfQuestion($q->q_id)?> questions</span></a></li>
                     <?php endif; ?>
                  <?php endforeach;?>
                  </ul>
				</div>
			</div>
      <?php endforeach;?>

		</div>
		<!-- Accordion end -->
		<!-- Accordion end -->
                </div>
                </div>
            </li>

            <!-- DISCUSSION -->
            <li class="discussion-learn">
                <a  class="pointer">
                <p class="fa_icon"><img class="icon2" src="<?=Url::base()?>/images/icon/tab_comment.png"></p>
                <p class="fa_icon2">Comment</p></a>
                <div class="list-item-body discussion-learn-body ps-container ps-active-y slide_bg">
                	<div class="right_slide_bg">
<div class="detailed-course-list">

      <?php $form=ActiveForm::begin(['id'=>'discussionform','options'=>['onsubmit'=>"return false"]]);?>
            <div class="text-title">
                <?= $form->field($discussion,"topic")->textInput(['placeholder'=>"Enter discussion topic"])->label(false)?>
            </div>

            <div class="post-editor ">


                 <?= $form->field($discussion,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
                <?= $form->field($discussion,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
                 <?= $form->field($discussion, 'comment')->widget(Summernote::className(),
        ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
            </div>

            <div class="form-submit">
                <input type="submit" value="Post Discussion" id="save-discussion" class=" course-learn-more review-input-box">
            </div>
            <?php $form=ActiveForm::end();?>

        <div class="post-dicuss-status">
        <p class="discuss-text"><?= \frontend\models\Discussions::getCount($course->course_id)?> Topics</p>
        <hr>

        </div>
        <div class="post-dicuss-status">
           <?= \frontend\models\Discussions::displayDiscussion($course->course_id)?>

        </div>

</div>
                    </div>
                </div>
             </li>

			<li class="note-learn">
                <a  class="pointer">
                <p class="fa_icon"><img class="icon3" src="<?=Url::base()?>/images/icon/tab_note.png"></p>
                <p class="fa_icon2">Note</p>
                </a>
                <div class="list-item-body note-learn-body ps-container ps-active-y">
                    <div class="right_slide_bg">
<div class="note-course-details">
     <div class="note-title">
        <h5 style="float:left;">Note</h5>
            <a  class="pointer" id="savenote" style="float:right;"><i class="icon fa fa-floppy-o"></i></a>
    </div>
</div>
     <div contenteditable="true" class="note-body">
       <?=$note?>
     </div>
                    </div>
           		</div>
            </li>


             <li class="backpage">
                <a href="<?=Url::toRoute(["/course/learn","slug"=>$course->slug])?>" title="Back to course dashboard" href="#"><p class="close_icon"><img src="<?=Url::base()?>/images/icon/tab_close.png"></p></a>
                  	close<br>
                    close<br>
                    close<br>
             </li>
             <!-- END / LIST ITEM -->

		</ul>
	</div>
</div>



<!---content section-->
<div class="content_bg">
<div class="wrap">
		<div class="body_content margin_space2">
            <div class="container-fluid">
            	<div class="row landing_page_row" style="background:#000">
                    <div class="landing_video_bg">
                    	<!-- <p class="landing_title"><?php //$course->course_title?></p> -->
                        <div style=""class="video_section embed-responsive embed-responsive-16by9">
                          <iframe  name="displayboard" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" id="displayboard" height="100%" width="100%" class="embed-responsive-item" scrolling="no" frameborder="0" ></iframe>

                        </div>
                    </div>
                </div>
            </div>

        </div>
</div>
</div>
<!---content section end-->
<?php
$script=<<<EOD
jQuery(document).ready(function($){
  $(".outline_accordion").smk_Accordion({
    closeAble: true, //boolean
  });
});
EOD;

$this->registerJs($script);
 ?>
<?php $this->registerJsFile( Url::base().'/js/smk-accordion.js',['depends' => [\yii\web\JqueryAsset::className()]]);?>
<?php $this->registerJsFile( Url::base().'/js/classroom.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/spin.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/ladda.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/courselearn.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]]); ?>
