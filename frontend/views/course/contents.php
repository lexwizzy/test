<?php
    use yii\helpers\Url;
    use yii\web\View;
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Button;
    use frontend\models\CourseParts;
    use frontend\models\QuestionType;
    use frontend\models\Quiz;
    use frontend\models\CourseLessons;
    use Zelenin\yii\widgets\Summernote\Summernote;

$script=<<<EOD
    var requires=[];
    var name="coursecontent";
EOD;
$this->registerJS($script,View::POS_HEAD);
$this->registerJS("var userid=".json_encode(Yii::$app->user->id),View::POS_HEAD);
$this->registerJsFile(Url::base()."/js/angular.js",['position'=>View::POS_BEGIN,'depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base()."/js/contents_app.js",['position'=>View::POS_BEGIN,'depends'=>[\yii\web\JqueryAsset::className()]]);
?>
<!-- <div class="loader"></div> -->
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/fileinput.min.css">
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/assets/95c33da1/themes/smoothness/jquery-ui.min.css">



<div class="main-content-title row  text-center"><h3 class="title">Course Curriculum</h3></div>
    <div class="main-content-body row text-center">

        <section id="create-course-section"  data-ng-controller="content-ctrler" data-ng-init="loadOutline('<?=Url::toRoute(["/course/outlinejson","slug"=>$course->slug])?>')">
          <div class="current-wrapper">
                                 <h4 class="sm title">Current outline</h4>
                                 <ul class="current-outline">
                                     <li><span><?=CourseParts::getPartCount($course->course_id)?></span>sections</li>
                                     <li><span><?=Quiz::getQuizCount($course->course_id)?></span>quizzes</li>
                                     <li><span><?=CourseLessons::getLessonCount($course->course_id)?></span>lessons</li>
                                 </ul>
          </div>
          <div class="sections">
            <div ng-if="data.dataLoading" class="loading">
                <i class="fa fa-circle-o-notch fa-spin fa-2x"></i>
          </div>
               <ng-include src="'<?=Url::base()?>/tpl/course_content_tpl.html'"></ng-include>
          </div>
          <div class="add-section">
            <a data-target="#addsection" class="btn btn-black" data-toggle="modal"><i class="fa fa-plus"></i> Add Section</a> <i data-placement="right" data-toggle="tooltip" data-html="true"  title="Arrange your course content in sections. e.g Just as a book is arranged in chapters and lessons." class="fa fa-question-circle"></i>
            <!-- New unit -->
            <div id="newunit_div"  class="hide" >
                  <div class="new-unit-table">
                    <?php $form_lesson= ActiveForm::begin(
                    ['action'=>'newunit',
                    'options'=>['id'=>'newunitform','onsubmit'=>"return false"],
                    "layout"=>"horizontal",
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-12 col-xs-12 col-md-12 text-left',
                            'wrapper'=>'col-md-12 col-xs-12 col-sm-12',
                            'error' => '',
                            'hint' => '',
                        ]
                      ]
                    ])
                    ?>
                    <input type="hidden" value="" name="lesson_id" class="lesson_id_hidden"/>
                    <input type="hidden" value="<?=$course->course_id?>" name="CourseLessons[courseid]" id="courselessons-courseid"/>
                    <?=$form_lesson->field($lesson,'lesson_title')->textInput(['maxlength'=>70])?>
                    <?=$form_lesson->field($lesson,'lesson_description')->textArea(['maxlength'=>200])?>
                  <?=Button::widget(["label"=>"Save Lesson","options"=>['type'=>'submit','class'=>"save-unit btn btn-success"]])?>
                  <?=Button::widget(["label"=>"Cancel","options"=>['type'=>'button','class'=>"cancel-unit btn btn-default"]])?>
                   <?php ActiveForm::end()?>
                  </div>
            </div>
            <!-- End new unit -->
            <!-- New Quiz -->
                <div id="newquiz-div" class="hidden">
                    <div class="new-unit-table">
                      <?php $form = ActiveForm::begin([
                        'id'=>'newquiz-form',
                        'options'=>['onsubmit'=>'return false'],
                        "layout"=>"horizontal",
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-12 col-xs-12 col-md-12 text-left',
                                'wrapper'=>'col-md-12 col-xs-12 col-sm-12',
                                'error' => '',
                                'hint' => '',
                            ]
                          ]
                          ])?>
                      <input type="hidden" value="" name="Quiz[partid]" class="quiz_part_id"/>
                      <input type="hidden" value="<?=$course->course_id?>" name="Quiz[courseid]" class="quiz_courseid"/>
                      <?=$form->field($quiz,'q_name')->textInput()?>
                      <?=$form->field($quiz,'q_description')->textArea()?>
                      <?=Button::widget(["label"=>"Save Quiz","options"=>['type'=>'submit','class'=>"save-quiz btn btn-success"]])?>
                      <?=Button::widget(["label"=>"Cancel","options"=>['type'=>'button','class'=>"cancel-quiz btn btn-default"]])?>

                     <?php ActiveForm::end()?>
                    </div>
                 </div>
            <!-- END QUIZ -->
          </div>
          <!-- New unit content-->
          <div id="add-content-table" class="hide">
            <div class="row">
              <div class="tb-upload-content video-content hide">
                    <div class="col-md-12 video-upload" >

                    </div>
              </div>
              <div class="tb-upload-content ppt-content hide">
                    <div class="col-md-12 ppt-upload">
                    </div>
              </div>
              <div class="tb-upload-content reference-content hide">
                        <div class="col-md-12 downloadable-upload">
                        </div>
              </div>
              <div class="tb-upload-content text-content hide">
                  <div class="alert alert-danger hide">Error occurred</div>
                      <?php $form =ActiveForm::begin([
                        'options'=>
                        ['onsubmit'=>'return false'],
                        "layout"=>"horizontal",
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-12 col-xs-12 col-md-12 text-left',
                                'wrapper'=>'col-md-12 col-xs-12 col-sm-12',
                                'error' => '',
                                'hint' => '',
                            ]
                          ]
                        ])?>
                          <input type="hidden" id ="lesson_no" name="lesson_no"/>
                                <?= $form->field($text,'content_defaultname')->textInput(['placeholder'=>'Enter title','style'=>"width:100%"])?>
                                <input type="hidden" value="<?=$course->course_id?>" name="LessonContent[courseid]" id="lessoncontent-courseid"/>
                                <?= $form->field($text,'text_content')->textArea(['placeholder'=>'Enter text','style'=>"width:100%;height:130px"])?>
                                  <input type="submit"  class="add-textcontent btn btn-success" value="Add Text">
                                  <a style="cursor:pointer" class="cancel-textcontent btn btn-default">Cancel</a>
                                <?php  ActiveForm::end()?>
              </div>
              <div class="tr-file ext-content hide">
                      <div class="alert alert-danger hide">Error occurred</div>
                      <?php $form =ActiveForm::begin(['options'=>['onsubmit'=>'return false']])?>
                              <input type="hidden" id ="content_id_hidden" name="lesson_no"/>
                          <?= $form->field($external,'content_defaultname')->textInput(['placeholder'=>'Link Title','style'=>"width:100%"])->label(false)?>
                           <?=$form_lesson->field($external,'courseid')->hiddenInput(["value"=>$course->course_id])->label(false)?>
                           <?= $form->field($external,'content_link')->textInput(['placeholder'=>'http://wwww.example.com','style'=>"width:100%"])->label(false)?>

                           <input type="submit"  class="add-link mc-btn-3 btn-style-1" value="Add Link">
                           <a style="cursor:pointer" class="cancel-link mc-btn-3 btn-style-5">Cancel</a>
                    <?php  ActiveForm::end()?>
              </div>
            </div>
          </div>

                                        <!--End Unit content -->
          <div class="modal fade" id="addsection" tabindex="-1" role="dialog" aria-labelledby="addsection" aria-hidden="true">
            <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Create Section</h4>
                      </div>
                      <div class="modal-body text-center" >
                        <?php $form = ActiveForm::begin([
                            "action"=>Url::toRoute("/course/saveparttitle"),
                            "layout"=>"horizontal",
                            'fieldConfig' => [
                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                'horizontalCssClasses' => [
                                    'label' => 'col-sm-12 col-xs-12 col-md-12 text-left',
                                    'wrapper'=>'col-md-12 col-xs-12 col-sm-12',
                                    'error' => '',
                                    'hint' => '',
                                ]
                                ]
                              ])?>
                                <input type="hidden" name="CourseParts[course_id]" value="<?=$course->course_id?>"/>
                                <?=$form->field($part,"part_name")->textInput(["placeholder"=>"Section name, maximum length: 70","maxlength"=>"70"])?>
                                <?=$form->field($part,"achievement")->textInput(["placeholder"=>"Enter section achievements, maximum length: 250","maxlength"=>"250"])?>
                                <?=Button::widget(["label"=>"Create Section","options"=>["class"=>"btn btn-success","type"=>"submit"]])?>
                              <?php ActiveForm::end(); ?>
                      </div>
                  </div>
            </div>
          </div>
  <!-- End section -->

<!-- Add question popup -->
<div class="modal fade" id="addquestion" tabindex="-1" role="dialog" aria-labelledby="addquestion" aria-hidden="true">
  <div class="modal-dialog">
        <div class="modal-content">

        </div>
  </div>
</div>
<!-- End question popup-->

<?= $this->render("//site/library")?>

        </section>
    </div>
<?php
$script=<<<EOD
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
EOD;

$this->registerJs($script);
$this->registerJsFile(Url::base()."/js/fileinput.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Url::base()."/assets/95c33da1/jquery-ui.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);

$this->registerJsFile(Url::base()."/js/course_content.js",['depends'=>[\yii\web\JqueryAsset::className()]] );
$this->registerJs('var courseid='.json_encode($course->course_id),View::POS_HEAD);
$this->registerJS("var currenturl=".json_encode(Url::current()),View::POS_HEAD);
$this->registerJS("var baseUrl=".json_encode(Url::base()),View::POS_HEAD);
 ?>
