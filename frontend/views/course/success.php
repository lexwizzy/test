<section class="setting" style="margin-top:9em">
        <div class="container">
            <div class="content-setting">
                <h3 class="title text-center">Your course has ben submitted for review</h3>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="notification-setting setting-box">
                            <h4 class="sm black bold">If auto publish is enable in <a href="<?=\yii\helpers\Url::toRoute(["/course/under-the-hood","courseid"=>$courseid])?>">"Course Settings"</a>,  his course will be automatically published after review</h4>
                            <h4 class="sm black bold">To change this setting go to <a href="<?=\yii\helpers\Url::toRoute(["/course/under-the-hood","courseid"=>$courseid])?>">"Course Settings"</a> </h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
