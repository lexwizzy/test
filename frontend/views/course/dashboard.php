
<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\models\Course;
use frontend\models\User;
use frontend\models\CourseCategory;
use yii\bootstrap\Modal;
use marqu3s\summernote\Summernote;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use scotthuangzl\googlechart\GoogleChart;
use yii\bootstrap\BootstrapPluginAsset;
?>
<style>
  .btn-default{
        height: 34px;
  }
  .modal-dialog {
    width: 400px;
}
</style>

   <!-- END / CONTENT BAR -->
<!-- COURSE CONCERN -->
    <section  class="tutor_content_bg">
        <div class="container">
            <?php if(Yii::$app->session->hasFlash("error")):?>
             <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
            <?php elseif(Yii::$app->session->hasFlash("success")):?>
                <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
            <?php endif?>
            <!-- <div class="price-course">
                <i class="icon md-database"></i>
                <h3>Available Balance </h3>
                <span>$0.0</span>
                <div class="create-coures">
                    <a href="#addsection" class="mc-btn btn-style-1 popup-with-zoom-anim">Create new course</a>
                    <a href="#" class="mc-btn btn-style-5">Request Payment</a>
                </div>

            </div> -->

            <div class="row" id="dashboard_sec">
              <div class="col-md-6" id="earnings">
                <div class="col-md-12" id="monthly-earning-chart">
                  <?=
                   GoogleChart::widget(array('visualization' => 'LineChart',
                   'data' => array(
                       array('Month', 'Gross',"Net"),
                       array('Work', 11,3),
                       array('Eat', 2,4),
                       array('Commute', 2,6),
                       array('Watch TV', 2,1),
                       array('Sleep', 7,4)
                   ),

                   'options' => array("height"=>"400",
                   'title' => 'Earning for this month',
                   "legend"=>[ "position"=> "top" ],
                   'chatArea'=>["left"=>"5em","top"=>"20px","width:100%","height"=>"400"],
                   'colors'=>["#ef4132","#4caf50"]
                  )));
                  ?>
                </div>
                <div class="row col-md-12">
                    <div id="gross" class="col-sm-6  col-xs-6  col-md-6 gross-net">
                        <strong><span>Gross Revenue</span></strong>
                        <h1>$500</h1>
                    </div>
                  <div id="net" class="col-sm-6  col-xs-6 col-md-6 gross-net">
                        <span>Net Revenue</span>
                        <h1>$400</h1>
                    </div>
                </div>
                <div class="col-md-12" id="transactions">
                      <table class="table table-stripe">
                          <thead>
                            <tr><th>Transaction ID</th><th>Date</th><th>Amount</th></tr>
                          </thead>
                          <tbody>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                              <tr><td>#12345</td><td>12 Jan 2015</td><td>$20</td></tr>
                          </tbody>
                      </table>
                </div>

              </div>
                  <div class="col-md-6">
                    <div class="create-coures text-right">
                        <a href="#createCourseModal" data-target="#createCourseModal" data-toggle="modal"  class="custom-btn grey-btn ">
                          <span></span>
                          Create a course</a>
                        <!-- <a href="#" class="mc-btn btn-style-5">Request Payment</a> -->
                    </div>

                    <div id="tut-list-wrapper">

                      <div id="search_filter" class="col-md-12">
                          <div class="col-md-6 text-left search-div" >
                            <?php ActiveForm::begin(["method"=>"get",'options' => ['data-pjax' => true ]])?>
                              <div class="input-group add-on">
                                    <input id="searchfield" class="form-control" type="text" name="q" placeholder="Search for your course"/>
                                    <div class="input-group-btn">
                                      <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                  </div>
                                <!-- <input id="searchfield" class="form-control" type="text" name="searchfilter" placeholder="Search for your course"/> -->
                            <?php ActiveForm::end();?>
                          </div>
                          <div class="col-md-6 text-right sorter-div">

                            <?php $form=ActiveForm::begin(["id"=>"sorter-form","method"=>"get",'options' => ['data-pjax' => true ]])?>
                                <select  id="sorter" name="sorter" class="form-control">
                                    <option value="newest">Sort By Newest</option>
                                    <option value="oldest">Oldest</option>
                                    <option value="title-asc">Title Ascending</option>
                                    <option value="title-desc">Title Descending</option>
                                    <option value="1">Published</option>
                                    <option value="4">Pending Publish</option>
                                    <option value="3">Pending Review</option>
                                    <option value="2">Draft</option>
                                </select>
                            <?php ActiveForm::end()?>
                          </div>
                        </div>
                        <?php Pjax::begin(["id"=>"pjax_tutors"]); ?>
                          <?=
                              ListView::widget([
                              'dataProvider' => $dataProvider,
                              'options' => [
                                  'tag' => 'div',
                                  'class' => '',
                                  'id' => 'tutor-course-list-wrapper',
                                      ],
                              'itemView'=>"_tutors_courses",
                                  'layout' => "{items}\n <div class='clear'></div>{pager}{summary}<div class='clear'></div>",
                              ]);


                            ?>
                          <?php Pjax::end(); ?>
                    </div>

                </div>
              </div>
    </section>
    <!-- END / COURSE CONCERN -->


<!-- SECTIONS -->
<div class="modal fade" id="createCourseModal" tabindex="-1" role="dialog" aria-labelledby="createCourseModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Create a Course</h4>
        </div>
        <div class="modal-body" >
                <div class="row">

                        <div class="row col-md-12 row">
                            <!-- start ActiveForm -->
                              <?php $form= ActiveForm::begin(['action'=>Url::base().'/course/createcourse','options'=>['method'=>"post"]])?>
                            <div class="col-md-12">
                              <?=$form->field($newcourse,'course_title')->textInput(['maxlength'=>70,"placeholder"=>"Course name max 70 characters"])->label(false)?>

                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <?=  Html::dropDownList("Course[course_category]",null,CourseCategory::getCategory(),['prompt'=>"Select course category",'class'=>"form-control"]) ?>
                              </div>
                            </div>
                            <div class="col-md-offset-3 col-md-offset-right-3 col-md-6">
                                <div class="form-group">
                                  <input type="submit" class="btn btn-success form-control" value="Create"/>
                                </div>
                            </div>
                        </div>
                       <?php ActiveForm::end(); ?>
                    </div>



           <div class="modal-footer">

            </div>
          </div>
        </div>

    </div>

</div>

<?php
$script=<<<EOD
$(window).resize(function(){
  drawChartw0();
});
EOD;

$this->registerJS($script);

 ?>
