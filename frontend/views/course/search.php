<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use frontend\models\Course;
use yii\widgets\ListView;
use frontend\models\IsoLanguages;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
?>
<style>
input[type="radio"], input[type="checkbox"] {
   display: inline-block;
}
</style>
<!--
<div itemscope itemtype="http://schema.org/WebSite">
  <meta itemprop="url" content="http://www.teachsity.com/"/>
  <form itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
    <meta itemprop="target" content="http://teachsity.com/course/search?q={<?=$q?>}"/>
    <input itemprop="query-input" type="text" name="<?=$q?>" required/>
    <input type="submit"/>
  </form>
</div>
-->
<div class="wrap">
  <div id="search-result" class="course_container courses_page">
    <div class="course_bg">
    <!-----left slide------>
           <div class="course_left">
             <div class="yes_no visible-xs">
               <p class="development_title">Show Only Courses on Sale</p>
               <p class="development">Filter <span><img src="<?=Url::base()?>/images/filter_icon.png"></span></p>
               <!-- <div class="mobile_hides">
                     <div class="switch">
                       <input type="radio" name="choice" id="yes" checked>
                       <label for="yes">Yes</label>
                       <input type="radio" name="choice" id="no">
                       <label for="no">No</label>
                       <span class="switchFilter"></span>
                     </div>
                 </div> -->
               </div>


             <div class="dev_slide">
               <div class="sale_bg">

               <div class="web_hide">
               </div>

                   <div class="price_bg">
                     <p class="text-right pointer" id="clear_filter">Clear filter</p>
                     <p>Price</p>
                       <?php $form=ActiveForm::begin(["id"=>"filter_form","method"=>"get",'options' => ['data-pjax' => true ]])?>
                       <!-- <input type="hidden" name="q" value="<>"/>
                       <input type="hidden" name="page" value=">"/> -->
                       <ul class="radio_button">
                         <li><input class="price_tag" value="paid" type='radio' name='price' /> Paid</li>
                         <li><input type='radio' name='price' value="free"/> Free</li>
                       </ul>

                   </div>
                 <p class="shadow"><img src="<?=Url::base()?>/images/shadow_line.png"></p>
               </div>

               <div class="sale_bg">
                   <div class="price_bg">
                     <p>Level</p>
                       <ul class="radio_button">
                         <?php foreach(\frontend\models\SkillLevel::find()->all() as $skill):?>
                           <li><input type='radio' value="<?= $skill->id?>" class="level_tag" name='level'/> <?= $skill->skill_level?></li>
                         <?php endforeach;?>
                       </ul>
                   </div>
                 <p class="shadow"><img src="<?=Url::base()?>/images/shadow_line.png"></p>
               </div>

               <!-- <div class="sale_bg">
                   <div class="price_bg">
                     <p>Features</p>
                       <ul class="radio_button">
                         <li><input type='radio' name='features'/> Close Captions (1328)</li>
                           <li><input type='radio' name='features'/> Quizzes (868)</li>
                         <li><input type='radio' name='features'/> Coding Exercises (25)</li>
                       </ul>
                   </div>
                 <p class="shadow"><img src="<?=Url::base()?>/images/shadow_line.png"></p>
               </div> -->

               <div class="sale_bg" style="margin-bottom:30px;">
                   <div class="price_bg">
                     <p>Language</p>
                       <ul class="radio_button">
                         <?php foreach(\frontend\models\IsoLanguages::find()->all() as $lang):?>
                           <li><input type='radio' value="<?= $lang->id?>" class="language_tag" name='language'/> <?= $lang->language?></li>
                         <?php endforeach;?>
                       </ul>
                   </div>
               </div>
               <?php ActiveForm::end();?>
             </div>
           </div>
           <!-----left slide------>
          <!-----right slide------>
        <div class="course_right">
              <div class="course_item_bg">
              <div class="pagination_bg">
                    <div class="pagination_left">
                          <p>Search result for '<?=$q?>'</p>
                    </div>
                    <div class="pagination_right">
                        <!-- <ul>
                            <li>Pages</li>
                            <li><a href="#">01</a></li>
                            <li>of</li>
                            <li class="last_page"><a href="#">30</a></li>
                            <li class="page_arrow"><a href="#"><img src="<?=Url::base()?>/images/pagination_left.jpg"></a></li>
                            <li class="page_arrow"><a href="#"><img src="<?=Url::base()?>/images/pagination_right.jpg"></a></li>
                        </ul> -->
                    </div>
              </div>
              <div class="row">
                  <div class="col-md-12">

                      <div class="content grid">
            <?php Pjax::begin(["id"=>"pjax_course_search_result"]); ?>
                  <?=
                      ListView::widget([
                        'dataProvider' => $pages,
                        'options' => [
                            'tag' => 'div',
                            'class' => 'row',
                            'id' => 'search-result-list-wrapper',
                                ],
                        'itemView'=>"_search_list_item",
                        'layout' => "{items}\n<div class='clear'></div><div class='text-center'>{pager}</div>",
                        // 'pager'=>[
                        //   'firstPageLabel'=>'<img src="'.Url::base().'/images/pagination_left.jpg">',
                        //   'prevPageLabel'=>'<img src="'.Url::base().'/images/pagination_left.jpg">',
                        //   'lastPageLabel'=>'<img src="'.Url::base().'/images/pagination_right.jpg">',
                        //   'nextPageLabel'=>'<img src="'.Url::base().'/images/pagination_right.jpg">',
                        //   //'pageCssClass'=>'pager'
                        // ]
                      ]

                      );


                  ?>
              <?php Pjax::end()?>
                      </div>
                  </div>
                </div>
              </div>


         </div>
    <!-----right slide------>

        </div>
    </div>


</div>
<?php

$script=<<<EOD
  $("input[type='radio']").on("change",function(){
  //  alert()
      $("#filter_form").submit();
  })
  $(document).on('submit', 'form[data-pjax]', function(event) {
    $.pjax.submit(event, '#pjax_course_search_result')
  })
  $("#clear_filter").click(function(){
       $("input[type='radio']").prop("checked",false)
       $("input[type='radio']").trigger("change")
  })
EOD;
$this->registerJs($script);
 ?>
