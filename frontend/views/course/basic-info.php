<?php
  use yii\bootstrap\ActiveForm;
  use yii\helpers\Url;
  use frontend\models\CourseCategory;
  use frontend\models\SkillLevel;
  use yii\helpers\ArrayHelper;
  use yii\web\View;
  use frontend\models\IsoLanguages;
  use yii\bootstrap\Button;
 ?>



<div class="main-content-title row  text-center"><h3 class="title">Basic Information</h3></div>
    <div class="main-content-body row text-center">

        <?php $form = ActiveForm::begin(['layout' => 'horizontal','fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4 col-xs-12',
            'error' => '',
            'hint' => '',
        ],
    ],])?>
            <div class="col-md-12">
                <?=$form->field($course,"course_title")->textInput();?>
            </div>

            <div class="col-md-12">
                 <?= $form->field($course,'course_category')->dropDownList(CourseCategory::getCategory(),['class'=>'select form-control','id'=>'category_select','prompt'=>'Select category','style'=>""]) ?>
            </div>
            <div class="col-md-12">
                 <?= $form->field($course,'course_subcategory')->dropDownList(\yii\helpers\ArrayHelper::map(CourseCategory::getSubCat($course->course_category),"id","subcategory"),['class'=>'select form-control','id'=>'subcategory','prompt'=>'','style'=>""]) ?>
            </div>
            <div class="col-md-12">
              <?= $form->field($course,'skill_level')->dropDownList(ArrayHelper::map(SkillLevel::find()->all(),'id','skill_level'),['class'=>'select form-control','prompt'=>'Select skill level','style'=>""])?>
            </div>
            <div class="col-md-12">
                  <?= $form->field($course,'language')->dropDownList(IsoLanguages::getISOLanguage(),['prompt'=>'Select language'])?>
            </div>
            <div class="col-md-12">
                <?=Button::widget(["label"=>"Save","options"=>['class'=>"btn btn-success","type"=>"submit"]])?>
            </div>

        <?php ActiveForm::end()?>

    </div>

<?php
$url=Url::base();
$script=<<<EOD
$('#category_select').change(function(){
    $("#subcategory").html("");
  $.get("$url/course/getsubcategory",{"category":$(this).val()},function(data){
      data = $.parseJSON(data)

      $.each(data,function(key, value)
        {
           $("#subcategory").append('<option value=' + value.id + '>' + value.subcategory+ '</option>');
        });

  })
})
EOD;
   $this->registerJs($script);
 ?>
