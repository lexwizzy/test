<?php
use yii\web\View;
use yii\helpers\Url;
use frontend\models\Course;
?>

<?php
 $this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);


 //$this->registerJs('var courseid='.json_encode($quiz->courseid),View::POS_HEAD);
 //$this->registerJs('var qui='.json_encode($quiz->courseid),View::POS_HEAD);
?>

    <?php if($type=="pdf"): ?>

    <?php endif;?>

<link href="<?=Url::base()?>/js/library/video-js/video-js.css" rel="stylesheet">
<link href="<?=Url::base()?>/css/videojs-resume.css" rel="stylesheet">
<link href="<?=Url::base()?>/css/videojs.autoplay-toggle.css" rel="stylesheet">
<link href="<?=Url::base()?>/js/library/video-js/css/videojs-overlay.css" rel="stylesheet">
<style>
    .video-js {/*padding-top: 49.25%*/    padding-top:43.25%; /*padding-top: 59.25%;*/}

    @media  screen and (max-width:1000px){
        .video-js{
            padding-top:56.25%;
        }
    }
    .vjs-fullscreen {padding-top: 0px}
    .videocontent{
        width:100%;
        max-width:100%;
        height:100%
    }
</style>
    <div class="wrapper">
     <div class="videocontent">
        <video id="learning_video" data-setup='{ "controls": true, "autoplay": false, "preload": "auto" }' class="video-js vjs-default-skin vjs-big-play-centered"
                                          controls preload="auto" width="auto" height="auto"
                                          poster="<?=$thumbnail?>"
                                         >
                                         <source src="<?=$path?>"  />
                                         <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                  </video>
     </div>
    </div>



<?php
    $this->registerJsFile(Url::base()."/js/library/video-js/store.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Url::base()."/js/library/video-js/videojs.persistvolume.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Url::base()."/js/library/video-js/videojs.autoplay-toggle.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Url::base()."/js/library/video-js/videojs-seek.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);$this->registerJsFile(Url::base()."/js/library/video-js/videojs-overlay.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Url::base()."/js/library/video-js/videojs-resume.min.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Url::base()."/js/library/video-js/video-speed.js",['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile( Url::base().'/js/learning.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
