<?php
use frontend\models\Answers;
 ?>
<style>
.full_height{
  background: #fff !important
}
</style>

<style>
  .q-header{
    border-bottom: 5px solid #EFEFEF;
      font-family: FuturaStd-Heavy
  }
  .quiz_questions li {
    padding: 12px 0;
    border-bottom: 1px dotted #EFEFEF;
    display: inline-block;
    width: 100%;
}
.quiz_questions .q {
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 20px;
    font-family: FuturaStd-Heavy
}
.answer-exp{
  margin-left: 25px;
}
.wrong{
  margin-bottom: 10px;

}
.expln{
  margin-bottom: 10px
}
#total_marks {
    font-size: 24px;
    margin: 15px 0;
}
#total_marks strong {
    float: right;
}
.fail::after{
  content:" \00D7";
  color: red;
}
.correct::after{
  content:" \2713";
  color: green;
}
.question-content-wrap {
    min-height: 400px;
}
/*#quizz-intro-section .container{
    padding-left: 100px;
    padding-right: 100px;
}*/
</style>
<section id="quizz-intro-section" class=" learn-section" style="min-height: 947px;">
  <div class="container">
    <div class="question-content-wrap">
                <div class="question-content">
                  <div class="q-header">
                    <h4 class="sm">Result for <?=$quiz->q_name?> Quiz</h4>
                  </div>
                    <div>
                        <ul class="quiz_questions">
                          <?php foreach($result["answers"] as $r):?>

                            <li>
                                <div class="q <?=$r['class']?>"> <?=$r['question']?></div>
                                <div class="answer-exp">
                                  <div class="wrong">
                                    <?php  foreach($r["user_marked_ans"] as $a):?>
                                      <?php $ans= Answers::findOne([$a])?>
                                      <strong><?= $ans->answer?></strong>
                                      <div class="expln"><em>Explanation:</em> <?=$ans->reason?></div>
                                    <?php endforeach;?>
                                    <!-- <strong>Marked Answer: <strong>: <strong>Generate many clicks and conversions</strong>
                                    <div class="expln">An advertiser is focused primarily on direct response, </div> -->
                                  </div>

                                    <!-- <div class="correct">
                                      <strong>Correct Answer: <strong>Generate many clicks and conversions</strong>
                                      <div class="expln">An advertiser is focused primarily on direct response, </div>
                                    </div> -->
                                </div>

                            </li>
                            <?php //endforeach;?>
                          <?php endforeach?>
                        </ul>
                        <div id="total_marks">Total Marks <strong><span><?=$result["correct"]?></span> / <?= $result["total"]?></strong> </div>
                    </div>

                    </div>
            </div>
          </div>
</section>
