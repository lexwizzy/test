<?php
 use frontend\models\QuestionType;
 use yii\bootstrap\ActiveForm;
 ?>
<style>
.modal-body input[type="radio"], .modal-body input[type="checkbox"] {
  display: inline-block;
}
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title">Add Question</h4>
</div>
<div class="modal-body">
  <div class="alert alert-success " id="question-success" style="display:none"></div>
            <div class="alert alert-danger " id="question-error" style="display:none"></div>
                      <?php $form = ActiveForm::begin(
                      ['action'=>'newquestion',
                      'id'=>'questionform',
                      'options'=>['onsubmit'=>"return false"],
                      "layout"=>"horizontal",
                      'fieldConfig' => [
                          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                          'horizontalCssClasses' => [
                              'label' => 'col-sm-12 col-xs-12 col-md-12 text-left',
                              'wrapper'=>'col-md-12 col-xs-12 col-sm-12',
                              'error' => '',
                              'hint' => '',
                          ]
                        ]

                      ])?>
                      <input type="hidden" name="quizid" id="quizid" />
                      <input type="hidden" name="" id="quiz-index" />
                      <input type="hidden" name="quiz-sectionid" id="quiz-sectionid" />
                      <input type="hidden" name="question-id" id="questionid" />
                      <?= $form->field($question,"title")->textInput(['placeholder'=>"How would you rate teachsity?"])?>
                      <?=$form->field($question,'type')->dropDownList(QuestionType::getQuestionType(),['prompt'=>'Select question type','class'=>'question-type form-control'])?>
                      <?= $form->field($question,"description")->textArea(['placeholder'=>"Description here"])?>
                      <div class="form-group">
                        <label  class="control-label col-sm-12 col-xs-12 col-md-12 text-left" for="">Answer</label>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                          <div class="checkbox-answer-div answer ">

                                  <div class="answer-row main"  >
                                      <div class="form-item-wrap" style="">

                                     <div class="input-group">
                                        <span class="input-group-addon">
                                          <input type="checkbox" name="checkedanswers[]" class="checkbox_ans" value ="on" aria-label="..." style="-webkit-appearance:checkbox;appearance:checkbox;-moz-appearance:checkbox">
                                        </span>
                                        <input type="text" class="form-control answer-field" name="answer-field-multiple[]" aria-label="...">
                                         <span class="input-group-addon"><div class="course-region-tool">
                                          <a  class="delete pointer" title="delete"><i class="fa fa-trash"></i></a>
                                      </div></span>
                                      </div><!-- /input-group -->
                                    </div>

                                      <div class="form-item">
                                              <input class="form-control reason" type="text" name="reason-multiple[]" style ="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer">
                                      </div>
                                  </div>

                          </div>
                           <div class="radio-answer-div answer hidden" >

                                  <div class="answer-row main">
                                   <div class="form-item-wrap" style="">
                                 <div class="input-group">

                                    <span class="input-group-addon">
                                      <input type="radio" aria-label="..." name="radioselected" class="radio-ans" style="-webkit-appearance:radio;appearance:radio;-moz-appearance:radio">
                                    </span>
                                    <input type="text" class="form-control answer-field" name="answer-field-radio[]" aria-label="...">
                                     <span class="input-group-addon"><div class="course-region-tool">
                                      <a class="pointer delete" title="delete"><i class="fa fa-trash"></i></a>
                                  </div></span>
                                  </div><!-- /input-group -->
                                </div>

                                  <div class="form-item">
                                          <input type="text" class="form-control reason" name="reason-single[]" style ="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer">
                                  </div>

                                   </div>

                          </div>
                           <div class="truefalse-answer-div answer hidden">

                                   <div class="form-item-wrap" style="margin-bottom:0">
                                     <div class="radio">
                                            <label><input type="radio" class="true" name="optradio"  value="true">True</label>
                                          </div>
                                  </div>

                                  <div class="form-item">
                                          <input type="text"  class="form-control reason true-reason" name="reason-truefalse[]"  style ="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer"></div>
                                   <div class="form-item-wrap" style=" margin-bottom:0">
                                       <div class="radio">
                                            <label><input type="radio" class="false" name="optradio" value="false" >False</label>
                                          </div>
                                  </div>

                                  <div class="form-item">
                                          <input type="text"  class="form-control reason false-reason" name="reason-truefalse[]" style ="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer">
                                  </div>



                          </div>
                        </div>

                  </div>

        <div class="modal-footer">
          <input type="submit" ng-click="saveQuestion($event)" class="btn btn-success" id="savequestion" value="Save question">

        </div>
      <?php ActiveForm::end()?>
</div>
