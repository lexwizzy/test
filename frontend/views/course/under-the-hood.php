


<?php
  use yii\bootstrap\ActiveForm;
  use yii\bootstrap\Button;
 ?>

<div class="main-content-title row  text-center"><h3 class="title">Under the Hood</h3></div>
    <div class="main-content-body row text-center">

      <div class="">
          <h5>Automatically publish my course</h5>
          <span> Enable this option if your want Teachsity to automatically publish your course after reviewing.
      </div>
      <?php $form = ActiveForm::begin(['layout' => 'horizontal','fieldConfig' => [
      'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
      'horizontalCssClasses' => [
          'label' => 'col-sm-4 col-xs-12',
          'error' => '',
          'hint' => '',
            ],
        ],])?>
      <!-- <div class="col-md-6">
          Automatically Publish Course
      </div> -->
      <!-- <div class="col-md-6" style="padding-left:25px">-->
        <div class="form-item form-radio radio-style">

          <?=$form->field($setting, 'autopub')->radioList(["1"=> 'Yes', "2" => 'No'],['item' =>                                                  function($index, $label, $name, $checked, $value) {
                      if($checked)
                          $checked="checked";
          $return = '<input type="radio"  '.$checked.' id="'.$label.'" name="' .$name. '" value="'. $value.'" separator="">';
          $return .= '<label for="'.$label.'"><i class="icon-radio"></i> '.$label.'</label>';
          return $return;
      }])->label(false)?>
      </div>
      <div >
        <div class="form-group">
        <?=Button::widget(["label"=>"Save","options"=>["class"=>"btn btn-success","type"=>"submit"]])?></div>
      </div>
      <?php ActiveForm::end();?>
      <div class="" style="margin-top:30px;">
          <div> <h5>When you delete your course, it gets deleted from Teachsity database. You cannot revert this once done.</h5></div>
          <div> <h5>Publishing your course makes it visible to students on Teachsity. Unpublishing on the other hand moves the course bask to draft.</h5></div>
          <?php if($course->status==1):?>
           <a href="unpublish?courseid=<?=$course->course_id?>"><input type="button" value="Unpublish" id="unpublish"  class="btn btn-default"></a>
           <?php else:?>
          <a href="publish?courseid=<?=$course->course_id?>"><input type="button" value="Publish Now" id="publish" <?= $course->status !=4? "disabled='disabled'": "" ?> class=" btn btn-info"></a>
          <?php endif;?>
          &nbsp;&nbsp;&nbsp;
               <input type="button" value="Delete Course" id="delete-course"  class=" btn btn-danger">

          </div>
    </div>
<?php

$script = <<<EOD
$("#delete-course").click(function(){
    answer = confirm("Are you sure you want to delete thise course?")

    if(answer==1){
        courseid = $course->course_id;
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            url:'delete?id='+$course->course_id,
            type:'post',
            data:{"_csrf": yii.getCsrfToken()},
            success:function(){

            }
        })
    }
})
EOD;
$this->registerJs($script);
?>
