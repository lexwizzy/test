<?php
    use yii\helpers\Url;
    use frontend\models\CourseParts;
    use frontend\models\QuestionType;
    use frontend\models\Quiz;
    use frontend\models\CourseLessons;
    use frontend\models\User;

?>

<section class="sub-banner sub-banner-create-course">
        <div class="awe-color bg-color-1"></div>
        <div class="container">
            <div id="course-title">
                <h2 class="md ilbl" id="title_label"><?= $course->course_title?></h2>
            </div>


        </div>
    </section>

<!-- CREATE COURSE CONTENT -->
    <section id="create-course-section" class="create-course-section">
        <div class="container">
            <div class="row">
                    <div class="col-md-3">
                        <div class="create-course-sidebar">
                            <ul class="list-bar">
                              <li><a href="<?=Url::toRoute(["/course/basicinfo","slug"=>$course->slug])?>"><span class="count">1</span>Basic Information</a></li>
                              <li><a href="<?=Url::toRoute(["/course/contents","slug"=>$course->slug])?>"><span class="count">2</span>Course Curriculum</a></li>
                              <li><a href="<?=Url::toRoute(["/course/settings","slug"=>$course->slug])?>"><span class="count">3</span>Course Settings</a></li>
                              <li><a href="<?=Url::toRoute(["/course/studentreport","slug"=>$course->slug])?>"><span class="count">4</span>Student Report</a></li>
                              <li class="active"><span class="count">5</span>Preview Course</li>

                            </ul>
                            <div class="support">
                                <a href="create-publish-course.html#"><i class="icon md-camera"></i>See how it work short tutorial</a>
                                <a href="create-publish-course.html#"><i class="icon md-flash"></i>Instant Support</a>
                            </div>
                        </div>
                    </div>

                <div class="col-md-9">
                    <div class="create-course-content">

                        <!-- PUBLISH COURSE -->
                        <div class="publish-course">
                            <h3 class="md black">Course summary</h3>
                            <div class="row">
                                <div class="col-md-5">
                                     <?=\frontend\models\Course::createGuestCourseAd($coursead,"mc-item-1");?>

                                </div>

                                <div class="col-md-7">
                                    <h2 class="big black"><?=$course->course_title ?></h2>

                                    <div class="new-course">
                                        <div class="item course-code">
                                            <i class="icon fa fa-signal"></i>
                                            <h4><a href="create-publish-course.html#">Skill Level</a></h4>
                                            <p class="detail-course"><?= \frontend\models\SkillLevel::getSkillLevel($course->skill_level)?></p>
                                        </div>
                                        <div class="item course-code">
                                            <i class="icon md-time"></i>
                                            <h4><a href="create-publish-course.html#">Lectures</a></h4>
                                            <p class="detail-course"><?=CourseParts::getPartCount($course->course_id)?></p>
                                        </div>
                                        <div class="item course-code">
                                            <i class="icon fa fa-commenting"></i>
                                            <h4><a href="create-publish-course.html#">Language</a></h4>
                                            <p class="detail-course"><?= $course->language?></p>
                                        </div>
                                    </div>

                                    <hr class="line">
                                    <div class="about-instructor">
                                        <h4 class="xsm black">About Instructor</h4>
                                        <ul>
                                            <li>
                                                <div class="image-instructor text-center">
                                                    <img src="<?= $coursead['tutorimage']?>" alt="">
                                                </div>
                                                <div class="info-instructor">
                                                    <cite class="sm black"><a href="create-publish-course.html#"><?= $coursead['name']?></a></cite>
                                                    <p><?= User::getAboutme($course->owner)?></p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <hr class="line">
                                    <div class="widget widget_equipment">
                                        <i class="icon md-config"></i>
                                        <h4 class="xsm black">Requirement</h4>
                                        <div class="equipment-body">
                                            <?= $course->course_tools?>
                                        </div>
                                    </div>

                                    <!--<div class="widget widget_tags">
                                        <i class="icon md-download-2"></i>
                                        <h4 class="xsm black">Tag</h4>
                                        <div class="tagCould">
                                            <a href="create-publish-course.html#">Design</a>,
                                            <a href="create-publish-course.html#">Photoshop</a>,
                                            <a href="create-publish-course.html#">Illustrator</a>,
                                            <a href="create-publish-course.html">Art</a>,
                                            <a href="create-publish-course.html">Graphic Design</a>
                                        </div>
                                    </div>-->

                                    <hr class="line">

                                    <div class="current-wrapper">
                                        <h4 class="xsm black">Current outline</h4>
                                        <ul class="current-outline">
                                            <li><span><?=CourseParts::getPartCount($course->course_id)?></span>section</li>
                                            <li><span><?=Quiz::getQuizCount($course->course_id)?></span>quizzes</li>
                                            <li><span><?=CourseLessons::getLessonCount($course->course_id)?></span>units</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / PUBLISH COURSE -->
                         <form  method="post" action="<?=Url::base()?>/course/submitforreview">
                            <div class="form-action confirm-save">
                               <input type="hidden" value="<?=$course->course_id?>" name="courseid"/>
                                  <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                               <input type="submit" value="Submit for review" class=" mc-btn-3 btn-style-1">
                                 <a class="mc-btn-3 btn-style-6" href="<?=\yii\helpers\Url::toRoute(['course/details', 'slug'=>$course->slug,'role'=>2])?>">Preview as student</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END / CREATE COURSE CONTENT -->
