<?php
    use frontend\models\User;
    use frontend\models\SocialLink;
    use frontend\models\CourseLessons;
    use frontend\models\Course;
    use frontend\models\CourseParts;
    use yii\helpers\Url;
    use yii\web\View;
    use yii\helpers\ArrayHelper;
    use kartik\rating\StarRating;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\BootstrapAsset;
    use marqu3s\summernote\Summernote;

    use frontend\models\LessonContent;
    use frontend\models\Enrollment;

    $sec=0;
    $sec = LessonContent::getCourseVideoLength($course->course_id);
    $pagecount=LessonContent::getCoursePptPages($course->course_id);

    if($sec >= 3600){
        $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";

    }
    else if($sec > 0 && $sec <3600){
        $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
    }
    else{
      $sec=0;
    }
    $course_content_length ='<p class="total"><a href="#"><span><img src="'.Url::base().'/images/icon/video.png"></span> Total Video</a></p>
                          <p class="totale_right">'.$sec.'</p>';

    $name= User::getName($course->owner);
    $link=SocialLink::getLinks($course->owner);
    $query =new yii\db\Query;
    //$icon_array=array("fa-video-camera","fa-file-powerpoint-o","fa-file-pdf-o","fa-file");
    $this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);
    $this->registerJs('var userid='.json_encode(Yii::$app->user->id),View::POS_HEAD);
    $this->registerJs('var course_id='.$course->course_id,View::POS_HEAD);
    $this->registerJs('var baseUrl='.json_encode(Url::base()),View::POS_HEAD);
    $current =Url::current([],true);
    $page_url =Url::current([],true);
    $counter=1;
    // $this->registerJs($fb,View::POS_READY);
    $this->registerJs("var current =".json_encode($current),View::POS_HEAD);
  //  Yii::$app->view->registerMetaTag(['property' => 'og:url','content' => $current]);

    Yii::$app->view->registerMetaTag(['itemprop' => 'og:headline','content' => "Teachsity - ".$course->course_title]);
    Yii::$app->view->registerMetaTag(['itemprop' => 'og:description','content' => html_entity_decode($course->course_desc)]);
    Yii::$app->view->registerMetaTag(['property' => 'og:type','content' => 'article']);
    Yii::$app->view->registerMetaTag(['property' => 'fb:app_id','content' => "[".Yii::getAlias("@fbid")."]"]);
    Yii::$app->view->registerMetaTag(['property' => 'og:title','content' => "Teachsity - ".$course->course_title]);
    Yii::$app->view->registerMetaTag(['property' => 'og:description','content' => html_entity_decode($course->course_desc)]);
    Yii::$app->view->registerMetaTag(['property' => 'og:image','content' => Url::home(true)."upload/coursebanners/".$course->course_banner]);
    $this->title= "Teachsity - ". $course->course_title;
?>
<link href="<?=Url::base()?>/css/right-slide.css" rel="stylesheet">

<link href="<?=Url::base()?>/js/library/video-js/video-js.css" rel="stylesheet">

<!---content section-->
<style>
.video-js {
      padding-top:24.25% !important;
  }
  .vjs-fullscreen {padding-top: 0px}
  .videocontent{
      width:100%;
      max-width:100%;
  }
  .video-course-intro {
      background-color: #fff;
      margin-top: 17px;
  }
</style>
<div class="loader text-center"><i class="fa fa-circle-o-notch fa-spin fa-2x"></i></div>
<div class="">
<div class="wrap">
	<div class="body_content margin_space">

          <div class="container-fluid">
              <div class="row courses_page">
              		<div class="col-sm-9 col-xs-1 top_left">
                    	<p class="mini_habit"><?=$course->course_title?></p>
                        <div class="h_line2"></div>
                        <p class="current_progress">Current Progress</p>
                        <p class="percentage"><?= $progress?>%</p>
                        <div class="percent_bg">
                        	<div class="progressbarsone" progress="<?= $progress?>%"></div>
                        </div>

                      	<div class="share-button sharer" style="display: block;">

                            <p class="share share-btn">
                              <a  class="do-icon pointer" data-toggle="modal" data-target="#flagabuse" title="Flag Abuse"> <span><img src="<?=Url::base()?>/images/icon/flag-2.png"></span></a>
                              <a  class="do-icon pointer" data-toggle="modal" data-target="#coursedesc"  title="Information about this course"> <span><img src="<?=Url::base()?>/images/icon/info.png"></span></a>
                              <a class="do-icon pointer" href="http://www.support.teachsity.com" title="Get in  touch with T-Support"> <span><img src="<?=Url::base()?>/images/icon/question.png"></span></a>
                              <a  class="do-icon" href="javascript:void(0)" id="share_btn"> <span><img src="<?=Url::base()?>/images/share_icon.png"></span></a>
                            </p>
                            <div class="social top center networks-3 ">
                             <!-- Facebook Share Button -->
                             <a class="fbtn share facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$page_url?>" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a>
                             <a class="fbtn share gplus" href="https://plus.google.com/share?url=<?=$page_url?>" target="_blank" title="google plus"><i class="fa fa-google-plus"></i></a>
                             <a class="fbtn share twitter" href="https://twitter.com/intent/tweet?text=<?=$course->course_title?>&amp;url=<?=$page_url?>&amp;via=teachsity" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a>
                           </div>
                        </div>

                    </div>


                    <div class="col-sm-3 col-xs-1 top_right">
                    	<div class="course_page_right">
                        <div class="right_side_box">
                          	<img class="line_bg" src="<?=Url::base()?>/images/right_box_list.png">
                              <p class="lectures"><a href="#"><span><img src="<?=Url::base()?>/images/icon/leacturs.png"></span> Lectures</a></p>
                              	<p class="lecture_right"><?=CourseParts::getPartCount($course->course_id)?></p>
                                <?=$course_content_length;//cretae dynamic content?>
                              <p class="level"><a href="#"><span><img src="<?=Url::base()?>/images/icon/skill.png"></span> Skill Level</a></p>
                              	<p class="skill_right"><?= \frontend\models\SkillLevel::getSkillLevel($course->skill_level)?></p>
                              <p class="languages"><a href="#"><span><img src="<?=Url::base()?>/images/icon/langu.png"></span> Languages</a></p>
                              	<p class="languages_right"><?= $course->language?></p>
                              <p class="includes"><a href="#"><span><img src="<?=Url::base()?>/images/icon/include.png"></span> Includes</a></p>
                              	<p class="includes_right">Lifetime access
                                30 day money back guarantee!
                                Available on iOS and Android
                                Certificate of Completion</p>
                          </div>
                        </div>
                    </div>
              </div>
          </div>


          <div class="container-fluid">
          	<div class="row tab_section">
            	<div class="col-sm-5 col-xs-1 video_left">
                	<div class="video_bg">
                    <div class="video-course-intro">
                           <div>
                                <video id="" class="video-js  vjs-big-play-centered"
                                         controls preload="auto" width="auto" height="auto"
                                         poster="<?=Course::getCoursePromoVideoThumbnail($course);?>"
                                         data-setup=''>
                                        <source src="<?=Course::getCoursePromoVideo($course)?>" type='' />
                                        <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                                </video>
                           </div>
                       </div>
                    </div>
                    <div class="profile_bg">
                    	<div class="photo_bg">
                        	<img width="88" height="88" src="<?=User::getPhoto($course->owner)?>" alt="<?=$name['firstname']." ".$name['lastname']?> teachsity profile picture">
                        </div>
                        <div class="about_bg">
                          <p class="about">About Instructor</p>
                          <p class="about_name pointer" data-toggle="modal" data-target="#instructor-popup" ><?=$name['firstname']." ".$name['lastname']?></p>
                          <p class="about_link">
                                      <a target="_blank" href="<?=$link['facebook']?>"><i class="fa fa-facebook-square fa-2x"></i></a>
                                       <a target="_blank" href="<?=$link['twitter']?>"><i class="fa fa-twitter-square fa-2x"></i></a>
                                       <a target="_blank"href="<?=$link['google']?>"><i class="fa fa-google-plus-square fa-2x"></i></a>
                                       <a target="_blank" href="<?=$link['linkedin']?>"><i class="fa fa-linkedin-square fa-2x"></i></a>
                                       <a target="_blank" href="<?=$link['github']?>"><i class="fa fa-github-square fa-2x"></i></a>
                          </p>
                          <p><?=User::getAboutme($course->owner);?></p>
                        </div>
                    </div>
              </div>

              	<!------Right side------->
            	<div class="col-sm-7 col-xs-1 tab_right_side">

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Outline</a></li>
  <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Announcement</a></li>
  <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Discussion</a></li>
  <li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Review</a></li>
  <li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Student</a></li>
</ul>


<!-- Tab panes -->
<div class="tab-content">

<div role="tabpanel" class="tab-pane tab_wrap fade active in" id="tab1">

		<!-- Accordion begin -->
		<div class="outline_accordion">

			<!-- Section  -->
      <?php foreach(Course::getSections($course->course_id) as $section):?>
			<div class="accordion_in <?= $counter==1 ? " acc_active":""?> <?= $counter==2 ? " acc_active":""?>">
        <?php ++$counter?>
				<div class="acc_head">Section <?=$section->counter?>: <?=$section->part_name?></div>
				<div class="acc_content">
                  <ul class="modifi_list">
                  <?php $outline = \frontend\models\CourseOutlineOrder::find()->where(['and',["section"=>$section->part_id],['courseid'=>$course->course_id]])->orderBy("counter asc")->all();?>
                   <?php foreach($outline as $order):?>
                     <?php $units= CourseLessons::findOne([$order->itemid]);?>
                    <?php if($order->type=="lesson"):?>
                      <?php
                          $main_content= CourseLessons::getUnitMainContent($units->lesson_id);
                          if($main_content!=null){
                            if($main_content["type"]=="video"){
                               $d=$main_content["video_length"];
                               $icon="lesson_icon2.png";
                            }else if($main_content["type"]=="pdf" ){
                              $d=$main_content["page_no"]." Pages";
                              $icon="lesson_icon.png";
                            }
                            else {
                              $icon="lesson_icon.png";
                            }
                             //  $d=  ?gmdate("i:s",$main_content->video_length):$main_content->no_of_pages." Pages";
                           }
                       ?>
                        <li><a data-outlineno="<?=$units->outlineno?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/lesson/".$units->lesson_id])?>"><p class="circle_active">
                          <p class="circle_active">
                            <img src="<?=Url::base()?>/images/icon/<?=\frontend\models\CourseLearnTracking::find()->where(["lesson_type"=>"lesson","courseid"=>$course->course_id,'userid'=>Yii::$app->user->id,"section"=>$units->part_id,"lesson"=>$units->lesson_id])->one() !=null? "circle_red.png": "circle_white.png"?> ">
                           <span class="circle_img"><img src="<?=Url::base()?>/images/icon/<?=$icon?>"></p>
                            Lecture <?=$order->counter?>: <?=$units->lesson_title?>
                         <span class="right_txt"><?=$d?></span></a></li>
                    <?php else :?>
                      <?php $q = \frontend\models\Quiz::findOne([$order->itemid]);?>
                       <li><a data-outlineno="<?=$q->outlineno?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/quiz/".$q->q_id])?>"><p class="circle_active">
                         <p class="circle_active">
                            <img src="<?=Url::base()?>/images/icon/<?=\frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$q->q_id])->one() !=null? "circle_red.png": "circle_white.png"?>">
                             <span class="circle_img"><img src="<?=Url::base()?>/images/icon/quiz_icon.png"></p>
                               Quiz <?=$q->counter?>: <?=$q->q_name?>
                            <span class="right_txt"><?=\frontend\models\Quiz::getNumberOfQuestion($q->q_id)?> questions</span></a></li>
                     <?php endif; ?>
                  <?php endforeach;?>
                  </ul>
				</div>
			</div>
      <?php endforeach;?>

		</div>
		<!-- Accordion end -->

</div>

<div role="tabpanel" class="tab-pane tab_wrap fade" id="tab2">
	<div class="tab_content">
    <ul class="list-announcement">
            <!-- LIST ITEM -->
         <?= \frontend\models\CourseAnnouncement::getCourseAnnouncement($course->course_id)?>
              <!-- END / LIST ITEM -->
      </ul>
  </div>
</div>

<div role="tabpanel" class="tab-pane tab_wrap fade" id="tab3">
	<div class="tab_content">
    <p class="discuss-text"><?= \frontend\models\Discussions::getCount($course->course_id)?> Topics</p>
    <div class="form-discussion">
                            <?php $form=ActiveForm::begin(['id'=>'discussionform','options'=>['onsubmit'=>"return false"]]);?>
                                      <div class="text-title">
                                          <?= $form->field($discussion,"topic")->textInput(['placeholder'=>"Enter discussion topic"])->label(false)?>
                                      </div>

                                      <div class="post-editor ">


                                           <?= $form->field($discussion,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
                                          <?= $form->field($discussion,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
                                           <?= $form->field($discussion, 'comment')->widget(Summernote::className(),
                                  ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
                                      </div>

                                      <div class="form-submit">
                                          <input type="submit" value="Post Discussion" id="save-discussion" class=" course-learn-more review-input-box">
                                      </div>
                        <?php $form=ActiveForm::end();?>
                              </div>

        <div class="post-dicuss-status">
           <?= \frontend\models\Discussions::displayDiscussion($course->course_id)?>

        </div>



      <div class="start-learning course-load-more-post">
      <a href="#" title=""><i class="icon md-time"></i>Load more previous update</a>
      </div>

    </div>
</div>

<div role="tabpanel" class="tab-pane tab_wrap fade" id="tab4">
	<div class="tab_content">
      <p class="discuss-text"><?=\frontend\models\CourseReviews::totalReviews($course->course_id)?> Reviews
        <?=  StarRating::widget([
                                            'name' => 'rating',
                                            'value'=> \frontend\models\CourseReviews::getAverageRating($course->course_id),
                                            'pluginOptions' => [
                                                'size' => 'xxs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>true,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]);

                                            ?>
      </p>

      <!--<p class="topic-hint">Topic cannot be blank.</p>-->

      <?php $form=ActiveForm::begin(['id'=>'ratingform']);?>
                                        <div class="review-editor">
                                             <?= $form->field($rating,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
                                            <?= $form->field($rating,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
                                             <?= $form->field($rating, 'comment')->widget(Summernote::className(),
                                    ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
                                        </div>


                                    <div class="row">
                                        <div class="col-md-2" style="padding-top:10px"><span>Your rate</span></div>
                                        <div class="col-md-6">

                                            <?=
                                                $form->field($rating, 'rating')->widget(StarRating::classname(), [
                                                    'pluginOptions' => [
                                                    'size' => 'xs',
                                                    'stars' => 5,
                                                    'step' => 1,
                                                    'min' => 0,
                                                    'max' => 5,
                                                    'clearButton'=>'',
                                                    'showCaption' => true,
                                                    'starCaptions' => [
                                                    1 => 'Extremely Poor',
                                                    2 => 'Poor',
                                                    3 => 'Good',
                                                    4 => 'Very Good',
                                                    5 => 'Extremely Good',
                                                    ]
                                                    ],
                                                ])->label(false);
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-submit">
                                                <input type="submit" value="Post Review" id="save-review"class="course-learn-more review-input-box">
                                            </div>
                                        </div>
                                    </div>

                <?php $form=ActiveForm::end();?>


        <!--<div class="post-dicuss-status">
        	<div class="discuss-section">
            	<div class="left-side-section-discuss">
                	<img src="<?=Url::base()?>/images/leatures_img.png">
                </div>
                <div class="right-side-section-discuss">
                	<p class="discuss-name">Jacks John</p>
                    <p class="discuss-title-name">Habits in Common</p>
                    <p class="discuss-text">There are some unchangeable habits that are born with us</p>
                    <p class="month-discuss">22 days | <Span><i class="fa fa-reply"></i> 0 replies</Span></p>
                </div>
            </div>
        </div>-->


        <ul class="list-review">
                    <?= \frontend\models\CourseReviews::designReviewBox2($course->course_id)?>
          </ul>
      <div class="start-learning course-load-more-post">
      <a href="#" title=""><i class="icon md-time"></i>Load more previous update</a>
      </div>

    </div>
</div>

<div role="tabpanel" class="tab-pane tab_wrap fade" id="tab5">
	<div class="tab_content">
      <p class="discuss-text"><?=\frontend\models\Enrollment::getEnrollmentCount($course->course_id)?> Students applied</p>

      <div class="post-dicuss-status post-student-status">
       <!-- LIST STUDENT -->
            <?= \frontend\models\Enrollment::designLearnEnrollment($course->course_id)?>
        <!-- END / LIST STUDENT -->



        </div>



      <div class="start-learning course-load-more-post">
      <a href="#" title=""><i class="icon md-time"></i>Load more previous update</a>
      </div>

    </div>
</div>


</div>

                </div>
                <!--Right side end-->
            </div>
          </div>





          <div class="container-fluids">
          	<div class="row courses_you">
            	<div class="col-sm-12 col_width">

                <div class="courses_bottom">
                	<p class="courses_title">Similar courses</p>

                      <div class="courses_page_box">
                        <?php $_course = Course::getCourseSameCategory($course->course_category,$course->course_id,4);?>
                                <?php foreach($_course as $c):?>
                                <?php $coursead=Course::getCourseAd($c->course_id);?>
                                    <?=Course::createGuestOtherCourseAd($coursead);?>

                                <?php endforeach;?>
                      </div>

                </div>
            </div>
          </div>
          </div>






	</div>
</div>
</div>
<!---content section end-->


<!-- Course Descriptions Modal -->
<div class="modal fade" id="coursedesc" tabindex="-1" role="dialog" aria-labelledby="coursedesc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">About this course</h4>
      </div>
      <div class="modal-body" style="border: 1px solid #eee;margin: 7px;">
           <p class="descrip_title">Course Description</p>
                                <p><?= $course->course_desc;?></p>
                                <div>
                                  <p class="descrip_title" id="requirements">What are the requirements?</p>
                                    <ul>
                                      <?php if(!empty($course->course_tools)):?>
                                       <?php foreach(json_decode($course->course_tools,true) as $r):?>
                                         <?php foreach($r as $rq):?>
                                          <?= "<li>".$rq."</li>"?>
                                          <?php endforeach;?>
                                       <?php endforeach;?>
                                       <?php endif;?>
                                    </ul>
                                </div>
                                <div class="goals">

                                  <p class="descrip_title">What am I going to get from this course?</p>
                                  <ul>
                                    <?php if(!empty($course->course_goal)):?>
                                       <?php foreach(json_decode($course->course_goal,true) as $r):?>
                                         <?php foreach($r as $rq):?>
                                          <?= "<li>".$rq."</li>"?>
                                          <?php endforeach;?>
                                       <?php endforeach;?>
                                    <?php endif;?>
                                  </ul>
                                </div>
                                <div id="target_audience">

                                  <p class="descrip_title" id="target">What is the target audience?</p>
                                    <ul>
                                      <?php if(!empty($course->target_audience)):?>
                                         <?php foreach(json_decode($course->target_audience,true) as $r):?>
                                           <?php foreach($r as $rq):?>
                                            <?= "<li>".$rq."</li>"?>
                                            <?php endforeach;?>
                                         <?php endforeach;?>
                                         <?php endif;?>
                                    </ul>
                                </div>

      </div>
  </div>
</div>
</div>
<!-- Flag Abuse Modal -->
<div class="modal fade" id="flagabuse" tabindex="-1" role="dialog" aria-labelledby="flagabuse">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Report Abuse for: <?= $course->course_title ?></h4>
      </div>
      <div class="modal-body">
          <div id="alert-flag-success" class="alert alert-success" style="display:none"></div>
          <div id="alert-flag-error"  class="alert alert-danger " style="display:none"></div>
        <?php $form=ActiveForm::begin(['id'=>'flagabuseform','options'=>['onsubmit'=>"return false"]]);?>

        <div>
            <?= $form->field($flag,"reason")->dropDownList(ArrayHelper::map(\frontend\models\TypesOfAbuse::find()->all(),'id','name'),['prompt'=>'What issue do you want to flag?'])->label(false)?>
        </div>

          <?= $form->field($flag,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
          <?= $form->field($flag,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
         <div>
              <?= $form->field($flag,"comment")->textArea(['placeholder'=>"Enter comment",'class'=>"form-control"])->label(false)?>
          </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-success" id="saveflag" value="Submit"/>
      </div>
          <?php ActiveForm::end();?>
        </div>
    </div>
  </div>
</div>
<?php
$script=<<<EOD

$(function() {
  $('.panel-heading').click(function() {
    if ($(this).hasClass('activestate')) {
      $(this).removeClass('activestate');
    } else {
      $('.panel-heading').removeClass('activestate');
      $(this).addClass('activestate');
    }
  });
});
	jQuery(document).ready(function($){
		$(".outline_accordion").smk_Accordion({
			closeAble: true, //boolean
		});
	});

$( document ).ready(function() {
    //custom share button for homepage
     $( "#share_btn" ).click(function(e) {
         $('.networks-3').not($(this).next( ".networks-3" )).each(function(){
            $(".share-btn").removeClass("active");
         });

               $(".share-btn").next( ".networks-3" ).toggleClass( "active" );
    });
});

	// activate jprogress
	$(".progressbars").jprogress();
	$(".progressbarsone").jprogress({
		background: "#ef4132"
	});
EOD;

$this->registerJS($script);
//$this->registerJsFile( Url::base().'/js/jprogress.js',['depends' => [\yii\web\JqueryAsset::className()]]);
 $this->registerJsFile( Url::base().'/js/smk-accordion.js',['depends' => [\yii\web\JqueryAsset::className()]]);
 $this->registerJsFile( Url::base().'/js/jprogress.js',['depends' => [\yii\web\JqueryAsset::className()]]);
 $this->registerJsFile( Url::base().'/js/ladda.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
 $this->registerJsFile( Url::base().'/js/courselearn.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]]);
 ?>
