<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use frontend\models\Course;
use kartik\popover\PopoverX;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$li = "";

?>
<div class="">
  <div class="wrap">
    <div class="course_container courses_page">
          <div class="course_bg">  <!-----right slide------>
          <div class="course_right">
                <div class="course_item_bg">
                <div class="pagination_bg">
                      <div class="pagination_left">
                          	<p>Recommended Courses</p>
                      </div>

                </div>
                  <div class="">

                    <?php foreach(Course::getRecommendedCourses()as $c):?>
                            <?php $coursead=Course::getCourseAd($c['course_id']);?>

                                <?=Course::createGuestOtherCourseAd($coursead,"mc-item-2")?>


                            <?php endforeach;?>
                  </div>
                </div>
                <div class="clear"></div>
                <div class="course_item_bg">
                <div class="pagination_bg">
                      <div class="pagination_left">
                            <p>Newly created courses</p>
                      </div>

                </div>
                  <div class="">

                      <?php $course = Course::find()->where(["status"=>1])->orderBy("createdate desc")->limit(8)->all();?>
                       <?php foreach($course as $c):?>

                       <?php $coursead=Course::getCourseAd($c->course_id);?>
                          <?=Course::createGuestOtherCourseAd($coursead,"mc-item-2");?>
                       <?php endforeach;?>
                  </div>
                </div>
                <div class="clear"></div>
           </div>
      <!-----right slide------>

          </div>
      </div>


  </div>
</div>
