<?php
  use yii\helpers\Html;
  use yii\helpers\Url;
  use frontend\models\Course;
 ?>
<style>
.row {
   margin-right: 0;
   margin-left: 0
}
</style>
<script type="text/x-handlebars-template" id="goals-list-tpl">

    {{#if goals.length}}
      <ul>
      {{#each goals}}
        <li>
            {{this}}
            <span class="icon" data-index={{@index}}><i class="fa fa-trash"></i></span>
        </li>
      {{/each}}
    </ul>
    {{/if}}
</script>
<script type="text/x-handlebars-template" id="audience-list-tpl">
  {{#if audience.length}}
  <ul>
  {{#each audience}}
    <li>
        {{this}}
        <span class="icon" data-index={{@index}}><i class="fa fa-trash"></i></span>
    </li>
  {{/each}}
  </ul>
  {{/if}}
</script>
<script type="text/x-handlebars-template" id="requirement-list-tpl">
  {{#if requirement.length}}
  <ul>
  {{#each requirement}}
    <li>
        {{this}}
        <span class="icon" data-index={{@index}}><i class="fa fa-trash"></i></span>
    </li>
  {{/each}}
  </ul>
  {{/if}}
</script>


<?php
$url=Url::base();
$script=<<<EOD
function templating(source,data){
  var template = Handlebars.compile(source);
  var html    = template(data);
  return html;
}
var goalsJSON
var audienceJSON
var requirementJSON
$.get("$url/course/getgoals",{"courseid":$course->course_id},function(data){

        data=$.parseJSON(data);
        goalsJSON=data;
        var html=templating($("#goals-list-tpl").html(),goalsJSON)
         $("#goals .added-item").html(html);
         $("#goals .added-item").removeClass("hidden").addClass("show");
});

$.get("$url/course/getaudience",{"courseid":$course->course_id},function(data){

        data=$.parseJSON(data);
        audienceJSON=data;
        var html=templating($("#audience-list-tpl").html(),audienceJSON)
         $("#audience .added-item").html(html);
         $("#audience .added-item").removeClass("hidden").addClass("show");
});
$.get("$url/course/getrequirements",{"courseid":$course->course_id},function(data){

        data=$.parseJSON(data);
        requirementJSON=data;
        var html=templating($("#requirement-list-tpl").html(),requirementJSON)
         $("#requirement .added-item").html(html);
         $("#requirement .added-item").removeClass("hidden").addClass("show");
})
EOD;
$this->registerJS($script);
 ?>
 <div class="main-content-title row  text-center"><h3 class="title">Course Target and Requirements</h3></div>
     <div class="main-content-body row text-center">
       <div id="goals" class="c_item col-md-10 col-md-offset-1 col-md-offset-right-1">
         <h6>What will your students learn after taking this course?</h6>
         <div>
             <input id="goal-input" data-placement="top" data rel="txtTooltip" data-html="true" title="e.g <li class='text-left'> Build webcrawler</li><li class='text-left'>Build site like google</li>"  data-toggle="tooltip"  type="text" class="form-control" style="max-width:50%" />
             <button id="add-goals" class="btn btn-sm btn-danger">Add</button>
         </div>
         <div class="added-item col-md-10 col-md-offset-1 col-md-offset-right-1">

         </div>
       </div>

       <div id="audience" class="c_item col-md-10 col-md-offset-1 col-md-offset-right-1">
         <h6>Who should take this course?</h6>
         <div>
             <input  id="audience-input" data-placement="top" data rel="txtTooltip"  data-html="true"  title="<p class='text-left'>Specify your target audience here, e.g This course is for python beginners. Add as many as possible</p>"  data-toggle="tooltip" type="text" class="form-control" style="max-width:50%" />
              <button id="add-audience" class="btn btn-sm btn-danger">Add</button>
         </div>
             <div class="added-item col-md-10 col-md-offset-1 col-md-offset-right-1">

             </div>
       </div>

       <div id="requirement" class="c_item col-md-10 col-md-offset-1 col-md-offset-right-1">
         <h6>What are the requirements for this course?</h6>
         <div>
             <input id="requirement-input" data-placement="top" data rel="txtTooltip" data-html="true"  title="<p class='text-left'>What should the student do or have before starting this course. e.g A notepad, A Pen, Install Adobe Photoshop</p>"  data-toggle="tooltip"  type="text" class="form-control" style="max-width:50%" />
             <button id="add-requirement"  class="btn btn-sm btn-danger">Add</button>
         </div>
           <div class="added-item col-md-10 col-md-offset-1 col-md-offset-right-1">

           </div>
       </div>


     </div>
<?php
$script=<<<EOD
$("#add-goals").bind("click",function(){
var goal= $("#goal-input").val()
// $(".added-item").append("<li>"+goal+"</li>");
// $(".added-item").removeClass("hidden").addClass("show");
var source   = $("#goals-list-tpl").html();
// data=$.parseJSON(JSON.stringify({"goals":[goal]}));
goalsJSON.goals.push(goal);
var html    = templating(source,goalsJSON)
 $("#goals .added-item").html(html);
 $("#goal-input").val("")
 $("#goals .added-item").removeClass("hidden").addClass("show");
 // goalsJSON= $.parseJSON(goalsJSON);

$.ajax({
  url:"$url/course/savegoals",
  contentType:"application/json",
  dataType:"json",
  data:{"goal":JSON.stringify(goalsJSON),"courseid":"$course->course_id"},
  success:function(data){
    console.log(data)
  }
})
})


$("#add-audience").bind("click",function(){
var audience= $("#audience-input").val()
// $(".added-item").append("<li>"+goal+"</li>");
// $(".added-item").removeClass("hidden").addClass("show");
var source   = $("#audience-list-tpl").html();
audienceJSON.audience.push(audience);
var html    = templating(source,audienceJSON)
 $("#audience .added-item").html(html);
 $("#audience-input").val("")
 $("#audience .added-item").removeClass("hidden").addClass("show");
 // goalsJSON= $.parseJSON(goalsJSON);

$.ajax({
  url:"$url/course/saveaudience",
  contentType:"application/json",
  dataType:"json",
  data:{"audience":JSON.stringify(audienceJSON),"courseid":"$course->course_id"},
  success:function(data){
  }
})
})

$("#add-requirement").bind("click",function(){
var requirement= $("#requirement-input").val()
$("#requirement-input").prop("disabled",true)
// $(".added-item").append("<li>"+goal+"</li>");
// $(".added-item").removeClass("hidden").addClass("show");
var source   = $("#requirement-list-tpl").html();
requirementJSON.requirement.push(requirement);
var html    = templating(source,requirementJSON)
 $("#requirement .added-item").html(html);
 $("#requirement-input").val("")
 $("#requirement .added-item").removeClass("hidden").addClass("show");
 // goalsJSON= $.parseJSON(goalsJSON);

$.ajax({
  url:"$url/course/saverequirements",
  contentType:"application/json",
  dataType:"json",
  data:{"requirement":JSON.stringify(requirementJSON),"courseid":"$course->course_id"},
  success:function(data){
    $("#requirement-input").prop("disabled",false)
  }
})
})


$(document).on("click","#goals .icon",function(){
  var ans = confirm("Are you sure you want to delete this item?");
   if(ans ==1){
     goalsJSON.goals.splice($(this).attr("data-index"),1);
     $.ajax({
       url:"$url/course/savegoals",
       contentType:"application/json",
       dataType:"json",
       data:{"goal":JSON.stringify(goalsJSON),"courseid":"$course->course_id"},
       success:function(data){
         console.log(data)
       }
     })
   }
   var source   = $("#goals-list-tpl").html();
   var html    = templating(source,goalsJSON)
    $("#goals .added-item ul").html(html);
    $("#goals .added-item").removeClass("hidden").addClass("show");
});

$(document).on("click","#audience .icon",function(){
  var ans = confirm("Are you sure you want to delete this item?");
   if(ans ==1){
     audienceJSON.audience.splice($(this).attr("data-index"),1);
     $.ajax({
       url:"$url/course/saveaudience",
       contentType:"application/json",
       dataType:"json",
       data:{"audience":JSON.stringify(audienceJSON),"courseid":"$course->course_id"},
       success:function(data){
         console.log(data)
       }
     })
   }
   var source   = $("#audience-list-tpl").html();
   var html    = templating(source,audienceJSON)
    $("#audience .added-item ul").html(html);
    $("#audience .added-item").removeClass("hidden").addClass("show");
})

$(document).on("click","#requirement .icon",function(){
  var ans = confirm("Are you sure you want to delete this item?");
   if(ans ==1){
     requirementJSON.requirement.splice($(this).attr("data-index"),1);
     $.ajax({
       url:"$url/course/saverequirements",
       contentType:"application/json",
       dataType:"json",
       data:{"requirement":JSON.stringify(requirementJSON),"courseid":"$course->course_id"},
       success:function(data){
         console.log(data)
       }
     })
   }
   var source   = $("#requirement-list-tpl").html();
   var html    = templating(source,requirementJSON)
    $("#requirement .added-item ul").html(html);
    $("#requirement .added-item").removeClass("hidden").addClass("show");
});
EOD;

$this->registerJS($script);
?>
<?php $this->registerJsFile( Url::base().'/js/library/handlebars.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
