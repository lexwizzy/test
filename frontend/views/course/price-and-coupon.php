
<?php
  use frontend\models\CourseCoupons;
  use yii\helpers\Url;
  use yii\widgets\ActiveForm;
  use frontend\models\Course;
  use yii\bootstrap\Button;
  use yii\bootstrap\BootstrapAsset;
  use yii\helpers\Html;
 ?>

<style>

.bootstrap-switch-container{
  /*width: 160px !important;*/
}
#pricing{
    margin-bottom: 30px;
}
.main-content-body .form-control {
    max-width: 100% !important;
     display: inline;
}
#price-toggle{
  float: left;
  margin-bottom: 10px
}
#price-toggle label{
  margin-right:0;
}
#price-toggle .btn-default.active {
    background-color: #2aabd2;
    border-color: #dbdbdb;
    color: #fff;
}
.pricing-footer{
  margin-top: 20px;
}
.pricing-footer .how-to{
  margin-left: 10px
}
.modal-dialog{
  max-width: 550px
}
.modal-body{
  margin-top: 0 !important
}
</style>
<?php
$script=<<<EOD
$("#free-price").click(function(){
    $("#amount-div").addClass("hidden").removeClass("show")
    $("#currency-div").addClass("hidden").removeClass("show")
});
$("#cancel-price").click(function(){
  $("#pricing").removeClass("show").addClass("hidden")
})
$("#show-pricing").click(function(){
  $("#pricing").removeClass("hidden").addClass("show")
})
$("#paid-price").click(function(){
    $("#amount-div").addClass("show").removeClass("hidden")
    $("#currency-div").addClass("show").removeClass("hidden")
})
$("#create-coupon-btn").click(function(){
    $("#couponform").removeClass("hidden").addClass("show")
})
EOD;

$this->registerJs($script);

?>
<div class="main-content-title row  text-center"><h3 class="title">Price and Coupon</h3></div>
    <div class="main-content-body row text-center">
        <div id="pricing" class="hidden">
            <div>
              <?php $form=ActiveForm::begin()?>
                <div id="tg-price">

                      <div class="row">
                        <div class="col-md-12 col-md-offset-2 form-group">
                          <div class="btn-group" data-toggle="buttons" id="price-toggle">
                            <label class="btn btn-default active" id="free-price">
                              <input type="radio"  value="free" name="Course[course_price]" id="option1" autocomplete="off" checked> Free
                            </label>
                            <label class="btn btn-default" id="paid-price">
                              <input type="radio" value="paid" name="Course[course_price]" id="option2" autocomplete="off"> Paid
                            </label>

                          </div>

                          </div>
                          <div class="col-md-4 col-md-offset-2 hidden" id="amount-div">
                                <?= $form->field($course,'amount')->textInput(['id'=>'amount','placeholder'=>'amount'])->label(false)?>
                          </div>
                          
                          <div class="col-md-4 col-md-offset-right-2 hidden" id="currency-div">
                                 <?=$form->field($course,"currency")->dropDownList([],['prompt'=>'Select currency','data-flags'=>"true","data-currency"=>!empty($course->currency)?$course->currency:"INR",'class'=>"form-control  bfh-currencies",'style'=>""])->label(false)?>
                          </div>
                      </div>
                    </div>
                
            </div>

              <div class="pricing-footer">
                <?=Button::widget(["label"=>"Save","options"=>["class"=>"btn btn-success","type"=>"submit"]])?> <?=Button::widget(["label"=>"Cancel","options"=>["id"=>"cancel-price","class"=>"btn btn-default","type"=>"button"]])?>
                  <span class="how-to"><a href="" data-target="#bestprice" data-toggle="modal">How do I determine my course price</a></span>

              </div>

              <?php ActiveForm::end()?>
        </div>

        <div class="row" id="price-and-coupon">
            <?php if($course->amount==""):?>
               <div class="alert alert-warning"> You cannot create coupon for a free course.</div>
            <?php endif;?>
            <h4 class="sm black bold">Course Coupon</h4>
            <div class="col-md-12 row">
                  <div style="" class="col-md-5">
                    Current course price
                  </div>
                 <div style="border: 1px solid #ccc;font-size: 4em;font-weight: bold;" class="col-md-3 text-center">
                    <?= Course::getPrice($course->course_id)?>
                  </div>
                   <div class="col-md-3">
                       <div><span>Click <a  class="cursor-pointer" id="show-pricing">here</a> to change price</span></div>
                        <input type="button" value="Create Coupon" id="create-coupon-btn" <?= $course->amount ==""? "disabled='disabled'": "" ?> class="btn btn-primary">
                   </div>
            </div>
            <div style="margin-top:2em"></div>
            <div class="col-md-4">

            </div>
             <?php $couponform= ActiveForm::begin(['id'=>'couponform','options'=>['class'=>'hidden']])?>
            <div class="col-md-12">


                    <div class="col-md-4">
                        <h4 class="">Create New Coupon</h4>
                        <?= $couponform->field($coupon,"coupon_code")->textInput()->label(false)?>

                    </div>
                     <div class="col-md-2">
                       <h4 class="">Discount</h4>
                        <?= $couponform->field($coupon,"discount")->textInput()->label(false)?>

                    </div>
                    <div class="col-md-4">
                       <h4>Expiry Date</h4>
                       <?= $couponform->field($coupon,'expirey_date')->widget(yii\jui\DatePicker::className(),['dateFormat'=>'yyyy-MM-dd','clientOptions' => ['dateFormat'=>'yy,M d'],'options' => ['class' => 'form-control','placeholder'=>'Expiry Date']])->label(false) ?>
                    </div>



            </div>
             <div class="col-md-12">
                        <input type="submit" value="Create" class="btn btn-success">
             </div>
             <?php ActiveForm::end();?>



            <div class="col-md-12 ">
                <table class="table table-striped table_responsive">
                    <thead><tr><th>Coupon</th><th>Discount(%)</th><th>Expiration Date</th><th>Action</th></tr></thead>
                    <tbody>
                        <?php $coupons= CourseCoupons::getCoupons($course->course_id);?>
                        <?php if($coupons!=null):?>
                        <?php foreach($coupons as $c):?>
                           <tr><td><?=$c->coupon_code?></td>
                               <td><?=$c->discount?></td>
                               <td><?=$c->expirey_date?></td>
                               <td><?= Html::a('<i title="delete coupon" class="fa fa-trash"></i>',['deletecoupon?courseid='.$c->id.'&redirect='.urlencode(Url::current())],['data-method'=>'post'])?></td></tr>
                        <?php endforeach;?>
                        <?php else:?>
                        <tr><td colspan="3" style="text-align:center">You have not created any coupon.</td></tr>
                        <?php endif;?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

<?php
// $this->registerJsFile(Url::base()."/js/bootstrap-switch.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);
$this->registerJsFile( Url::base().'/js/bootstrap-formhelpers.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
 ?>

 <div class="modal fade" id="bestprice" tabindex="-1" role="dialog" aria-labelledby="bestprice" aria-hidden="true">
   <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                 <h4 class="modal-title" id="myModalLabel">How To Determine Course Price</h4>
             </div>
             <div class="modal-body text-left" >
               <ul>
                  <li>Compare your price with that of your competitors on other course publishing site.
                  <li>Make your price as cheap as possible. Most buyers feels the cheaper the better.
                  <li>Compare your price with similar courses on Teachsity.
                  <li>Use coupons to give discount on your courses.
                  <li>Do not forget to mentiont in course description what makes your course unique and why they should enroll.
               </ul>
             </div>
         </div>
   </div>

 </div>
