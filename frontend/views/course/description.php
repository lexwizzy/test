<?php
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use frontend\models\Course;
    use marqu3s\summernote\Summernote;


 ?>


 <div class="main-content-title row  text-center"><h3 class="title">Course Description</h3></div>
     <div class="main-content-body row text-center">


       <?php $form= ActiveForm::begin()?>
       <h6 class="title">In less than 150 words give a sneak peek into about this course.</h6>
       <div class="col-md-12 text-left">
                    <?= $form->field($course, 'short_desc')->textArea(["max-length"=>"150","class"=>"form-control"])->label(false);?>
        </div>
       <h6 class="title">In less than 600 words talk about this course. Highlight what students will gain from taking this course.</h6>
       <div class="col-md-12 text-left">
                    <?= $form->field($course, 'course_desc')->widget(Summernote::className(),
                      ['clientOptions' => ["height"=>"100",'toolbar'=>[['style',['bold','italic','underline']]]]])->label(false) ?>

        </div>
        <div class="col-md-12 ">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
        <?php ActiveForm::end()?>

     </div>
