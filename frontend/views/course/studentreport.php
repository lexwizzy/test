<?php
 use yii\helpers\Url;
 use yii\helpers\Html;
 use app\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;

$this->registerJs('var baseUrl = '.json_encode(Url::base()),View::POS_BEGIN, 'my-options');
?>

<section id="course_edit" class="sub-banner sub-banner-create-course">
        <div class="awe-color bg-color-1"></div>
        <div class="container">
            <div id="course-title">
                <h2 class="md ilbl" id="title_label"><?= $course->course_title?></h2>
            </div>


        </div>
    </section>
    <!-- END / BANNER CREATE COURSE -->

    <!-- CREATE COURSE CONTENT -->
    <section id="create-course-section" class="create-course-section">
        <div class="container">
            <div class="row">
                    <div class="col-md-3">
                        <div class="create-course-sidebar">
                            <ul class="list-bar">
                              <li><a href="<?=Url::toRoute(["/course/basicinfo","slug"=>$course->slug])?>"><span class="count">1</span>Basic Information</a></li>
                              <li><a href="<?=Url::toRoute(["/course/contents","slug"=>$course->slug])?>"><span class="count">2</span>Course Curriculum</a></li>
                              <li><a href="<?=Url::toRoute(["/course/settings","slug"=>$course->slug])?>"><span class="count">3</span>Course Settings</a></li>
                              <li class="active"><span class="count">3</span>Student Report</li>
                              <li><a href="<?=Url::toRoute(["/course/preview","slug"=>$course->slug])?>"><span class="count">4</span>Preview Course</a></li>


                              </ul>
                            <div class="support">
                                <a href="create-basic-information.html#"><i class="icon md-camera"></i>See how it work short tutorial</a>
                                <a href="create-basic-information.html#"><i class="icon md-flash"></i>Instant Support</a>
                            </div>
                        </div>
                    </div>

                <div class="col-md-9" >

                    <div class="create-course-content">
                        <!-- Course goal-->
                        <div class="course-banner create-item">
                        <div class="row text-center">
                                <div  class="col-md-12 ">
                                                                 <h2 class="sm black bold">Student Reviews</h2>
                                </div>
                                <div class="col-md-9" >
                                    We have not collected any data yet.

                                </div>
                        </div>
                         </div>

                         <!-- Course goal-->
                        <div class="course-banner create-item">
                       <div class="row text-center">
                                <div  class="col-md-12 ">
                                                                 <h2 class="sm black bold">Course Engagement</h2>
                                </div>
                                <div class="col-md-9" >
                                    We have not collected any data yet.

                                </div>
                        </div>
                         </div>
                    </div>


            </div>
        </div>
    </section>

    <!-- END / CREATE COURSE CONTENT -->

<?php


?>
