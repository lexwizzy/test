<!-- SUB BANNER -->
<?php
    use frontend\models\User;
    use yii\helpers\Url;
    use frontend\models\Course;
    use frontend\models\SocialLink;
    use frontend\models\CourseParts;
    use frontend\models\CourseLessons;
    use yii\web\View;
    use yii\bootstrap\Modal;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\authclient\widgets\AuthChoice;
    use kartik\rating\StarRating;
    use frontend\models\LessonContent;
    use frontend\models\Enrollment;

    $icon="";
    $d="";
    $sec=0;
    $sec = LessonContent::getCourseVideoLength($course->course_id);
    $pagecount=LessonContent::getCoursePptPages($course->course_id);

    if($sec >= 3600){
        $sec = LessonContent::convertSeconds($sec,"H")." Hrs Videos";

    }
    else if($sec > 0 && $sec <3600){
        $sec = LessonContent::convertSeconds($sec,"s")." Mins Videos";
    }
    else{
      $sec=0;
    }
    $course_content_length ='<p class="total"><a href="#"><span><img src="'.Url::base().'/images/icon/video.png"></span> Total Video</a></p>
                          <p class="totale_right">'.$sec.'</p>';
    //
    // if(empty($pagecount)){
    //     $course_content_length .='<p class="total"><a href="#"><span><img src="/teachsity/frontend/web/images/icon/lesson_icon.png"></span> Total Video</a></p>
    //                           <p class="totale_right">'.$pagecount.' Pages</p>';
    // }
?>
<?php
$script=<<<JS
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
$(document).ready(function () {
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
$( document ).ready(function() {
    //custom share button for homepage
     $( ".share-btn" ).click(function(e) {
         $('.networks-3').not($(this).next( ".networks-3" )).each(function(){
            $(this).removeClass("active");
         });

            $(this).next( ".networks-3" ).toggleClass( "active" );
    });
});
//reaad more
  $('#info').readmore({
    moreLink: '<a href="#">Usage, examples, and options</a>',
    collapsedHeight: 384,
    afterToggle: function(trigger, element, expanded) {
      if(! expanded) { // The "Close" link was clicked
        $('html, body').animate({scrollTop: element.offset().top}, {duration: 100});
      }
    }
  });

  $('.readmore_bg').readmore({speed: 500});
        // $.ajaxSetup({ cache: true });
        //   $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
        //     FB.init({
        //       appId: '1680785638822349',
        //       version: 'v2.4' // or v2.0, v2.1, v2.2, v2.3
        //     });
        //     $('#loginbutton,#feedbutton').removeAttr('disabled');
        //     FB.getLoginStatus(updateStatusCallback);
        //   });
JS;
$this->registerJs($script);
?>
<?php
    $current =Url::current([],true);
    $page_url =Url::current([],true);
    // $this->registerJs($fb,View::POS_READY);
    $this->registerJs("var current =".json_encode($current),View::POS_HEAD);
  //  Yii::$app->view->registerMetaTag(['property' => 'og:url','content' => $current]);
    $this->registerMetaTag(['name' => 'description', 'content' => "Teachsity - ".$course->course_title],"description");
    Yii::$app->view->registerMetaTag(['itemprop' => 'og:headline','content' => "Teachsity - ".$course->course_title]);
    Yii::$app->view->registerMetaTag(['itemprop' => 'og:description','content' => strip_tags($course->course_desc)]);
    Yii::$app->view->registerMetaTag(['property' => 'og:type','content' => 'article']);
    Yii::$app->view->registerMetaTag(['property' => 'fb:app_id','content' => "[".Yii::getAlias("@fbid")."]"]);
    Yii::$app->view->registerMetaTag(['property' => 'og:title','content' => "Teachsity - ".$course->course_title]);
    Yii::$app->view->registerMetaTag(['property' => 'og:description','content' => strip_tags($course->course_desc)]);
    Yii::$app->view->registerMetaTag(['property' => 'og:image','content' => Url::home(true)."upload/coursebanners/".$course->course_banner]);
    $this->title= "Teachsity - ". $course->course_title;
    //$icon_array=array("fa-video-camera","fa-file-powerpoint-o","fa-file-pdf-o","fa-file");
    $name= User::getName($course->owner);
    $link=SocialLink::getLinks($course->owner);
    $this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);
    $lesson_counter=0;
    $quiz_counter=0;
    $counter=0;
    $sectionArray=array();
?>

<link href="<?=Url::base()?>/js/library/video-js/video-js.css" rel="stylesheet">

<div class="content_bg">
<div class="wrap">
	<div class="body_content">

           <div class="parallax_menu">
              <nav id="topnav" class="navbar navbar-fixed-top navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                      <ul class="nav navbar-nav">
                          <li><a href="#section1">Course Description</a></li>
                          <li><a href="#section2">Requirements</a></li>
                          <li><a href="#section3">Target Audience</a></li>
                          <li><a href="#section4">Lesson</a></li>
                          <li><a href="#section5">Review</a></li>
                      </ul>
                  </div>
              </nav>
		</div>



        <div class="course_width">
          <?php if(Yii::$app->session->hasFlash("error")):?>
                  <div class="col-md-12">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <div class="alert alert-danger text-center">  <?=Yii::$app->session->getFlash("error");?> </div>
                   </div>
           <?php endif;?>
          <div class="container-fluid">
          	<div class="page_width">
            <div class="row row_bg">
              <div class="col-sm-12 art_bg">
              		<div class="name_bg">
                      <img width="88" height="88" src="<?=User::getPhoto($course->owner,"black")?>" alt="<?=$name['firstname']." ".$name['lastname']?> teachsity profile picture">
                        <p class="name_txt"><?=$name['firstname']." ".$name['lastname']?> </p>
                    </div>

                    <div class="art_of_bg">
                    	<p class="art"><?=$course->course_title?></p>
                        <p class="art_tex"><?=$course->short_desc?></p>
                        <div class="rating"><?=  StarRating::widget([
                                            'name' => 'rating',
                                            'value'=> \frontend\models\CourseReviews::getAverageRating($course->course_id),
                                            'pluginOptions' => [
                                                'size' => 'xxs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>true,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]);

                                            ?></div>
                      <p class="rating_count"><?=\frontend\models\CourseReviews::totalReviews($course->course_id)?> ratings, <?=Enrollment::getEnrollmentCount($course->course_id)?> students enrolled</p>

                      	<div class="share-button sharer" style="display: block;">
                            <p class="share share-btn"><a href="javascript:void(0)">Share <span><img src="<?=Url::base()?>/images/share_icon.png"></span></a></p>
                            <div class="social top center networks-3 ">
                             <!-- Facebook Share Button -->
                                <a class="fbtn share facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?=$page_url?>" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a>
                                <a class="fbtn share gplus" href="https://plus.google.com/share?url=<?=$page_url?>" target="_blank" title="google plus"><i class="fa fa-google-plus"></i></a>
                                <a class="fbtn share twitter" href="https://twitter.com/intent/tweet?text=<?=$course->course_title?>&amp;url=<?=$page_url?>&amp;via=teachsity" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>

                    </div>

                    <div class="take_bg">
                    	<div class="take_box">
                       	  <p class="take_course">
                          <?php if(Yii::$app->user->isGuest):?>
                            <a href="javascript:void(0);" onclick="login();return false;">Take this course</a>
                          <?php else:?>
                                <?php if(strtolower($course->course_price)=="free"):?>
                                  <a href="<?=Url::toRoute(["/course/subscribe","slug"=>$course->slug])?>">Take this course</a></p>
                                <?php else:?>
                                    <?php //if():?>
                                      <a href="<?=Url::toRoute(["/payment/checkout","id"=>$course->course_id,"coupon"=>$coupon])?>">Take this course</a></p>
                                    <!-- < //else:
                                        <a href="//Url::toRoute(["/payment/checkout","id"=>$course->course_id,"coupon"=>$coupon])?>">Take this course</a></p>
                                     //endif -->
                              <?php endif;?>
                          <?php endif?>
                          </p>
                          <p class="dollor"><?=$processed_amount//after applying coupon and conversion rate?></p>
                          <p class="coupon"><a href="#" data-toggle="popover" data-placement="top" data-html=true data-content="<?=$couponForm?>">Apply coupon<span><img src="<?=Url::base()?>/images/right_icon.png"></span></a></p>
                          <!-- <p class="coupon"><a href="#">Start free preview<span><img src="<?php//Url::base()?>/images/right_icon.png"></span></a></p> -->
                        </div>
                    </div>

              </div>
            </div>
            </div>
          </div>
        </div>

          <div class="container-fluid">
          	<div class="page_width">
            <div class="row">
              <!-- class="col-sm-12 margin_top" style="z-index: -900; -->
              <div>
                <div class="description_right">
                	<div class="right_side_box">
                    	<img class="line_bg" src="<?=Url::base()?>/images/right_box_list.png">
                        <p class="lectures"><a href="#"><span><img src="<?=Url::base()?>/images/icon/leacturs.png"></span> Lectures</a></p>
                        	<p class="lecture_right"><?=CourseParts::getPartCount($course->course_id)?></p>
                          <?=$course_content_length;//cretae dynamic content?>
                        <p class="level"><a href="#"><span><img src="<?=Url::base()?>/images/icon/skill.png"></span> Skill Level</a></p>
                        	<p class="skill_right"><?= \frontend\models\SkillLevel::getSkillLevel($course->skill_level)?></p>
                        <p class="languages"><a href="#"><span><img src="<?=Url::base()?>/images/icon/langu.png"></span> Languages</a></p>
                        	<p class="languages_right"><?= $course->language?></p>
                        <p class="includes"><a href="#"><span><img src="<?=Url::base()?>/images/icon/include.png"></span> Includes</a></p>
                        	<p class="includes_right">Lifetime access
                          30 day money back guarantee!
                          Available on iOS and Android
                          Certificate of Completion</p>
                    </div>

                    <p class="right_title">Course you may like</p>
                    <div class="right_side_course">

                      <?php foreach(Course::getOtherCourseViewedByOthers($course->course_id)as $c):?>
                            <?php $coursead=Course::getCourseAd(0,$c);?>
                                <?=Course::createCourseDetailListAd($coursead)?>
                            <?php endforeach;?>

                    </div>
                </div>



                <div class="description_left">
                  <video id="" class="video-js vjs-default-skin vjs-big-play-centered"
                                          controls preload="auto" width="100%" height="450"
                                          poster="<?=Course::getCoursePromoVideoThumbnail($course);?>"
                                          data-setup=''>
                                         <source src="<?=Url::base()."/upload/promovideos/".$course->promo_video?>" type='<?=$course->promo_video_mime?>' />
                                         <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                              </video>

                  <div class="description_bg readmore_bg" id="section1">
                  	<p class="descrip_title">Course Description</p>
                    <?= $course->course_desc;?>
                    <!--<p class="full_detail"><a href="#">Full Detail</a></p>-->
                  </div>


                  <div class="requirements_bg" id="section2">
                  	<p class="descrip_title" id="requirements">What are the requirements?</p>
                      <ul>
                          <?php if(!empty($course->course_tools)):?>
                           <?php foreach(json_decode($course->course_tools,true) as $r):?>
                             <?php foreach($r as $rq):?>
                              <?= "<li>".$rq."</li>"?>
                              <?php endforeach;?>
                           <?php endforeach;?>
                           <?php endif;?>
                      </ul>
                  </div>

                  <div class="requirements_bg">
                  	<p class="descrip_title">What am I going to get from this course?</p>
                    <ul>
                      <?php if(!empty($course->course_goal)):?>
                         <?php foreach(json_decode($course->course_goal,true) as $r):?>
                           <?php foreach($r as $rq):?>
                            <?= "<li>".$rq."</li>"?>
                            <?php endforeach;?>
                         <?php endforeach;?>
                      <?php endif;?>
                    </ul>
                  </div>


                  <div class="requirements_bg" id="section3">
                  	<p class="descrip_title" id="target">What is the target audience?</p>
                      <ul>
                        <?php if(!empty($course->target_audience)):?>
                           <?php foreach(json_decode($course->target_audience,true) as $r):?>
                             <?php foreach($r as $rq):?>
                              <?= "<li>".$rq."</li>"?>
                              <?php endforeach;?>
                           <?php endforeach;?>
                           <?php endif;?>
                      </ul>
                  </div>


                </div>

                <div class="v_line"></div>

              </div>
            </div>
          </div>
          </div>






          <div class="container-fluid lesson_bg" id="section4">
          		<div class="row lessons_row">

                    <div class="lesson_tab">
                    <p class="tab_title">Lessons</p>
                    <div id="verticalTab">
                      <ul class="resp-tabs-list">
                        <?php foreach(Course::getSections($course->course_id) as $section):?>
                            <li>Section <?=$section->counter?>: <?=$section->part_name?></li>
                            <?php
                              $sectionArray[]=$section->part_id;
                             ?>
                        <?php endforeach?>
                      </ul>

                      <div class="resp-tabs-container">
                        <?php foreach($sectionArray as $counter):?>
                        <?php $outline = \frontend\models\CourseOutlineOrder::find()->where(['and',["section"=>$counter],['courseid'=>$course->course_id]])->orderBy("counter asc")->all();?>


                          <div class="tav1 tab_commen">
                            	<div class="tab_bg">
                                	<ul>
                                    <?php foreach($outline as $order):?>
                                        <?php if($order->type=="lesson"):?>
                                          <?php
                                          $units= CourseLessons::findOne([$order->itemid]);
                                          ++$lesson_counter;
                                           $main_content= CourseLessons::getUnitMainContent($units->lesson_id);

                                           if($main_content!=null){

                                             if($main_content["type"]=="video"){
                                                $d=$main_content["video_length"];
                                                $icon="lesson_icon2.png";
                                             }else if($main_content["type"]=="pdf"){
                                               $d=$main_content["page_no"]." Pages";
                                               $icon="lesson_icon.png";
                                             }
                                             else {
                                               $icon="lesson_icon.png";
                                             }
                                              //  $d=  ?gmdate("i:s",$main_content->video_length):$main_content->no_of_pages." Pages";
                                            }
                                          ?>
                                        	<li><a href="#"><span class="lesson_icon"><img src="<?=Url::base()?>/images/icon/<?=$icon?>"/></span>Lecture <?=$lesson_counter?>: <?=$units->lesson_title?>  <span class="lesson_time"><?=$d?></span></a></li>
                                          <?php else :?>
                                          <?php ++$quiz_counter;?>
                                          <?php $q = \frontend\models\Quiz::findOne([$order->itemid]);//?>
                                          <li><a href="#"><span class="lesson_icon"><img src="<?=Url::base()?>/images/icon/quiz_icon.png"></span>Quizz <?=$quiz_counter?>: <?=$q->q_name?> <span class="lesson_time"><?=\frontend\models\Quiz::getNumberOfQuestion($q->q_id)?> questions</span></a></li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                  </ul>
                                </div>
                              </div>
                              <?php
                                $quiz_counter=0;
                                $lesson_counter=0;
                              ?>
                              <?php endforeach;?>
                          </div>
                    </div>
                    </div>


                </div>
          </div>




          <div class="review_bg" id="section5">
              <div class="container review_width">
                    <div class="row review_content">
                        <p class="review">Courses Reviews</p>
                        <div class="col-sm-3 col-xs-1 review_left">
                            <div class="average">Average Rating</div>
                            <p class="rate_left"><?=\frontend\models\CourseReviews::getAverageRating($course->course_id)?></p>
                            <p class="rate_star"><span class="rate_number"><?=\frontend\models\CourseReviews::totalReviews($course->course_id)?> ratings</span>
                              <?=  StarRating::widget([
                                                  'name' => 'rating',
                                                  'value'=> \frontend\models\CourseReviews::getAverageRating($course->course_id),
                                                  'pluginOptions' => [
                                                      'size' => 'xs',
                                                      'stars' => 5,
                                                      'min' => 0,
                                                      'step' => 1,
                                                      'readonly'=>true,
                                                      'clearButton'=>'',
                                                      'max' => 5,
                                                      'showCaption' => false,

                                                      ],
                                                  ]);

                                                  ?></p>
                        </div>
                      <div class="col-sm-9 col-xs-1 review_right">
                            <div class="v_line2"></div>
                             <?= \frontend\models\CourseReviews::designReviewBox($course->course_id)?>




						  <p class="show_more"><a href="#">Show more reviews <span><img src="<?=Url::base()?>/images/more_arrow.png"></span></a></p>

                        </div>
                    </div>
              </div>
          </div>





	</div>
</div>
</div>

<!---content section end-->





<?php $this->registerJsFile( Url::base().'/js/easy-responsive-tabs.js',['position'=>View::POS_BEGIN,'depends' => [\yii\web\JqueryAsset::className()]] ); ?>
<?php $this->registerJsFile( Url::base().'/js/readmore.js',['depends' => [\yii\web\JqueryAsset::className()]] ); ?>
<?php $this->registerJsFile( Url::base().'/js/coursedetails.js',['depends' => [\yii\web\JqueryAsset::className()]] ); ?>
