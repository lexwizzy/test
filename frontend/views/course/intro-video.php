<?php
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\BootstrapAsset;
use frontend\models\Course;


$this->registerJsFile(Url::base()."/js/library/video-js/video.js",['position'=>View::POS_HEAD]);
$this->registerJs('var baseUrl = '.json_encode(Url::base()),View::POS_BEGIN, 'my-options');
 ?>
<style>
#video-display{
  width: 100%;
  height: 100%;
  background: #bbb;
  padding: 10px;
}
.video-js {
      padding-top:0 !important;
  }
#promo_video{
  width: 100%;
  height: 350px;
}
#video-text-content{
    background: #fff;
    padding: 20px;
    color: #333;
}
#file-input-upload-div{
  margin-top: 20px;
}
.no-video{
    /* min-width: 30%; */
    min-height: 200px;
    background: #eee;
    padding:80px;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/fileinput.min.css">
<div class="main-content-title row  text-center"><h3 class="title">Introduction Video</h3></div>
    <div class="main-content-body row text-center">

      <div class="col-md-8 col-md-offset-2 col-md-offset-right-2">
            <div id="video-display">
              <?php if(!empty($course->promo_video)):?>
              <video id="promo_video" class="video-js vjs-defau-skin vjs-big-play-centered"
                       controls preload="auto" width="auto" height="auto"
                       poster="<?=Course::getCoursePromoVideoThumbnail($course);?>"
                       >
                      <source src="<?=$videopath?>"  />
                      <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
              </video>
            <?php else:?>
              <div class="no-video">
                  <h4 class="title">Upload an introduction video.</h4>
              </div>
            <?php endif;?>
              <div id="video-text-content">
                  Upload an introductory video to give student an insight on your course.
                  A good introductory video is likely to entise student to enroll for a course

                  <p><a href="http://www.support.teachsity.com">See instructions on how to make a good intro video</a></p>
              </div>

            </div>
      </div>

      <div id="file-input-upload-div" class="col-md-8 col-md-offset-2 col-md-offset-right-2">
        <input type="file" class="file-loading" id="promovideo"  name="TcUploadedContent[single_upload]">
        <div class="form-item">
              <span>maximum video size is 5GB. file types 'mp4','avi'</span>
        </div>
      </div>
    </div>

<?php
$this->registerJsFile( Url::base().'/js/fileinput.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
$script=<<<EOD
$("#promovideo").fileinput({
         previewFileType:"any",
        showCaption:true,
        showPreview :false,
        showRemove : true,
        showUpload: true,
        overwriteInitial:true,
        maxFileSize:625000,
        browseClass:"btn btn-danger",
        allowedFileExtensions:['mp4','avi'],
        uploadUrl: baseUrl+"/course/promovideo", // server upload action
        browseLabel: " Select Video",
        browseIcon: "<i class='fa fa-file-video-o'></i>",
        removeClass: "btn btn-danger",
        removeLabel: " Delete",
        removeIcon: "<i class=\"fa fa-trash\"></i>",
        uploadClass: "btn btn-info",
        uploadLabel: "Upload",
        uploadIcon: "<i class=\"icon md-upload\"></i>",
        uploadExtraData:function() {
            var obj = {};

              obj['courseid']=$course->course_id;
            return obj;
        }
  });
EOD;
$this->registerJS($script);
 ?>
