<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use frontend\models\Course;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
$li = "";

?>
<style>
  #category-result{
    background: #fff;
  }
  .pagination_bg {
     width: 100%;
  }
  /*#category-result .course_right {
    float: right;
    width: 100%;
    height: auto;
    padding: 20px;
    padding-left: 4em;
    padding-right: 4em;
}*/
</style>
<div class="">
  <div class="wrap">
    <div id="category-result" class="course_container courses_page">
          <div class="course_bg">  <!-----right slide------>
          <div class="course_right">
                <div class="course_item_bg">
                <div class="pagination_bg">
                      <div class="pagination_left">
                          	<p>Showing courses in <?=$q?> categories</p>
                      </div>

                </div>
                  <div class="">

                    <?php Pjax::begin(["id"=>"pjax_course_search_result"]); ?>
                          <?=
                              ListView::widget([
                                'dataProvider' => $pages,
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'row',
                                    'id' => 'category-result-list-wrapper',
                                        ],
                                'itemView'=>"_search_list_item",
                                'layout' => "{items}\n<div class='clear'></div><div class='text-center'>{pager}</div>",
                                // 'pager'=>[
                                //   'firstPageLabel'=>'<img src="'.Url::base().'/images/pagination_left.jpg">',
                                //   'prevPageLabel'=>'<img src="'.Url::base().'/images/pagination_left.jpg">',
                                //   'lastPageLabel'=>'<img src="'.Url::base().'/images/pagination_right.jpg">',
                                //   'nextPageLabel'=>'<img src="'.Url::base().'/images/pagination_right.jpg">',
                                //   //'pageCssClass'=>'pager'
                                // ]
                              ]

                              );
                          ?>
                      <?php Pjax::end()?>
                  </div>
                </div>
                <div class="clear"></div>

                <div class="clear"></div>
           </div>
      <!-----right slide------>

          </div>
      </div>


  </div>
</div>
