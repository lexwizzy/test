<style>
    .video-js {
        padding-top:24.25% !important;
    }


    .vjs-fullscreen {padding-top: 0px}
    .videocontent{
        width:100%;
        max-width:100%;
    }
    .sub-banner {
        position: relative;
        padding: 6px 0;
        background:#000;
        color:#fff;
        /* z-index: 99; */
    }
    .tabs-page {
         background: none !important;
    }
    .sub-banner h2{
        color:#fff !important;
    }
    #tool-bar{
        padding-top: 19px;
    }
    #tool-bar i{
        font-size: 20px; color: #fff;
    }
    #tool-bar ul li{
        list-style:none;
        display:inline;
    }
    #tool-bar  ul li i{
        cursor:pointer;
    }
    .tutor-name{
        font-weight:bold;
        padding-left:5px;
        max-width:190px;
    }
    .profile-pix{
        height:45px;
        width:45px;
    }
    .flag-this{
        padding-left:20px
    }
    .course-desc,.support{
        padding-left:15px
    }
    .video-course-intro {
        background-color: #fff;
        margin-top: 35px;
    }
    .current-progress {
        margin-top: 12px;
    }
</style>
<link href="<?=Url::base()?>/js/library/video-js/video-js.css" rel="stylesheet">
<link rel="stylesheet" href="<?=Url::base()?>/css/ladda.min.css">
<section class="sub-banner sub-banner-course">

        <div class="container">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <h2><?=$course->course_title?></h2>
                    </div>
                    <div class="col-md-6 text-right " id="tool-bar" >
                        <ul>
                            <li>
                            <img style="height:45px; width:45px" class="image-responsive" src="<?=User::getPhoto($course->owner)?>" alt="<?=$name['firstname']." ".$name['lastname']?> teachsity profile picture">
                            </li>
                            <li class="tutor-name">
                                <?=$name['firstname']." ".$name['lastname']?>
                            </li>
                            <li class="flag-this">
                               <i data-toggle="modal" data-target="#flagabuse" title ="Flag Abuse" class="fa fa-flag  fa-3x"></i>
                            </li>
                            <li class="course-desc">
                                <i data-toggle="coursedesc" data-target="#coursedesc"  class="fa fa-info-circle fa-3x " id="courseinfo" title="Information about this course"></i>
                            </li>
                            <li class="support">
                                <a href="http://t-support.teachsity.com"><i class="fa fa-question" title="Get in  touch with T-Support"></i></a>
                            </li>
                            <li>

                            </li>
                        </ul>


                    </div>
                </div>

        </div>
    </section>
<!--<section>
    <div class="container">
         <div class="row">
                <?php /* StarRating::widget([
            'name' => 'rating',
            'id'=>'camo-rating',
            'value'=> $rating->rating,
            'pluginOptions' => [
                'size' => 'xs',
                'stars' => 5,
                'min' => 0,
                'step' => 1,
                'max' => 5,
                'showCaption' => true,
                'starCaptions' => [
                1 => 'Extremely Poor',
                2 => 'Poor',
                3 => 'Good',
                4 => 'Very Good',
                5 => 'Extremely Good',
                ]
                ],
            ]);
                */
            ?>
            </div>
        </div>

        </div>
    </div>

</section>-->
<section class="course-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="sidebar-course-intro">


                        <div class="video-course-intro">
                            <div>
                                 <video id="example_video_1" class="video-js vjs-defau-skin vjs-big-play-centered"
                                          controls preload="auto" width="auto" height="auto"
                                          poster="<?=Yii::getAlias("@web")."/upload/promovideos/thumbnails/".$course->promo_video_thumbnail?>"
                                          data-setup='{"example_option":true}'>
                                         <source src="<?=Url::base()."/upload/promovideos/".$course->promo_video?>" type='<?=$course->promo_video_mime?>' />
                                         <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                                        </video>
                            </div>
                        </div>
                         <!-- CURRENT PROGRESS -->

                        <div class="current-progress">
                            <h4 class="sm black">Current Progress</h4>
                            <div class="percent">Complete <span class="count"><?= $progress?>%</span></div>
                            <div class="progressbar">
                                <div class="progress-run progress-run-add" style="width: <?=$progress?>%;"></div>
                            </div>
                            <ul class="current-outline">
                               <li><span><?=\frontend\models\CourseParts::getPartCount($course->course_id)?></span>section</li>
                                <li><span><?=\frontend\models\Quiz::getQuizCount($course->course_id)?></span>quizzes</li>
                                <li><span><?=\frontend\models\CourseLessons::getLessonCount($course->course_id)?></span>units</li>
                            </ul>
                        </div>
                        <!-- END / CURRENT PROGRESS -->
                        <div class="widget widget_share">
                            <i class="icon md-forward"></i>
                            <h4 class="xsm black bold">Share course</h4>
                            <div class="share-body">
                                <a href="#" class="twitter" title="twitter">
                                    <i class="icon md-twitter"></i>
                                </a>
                                <a href="#" class="pinterest" title="pinterest">
                                    <i class="icon md-pinterest-1"></i>
                                </a>
                                <a href="#" class="facebook" title="facebook">
                                    <i class="icon md-facebook-1"></i>
                                </a>
                                <a href="#" class="google-plus" title="google plus">
                                    <i class="icon md-google-plus"></i>
                                </a>
                            </div>
                        </div>
                        <hr class="line">
                        <div class="about-instructor">
                            <h4 class="xsm black bold">About Instructor</h4>
                            <ul>
                                <li>
                                     <div class="image-instructor text-center">
                                        <img src="<?=User::getPhoto($course->owner)?>" alt="<?=$name['firstname']." ".$name['lastname']?> teachsity profile picture">
                                    </div>
                                    <div class="info-instructor">
                                        <cite class="sm black"><a href="#instructor-popup"><?=$name['firstname']." ".$name['lastname']?> </a></cite>
                                        <a target="_blank" href="<?=$link['facebook']?>"><i class="fa fa-facebook-square"></i></a>
                                        <a target="_blank" href="<?=$link['twitter']?>"><i class="fa fa-twitter-square"></i></a>
                                        <a target="_blank"href="<?=$link['google']?>"><i class="fa fa-google-plus-square"></i></a>
                                        <a target="_blank" href="<?=$link['linkedin']?>"><i class="fa fa-linkedin-square"></i></a>
                                        <a target="_blank" href="<?=$link['github']?>"><i class="fa fa-github-square"></i></a>
                                        <p><?=User::getAboutme($course->owner);?></p>
                                    </div>
                                </li>

                            </ul>
                        </div>


                    </div>
                </div>
                <div class="col-md-7">
                    <div class="tabs-page">
                        <div class="nav-tabs-wrap"><ul class="nav-tabs" role="tablist">
                            <li class="active"><a href="#outline" role="tab" data-toggle="tab">Outline</a></li>
                            <li class="itemnew"><a href="#announcement" role="tab" data-toggle="tab">Announcement</a></li>
                            <li class="itemnew"><a href="#discussion" role="tab" data-toggle="tab">Discussion</a></li>
                            <li><a href="#review" role="tab" data-toggle="tab">Review</a></li>
                            <li class=""><a href="#student" role="tab" data-toggle="tab">Student</a></li>
                        <li class="tabs-hr" style="left: 0px; width: 53px;"></li></ul></div>
                        <!-- Tab panes -->
                        <div class="tab-content">

                            <!-- OUTLINE -->
                            <div class="tab-pane fade active in" id="outline">


                                    <?php foreach(Course::getSections($course->course_id) as $section):?>
                                    <!-- SECTION OUTLINE -->
                                    <div class="section-outline">
                                        <h4 class="tit-section xsm">Section <?=$section->counter?>: <?=$section->part_name?></h4>
                                         <ul class="section-list">

                                             <?php $outline = \frontend\models\CourseOutlineOrder::find()->where(['and',["section"=>$section->part_id],['courseid'=>$course->course_id]])->orderBy("counter asc")->all();?>
                                            <?php foreach($outline as $order):?>
                                             <?php if($order->type=="lesson"):?>
                                             <?php $units= CourseLessons::findOne([$order->itemid]);//foreach(Course::getUnits($section->part_id) as $units):?>
                                        <li class="<?=\frontend\models\CourseLearnTracking::find()->where(["lesson_type"=>"lesson","courseid"=>$course->course_id,'userid'=>Yii::$app->user->id,"section"=>$units->part_id,"lesson"=>$units->lesson_id])->one() !=null? "o-view": ""?> ">

                                            <div class="count"><span><?=$order->counter?></span></div>
                                            <div class="list-body">
                                                <?php
                                                     $main_content= CourseLessons::getUnitMainContent($units->lesson_id);
                                                 ?>

                                                <i class="icon fa <?= $main_content!=null? $icon_array[CourseLessons::getContentType($main_content->content_no)]: "fa-file-text-o" ?>"></i>
                                                <p><a><?=$units->lesson_title?></a></p>
                                                <div class="data-lessons">

                                                    <span><?php
                                                                if($main_content!=null){
                                                                   echo $main_content->video_length!=0 ?gmdate("i:s",$main_content->video_length):$main_content->no_of_pages." Pages";
                                                                }

                                                        ?></span>
                                                </div>
                                                <!--<div class="download">
                                                    <a href="#"><i class="icon md-download-1"></i></a>
                                                    <div class="download-ct">
                                                        <span>Reference 12 mb</span>
                                                    </div>
                                                </div>-->
                                                <div class="div-x"><i class="icon md-check-2"></i></div>
                                                <div class="line"></div>
                                            </div>
                                                <div style="clear:both"></div>
                                             <a data-outlineno="<?=$units->outlineno?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/lesson/".$units->lesson_id])?>" class="wrap-div"> </a>
                                           <!-- <a href="course-intro.html#" class="mc-btn-2 btn-style-2">Preview</a>-->
                                        </li>
                                        <!--end lesson-->
                                        <?php else :?>
                                        <?php $q = \frontend\models\Quiz::findOne([$order->itemid]);//foreach(\frontend\models\Quiz::getAllQuiz($section->part_id) as $q):///get quizzes?>
                                             <li class="<?=\frontend\models\CompletedQuiz::find()->where(['userid'=>Yii::$app->user->id,"quizid"=>$q->q_id])->one() !=null? "o-view": ""?>">
                                                  <div class="count"><span><i class="icon md-search"></i></span></div>
                                                 <!--<div class="count"><span><?=$order->counter?></span></div>-->
                                            <div class="list-body">
                                                <i class="icon md-files"></i>
                                                <p><a href="#"><span>Quiz <?=$q->counter?> :</span> <?=$q->q_name?></a></p>
                                                <div class="data-lessons">
                                                    <span><?= \frontend\models\Quiz::getNumberOfQuestion($q->q_id)?> Questions</span>
                                                </div>
                                                <div class="div-x"><i class="icon md-check-2"></i></div>
                                                <div class="line"></div>
                                            </div>
                                                 <a data-outlineno="<?=$q->outlineno?>" href="<?=Url::toRoute(['/course/classroom','slug'=>$course->slug,"#"=>"/quiz/".$q->q_id])?>" class="wrap-div"> </a>
                                             </li>

                                        <!--<?php //endforeach;?> End quiz-->
                                             <?php endif; ?>
                                             <?php endforeach;?>
                                               </ul>
                                         </div>
                                <!-- END / SECTION OUTLINE -->
                                   <?php endforeach;?>

                            </div>
                            <!-- END / OUTLINE -->

                            <!-- ANNOUNCEMENT -->
                            <div class="tab-pane fade" id="announcement">
                                <ul class="list-announcement">

                                    <!-- LIST ITEM -->
                                   <?= \frontend\models\CourseAnnouncement::getCourseAnnouncement($course->course_id)?>
                                    <!-- END / LIST ITEM -->
                                </ul>
                            </div>
                            <!-- END / ANNOUNCEMENT -->

                            <!-- DISCUSSION -->
                            <div class="tab-pane fade" id="discussion">
                                <h3 class="md black"><?= \frontend\models\Discussions::getCount($course->course_id)?> Topics</h3>
                                <div class="form-discussion">
                                     <?php $form=ActiveForm::begin(['id'=>'discussionform','options'=>['onsubmit'=>"return false"]]);?>
                                        <div class="text-title">
                                            <?= $form->field($discussion,"topic")->textInput(['placeholder'=>"Enter discussion topic"])->label(false)?>
                                        </div>

                                        <div class="post-editor ">


                                             <?= $form->field($discussion,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
                                            <?= $form->field($discussion,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
                                             <?= $form->field($discussion, 'comment')->widget(Summernote::className(),
                                    ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
                                        </div>

                                        <div class="form-submit">
                                            <button type="submit" id="save-discussion" class="ladda-button" data-color="blue" data-style="expand-right" data-size="xs">Post Discussion</button>
                                        </div>
                                    <?php $form=ActiveForm::end();?>
                                </div>

                                <ul class="list-discussion">

                                    <!-- LIST ITEM -->
                                    <?= \frontend\models\Discussions::displayDiscussion($course->course_id)?>
                                    <!-- END / LIST ITEM -->


                                </ul>

                                <div class="load-more">
                                    <a href="">
                                    <i class="icon md-time"></i>
                                    Load more previous update</a>
                                </div>
                            </div>
                            <!-- END / DISCUSSION -->

                            <!-- REVIEW -->
                            <div class="tab-pane fade" id="review">
                                <div class="row">
                                    <div col-md-12>
                                        <div id="alert-rating-success" class="alert alert-success" style="display:none"></div>
                                        <div id="alert-rating-error"  class="alert alert-danger " style="display:none"></div>
                                    </div>
                                    <div class="col-md-3">
                                        <h3 class="md black"><?=\frontend\models\CourseReviews::totalReviews($course->course_id)?> Reviews</h3>
                                    </div>

                                    <div class="col-md-4 col-md-push-5 text-right" style="padding-top: 17px;">

                                       <?=  StarRating::widget([
                                            'name' => 'rating',
                                            'value'=> \frontend\models\CourseReviews::getAverageRating($course->course_id),
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'stars' => 5,
                                                'min' => 0,
                                                'step' => 1,
                                                'readonly'=>true,
                                                'clearButton'=>'',
                                                'max' => 5,
                                                'showCaption' => false,

                                                ],
                                            ]);

                                            ?>
                                    </div>
                                </div>
                                <div class="form-review">
                                   <?php $form=ActiveForm::begin(['id'=>'ratingform']);?>
                                        <div class="review-editor">
                                             <?= $form->field($rating,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
                                            <?= $form->field($rating,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
                                             <?= $form->field($rating, 'comment')->widget(Summernote::className(),
                                    ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
                                        </div>


                                    <div class="row">
                                        <div class="col-md-2" style="padding-top:10px"><span>Your rate</span></div>
                                        <div class="col-md-6">

                                            <?=
                                                $form->field($rating, 'rating')->widget(StarRating::classname(), [
                                                    'pluginOptions' => [
                                                    'size' => 'xs',
                                                    'stars' => 5,
                                                    'step' => 1,
                                                    'min' => 0,
                                                    'max' => 5,
                                                    'clearButton'=>'',
                                                    'showCaption' => true,
                                                    'starCaptions' => [
                                                    1 => 'Extremely Poor',
                                                    2 => 'Poor',
                                                    3 => 'Good',
                                                    4 => 'Very Good',
                                                    5 => 'Extremely Good',
                                                    ]
                                                    ],
                                                ])->label(false);
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                             <div class="form-submit">
                                                <button type="submit" id="save-review" class="ladda-button" data-color="blue" data-style="expand-right" data-size="xs">Post Review</button>
                                                
                                            </div>
                                        </div>
                                    </div>

                                     <?php $form=ActiveForm::end();?>
                                    
                                </div>
                                <ul class="list-review">
                                    <?= \frontend\models\CourseReviews::designReviewBox($course->course_id)?>
                                </ul>
                                <div class="load-more">
                                    <a href="">
                                    <i class="icon md-time"></i>
                                    Load more previous update</a>
                                </div>
                            </div>
                            <!-- END / REVIEW -->

                            <!-- STUDENT -->
                            <div class="tab-pane fade" id="student">
                                <h3 class="md black"><?=\frontend\models\Enrollment::getEnrollmentCount($course->course_id)?> Students applied</h3>
                                <div class="tab-list-student">
                                    <ul class="list-student">
                                        <!-- LIST STUDENT -->
                                       <?= \frontend\models\Enrollment::designLearnEnrollment($course->course_id)?>
                                        <!-- END / LIST STUDENT -->

                                    </ul>
                                </div>
                                <div class="load-more">
                                    <a href="">
                                    <i class="icon md-time"></i>
                                    Load more previous update</a>
                                </div>
                            </div>
                            <!-- END / STUDENT -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section id="course-concern" class="course-concern">
        <div class="container">
            <h3 class="md black">Courses you may concern</h3>
            <div class="row">
                   <!-- MC ITEM -->
                   <?php $_course = Course::getCourseSameCategory($course->course_category,$course->course_id,4);?>
                        <?php foreach($_course as $c):?>
                        <?php $coursead=Course::getCourseAd($c->course_id);?>
                <div class="col-xs-6 col-sm-4 col-md-3">
                            <?=Course::createGuestCourseAd($coursead,"mc-item-2");?>
                </div>
                        <?php endforeach;?>
                    <!-- END / MC ITEM -->
            </div>
        </div>
    </section>


<!-- Course Descriptions Modal -->
<div class="modal fade" id="coursedesc" tabindex="-1" role="dialog" aria-labelledby="coursedesc">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">About this course</h4>
      </div>
      <div class="modal-body" style="border: 1px solid #eee;margin: 7px;">
           <h4 class="sm black bold">Course Description</h4>
                                <p><?= $course->course_desc;?></p>
                                <div class="goals">

                                <h4 class="sm black bold">Goal of Course</h4>
                                    <?= $course->course_goal?>
                                </div>
                                <div id="target_audience">

                                <h4 class="sm black bold">Target Audience</h4>
                                     <?= $course->target_audience?>
                                </div>

      </div>
      <div class="modal-footer">

    </div>
  </div>
</div>
</div>
<!-- Flag Abuse Modal -->
<div class="modal fade" id="flagabuse" tabindex="-1" role="dialog" aria-labelledby="flagabuse">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Report Abuse for: <?= $course->course_title ?></h4>
      </div>
      <div class="modal-body">
          <div id="alert-flag-success" class="alert alert-success" style="display:none"></div>
          <div id="alert-flag-error"  class="alert alert-danger " style="display:none"></div>
        <?php $form=ActiveForm::begin(['id'=>'flagabuseform','options'=>['onsubmit'=>"return false"]]);?>

        <div>
            <?= $form->field($flag,"reason")->dropDownList(ArrayHelper::map(\frontend\models\TypesOfAbuse::find()->all(),'id','name'),['prompt'=>'What issue do you want to flag?'])->label(false)?>
        </div>

          <?= $form->field($flag,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
          <?= $form->field($flag,"courseid")->hiddenInput(['value'=>$course->course_id])->label(false)?>
         <div>
              <?= $form->field($flag,"comment")->textArea(['placeholder'=>"Enter comment",'class'=>"form-control"])->label(false)?>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" id="saveflag" value="Submit"/>
      </div>
          <?php ActiveForm::end();?>
    </div>
  </div>
</div>
<?php $this->registerJsFile( Url::base().'/js/spin.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/ladda.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile( Url::base().'/js/courselearn.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]]); ?>
