<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\models\Course;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses');
?>
<style>

     #subscribe-mesg-div{
        padding-top: 20px;
        padding-bottom: 20px;
     }
    .subscribe-success h5{
        color:#1F1B1B !important;
        font-weight:bold;
        font-family: !inherit;
    }
    .subscribe-success  btn{
        font-weight:bold;
    }
    .btn-div{
        padding-top:3em;
    }
  .content_bg {
      padding-bottom: 66px;
    }
    .pagination_left p {
    font-size: 24px;
}
</style>
<section class="page-control">
        <div class="container" id="subscribe-mesg-div">

            <div>
                <?php if(Yii::$app->session->hasFlash("error")):?>
                    <div class="alert alert-danger"><?=Yii::$app->session->getFlash("error")?></div>
                <?php else:?>
                    <div class="subscribe-success">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?=Course::getCourseBanner($course->course_banner)?>"/>
                            </div>
                            <div class="col-md-7">
                                <div><h5 class="title success-msg">Good Stuff! You have enrolled in</h5></div>

                                <div><h2 class="title"><?=$course->course_title?> </h2></div>
                            </div>
                            <div class="col-md-2 btn-div">
                                <a href="<?=Url::toRoute(["/course/learn",'slug'=>$course->slug])?>"><button class="btn btn-success btn-lg">Enter course room</button></a>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </section>
<div class="">
  <div class="wrap">
    <div class="course_container courses_page">
          <div class="course_bg">  <!-----right slide------>
          <div class="course_right">
                <div class="course_item_bg">
                <div class="pagination_bg">
                      <div class="pagination_left">
                          	<p>Because you viewed "<?=$course->course_title?>"</p>
                      </div>
                      <!-- <div class="pagination_right">
                          <ul>
                              <li>Pages</li>
                              <li><a href="#">01</a></li>
                              <li>of</li>
                              <li class="last_page"><a href="#">30</a></li>
                              <li class="page_arrow"><a href="#"><img src="images/pagination_left.jpg"></a></li>
                              <li class="page_arrow"><a href="#"><img src="images/pagination_right.jpg"></a></li>
                          </ul>
                      </div> -->
                </div>
                  <div class="">
                    <?php foreach(Course::getCourseSameCategory($course->course_category,$course->course_id,8)as $c):?>
                    <?php $coursead=Course::getCourseAd($c->course_id);?>
                            <?=Course::createGuestOtherCourseAd($coursead,"mc-item-2")?>

                    <?php endforeach;?>
                  </div>
                </div>
                <div class="clear"></div>
                <div class="course_item_bg">
                <div class="pagination_bg">
                      <div class="pagination_left">
                            <p>Newly created courses</p>
                      </div>
                      <!-- <div class="pagination_right">
                          <ul>
                              <li>Pages</li>
                              <li><a href="#">01</a></li>
                              <li>of</li>
                              <li class="last_page"><a href="#">30</a></li>
                              <li class="page_arrow"><a href="#"><img src="images/pagination_left.jpg"></a></li>
                              <li class="page_arrow"><a href="#"><img src="images/pagination_right.jpg"></a></li>
                          </ul>
                      </div> -->
                </div>
                  <div class="">

                      <?php $course = Course::find()->where(["status"=>1])->orderBy("createdate desc")->limit(8)->all();?>
                       <?php foreach($course as $c):?>

                       <?php $coursead=Course::getCourseAd($c->course_id);?>
                          <?=Course::createGuestOtherCourseAd($coursead,"mc-item-2");?>
                       <?php endforeach;?>
                  </div>
                </div>
                <div class="clear"></div>
           </div>
      <!-----right slide------>

          </div>
      </div>


  </div>
</div>
