<?php
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Button;
use yii\helpers\Html;
use kartik\grid\GridView;
use \kartik\editable\Editable;
use yii\helpers\ArrayHelper;

?>
<style>
  #creategroup .modal-dialog, #addusermodal .modal-dialog{
    max-width: 420px
  }
  .field-tccoursegroup-createdby{
    display: none;
  }
</style>
<div class="main-content-title row  text-center"><h3 class="title">Course Privacy</h3></div>
    <div class="main-content-body row text-center">
        <div style="margin-bottom:30px;"class="sub-heading">By default your course is viewable to everyone on Teachsity. To make your course accessible to only certain Students/Employees( T-Executive) make your course private and create groups. Private courses are accessible only via invitations.</div>
        <?php $form = ActiveForm::begin(["layout"=>"horizontal",'fieldConfig'=>[
          'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
          'horizontalCssClasses' => [
              'label' => 'col-sm-4 col-xs-12',
              'error' => '',
              'hint' => '',
          ]
          ]])?>
          <?=$form->field($settings,"privacy")->dropDownList(["public"=>"Public","private"=>"Private"],["id"=>"privacy"])?>
          <?=$form->field($settings,"password")->passwordInput(["id"=>"pwd"]);?>

          <?=Button::widget(["label"=>"Save","options"=>["type"=>"submit","class"=>"btn btn-success"]])?>
        <?php ActiveForm::end()?>

        <div class="group-grid text-left">
          <p>
              <?= Html::a('Create Group', ['create'], ['class' => 'btn btn-danger',"data-target"=>"#creategroup","data-toggle"=>"modal"]) ?>
          </p>

          <?php

              $gridColumns =  [
                      ['class' => 'kartik\grid\EditableColumn',
                      'attribute'=>'name',
                      'label' => 'Group name',
                      'editableOptions'=>function ($model, $key, $index){
                        return[
                              'header' => 'profile',
                              'asPopover'=>false,
                              'inputType' => Editable::INPUT_TEXT,

                              'formOptions'=>['action'=>Url::toRoute('/group/update')]
                        ];
                      }
                    ],
                    [
                      'attribute'=>'createdby',
                      'label'=>'Created By',
                      'value'=>function($model){
                        $name=\frontend\models\User::getName($model->createdby);
                        return $name['firstname']." ".$name["lastname"];
                      }
                    ],
                    [
                      'attribute'=>'tcGroupUsers.userid',
                      'label'=>"No of Users",
                      'value'=>function($model){
                        $count = \frontend\models\TcGroupUser::find()->where(["groupid"=>$model->id])->count();
                        return $count;
                      }
                    ],
                   //'noofusers',
                   'createdate',
                   [
                     'class' => 'yii\grid\ActionColumn',
                     'template' => '{delete}{adduser}{viewuser}',
                     'buttons' => [
                         'adduser' => function ($url, $model) {
                             return Html::a(' <span><i class="fa fa-user-plus"></i></span>',"#",['title'=>"Add user to this group","data-groupid"=>$model->id,'class'=>"addnewuser"]);
                         },
                         'viewuser'=>function($url,$model){
                           return Html::a(' <span><i class="fa fa-eye"></i></span>',"#",['title'=>"View users in this group","data-groupid"=>$model->id,"data-groupname"=>$model->name,'class'=>"viewuser"]);
                         }

                     ],
                     'urlCreator' => function ($action, $model, $key) {
                         if ($action === 'delete') {
                           $url =Url::toRoute(["group/delete","id"=>$model->id]);
                           return $url;
                         }
                     }
                 ]
              ];
           ?>
          <?=

          GridView::widget([
           'dataProvider' => $dataProvider,
           'filterModel' => $searchModel,
           'showOnEmpty'=>true,
           'pjax'=>false,
           'export'=>false,
            'columns'=>$gridColumns

   ]); ?>
        </div>

    </div>

    <div class="modal fade" id="creategroup" tabindex="-1" role="dialog" aria-labelledby="creategroup" aria-hidden="true">
      <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Create A Group</h4>
                </div>
                <div class="modal-body text-center" >

                      <?php $form = ActiveForm::begin(["action"=>Url::toRoute("/group/create")])?>
                        <?=$form->field($group, "createdby")->hiddenInput(["value"=>Yii::$app->user->id])->label(false)?>
                          <?=$form->field($group,"name")->textInput(["placeholder"=>"Group Name"])->Label(false)?>
                          <?=Button::widget(["label"=>"Create Group","options"=>["class"=>"btn btn-success","type"=>"submit"]])?>
                      <?php ActiveForm::end()?>
                </div>
            </div>
      </div>

    </div>

    <div class="modal fade" id="addusermodal" tabindex="-1" role="dialog" aria-labelledby="addusermodal" aria-hidden="true">
      <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User to a Group</h4>
                </div>
                <div class="modal-body text-center" >
                  <div class="row">
                    <?php $form = ActiveForm::begin([
                        "action"=>Url::toRoute("/group/add-user"),
                        "layout"=>"horizontal",
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4 col-xs-12',
                                'error' => '',
                                'hint' => '',
                            ]
                            ]
                          ])?>
                          <div class="col-md-12">
                            <div class="form-group">
                                <?=Html::textInput("useremail","",["placeholder"=>"Enter registered Teachsity e-mail address","class"=>"form-control"])?>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <?=Html::dropDownList("group",[],ArrayHelper::map(\frontend\models\TcCourseGroup::find()->all(),"id","name"),["id"=>"group-drop-down",'class'=>'select form-control','prompt'=>'Select Group','style'=>""])?>
                            </div>

                          </div>


                        <?=Button::widget(["label"=>"Add User","options"=>["class"=>"btn btn-success","type"=>"submit"]])?>
                    <?php ActiveForm::end()?>
                  </div>

                </div>
            </div>
      </div>

    </div>
    <div class="modal fade" id="viewusermodal" tabindex="-1" role="dialog" aria-labelledby="viewusermodal" aria-hidden="true">
      <div class="modal-dialog">

      </div>
    </div>
<?php
$script=<<<EOD
$("#privacy").change(function(){
    if($(this).val()=="public"){
        $(".field-coursesettings-password").removeClass("show").addClass("hidden")
    }
    else
        {
            $(".field-coursesettings-password").removeClass("hidden").addClass("show")

        }
})

$(document).ready(function(){
    $("#privacy").trigger("change")
})

$(".addnewuser").click(function(){
  $("#addusermodal").modal("show");
  $("#group-drop-down").val($(this).data("groupid"))

})

$(".viewuser").click(function(){
  var groupid = $(this).data("groupid");
  $("#group-name").text($(this).data("groupname"))
  console.log($(this).data("groupname"))
  $.ajax({
      type:'POST',
      url:baseUrl+'/group/view?id='+groupid+'&ajax=1',
      success: function(data)
               {
                   $('#viewusermodal').modal();
                   $('#viewusermodal .modal-dialog').html(data);
               }
  });
})
EOD;

$this->registerJs($script);
?>
