<?php
use yii\helpers\Url;
?>
<div class="inner-head">
                <div class="container">
                    <h1 class="entry-title">Teachsity <span style="color: #fe9d68;">HelpLearn</span></h1>
                    <p class="description">
                        Teachsity <span style="color: #fe9d68;">HelpLearn</span>is your personel learning assitant that advises you on what you 
                        need to learn to <span style="color:#fff">stay head</span> and up to date.
                    </p>
                  
                </div><!-- End container -->
            </div><!-- End Inner Page Head -->

            <div class="clearfix"></div>

            <div class="clearfix"></div>

            <section class="full-section features-section fancy-shadow">
                <div class="container">
                    <h3 class="section-title">How Teachsity HelpLearn Works</h3>
                    
                </div>
                <div class="section-content features-content fadeInDown-animation">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="feature-box">
                                    <div class="icon"><img src="assets/img/user.png" class="es-tr" alt=""></div><!-- End Icon -->
                                    <h5 class="feature-title">Create a Teachsity account</h5>
                                    <p class="feature-description">
                                        You can directly signin using your LinkedIn or Facebook account.
                                    </p>
                                </div><!-- End Features Box -->
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="feature-box">
                                    <div class="icon"><img src="assets/img/cv.png" class="es-tr" alt=""></div><!-- End Icon -->
                                    <h5 class="feature-title">Upload your cv</h5>
                                    <p class="feature-description">
                                        You can also tell HelpLearn your dream job and HelpLearn will suggest relevant courses to get your dream job.
                                    </p>
                                </div><!-- End Features Box -->
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="feature-box">
                                    <div class="icon"><img src="assets/img/icons/feature-icon-3.png" class="es-tr" alt=""></div><!-- End Icon -->
                                    <h5 class="feature-title">Learn Online</h5>
                                    <p class="feature-description">
                                        Learn online using Teachsity online classroom. Teachsity provides a user friendly online classrooms.
                                    </p>
                                </div><!-- End Features Box -->
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class="feature-box">
                                    <div class="icon"><img src="assets/img/rocket.png" class="es-tr" alt=""></div><!-- End Icon -->
                                    <h5 class="feature-title">Boost your career</h5>
                                    <p class="feature-description">
                                        bla bla bla bla bla.
                                    </p>
                                </div><!-- End Features Box -->
                            </div>
                        </div>
                    </div>
                </div><!-- End Features Section Content -->
            </section><!-- End Features Section -->

            <div class="clearfix"></div>

            <section class="testimonials-section">
                <div class="testimonials-content fadeInDown-animation">
                    <h4 class="title">Testimonials</h4>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="testimonials-slider" class="flexslider">
                                    <ul class="slides">
                                        <li class="testimonial">
                                            <p class="description">
                                                “ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique,<br>eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie. ”
                                            </p>
                                            <div class="image">
                                                <img src="assets/img/content/instructor-avatar-2-72x72.jpg" alt="">
                                            </div>
                                            <div class="info">
                                                <p class="name">Begha</p>
                                                <p class="position">Senior UI Designer</p>
                                            </div>
                                        </li><!-- End 1st Slide Item -->
                                        <li class="testimonial">
                                            <p class="description">
                                                “ Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique,<br>eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam ”
                                            </p>
                                            <div class="image">
                                                <img src="assets/img/content/instructor-avatar-6-72x72.jpg" alt="">
                                            </div>
                                            <div class="info">
                                                <p class="name">Ibrahim</p>
                                                <p class="position">Ninja Developer</p>
                                            </div>
                                        </li><!-- End 1st Slide Item -->
                                    </ul><!-- End ul Items -->
                                </div>
                            </div>
                        </div><!-- End row -->
                    </div><!-- End container -->
                </div><!-- End Testimonials Content -->
            </section><!-- End Testimonials Section -->


            <div class="clearfix"></div>