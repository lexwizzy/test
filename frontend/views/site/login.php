<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Teachsity - Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<!-- LOGIN -->
    <section id="login-content" class="login-content">
        <div class="awe-parallax bg-login-content"></div>
        <div class="awe-overlay"></div>
        <div class="container">
            <div class="row">

                <!-- FORM -->
                <div class="col-xs-12 col-lg-5 col-lg-offset-3">
                    <div class="form-login">
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                            <h2 class="text-uppercase">sign in</h2>
                            <div class="form-email">
                                 <?= $form->field($model, 'email')->input("email")->label(false)//array("class"=>"username-input", "placeholder"=>"Email","type"=>"email">)) ?>
                            </div>
                            <div class="form-password">
                               <?= $form->field($model, 'password')->passwordInput(["autocomplete"=>"off"])->label(false) //,array("class"=>"password-input","placeholder"=>"Password","type"=>"password")?>
                            </div>
                            <div class="form-check">
                                 <?= $form->field($model, 'rememberMe')->checkbox()->label(false);?>
                                <label for="check">
                                <i class="icon md-check-2"></i>
                                Remember me</label>
                                <a href="site/forgotpassword">Forget password?</a>
                            </div>
                            <div class="form-submit-1">
                                <input type="submit" value="Sign In" class="mc-btn btn-style-1">
                            </div>
                            <div class="link">
                                <a href="signup">
                                    <i class="icon md-arrow-right"></i>Don’t have account yet ? Join Us
                                </a>
                            </div>
                        <?php ActiveForm::end(); ?>
                        <div class="row">
                             <?php use yii\authclient\widgets\AuthChoice;$plus=""; ?>
                             <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'autoRender' => false]); ?>

                                        <?php foreach ($authAuthChoice->getClients() as $client): 
                                          if($client->name=="google")
                                              $plus="-plus";
                                         else
                                             $plus="";
                                        ?>
                                    
                                    <div class="col-md-6 col-sm-6">
                                        <?= Html::a( 'Log in with <i class="fa pull-right fa-'.$client->name.$plus.' fa-lg"></i>'. $client->title, ['site/auth', 'authclient'=> $client->name, ], ['class' => "cust-btn $client->name "]) ?>
                                      </div>
                                    <?php endforeach; ?>
                                <?php AuthChoice::end(); ?>
                    </div>
                    </div>
                    
                </div>
                <!-- END / FORM -->
    
               <!-- <div class="image">
                    <img src="images/homeslider/img-thumb.png" alt="">
                </div>
                -->
    
            </div>
        </div>
    </section>
    <!-- END / LOGIN -->
