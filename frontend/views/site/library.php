<?php
  use yii\helpers\Url;
  use frontend\models\TcUploadedContent;
 ?>
<div class="modal fade" id="library" tabindex="-1" role="dialog" aria-labelledby="libraryModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

            <div class="modal-body" >
                <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <div class="row">
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#uploadfile" aria-controls="home" role="tab" data-toggle="tab">Upload Files</a></li>
                                <li role="presentation"><a href="#library-tab" aria-controls="profile" role="tab" data-toggle="tab">Library</a></li>
                              </ul>
                        <!-- Tab panes -->
                                <div class="tab-content">
                                     <div role="tabpanel" class="tab-pane active" id="uploadfile">

                                        <?=
                                          kartik\file\FileInput::widget([
                                                'name' => 'TcUploadedContent[uploaded_file][]',
                                                'options'=>[
                                                    'multiple'=>true
                                                ],
                                                'pluginOptions' => [
                                                    'uploadUrl' => Url::to(['/user/file-upload']),
                                                    'showPreview' => true,
                                                    'showCaption' => true,
                                                    'showRemove' => true,
                                                    'showUpload' => true,
                                                    'browseClass'=>"btn btn-danger",
                                                    'allowedFileTypes'=>["png","jpg","mp4","bmp","pdf","gif","zip","rar","tar","ppt","pptx","avi","doc","docx","mp3"],
                                                    //
                                                    'dropZoneTitle'=>"Drag and drop files here.<div>Maximum file size 5GB</div>",
                                                    // 'uploadExtraData' => [
                                                    //     'album_id' => 20,
                                                    //     'cat_id' => 'Nature'
                                                    // ],
                                                    'maxFileCount' => 10
                                                ]
                                                ]);
                                        ?>
                                     </div>
                                     <div role="tabpanel" class="tab-pane" id="library-tab">
                                       <div class="col-md-7 col-lg-9 content-thumbnails" >
                                           <ul class="uploaded-contents">

                                             <li ng-repeat="lib in library track by $index" tabindex="0" role="checkbox" aria-label="test123" aria-checked="false" data-id="{{lib.id}}" class="attachment save-ready">
                                                   <div data-index={{$index}} ng-click="showFileDetails($index)" class="attachment-preview js--select-attachment type-video subtype-mp4 landscape">
                                                     <div class="thumbnail">
                                                         <div class="centered">
                                                           <img ng-src="{{lib.thumbnail}}" class="icon" draggable="false" alt="">
                                                         </div>
                                                         <div class="filename">
                                                           <div class="file-name">{{lib.filename}}</div>
                                                         </div>
                                                     </div>
                                                   </div>
                                                     <button type="button" class="button-link check" tabindex="-1"><span class="media-modal-icon"></span><span class="screen-reader-text">Deselect</span></button>
                                             </li>

                                             <!-- <li tabindex="0" role="checkbox" aria-label="test123" aria-checked="false" data-id="1511" class="attachment save-ready">
                                                   <div class="attachment-preview js--select-attachment type-video subtype-mp4 landscape">
                                                     <div class="thumbnail">
                                                         <div class="centered">
                                                           <img src="http://about.teachsity.com/wp-includes/images/media/video.png" class="icon" draggable="false" alt="">
                                                         </div>
                                                         <div class="filename">
                                                           <div>test123.mp4</div>
                                                         </div>
                                                     </div>
                                                   </div>
                                                     <button type="button" class="button-link check" tabindex="-1"><span class="media-modal-icon"></span><span class="screen-reader-text">Deselect</span></button>
                                             </li> -->

                                           </ul>
                                       </div>
                                       <div class="col-md-5 col-lg-3 file-details text-left">

                                          <div class="" ng-show="show_file_detail" >
                                              <h4 class="title">File Details</h4>
                                            <div class="thumbnail thumbnail-image">
                                               <img ng-src="{{c_thumbnail}}" draggable="false" alt="">
                                             </div>
                                             <div class="details">
                                             <div class="filename ellipsis2"><h6>{{c_file_name}}</h6></div>
                                             <div class="uploaded">{{c_uploaded_date}}</div>
                                             <div class="file-size">{{c_size}}</div>
                                               <div class="dimensions" ng-if="c_dimension!=''">{{c_dimension}}</div>
                                                    <a   data-content-id="{{content_id}}" ng-click="deleteContent(content_id,c_index)" class="delete-file pointer"  >Delete Permanently</a>
                                             </div>

                                             <button class="btn btn-danger" data-lesson-id="current_lesson" ng-click="useThisItem(content_id,current_lesson,l_section,l_lessonindex)">Use this item</button>
                                           </div>

                                         </div>
                                     </div>

                                </div>

                        </div>

            </div>
        </div>
  </div>

</div>
