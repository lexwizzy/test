<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

?>
<div style="
    max-width: 400px;
    margin-left: auto;
    margin-right: auto;
    padding-top: 127px;
">
<?php if(Yii::$app->session->hasFlash("error")):?>
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <?= Yii::$app->session->getFlash("error")?></strong>
    </div>
  <?php elseif(Yii::$app->session->hasFlash("success")):?>
      <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?= Yii::$app->session->getFlash("success")?> 
      </div>
<?php endif;?>
<div class="modal-content reset-password">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" ></button>
    <h4 class="modal-title" >New password</h4>
</div>
    <div class="modal-body" >
        <div class="row">
          <div class="row col-md-12 ">
              <!-- start ActiveForm -->
              <?php $form = ActiveForm::begin(['id'=>"request-password-reset-form",'enableClientValidation' => true]); ?>
              <div class="col-md-12">
                  <?= $form->field($model, 'password')->passwordInput(['placeholder'=>"Password"])->label(false); ?>
              </div>
              <div class="col-md-6 col-md-offset-3 col-md-offset-right-3">
                    <input type="submit" value="Reset Password" class="btn btn-success">
              </div>

              </div>
             <?php ActiveForm::end(); ?>
          </div>



       <div class="modal-footer">
            Already have an account? <a href="javascript:void(0);" onclick="login();return false">Login</a>
        </div>
    </div>
  </div>
</div>
