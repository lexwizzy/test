<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    $this->title ="Teachsity - Student SignUp";
    //$this->registerMetaTag(['name' => 'description', 'content' => ""]);
?>
<!-- modal dialog for display pop up login -->
<div class="div-before-modal-content" style="max-width:450px">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel">Sign up and start learning</h4>
</div>
    <div class="modal-body">
        <div class="row">

            <div class="row col-md-12">
                <!-- start ActiveForm -->
                <?php $form = ActiveForm::begin(['id' => 'form-signup',"enableClientValidation"=>true]); ?>

                            <div class="col-md-12">
                                <?= $form->field($model, 'firstname')->textInput(["placeholder"=>"First Name"])->label(false) ?>
                                 </div>
                            <div class="col-md-12">
                                 <?= $form->field($model, 'lastname')->textInput(["placeholder"=>"Last Name"])->label(false) ?>
                            </div>

                            <div class="col-md-12">
                               <?= $form->field($model, 'email')->textInput(["placeholder"=>"E-mail",'type'=>"email"])->label(false) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'password')->passwordInput(["autocomplete"=>"off",'placeholder'=>"Password"])->label(false)?>
                            </div>
                             <input type="hidden" value="1" name="SignupForm[roleid]"/>
                            <div class="col-md-6 col-md-offset-3 col-md-offset-right-3">
                                <input type="submit" value="Become a member" class="btn btn-success">
                            </div>

                <?php ActiveForm::end(); ?><!-- End form -->
            </div>

        </div>

       <div class="modal-footer">
            Already have an account? <a href="javascript:void(0);" onclick="login();return false">Log in</a>
        </div>
    </div>
  </div>
</div>
