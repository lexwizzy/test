<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Teachsity - Signup';
?>

<section id="login-content" class="login-content">
        <div class="awe-parallax bg-login-content"></div>
        <div class="awe-overlay"></div>
        <div class="container">
            <div class="row">
    
    
                
                <!-- FORM -->
                <div class="col-xs-12 col-lg-5 col-lg-offset-3">
                    <div class="form-login">
                       <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                            <h2 class="text-uppercase">sign up</h2>
                        <div class="form-email">
                                <?= $form->field($model, 'firstname')->textInput(["placeholder"=>"First Name"])->label(false) ?>
                                 </div>
                            <div class="form-email">
                                 <?= $form->field($model, 'lastname')->textInput(["placeholder"=>"Last Name"])->label(false) ?>
                            </div>
                            
                            <div class="form-email">
                               <?= $form->field($model, 'email')->textInput(["placeholder"=>"Email",'type'=>"email"])->label(false) ?>
                            </div>
                            <div class="form-password">
                                <?= $form->field($model, 'password')->passwordInput(["autocomplete"=>"off",'placeholder'=>"Password",'class'=>"password-input"])->label(false)?>
                            </div>
                             <input type="hidden" value="1" name="SignupForm[roleid]"/>
                            <div class="form-submit-1">
                                <input type="submit" value="Become a member" class="mc-btn btn-style-1">
                            </div>
                            <div class="link">
                                <a href="login.html">
                                    <i class="icon md-arrow-right"></i>Already have account ? Log in
                                </a>
                            </div>
                         <?php ActiveForm::end(); ?><!-- End form -->
                        <div class="row">
                             <?php use yii\authclient\widgets\AuthChoice;$plus=""; ?>
                             <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'autoRender' => false]); ?>

                                        <?php foreach ($authAuthChoice->getClients() as $client): 
                                          if($client->name=="google")
                                              $plus="-plus";
                                         else
                                             $plus="";
                                        ?>
                                    
                                    <div class="col-md-6 col-sm-6">
                                        <?= Html::a( 'Log in with <i class="fa pull-right fa-'.$client->name.$plus.' fa-lg"></i>'. $client->title, ['site/auth', 'authclient'=> $client->name, ], ['class' => "cust-btn $client->name "]) ?>
                                      </div>
                                    <?php endforeach; ?>
                                <?php AuthChoice::end(); ?>
                    </div>
                    </div>
                </div>
                <!-- END / FORM -->
    
                <!--<div class="image">
                    <img src="images/homeslider/img-thumb.png" alt="">
                </div>-->
    
            </div>
        </div>
    </section>
