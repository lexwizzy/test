

<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use frontend\models\CourseCategory;
    use frontend\models\Course;

    $template = '<p ><a href="'.Url::home(true).'course/search?q={{value}}&page=home">{{value}}</a></p>';

    /*$template = '<div><p class="repo-language">{{language}}</p>' .
                '<p class="repo-name">{{name}}</p>' .
                '<p class="repo-description">{{description}}</p></div>';*/
?>


<div class="banner_content_bg">
        	<p class="thinking">Do you want to stay ahead?</p>
            <p class="helplearn">Stay incharge of your future, start learning new skills.</p>
            <p class="become"><a href="<?= Url::toRoute('/courses')?>">Browse courses</a></p>
                <form action="<?=Url::toRoute('/course/search')?>" method="get">
                    <div class="search">
                      <input type="hidden" name="page" value="home" />
                      <?=
                           kartik\typeahead\Typeahead::widget([
                              'name' => 'q',
                              'options' => ['id'=>"home_searchbox",'placeholder' => 'Find a course on nearly anything.'],
                              'pluginOptions' => ['highlight'=>true],
                              'dataset' => [
                                  [
                                      'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                      'display' => 'value',
                                      //'prefetch' => Url::home("true") . '/courses/autosuggestjson',
                                      'remote' => [
                                          'url' => Url::toRoute(['course/autosuggestjson']) . '?q=%QUERY',
                                          'wildcard' => '%QUERY'
                                      ],
                                      'limit'=>10,
                                        'templates' => [
                                             // 'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                              'suggestion' => new \yii\web\JsExpression("Handlebars.compile('{$template}')")
                                          ]
                                  ]
                              ]
                          ]);
                      ?>
                        <!-- <input type="text" placeholder="Find a course">
                        <span></span> -->
                        <!-- <div class="input-group add-on">
                              <input id="searchfield" class="form-control" type="text" name="q" placeholder="Find course on anything"/>
                              <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                              </div>
                        </div> -->
                        <button class="btn btn-black" id="typeahead-submit" type="submit"><i class="fa fa-search"></i></button>

                    </div>
                </form>
        </div>

</div><!--- End Div from home_page layout-->
<!-- END / home banner -->
<div class="content_bg" style="margin-top:0">
<div class="wrap">


  <div class="cost_bg">
          <div class="cost_box" style="background: #bbb;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"development"])?>">
              <p><img src="images/website-lg.png"></p>
                <p class="courses_txt">Development</p>
                <!-- <p class="amount">25,000</p> -->
            </a>
          </div>
          <div class="cost_box" style="background: #22abeb;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"business"])?>">
              <p><img src="images/business-lg.png"></p>
                <p class="courses_txt">Business</p>
              </a>
                <!-- <p class="amount">25,000</p> -->
            </div>
          <div class="cost_box" style="background: #22d6eb;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"music"])?>">
              <p><img src="images/music.png"></p>
                <p class="courses_txt">Music</p>
              </a>
                <!-- <p class="amount">25,000</p> -->
            </div>
          <div class="cost_box" style="background: #E8ABAB;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"health-and-fitness"])?>">
              <p><img src="images/health-and-fitness.png"></p>
                <p class="courses_txt">Health & Fitness</p>
              </a>
                <!-- <p class="amount">25,000</p> -->
          </div>
          <div class="cost_box" style="background: #3c763d;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"marketing"])?>">
              <p><img src="images/marketing.png"></p>
                <p class="courses_txt">Marketing</p>
              </a>
                <!-- <p class="amount">25,000</p> -->
          </div><div class="cost_box" style="background: #ef4132;">
            <a href="<?=Url::toRoute(["/course/coursecategory","category"=>"design"])?>">
              <p><img src="images/design.png"></p>
                <p class="courses_txt">Design</p>
              </a>
                <!-- <p class="amount">25,000</p> -->
          </div>
          <!-- <div class="cost_box join_bg">
              <p class="join_icon"><img src="images/join_icon.png"></p>
                <p class="join"><a href="javascript:void(0);" onclick="login();return false;">Join Now</a></p>
            </div>
          <div class="cost_box tutor_bg">
              <p class="join_icon"><img src="images/tutor_icon.png"></p>
                <p class="tutor"><a href="<?= Url::toRoute('/tutors/')?>">Become a tutor</a></p>
        </div> -->
  </div>
  <div class="popular_courses_bg">
        <p class="main_title poular_title">Popular Courses</p>
      <p class="view_all popular_view"><a href="<?= Url::toRoute('/courses')?>">View All</a></p>
        <div class="nav_arrow">
            <a class="btn prev"><img src="images/arrow_left.png"></a>
            <a class="btn next"><img src="images/arrow_right.png"></a>
      </div>
      <div class="popular_bg">
      <div id="owl-demo" class="owl-carousel">
                <?php foreach($course as $c):?>
                    <?php $coursead=Course::getCourseAd($c->course_id);?>
                     <?=Course::createGuestCourseAd($coursead,"mc-item-1");?>
            <?php endforeach;?>
      </div>
      </div>

  </div>
	<div class="features_bg">
        <p class="main_title">FEATURES</p>
    	<p class="view_all"><a href="#">View All</a></p>
    	<div class="features_box_bg">
        	<div class="feature_bg">
        	<div class="feature_item_bg">


                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/course-tool.png"/>
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">Easy Course Tool</p>
                        <p>As an instructor, use Teachsity state of the art course creation tool to build and manage your courses at ease.  </p>
                    </div>
                </div>
                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/life-time-access.png"/>
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">Life Time Access</p>
                        <p>Buy once and get a life time access. When you buy a course on Teachsity you can access the course from anywhere at anytime. You resume exactly from where you stopped.</p>
                    </div>
                </div>
                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/clouds8.png"/>
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">Cloud Base Storage</p>
                        <p>Teachsity provides free course hosting but let you control your course contents. You can reuse your course contents as much as you wish.</p>
                    </div>
                </div>
                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/mobile-friendly.png"/>
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">Very Mobile Friendly</p>
                        <p>Teachsity works perfectly on mobile and tablet devices and is avaiable on Android, iOS and Windows platforms.</p>
                    </div>
                </div>
                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/secure-content.png"/>
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">High Content Security</p>
                        <p>Teachsity ensures that your contents remain secured. We provide settings in your dashboard to control access to your contents.</p>
                    </div>
                </div>
                <div class="features_box">
                	<div class="features_icon">
                    	<img src="<?=Url::base()?>/images/advance-report.png" >
                    </div>
                    <div class="features_txt_bg">
                    	<p class="feature_course">Advance Reporting System</p>
                        <p>Tracking students progress on Teachsity is very easy. Teachsity provides a wide range of reports to help you track performance. We also provide support for sophisticated quiz to test students.</p>
                    </div>
                </div>

            </div>
        </div>
        </div>
    </div>





      <div class="testimonial_bg">
        <div class="row">
 <div class="col-sm-12 col-md-6 animation-desc" >
              <h2 class="title">Easy to use for all users</h2>
              <p class="lead">
                Online learning has never been this easier. Use our marketplace, learn virtually anything.
              </p>
              <a href="javascript:void(0);" onclick="login();return false;" class="animation-a animation-a-red" style="">Join Now</a>
              <a href="<?= Url::toRoute('/tutors/')?>" class="animation-a animation-a-red" style="">Become a Tutor<a>
            </div>
  <div class="col-sm-12 col-md-6"><img src="/images/teachsity-animation.gif"></div>
</div>
     	 <!-- <div id="owl-demo-testimonials" class="owl-carousel">

                <div class="item">
      				<div class="testimonial">
            			<p><img src="images/testnimonial_photo.png"></p>
              			<p class="testimonial_txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit </p>
           			 </div>
      			</div>
                <div class="item">
      				<div class="testimonial">
            			<p><img src="images/testnimonial_photo.png"></p>
              			<p class="testimonial_txt">second slide Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit </p>
           			 </div>
      			</div>
                <div class="item">
      				<div class="testimonial">
            			<p><img src="images/testnimonial_photo.png"></p>
              			<p class="testimonial_txt">Third slide Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit </p>
           			 </div>
      			</div>
          </div>
        <div class="nav_arrow2">
            <a class="btn prevs"><img src="images/arrow_l.png"></a>
            <a class="btn nexts"><img src="images/arrow_r.png"></a>
       </div> -->

     </div>


</div>
</div>
