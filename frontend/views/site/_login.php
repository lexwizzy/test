<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    $this->title ="Teachsity - Login";
    //$this->registerMetaTag(['name' => 'description', 'content' => "Teachsity"]);
?>

<!-- modal dialog for display pop up login -->
<div class="div-before-modal-content" style="max-width:600px">
<div class="modal-content">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title" id="myModalLabel">Login to the City of Knowledge</h4>
</div>
    <div class="modal-body" >
        <div class="row">
            <div class="row col-md-6">
                <?php use yii\authclient\widgets\AuthChoice;$plus=""; ?>
                                 <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'autoRender' => false]); ?>

                                            <?php foreach ($authAuthChoice->getClients() as $client):
                                              if($client->name=="google")
                                                  $plus="-plus";
                                             else
                                                 $plus="";
                                            ?>

                                        <div class="col-md-12 col-sm-12">
                                            <?= Html::a( 'Log in with <i class="fa pull-right fa-'.$client->name.$plus.' fa-lg"></i>'. $client->title, ['site/auth', 'authclient'=> $client->name, ], ['class' => "cust-btn $client->name "]) ?>
                                          </div>
                        <?php endforeach; ?>
                 <?php AuthChoice::end(); ?>
            </div>
            <div class="row col-md-6 row">
                <!-- start ActiveForm -->
                <?php $form = ActiveForm::begin(['id'=>"login-form",'enableClientValidation' => true]); ?>
                <div class="col-md-12">
                    <?= $form->field($model, 'email')->input("email",["placeholder"=>"E-mail"])->label(false)//array("class"=>"username-input", "placeholder"=>"Email","type"=>"email">)) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'password')->passwordInput(["placeholder"=>"Password"])->label(false) //,array("class"=>"password-input","placeholder"=>"Password","type"=>"password")?>
                </div>
                <div class="col-md-12">
                     <?= Html::a('Forgot your password?', ['/site/forgotpassword'],['class'=>'password-recovery']) ?>
                </div>
                <div class="col-xs-offset-4 col-xs-offset-right-4 col-xs-4 col-sm-offset-4 col-sm-offset-right-4 col-sm-4 col-md-offset-4 col-md-offset-right-4 col-md-4">
                    <div class="form-group our-form-group">

                     <?= Html::submitButton('Login', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
               </div>
                </div>
               <?php ActiveForm::end(); ?>
            </div>

        </div>

       <div class="modal-footer">
            Need an account? <a href="javascript:void(0);" onclick="signup();return false">Sign up</a>
        </div>
    </div>
  </div>
</div>
