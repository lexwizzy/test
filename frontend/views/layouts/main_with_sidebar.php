<?php
  use frontend\models\User;
  use yii\helpers\Url;

?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<style>
.row {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
}</style>

        <div class="container">
          <section id="user-profile-setting" class="create-course-section course-content-body">
            <div class="row" id="profile-content">
                    <?php if (isset($this->blocks['sidebar'])): ?>
                        <?= $this->blocks['sidebar'] ?>
                    <?php else:?>

                    <div class="col-md-3 col-sm-12 col-xs-12" id="sidebar">

                        <div class="create-course-sidebar profile-sidebar text-left">
                             <div class="profile-image text-center">
                                <img class="img-thumbnail" src="<?= User::getPhoto(Yii::$app->user->id,"black")?>" />
                            </div>
                            <ul class="list-bar">
                                <?php if(isset($this->params['elementclass_pro'])):?>
                                    <li class="active">Profile</li>
                                <?php else:?>
                                       <li ><a href="<?=Url::base()?>/user/profile">Profile</a></li>
                                <?php endif;?>

                                <?php if(isset($this->params['elementclass_ph'])):?>
                                    <li class="active">Photo</li>
                                <?php else:?>
                                       <li ><a href="<?=Url::base()?>/user/photo">Photo</a></li>
                                <?php endif;?>


                                <?php if(isset($this->params['elementclass_se'])):?>
                                    <li class="active">Settings</li>
                                <?php else:?>
                                       <li><a href="<?=Url::base()?>/user/settings">Settings</a></li>
                                <?php endif;?>


                                <?php if(isset($this->params['elementclass_pre'])):?>
                                    <li class="active">Premium Tutor</li>
                                <?php else:?>
                                       <li ><a href="<?=Url::base()?>/user/premium-tutor">Premium Tutor</a></li>
                                <?php endif?>


                                <?php if(isset($this->params['elementclass_ac'])):?>
                                    <li class="active">Account</li>
                                <?php else:?>
                                       <li ><a href="<?=Url::base()?>/user/account">Account</a></li>
                                <?php endif?>


                            </ul>
                        </div>
                    </div>
                     <?php endif; ?>
                     <?php if (isset($this->blocks['center'])): ?>
                         <div class="col-md-9 col-sm-12 col-xs-12" id="page-main-content" >
                            <?= $this->blocks['center'] ?>
                        </div>
                    <?php  endif; ?>
                    </section>
            </div>


<?php $this->endContent(); ?>
