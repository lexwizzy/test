<?php
    use frontend\models\User;
    use yii\helpers\Url;
    use yii\bootstrap\BootstrapAsset;
    use yii\web\View;

    // $location = User::getLocation(Yii::$app->user->id);
    // $this->registerJs("var country = ".json_encode($location['country']),View::POS_HEAD);
    // $this->registerJs("var state = ".json_encode($location['state']),View::POS_HEAD);
    $this->registerJsFile(Url::base()."/js/library/jquery.magnific-popup.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile( Url::base().'/js/bootstrap-formhelpers.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
    $this->registerJsFile(Url::base()."/js/dashboard.js",['depends'=>[\yii\web\JqueryAsset::className()]]);

    $name=User::getName(Yii::$app->user->id);
//echo $name['firstname']." ".$name['lastname'];
?>
<style>
  .wrap{
    width: 100% !important;
  }
</style>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/library/magnific-popup.css">
<section class="">
    <!-- PROFILE FEATURE -->
    <div class="use-profile-details">
        <div class="user-profile">
        	<div class="profile-left">
            	<div class="user-img">
                	<img class="profile-pix big_profile_pix"src="<?=User::getPhoto(Yii::$app->user->id);?>" alt="">
                </div>
            	<div class="user-name">
                	<div class="acc-name"><span class="user-edit-icon"><?=$name["firstname"]." ".$name['lastname']?></span></div>
                    <!-- <div class="acc-country"><span class="location-img"><img src="images/location.png"></span>Country Name</div> -->
                </div>
            </div>
            <div class="profile-right">
            	<div class="user-balance">
                    <div class="user-trohies">
                        <p>Trophies</p>
                        <p class="user-digit user-num">0</p>
                    </div>
                    <div class="user-trohies">
                        <p>Balance</p>
                        <p class="user-digit">$0.0</p>
                    </div>
                </div>
            </div>
        </div>
        </div>

    <!-- END / PROFILE FEATURE -->
    <!-- CONTEN BAR -->

    <div class="">

    <div class="two-tab">
    	<div class="learing">
            <div class="learn-link <?php if(isset($this->params['s_class'])) echo $this->params['s_class'];?>">
            <a href="<?= Url::toRoute("/student/dashboard");?>" title="Learning"><span class="learn-icon"></span>Learning</a>
            </div>
            <div class="teach-link <?php if(isset($this->params['d_class'])) echo $this->params['d_class'];?>">
            <a href="<?= Url::toRoute("/tutors/dashboard");?>" title="Teaching"><span class="teach-icon"></span>Teaching</a>
            </div>
        </div>
    </div>

  </div>

    <?= $content?>
  </section>



<?php $this->endContent()?>
