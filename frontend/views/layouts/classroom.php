

<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
    use frontend\assets\AppAsset;
    use yii\web\View;

    AppAsset::register($this);
?><?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" ng-app="coursecontent">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
    <meta name="format-detection" content="telephone=no">
    <meta property="og:locale" content="en_US" />            <!-- Default -->
    <meta property="og:locale:alternate" content="fr_FR" />  <!-- French -->
    <meta property="og:locale:alternate" content="it_IT" />
    <meta property="og:site_name" content="Teachsity" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--favicon -->
    <!-- Css -->
<!--
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/md-font.css">
-->
    <!--<link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/style.css">-->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <link href="<?=Url::base()?>/css/right-slide.css" rel="stylesheet">
     <?= Html::csrfMetaTags() ?>
    <!-- <title><?= Html::encode($this->title) ?></title>-->
        <title>Teachsity - Inovative learning system</title>
     <?php $this->head() ?>
    <script>var scope_array=new Array()</script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63028576-1', 'auto');
  ga('send', 'pageview');
</script>
<?php $this->registerJS("var baseUrl=".json_encode(Url::base()),View::POS_HEAD); ?>
<script src="https://apis.google.com/js/platform.js" async defer></script>
    <link rel="shortcut icon" href="<?=Url::home(true)?>/images/fav.png" type="image/x-icon"/>
</head>
<body id="">
    <?php $this->beginBody() ?>
    <!-- PAGE WRAP -->
    <div class="full_height">
        <?= $content?>
        <div class="footer_bg">
        		<div class="foot_bg">
             	  <div class="footer_left_bg">
                  	<ul>
                        <li><a href="#" title="apple"><img src="<?=Url::base()?>/images/apple_icon.png" alt="apple"></a></li>
                        <li><a href="#" title="android"><img src="<?=Url::base()?>/images/android_icon.png" alt="android"></a></li>
                      </ul>
                </div>
                <div class="foot_center">
                		<p><a title="About Us" href="#">About Us</a> | <a title="Contact Us" href="#">Contact Us</a> | <a title="Support" href="#">Support</a></p>
                </div>
                <div class="foot_right">
                	<ul>
                  	<li><a href="#" title="facebook"><img src="<?=Url::base()?>/images/fb.png" alt="facebook"></a></li>
                      <li><a href="#" title="twitter"><img src="<?=Url::base()?>/images/tw.png" alt="twitter"></a></li>
                      <li><a href="#" title="linkedin"><img src="<?=Url::base()?>/images/in.png" alt="linkedin"></a></li>
                      <li><a href="#" title="youtube"><img src="<?=Url::base()?>/images/youtube.png" alt="youtube"></a></li>
                  </ul>
                </div>
             </div>
        </div>
    </div>
    <!-- END / PAGE WRAP -->

     <?php $this->endBody() ?>
    <!--<script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>-->
</body>

</html>
<?php $this->endPage() ?>
