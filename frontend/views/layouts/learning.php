

<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
    use frontend\assets\AppAsset;
    use yii\web\View;

    AppAsset::register($this);
?><?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" ng-app="coursecontent">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
    <meta name="format-detection" content="telephone=no">
    <meta property="og:locale" content="en_US" />            <!-- Default -->
    <meta property="og:locale:alternate" content="fr_FR" />  <!-- French -->
    <meta property="og:locale:alternate" content="it_IT" />
    <meta property="og:site_name" content="Teachsity" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

     <?= Html::csrfMetaTags() ?>
    <!-- <title><?= Html::encode($this->title) ?></title>-->
        <title>Teachsity - Inovative learning system</title>
     <?php $this->head() ?>
    <script>var scope_array=new Array()</script>

<?php $this->registerJS("var baseUrl=".json_encode(Url::base()),View::POS_HEAD); ?>
    <link rel="shortcut icon" href="<?=Url::home(true)?>/images/fav.png" type="image/x-icon"/>
</head>
<body id="">
    <?php $this->beginBody() ?>
    <!-- PAGE WRAP -->
    <div class="full_height" style="background:#000">
        <?= $content?>

    <!-- END / PAGE WRAP -->

     <?php $this->endBody() ?>
    <!--<script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>-->
</body>

</html>
<?php $this->endPage() ?>
