<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
?>
<link href="<?=Url::base()?>/css/tutor.css" rel="stylesheet" />
<?php $this->beginContent('@app/views/layouts/parent_layout.php'); ?>

<?php
$script=<<<EOD
(function($) {
     "use strict"

     // Accordion Toggle Items
       var iconOpen = 'fa fa-minus',
            iconClose = 'fa fa-plus';

        $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function (e) {
            var \$target = $(e.target)
              \$target.siblings('.accordion-heading')
              .find('em').toggleClass(iconOpen + ' ' + iconClose);
              if(e.type == 'show')
                  \$target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
              if(e.type == 'hide')
                  $(this).find('.accordion-toggle').not(\$target).removeClass('active');
        });

    })(jQuery);
EOD;

 ?>
<?php
$this->registerJs($script);
$this->registerJsFile( Url::base().'js/bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile( Url::base().'/js/jquery.easing.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile( Url::base().'/js/jquery.fittext.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile( Url::base().'/js/wow.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile( Url::base().'/js/creative.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
       <div class="container-fluid">
           <!-- Brand and toggle get grouped for better mobile display -->
           <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                   <span class="sr-only">Toggle navigation</span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
                   <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand page-scroll" href="<?=Url::home(true)?>"><img src="<?=Url::base()?>/images/logo.png" alt="logo"></a>
           </div>

           <!-- Collect the nav links, forms, and other content for toggling -->
           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav navbar-right">
                   <li>
                       <a class="page-scroll" href="<?=Url::toRoute("/tutors/")?>">Become a Tutor</a>
                   </li>
                   <li class="text-primary menu-devider">|</li>
                   <li>
                       <a class="page-scroll" href="javascript:void(0);" onclick="login();return false">Login</a>
                   </li>
                   <li class="text-primary menu-devider">|</li>
                   <li>
                       <a class="page-scroll" href="<?=Url::toRoute("/tutors/signup")?>">Sign Up</a>
                   </li>
               </ul>
           </div>
           <!-- /.navbar-collapse -->
       </div>
       <!-- /.container-fluid -->
   </nav>



<!--Content -->
 <?= $content?>
<!-- End content -->
<?php $this->endContent()?>
