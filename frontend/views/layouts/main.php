<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
    $li="";
    $template = '<p ><a href="'.Url::home(true).'course/search?q={{value}}&page=top_search_box">{{value}}</a></p>';
?>
<?php $this->beginContent('@app/views/layouts/parent_layout.php'); ?>
    <!-- <div id="fb-root"></div> -->
<!-- <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=494942030598178";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> -->
<?php $this->beginBody() ?>
<!-- header -->
<nav class="navbar navbar-inverse navbar-fixed-top nav_bg">
              <div class="head_left2">
                  <a href="<?=Url::home(true)?>" title="Teachsity"><img class="logo2" src="<?=Url::base()?>/images/logo.png" alt="Teachsity"/></a>
              </div>


			   <div class="profile_picture">
                  <?php if(!Yii::$app->user->isGuest):?>
          					<span  class="drow_down drop-down-toggler pointer" data-toggle="dropdown">
                      <span class="profile">
                        <img class="menubar_profile_pix profile_pix" src="<?=User::getPhoto(Yii::$app->user->id)?>">
                      </span>
                      <i class="fa fa-angle-down"></i>
                    </span>
                  <?php else:?>
                    <a href="#" class="hidden-lg hidden-md hidden-md drow_down not_guest_bar" data-toggle="dropdown"><i class="fa fa-bars"></i></a>
                  <?php endif;?>
                  <ul class="tutor_right">
                           <?php if(Yii::$app->user->isGuest):?>
                             <li><a href="<?= Url::toRoute('/tutors')?>">Become a Tutor</a> <span>|</span></li>
                             <li><a href="javascript:void(0);" onclick="login();return false;">Login</a> <span>|</span></li>
                             <li><a href="javascript:void(0);" onclick="signup();return false">Signup</a></li>
                           <?php else:?>
                             <li><a title="MyCourses" href="<?=Url::toRoute("/site/dashboard")?>">MyCourses</a><span>|</span></li>
                             <li><a title="For instructor" href="<?=Url::toRoute("/site/dashboard")?>">For Tutors</a></li>
                           <?php endif;?>
                   </ul>
          					<ul class="dropdown-menu">
                        <?php if(!Yii::$app->user->isGuest):?>
                            <li class="mobile_only"><a title="MyCourses" href="<?=Url::toRoute("/site/dashboard")?>">MyCourses</a></li>
                          <li><a title="Setting" href="<?=Url::toRoute("/user/settings")?>">Setting</a></li>
                          <li><a title="Profile" href="<?=Url::toRoute("/user/profile")?>">Profile</a></li>
                          <li><?=Html::a('Signout',[Url::to('/site/logout')],['data-method'=>'post'])?> </li>
                        <?php else:?>
                          <li class="mobile_only"><a href="<?=Url::toRoute('/site/login')?>" >Login</a></li>
                          <li class="mobile_only"><a href="<?=Url::toRoute('/site/signup')?>" >Signup</a></li>
                      	  <li class="mobile_only"><a title="Become a tutor" href="<?= Url::toRoute('/tutors')?>">Become a tutor</a></li>

                      <?php endif;?>
          					</ul>

				</div>


              <div class="search_bg">
              	<div class="mobile_center">
                <div class="btn-group category_list">
                  <button type="button" class="btn btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span><i class="fa fa-bars"></i></span> <span class="cate_txt">Category</span>
                  </button>

                  <ul class="dropdown-menu">
                  	<div class="scroll_cate">
                      <?php
                      //loop through categories
                          foreach(\frontend\models\CourseCategory::find()->with("courseSubcategories")->each() as $c):
                           $li.='<li><a title="'.$c->category.'" href="'.
                            Url::toRoute([
                              '/course/coursecategory',
                              'category' => strtolower($c->category)]).'">';
                              if(!empty($c->image))
                                  $li.='<span><img src="'.Url::base().'/images/icon/'.$c->image.'"></span>';
                              else
                              $li.='<span><i class="'.$c->icon.'"></i> &nbsp;&nbsp;</span>';
                              $li.=str_replace("and","&",str_replace("-"," ",$c->category)).'</a></li>';

                          endforeach;
                      ?>
                      <?=
                              $li;
                      ?>
                    </div>
                  </ul>
                </div>


                <div class="search_box">
                  <div class="top_search_box">
                    <form action="<?=Url::toRoute('/course/search')?>" method="get">
                    <input type="hidden" name="page" value="top_search_box" />
                    <?=
                         kartik\typeahead\Typeahead::widget([
                            'name' => 'q',
                            'options' => ['id'=>"home_searchbox",'placeholder' => 'Find a course on nearly anything.'],
                            'pluginOptions' => ['highlight'=>true],
                            'dataset' => [
                                [
                                    'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                    'display' => 'value',
                                    //'prefetch' => Url::home("true") . '/courses/autosuggestjson',
                                    'remote' => [
                                        'url' => Url::toRoute(['course/autosuggestjson']) . '?q=%QUERY',
                                        'wildcard' => '%QUERY'
                                    ],
                                    'limit'=>10,
                                      'templates' => [
                                           // 'notFound' => '<div class="text-danger" style="padding:0 8px">Unable to find repositories for selected query.</div>',
                                            'suggestion' => new \yii\web\JsExpression("Handlebars.compile('{$template}')")
                                        ]
                                ]
                            ]
                        ]);
                    ?>
                      <!-- <input type="text" placeholder="Find a course">
                      <span></span> -->
                      <!-- <div class="input-group add-on">
                            <input id="searchfield" class="form-control" type="text" name="q" placeholder="Find course on anything"/>
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                            </div>
                      </div> -->
                      <button class="btn btn-black" id="typeahead-submit" type="submit"><i class="fa fa-search"></i></button>
                      </form>
                  </div>
                </div>

              </div>
              </div>

</nav>

<!-- END / header -->
<section class="content_bg">
<?= $content?>
</section>

<?php $this->endContent()?>
