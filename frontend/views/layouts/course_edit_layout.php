<?php
  use yii\helpers\Url;
  use frontend\models\Course;

  $course = $this->params["course"];
 ?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<style>
.row {
   margin-right: 0;
   margin-left: 0
}
</style>
<div class="semi-content">
  <div class="container">
      <div class="row">
            <div id="course-title-div" class="col-md-12">
                <div class="row">
                  <?php if(Yii::$app->session->hasFlash("error")):?>
                   <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error')?></div>
                  <?php elseif(Yii::$app->session->hasFlash("success")):?>
                      <div class="alert alert-success"><?=Yii::$app->session->getFlash('success')?></div>
                  <?php endif?>
                  <?php if(Yii::$app->session->hasFlash("premium-tutor-error")):?>
                   <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     <?= Yii::$app->session->getFlash("premium-tutor-error")?> <strong><a href="<?=Url::toRoute('/user/premium-tutor')?>">Apply Now</a></strong>
                   </div>
            <?php endif;?>
                  <div class="banner_img col-md-8">
                        <img src="<?=Course::getCourseBanner($course->course_banner)?>" alt="<?=$course->course_title?>"/>
                        <div><h4 class="title"><?=$course->course_title?></h4></div>
                  </div>
                  <div class="col-md-4">
                      <div class="">
                          <!-- <a href="#" class="btn btn-primary">Submit for Review</a>
                          <a href="#" class="btn btn-default">Preview As Student</a> -->
                          <form  method="post" action="<?=Url::base()?>/course/submitforreview">
                            <button  class="btn btn-primary" type="submit">Submit for Review</button>
                            <a href="<?=\yii\helpers\Url::toRoute(['course/details', 'slug'=>$course->slug,'role'=>2])?>" class="btn btn-default">Preview As Student</a>
                                <input type="hidden" value="<?=$course->course_id?>" name="courseid"/>
                                   <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                         </form>
                      </div>
                  </div>
                </div>

            </div>

            <div class="course-content-body col-md-12">
                <div class="row">
                  <div class="sidebar col-md-2">
                     <div id="course-content-sidebar">
                       <h5 class="title">Course Content</h5>
                       <ul>
                         <li><a href="<?=Url::toRoute(["/course/target-and-requirements","courseid"=>$course->course_id])?>">Course Goals</a></li>
                         <li><a href="<?=Url::toRoute(["/course/contents","slug"=>$course->slug])?>">Course Curriculum</a></li>
                         <li><a href="<?=Url::toRoute(["/course/description","courseid"=>$course->course_id])?> ">Course Description</a></li>
                       </ul>
                     </div>
                     <div id="course-info-sidebar">
                       <h5 class="title">Course Info</h5>
                       <ul>
                         <li><a href="<?=Url::toRoute(["/course/basicinfo","slug"=>$course->slug])?>">Basics</a></li>
                         <li><a href="<?=Url::toRoute(["/course/intro-video","courseid"=>$course->course_id])?>">Intro Video</a></li>
                         <li><a href="<?=Url::toRoute(["/course/course-banner","courseid"=>$course->course_id])?>">Course Banner</a></li>
                       </ul>
                     </div>
                     <div id="course-setting">
                       <h5 class="title">Course Setttings</h5>
                       <ul>
                         <li><a href="<?=Url::toRoute(["/course/settings","slug"=>$course->slug])?>">Google Tracking</a></li>
                         <li><a href="<?=Url::toRoute(["/course/privacy","slug"=>$course->slug])?>">Privacy</a></li>
                         <!-- <li><a href="">Sudent Report</a></li> -->
                         <li><a href="<?=Url::toRoute(["/course/price-and-coupons","courseid"=>$course->course_id])?>">Price & Coupons</a></li>
                          <li><a href="<?=Url::toRoute(["/course/under-the-hood","courseid"=>$course->course_id])?>">Under the Hood</a></li>
                       </ul>
                     </div>

                  </div> <!--END Side bar-->
                  <div class="main col-md-10">
                      <?=$content?>
                  </div><!--END MAIN-->

                </div>

        </div><!--END COurse body content-->

          </div>
      </div><!--END Container-->
</div><!--END SEMI_CONTENT-->

<?php $this->endContent()?>
