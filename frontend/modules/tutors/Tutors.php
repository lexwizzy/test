<?php

namespace app\modules\tutors;
use yii;
use yii\db\Query;
class Tutors extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tutors\controllers';
    public $defaultRoute='tutors';
    public $defaultController='tutors';
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }


}
