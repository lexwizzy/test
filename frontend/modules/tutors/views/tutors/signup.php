<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\models\CourseCategory;
use yii\web\Request;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Teachsity - Become a Tutor';
?>
<section class="content_bg">
<div class="div-before-modal-content" style="max-width:450px">
<div class="modal-content">
<div class="modal-header">

    <h4 class="modal-title" id="myModalLabel">Join Teachsity Online Tutors</h4>
</div>
    <div class="modal-body">
        <div class="row">

            <div class="row col-md-12">
                <!-- start ActiveForm -->
                <?php $form = ActiveForm::begin(['id' => 'form-signup',"enableClientValidation"=>true]); ?>
                            <div class="col-md-12">
                                <?= $form->field($model, 'course_title')->textInput(["placeholder"=>"Course Title ( 70 characers limit)"])->label(false) ?>
                            </div>
                            <div class="col-md-12">
                                <?=$form->field($model,'coursecat')->dropDownList(CourseCategory::getCategory(),['prompt'=>'Select course category'])->label(false);?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'firstname')->textInput(["placeholder"=>"Full Name"])->label(false) ?>
                             </div>
                            <div class="col-md-12">
                               <?= $form->field($model, 'email')->textInput(["placeholder"=>"E-mail",'type'=>"email"])->label(false) ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'password')->passwordInput(["autocomplete"=>"off",'placeholder'=>"Password"])->label(false)?>
                            </div>
                             <input type="hidden" value="1" name="SignupForm[roleid]"/>
                            <div class="col-xs-offset-4 col-xs-offset-right-4 col-xs-4 col-sm-offset-4 col-sm-offset-right-4 col-sm-4 col-md-offset-4 col-md-offset-right-4 col-md-4">
                                <input type="submit" value="Become a tutor" class="btn btn-success">
                            </div>

                <?php ActiveForm::end(); ?><!-- End form -->
            </div>

        </div>

       <div class="modal-footer">
            Already have an account? <a href="javascript:void(0);" onclick="login();return false">Log in</a>
        </div>
    </div>
  </div>
</div>
</section>
