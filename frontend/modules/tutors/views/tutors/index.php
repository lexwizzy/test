<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\CourseCategory;
     use yii\helpers\ArrayHelper;
?>
<!-- banner starts here -->

<header>
    <div class="header-content">
        <div class="header-content-inner text-left">
            <h1>Join our online tutors</h1>
            <p>And Monitize your knowledge</p>
            <a href="#howitworks" class="btn btn-primary btn-xl page-scroll">How it work</a>
            <a href="<?=Url::toRoute("/tutors/signup")?>" class="btn btn-secondary btn-xl page-scroll">Create a course</a>
        </div>
    </div>
</header>

<!-- banner ends here -->
   <!-- teachsity starts here -->

    <section>
        <div class="container teachsity-gives">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading-height">Teachsity gives you...</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12 col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="<?=Url::base()?>/images/24x7.png" alt="24x7">
                        <h3>24x7 Support</h3>
                        <p class="text-muted text-left">Use Teachsity support community, get your questions answered by our active community support team.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="<?=Url::base()?>/images/easyuse.png" alt="easyuse">
                        <h3>Easy-to-use Tools</h3>
                        <p class="text-muted text-left">Teachsity provides easy-to-use course creation tools to let you build your courses with ease.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="<?=Url::base()?>/images/absolutecontroll.png" alt="absolutecontroll">
                        <h3>Absolute Control</h3>
                        <p class="text-muted text-left">Teachsity provides free course hosting but let you control your course content. You can reuse your course content as much as you wish.</p>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <img src="<?=Url::base()?>/images/onlinebrand.png" alt="onlinebrand">
                        <h3>Build an Online Brand</h3>
                        <p class="text-muted text-left">Build and online brand on Teachsity, get to know your audience and become famous in what you teach.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- teachsity ends here -->

    <!-- heow it work starts here -->

    <aside class="bg-dark">
        <div class="container">
            <div class="row" id="howitworks">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading-height">How it Works</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12 works-box works-border-bottom works-border-right">
                  <div class="row margin-mutted">
                    <div class="col-sm-4 col-xs-12 padding-mutted">
                      <p class="text-center img-margine"><img src="<?=Url::base()?>/images/imagin.png" alt="imagine"></p>
                    </div>
                    <div class="col-sm-8 col-xs-12 padding-mutted">
                      <h3 class="img-sub-head">Imagine</h3>
                      <p class="img-sub-cont">There are no limit to what you can teach on Teachsity. Become famous for what you do best.</p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-12 works-box works-border-bottom">
                  <div class="row margin-mutted">
                    <div class="col-sm-4 col-xs-12 padding-mutted">
                      <p class="text-center img-margine"><img src="<?=Url::base()?>/images/design.png" alt="design"></p>
                    </div>
                    <div class="col-sm-8 col-xs-12 padding-mutted">
                      <h3 class="img-sub-head">Design</h3>
                      <p class="img-sub-cont">Use our easy-to-use course creation tool to build your online course.</p>
                    </div>
                  </div>
                </div>
            <div class="col-sm-6 col-xs-12 works-box works-border-right">
              <div class="row margin-mutted">
                <div class="col-sm-4 col-xs-12 padding-mutted">
                  <p class="text-center img-margine"><img src="<?=Url::base()?>/images/teach.png" alt="teach"></p>
                </div>
                <div class="col-sm-8 col-xs-12 padding-mutted">
                  <h3 class="img-sub-head">Teach</h3>
                  <p class="img-sub-cont">Teach millions of hungry student ready to learn, anytime and on any device.</p>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 works-box">
              <div class="row margin-mutted">
                <div class="col-sm-4 col-xs-12 padding-mutted">
                  <p class="text-center img-margine"><img src="<?=Url::base()?>/images/earn.png" alt="earn"></p>
                </div>
                <div class="col-sm-8 col-xs-12 padding-mutted">
                  <h3 class="img-sub-head">Earn</h3>
                  <p class="img-sub-cont">Monitize your expertise, Earn handsomely while impacting positively on the world. </p>
                </div>
              </div>
            </div>
            </div>
        </div>
    </aside>
    <!-- heow it work ends here -->

    <!-- join teachsity start here -->

    <section class="2nd-bg who-can-join">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading-height">Who Can Join Teachsity</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-xs-12 col-sm-4 padding mutted ">
              <div class="thumbnail">
                  <div class="thumb-bg">
                    <img src="<?=Url::base()?>/images/expert-icn.png">
                  </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 padding mutted">
              <div class="thumbnail">
                  <div class="thumb-bg">
                    <img src="<?=Url::base()?>/images/instructor-icn.png">
                  </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 padding mutted">
              <div class="thumbnail">
                  <div class="thumb-bg">
                    <img src="<?=Url::base()?>/images/hobby-list-icn.png">
                  </div>
              </div>
            </div>
        </div>
    </section>
    <!-- join teachsity ends here -->

    <!-- faq start here -->

    <aside class="bg-light">
        <div class="container text-center">
            <div class="section-heading-height">
                <h2>FAQ</h2>
            </div>
        </div>
        <div class="container">
          <div class="row faq-div">
             <div class="col-md-offset-2 col-md-offset-right-2 col-md-8 ">

           <div id="accordion-first" class="clearfix">
              <div class="accordion" id="accordion2">
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                      <em class="icon-fixed-width fa fa-plus"></em>Are my account details safe with you?
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseOne" class="accordion-body collapse">
                    <div class="accordion-inner">
                    Teachsity strive to ensure maximum security of our user account details. Our team employs procedural and technological measures to secure users account details.
                    </div>
                  </div>
                </div>
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                      <em class="icon-fixed-width fa fa-plus"></em>When do I get paid if someone purchases my course?
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseTwo" class="accordion-body collapse">
                    <div class="accordion-inner">
                      Teachsity tutors get paid on a monthly bases, but a tutor can request for payment at any point without any minimum amount requirement.
                    </div>
                  </div>
                </div>
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                      <em class="icon-fixed-width fa fa-plus"></em>What is the commission that I pay to you?
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseThree" class="accordion-body collapse">
                    <div class="accordion-inner">
                      Since we operate a commission based system. Click <a href="http://support.teachsity.com/revenue-share">here</a> to see Teachsity’s commission breakdown.
                    </div>
                  </div>
                </div>
                  <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                      <em class="icon-fixed-width fa fa-plus"></em>Can I become a full time tutor for you?
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseFour" class="accordion-body collapse">
                    <div class="accordion-inner">
                     Teachsity do not keep a tutor on payroll we only work with tutors that are freelancers.
                    </div>
                  </div>
                </div>
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
                      <em class="icon-fixed-width fa fa-plus"></em>How do you make sure my course is not stolen? 
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseFive" class="accordion-body collapse">
                    <div class="accordion-inner">
                      Teachsity strive to protect course contents on our platform. We keep an eye on the internet to ensure that there has been no leakage of course content, if any we apply some legal means to checkmate the leakage. We all trademark every course content on Teachsity.
                    </div>
                  </div>
                </div>
                  <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
                      <em class="icon-fixed-width fa fa-plus"></em>Do you’ll support in case i get stuck with some issue? 
                    </a>
                  </div>
                  <div style="height: 0px;" id="collapseSix" class="accordion-body collapse">
                    <div class="accordion-inner">
                      We have a 24/7 support service that is ready to help you out when you get stuck. Email us on support@teachsity.com
                    </div>
                  </div>
                </div>
              </div><!-- end accordion -->
          </div>
          </div>
        </div>
      </div>
    </aside>

  <!-- faq ends here -->
