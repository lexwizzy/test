<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
?>
<?php $this->beginContent('@app/views/layouts/parent_layout.php'); ?>

<!-- home banner -->
<div class="home_banner">
        <img class="banner_img" src="<?= Url::base()?>/images/home_banner.jpg" />
        <img class="mobile_banner" src="<?= Url::base()?>/images/mobile_banner.jpg" />
        <div class="header">
              <div class="head_left">
                  <a href="<?Url::toRoute("/")?>" title="Teachsity"><img class="logo" src="<?= Url::base()?>/images/logo.png" alt="Teachsity"/></a>
              </div>
              <div class="head_right">
              		<p class="top_nav"><a href="javascript:void(0)"><img src="<?= Url::base()?>/images/top_nave.jpg"></a></p>
                    <div class="top_nav_bg">
                      <ul class="login_right" style="display:block;">
                        <li><a href="<?= Url::toRoute('/tutors')?>">Become an Instructor</a> <span>|</span></li>
                        <li><a href="javascript:void(0);" onclick="login();return false;">Login</a> <span>|</span></li>
                        <li><a href="javascript:void(0);" onclick="signup();return false">Signup</a></li>
                      </ul>
                    </div>
              </div>
        </div>


<!-- this div will be close in the content page	</div> -->

<!--Content -->
 <?= $content?>
<!-- End content -->
<?php $this->endContent()?>
