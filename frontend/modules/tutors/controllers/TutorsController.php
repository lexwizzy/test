<?php

namespace app\modules\tutors\controllers;
use yii\filters\AccessControl;
use frontend\models\SignupForm;
use frontend\models\LoginForm;
use yii\helpers\Url;
use Yii;
use frontend\models\Course;
use frontend\models\User;
use yii\db\Expression;
use frontend\models\CourseAnnouncement;
use frontend\models\Utility;
use yii\data\ActiveDataProvider;
use frontend\models\TcCoursesSalesTransaction;

class TutorsController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['index','signup'],
                'rules' => [
                    [
                        'actions' => ['index','signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                   [
                        'actions' => ['dashboard','profile','index','signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout="@app/views/layouts/home_tutor";
        return $this->render('index');
    }

      public function actionDashboard()
    {
        $this->layout="@app/views/layouts/user_dashboard";
        $this->view->params['d_class']="current";
        $newcourse = new Course;
        $announcement = new CourseAnnouncement;
        $userid=Yii::$app->user->id;
        $newcourse->scenario="isguest";
        $searchModel = new \frontend\models\CourseSearch();
        $currentMonthData=TcCoursesSalesTransaction::getUserCurrentMonthData(Yii::$app->user->id,date("m"));
        $gross=round(TcCoursesSalesTransaction::getUserCurrentMonthGross($userid,date("m")),2);
        $net=round(TcCoursesSalesTransaction::getUserCurrentMonthNet($userid,date("m")),2);
        $dataProvider = $searchModel->searchTutorCourse(Yii::$app->request,$userid,4);
        return $this->render('dashboard',array("currentMonthData"=>$currentMonthData,"net"=>$net,"gross"=>$gross,"transaction"=>TcCoursesSalesTransaction::getTransaction($userid),"dataProvider"=>$dataProvider,"newcourse"=>$newcourse,"userid"=>$userid,"announcement"=>$announcement));
    }

    public function actionSignup(){

        //echo 1;

        //$this->layout="@app/views/layouts/main_guest";
      $coursecat = Utility::cleanString(Yii::$app->request->get("cc"));
      $coursetitle =Utility::cleanString(Yii::$app->request->get("c_title"));
        $model=new SignupForm();
        if(Yii::$app->user->isGuest){
          $model->scenario ="tutor";

          if($model->load(Yii::$app->request->post())){

              if($user = $model->tutorsignup()){

                  if(Yii::$app->getUser()->login($user)){
                       LoginForm::setSession($user);
                       \Yii::$app->mailer->compose(['html' => 'signupComplete-html'], ['user' => $user])
                      ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                      ->setTo($user->email)
                      ->setSubject('Welcome to ' . \Yii::$app->name)
                      ->send();
                      return $this->redirect(array("/course/basicinfo",'slug'=>Yii::$app->session->get("new_course_slug")));
                  }

              }

          }
            else{
                //print_r(Yii::$app->request->get());
               //// echo "ds";
            }


        }
        else {
          if($model->load(Yii::$app->request->post())){
            $user = User::findOne([Yii::$app->user->id]);
            $user->roleid=2;
            $transaction = $user->getDb()->beginTransaction();
            $user->save();
            $course= new Course(['scenario' => 'isguest']);
            $course->course_title=$model->course_title;

            $course->course_category=$model->coursecat;
            $course->owner = Yii::$app->user->id;

            //CourseCategory::convertToInt($this->coursecat);
            if($course->save()){
                $instructors=  new \frontend\models\CourseInstructors();
                $instructors->course_id=$course->getPrimaryKey();
                $instructors->user_id=Yii::$app->user->id;
                if($instructors->save()){
                     $transaction->commit();
                        //if(!Yii::$app->session->isActive)
                            //    Yii::$app->session->open();

                    Yii::$app->session->set("new_course",$course->course_id);
                }
                else
                    print_r($instructors->getErrors());
                //return $this->redirect(array("/course/basicinfo",'slug'=>Yii::$app->session->get("new_course_slug")));

           }

        }
        }
          return $this->render('signup', [
            'model' => $model,
            'coursecat'=>$coursecat,
            'coursetitle'=>$coursetitle
        ]);

    }

}
