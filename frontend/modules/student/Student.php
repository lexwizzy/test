<?php

namespace app\modules\student;

class Student extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\student\controllers';
    public $defaultRoute='student';
    public $defaultController='student';
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
