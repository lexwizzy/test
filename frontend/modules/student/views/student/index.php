<?php
    use yii\helpers\Url;
    use frontend\models\User;
    use frontend\models\Course;
    use yii\widgets\ActiveForm;
    use marqu3s\summernote\Summernote;
    use kartik\rating\StarRating;
    use yii\bootstrap\BootstrapAsset;

    $counter=0;
    $rating_array=array();
    $script="";
?>
<!-- PROFILE FEATURE -->





    <!-- COURSE CONCERN -->
        <div class="container student_courses">
            <div class="row ">
               <?php if(User::getMyCourse(Yii::$app->user->id)):?>
                    <?php foreach(User::getMyCourse(Yii::$app->user->id) as $c):?>
                            <?php $coursead=Course::getCourseAd($c->courseid);?>
                                <div class=" coursebox">
                                    <?=Course::createStudentCourseAd($coursead,++$counter);$rating_array[]=$counter?>

                                </div>

                            <?php endforeach;?>
                <?php else:?>
                   <div class="col-md-12 text-center " style="padding-top:6em">
                       <h2>You have not joined  any course on Teachsity</h2 >
                       <p style="color:#175690">Take a walk down Teachsity MarketPlace and start learning NOW!</p>
                       <div><a href="<?=Url::toRoute("/courses")?>" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i> Start learning Now!</a></div>

                   </div>

                <?php endif; ?>
            </div>
        </div>

    <!-- END / COURSE CONCERN -->

<!-- Modal Rating -->
  <div class="modal fade" id="rating-modal" tabindex="-1" role="dialog" aria-labelledby="rating-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Rate : <span id="course-title"></span></h4>
        </div>
        <div class="modal-body">
            <div id="alert-rating-success" class="alert alert-success" style="display:none"></div>
            <div id="alert-rating-error"  class="alert alert-danger " style="display:none"></div>
          <?php $form=ActiveForm::begin(['id'=>'ratingform','options'=>['onsubmit'=>"return false"]]);?>

          <div>
            <?= $form->field($rating,"userid")->hiddenInput(['value'=>Yii::$app->user->id])->label(false)?>
           <?= $form->field($rating,"courseid")->hiddenInput()->label(false)?>
            <?= $form->field($rating, 'comment')->widget(Summernote::className(),
   ['clientOptions' => ["height"=>"100",'toolbar'=>[['para', ['ul', 'ol', 'paragraph']],['style',['bold','italic','underline']]]]])->label(false) ?>
          </div>

          <!-- <div class="col-md-2" style="padding-top:10px"><span>Your rate</span></div> -->
          <div class="col-md-12">

              <?=
                  $form->field($rating, 'rating')->widget(StarRating::classname(), [

                      'pluginOptions' => [
                      'size' => 'xs',
                      'stars' => 5,
                      'step' => 1,
                      'min' => 0,
                      'max' => 5,
                      'clearButton'=>'',
                      'showCaption' => true,
                      'starCaptions' => [
                      1 => 'Extremely Poor',
                      2 => 'Poor',
                      3 => 'Good',
                      4 => 'Very Good',
                      5 => 'Extremely Good',
                      ]
                      ],
                  ])->label(false);
              ?>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-success" id="save-review" value="Submit Review"/>
          </div>
              <?php ActiveForm::end();?>
        </div>

      </div>
    </div>
  </div>
  <!-- End Rating -->

  <?php
      foreach($rating_array as $i):
        $script.="$('#rating".$i."').on('rating.change', function(event, value, caption) {
            $('#rating-modal').modal();
            $('#coursereviews-rating').rating('update',value);
            $('#course-title').text($('#rating".$i."').parents('.coursebox').find('.course_name').text())
            $('#coursereviews-courseid').val($('#rating".$i."').parents('.course-box').attr('data-courseid'))
         });";
      endforeach;

          $this->registerJS($script);
  ?>
  <?php $this->registerJsFile( Url::base().'/js/ladda.min.js',['depends' => [\yii\web\JqueryAsset::className()]]); ?>
  <?php $this->registerJsFile( Url::base().'/js/courselearn.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]]); ?>
