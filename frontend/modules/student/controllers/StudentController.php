<?php

namespace app\modules\student\controllers;

use yii\filters\AccessControl;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
class StudentController extends \yii\web\Controller
{
    public $layout ="@app/views/layouts/main";
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    /*[
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],*/
                    [
                        'actions' => ['index','logout','dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $this->view->params['s_class']="current";
        $this->layout="@app/views/layouts/user_dashboard";
        if(\frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->exists())
             $rating =  \frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->one();
        else
           $rating = new \frontend\models\CourseReviews;
        return $this->render('index',["rating"=>$rating]);
    }
    public function actionDashboard()
    {
        $this->view->params['s_class']="current";
        $this->layout="@app/views/layouts/user_dashboard";
      
        $rating = new \frontend\models\CourseReviews;
        return $this->render('index',["rating"=>$rating]);
    }

}
