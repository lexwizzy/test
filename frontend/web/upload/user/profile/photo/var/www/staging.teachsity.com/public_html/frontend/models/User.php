<?php

namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\helpers\FileHelper;
/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $email
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $firstname
 * @property string $lastname
 * @property string $gender
 * @property string $dob
 * @property string $location
 * @property string $aboutme
 * @property string $photo
 * @property string $roleid
 * @property string $lastlogin
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property SocialLink $socialLink
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    public $update_profile_pwd;
    public $new_password;
    public $new_password_repeat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [

           /* [
                TimestampBehavior::className(),
                'createdAttributes'=>'created_at',
                'updatedAttributes'=>'updated_at',
                'value'=>new Expression('time()')
            ],
           [
                'class'=>BlameableBehavior::className(),
                'createdByAttribute'=>'created_by',
                'updatedByAttribute'=>''
            ]*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'firstname'], 'required'],
            [['email', 'firstname','gender','dob','headline','country','state','aboutme'],'required',"on"=>"premiumtutor"],
            [['update_profile_pwd','new_password','new_password_repeat'],'required','on'=>'changepwd', 'message'=>'Enter a correct password to change email address'],
            [['update_profile_pwd'],'required','on'=>'changeemail','message'=>'Enter a correct password to change email address'],
            [['password_reset_token', 'auth_key'], 'string'],
            [['password'],'string',"min"=>6,'tooShort'=>"Minimum password length is 6"],
            [['update_profile_pwd'],'string',"min"=>6,'tooShort'=>"You have entered an invalid password"],
            [['new_password','new_password_repeat'],'string',"min"=>6,"tooShort"=>"New Password should be at least {min}"],
            [['new_password'],"compare","compareAttribute"=>"new_password_repeat",'on'=>"changepwd"],
            [['dob', 'lastlogin','updated_at','created_at'], 'safe'],
            [['roleid'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['firstname', 'lastname', 'country'], 'string', 'max' => 200],
            [['state'],'string','max'=>100],
            [['gender'], 'string', 'max' => 10],
            [['aboutme'], 'string', 'max' =>800],
            [['dob'], 'default', 'value' => null],
            [['headline'],'string','max'=>200],
            [
            'photo',
            'image',
            'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
            'mimeTypes' => ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'],
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'gender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Dob'),
           // 'location' => Yii::t('app', 'Location'),
            'aboutme' => Yii::t('app', 'Aboutme'),
            'photo' => Yii::t('app', 'Photo'),
            'roleid' => Yii::t('app', 'Roleid'),
            'lastlogin' => Yii::t('app', 'Lastlogin'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

     public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email'=> $username]);
    }

    public static function findByUserId($id){
        return static::findOne(['id'=> $id]);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            //'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialLink()
    {
        return $this->hasOne(SocialLink::className(), ['userid' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

     /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

     public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
    public function validatePasswordFromController($password,$main_password)
    {
        return Yii::$app->security->validatePassword($password, $main_password);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getPhoto($id){
        $user=User::find()->where(['id'=>$id])->one();
        if($user !=null)
          $photo=$user->photo;
        if(!empty($photo)){
            if(!Auth::find()->where(["user_id"=>$id])->exists())
                return Yii::getAlias("@awsphoto")."/".$photo;
            else
                return $photo;
        }
        else{
            return  Yii::getAlias("@web")."/upload/user/profile/photo/default.jpg";
        }
    }
    public function getLocation($id){
        $user=User::find()->where(['id'=>$id])->one();
        $location['country']=$user->country;
        $location['state']=$user->state;
        return $location;
    }
    public function getName($id){
        $name =array();
        $user=User::find()->where(['id'=>$id])->one();
        if($user !=null){
          $name['firstname']=$user->firstname;
          $name['lastname']=$user->lastname;
        }

        return $name;
    }
    public function isRole($userid,$role){
        $model = User::find()->where(['id'=>$userid,"roleid"=>$role]);
        if($model->exists())
            return 1;
        else
            return 0;
    }
    public function getAboutme($id){
        $user=User::findOne(['id'=>$id]);
        return $user->aboutme;
    }

    public  function getMyCourse($userid){
       $enroll = \frontend\models\Enrollment::getMyEnrollments($userid);
       return $enroll;
    }
    public function isPremium($userid){
           $query= (new \yii\db\Query())
           ->select(["userid"])
           ->from("tc_premium_tutors")
           ->where(["userid"=>$userid])
           ->exists();
           return $query;
       }



}
