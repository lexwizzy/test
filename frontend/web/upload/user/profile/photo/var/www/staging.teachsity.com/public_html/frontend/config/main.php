<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'=>'Teachsity',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
         '@tmp' => '/var/www/staging.teachsity.com/public_html/frontend/web/upload/temp',//__DIR__ . '/../../frontend/web'
         '@fbid'=>'1680785638822349',
         '@awsphoto'=>"http://162.243.118.48/upload/user/profile/photo",
         '@aws_cc'=>"/var/www/staging.teachsity.com/public_html/frontend/web/upload"//course content

    ],
    'components' => [


        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => 'teach_staging',
            'password' => '10/11/2015stagingteachsity',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'info@teachsity.com',
                'password' => 'teachsity123',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=staging_teachsity',
            'username' => 'teach_staging',
            'password' => '10/11/2015stagingteachsity',
            'charset' => 'utf8',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'loginUrl'=>'site/login'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'storage' => [
            'class' => '\frontend\models\AmazonS3',
            'key' => 'AKIAIADVZAOP5UMTJ2QA',
            'secret' => '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',
            'bucket' => 'teachsity-india',

        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
             //  'baseUrl'=>$baseUrl,
                'class' => 'yii\web\UrlManager',
                // Disable index.php
                'showScriptName' => false,
                // Disable r= routes
                'enablePrettyUrl' => true,
                'rules' => array(
                    'index' => 'site/index',
                    'helplearn' => 'site/learn',
                    'course'=>'course/index',
                    'course/details/<slug>' => 'course/details',
                    'course/studentpreview/<slug>' => 'course/studentpreview',
                    'course/classroom/<slug>' => 'course/classroom',
                    'course/subscribe/<slug>' => 'course/subscribe',
                    'course/learn/<slug>'=>'course/learn',
                    'course/basicinfo/<slug>'=>'course/basicinfo',
                    'course/settings/<slug>'=>'course/settings',
                    'course/preview/<slug>'=>'course/preview',
                    'course/contents/<slug>'=>'course/contents',
                    'course/studentreport/<slug>'=>'course/studentreport',
                    'course/coursecategory/<category>' => 'course/coursecategory',
                    'course/savereview'=>'course/savereview',
                    'course/<action>/<slug>' =>'course/<action>',
                    //'site/<action:\w+(-\w+)*>' => 'site/<action>',
                    //'course/basicinfo'=>'course/basicinfo',
                   // 'course/savecoursetitle'=>"course/savecoursetitle",
                    '<_a:(profile|photo|settings|account)>'=>'user/<_a>',
                    '<_a:(login|index|signup|auth|indextest)>'=>'site/<_a>',
                    '<module:(tutors)>/<action:\w+>' => '<module>/tutors/<action>',
                    '<module:(student)>/<action:\w+>' => '<module>/student/<action>',
                    '<controller:\w+>/<action>'=>'<controller>/<action>',
                    '<controller>/<id:\d+>/<action>' => '<controller>/<action>',




                   // '<_a:(index)>'=>'tutors/<_a>',
                   /* '<controller:(user|site)>/<id:\d+>/<action:(login|signup)>' => '<controller>/<action>',
                        '<controller:\w+>/<id:\d+>' => '<controller>/view',
                        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                        '<module:\w+>/<action:\w+>' => '<module>/tutors/<action>',
                        '<module:\w+>/<action:\w+>/<id:\d+>' => '<module>/default/<action>',
                        '<module:\w+>/<controller:\w+>' => '<module>/<controller>/index',
                        '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>'*/
                ),
        ],

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'linkedin' => [
                    'class' => 'yii\authclient\clients\LinkedIn',
                    'clientId' => '75s20s0zytlbeh',
                    'clientSecret' => 'agVLciPdN8j5tSlx',
                    'scope'=>'r_basicprofile r_emailaddress w_share'
                  //  'returnUrl'=>'http://localhost/teachsity/frontend/web/auth'
                        ],
                    /*'google' => [
                        'class' => 'yii\authclient\clients\GoogleOpenId',
                        'clientId'=>'394810486919-rj9vm1ch84bfv4to59v5gh5ru9gcos30.apps.googleusercontent.com',
                        'clientSecret'=>'E9DtOaRBDexHP3F-QQas2d7w'
                    ],*/
                    'facebook' => [
                        'class' => 'yii\authclient\clients\Facebook',
                        'clientId' => '1680785638822349',
                        'clientSecret' => '78bd0d1a1eac9152f182ad8cebb2a412',

                      ],
                    /*'twitter' => [
                        'class' => 'yii\authclient\clients\Twitter',
                        'consumerKey' => '5Fv32SSQ4D3A6QANTMB0OL0l1',
                        'consumerSecret' => 'qkfUQZRCQ1HiRWbXYFehPQmBWRmrzzYz6ul3OsyrG8lLNX9Mma'
                    ]*/

                ],

            ]
        ],
        'modules' => [
          'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [
                'sphinxModel' => [
                    'class' => 'yii\sphinx\gii\model\Generator'
                ]
            ],
        ],
            'student' => [
                'class' => 'app\modules\student\Student',
            ],

             'tutors' => [
                 'class' => 'app\modules\tutors\Tutors',
             ],
             'social' => [
                  // the module class
                  'class' => 'kartik\social\Module',



                  // the global settings for the facebook plugins widget
                  'facebook' => [
                      'appId' => '1680785638822349',
                      'secret' => '78bd0d1a1eac9152f182ad8cebb2a412',
                  ],

                  // the global settings for the google plugins widget
                  'google' => [
                      'clientId' => '394810486919-rj9vm1ch84bfv4to59v5gh5ru9gcos30.apps.googleusercontent.com',
                      'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                      'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
                  ],



                  // the global settings for the twitter plugins widget
                  /*'twitter' => [
                      'screenName' => 'TWITTER_SCREEN_NAME'
                  ],*/
              ],
        ],


    'params' => $params,
];
