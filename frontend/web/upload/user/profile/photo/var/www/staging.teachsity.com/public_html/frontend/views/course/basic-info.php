<?php
 use yii\helpers\Url;
 use yii\helpers\ArrayHelper;
 use yii\helpers\Html;
 use frontend\models\CourseCategory;
 use frontend\models\Course;
 use yii\widgets\ActiveForm;
 use yii\bootstrap\BootstrapAsset;
 use yii\web\View;
 use frontend\models\IsoLanguages;
 use Zelenin\yii\widgets\Summernote\Summernote;
 use frontend\models\SkillLevel;

$this->registerJs('var baseUrl = '.json_encode(Url::base()),View::POS_BEGIN, 'my-options');
$this->registerJs('var promo_video_thumbnail = '.json_encode(Course::promoVideoThumbnail($course->course_id)),View::POS_HEAD);
    $this->registerJs('var coursebanner = '.json_encode(Course::getCourseBanner($course->course_id)),View::POS_HEAD);
    $this->registerJs('var courseid='.json_encode($course->course_id),View::POS_HEAD);

?>
 <link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/fileinput.min.css">

<link rel="stylesheet" type="text/css" href="<?=Url::base()?>/css/bootstrap-select.min.css">
<link rel="stylesheet" href="<?=Url::base()?>/css/bootstrap-tagsinput.css" type="text/css"/>
<style>

.file-drop-zone{
    height:238px;
    padding:0px;
}
.file-preview {
    width:270px;
}
.file-drop-zone-title {
    color: #aaa;
    font-size: 26px;
    padding: 30px 5px;
}
.bootstrap-tagsinput{
    width:500px;
}
.bootstrap-tagsinput span {
    display: inline !important;
    color:#fff !important;
    font-size:11px !important;
}
.bootstrap-tagsinput input{
    border:none !important;
    width:6em !important;
}
#amount-item .form-group {
    margin-bottom:0 !important;
}
#amount-item .help-block{
    margin:0;
}
    .multiselect-container{
        width:300px;
        overflow:hidden;
    }
</style>

<section id="course_edit" class="sub-banner sub-banner-create-course">
        <div class="awe-color bg-color-1"></div>
        <div class="container">
            <div id="course-title">
                <h2 class="md ilbl" id="title_label"><?= $course->course_title?></h2>
            <i class="icon md-pencil" id="course-name"></i>
            </div>


        <div id="course-name-div" style="background:#fff; padding:10px" class="hidden col-lg-6">
           <?php $form =ActiveForm::begin(['action'=>'','options'=>['onsubmit'=>"return false"]])?>
                <div class="form-item">
                    <?=$form->field($course,"course_title")->textInput(['placeholder'=>"Course title",'style'=>"border:1px solid #D4D4D4 ",'maxlength'=>70])->label(false)?>

                </div>
            <div class="form-action">
        <input type="submit" value="Save" id="save-course-name" class="submit mc-btn-3 btn-style-1">
                &nbsp;&nbsp;<input type="submit" value="Cancel" class="submit mc-btn-3 btn-style-1" id="cancel-course-name">
                &nbsp;&nbsp;
                <i class="fa fa-circle-o-notch fa-spin" style="display:none"></i>
         </div>
            <?php ActiveForm::end()?>
        </div>
        </div>
    </section>
    <!-- END / BANNER CREATE COURSE -->

    <!-- CREATE COURSE CONTENT -->
    <section id="create-course-section" class="create-course-section">
        <div class="container">
            <div class="row">
                    <div class="col-md-3">
                        <div class="create-course-sidebar">
                            <ul class="list-bar">
                                <li class="active"><span class="count">1</span>Basic Information</li>
                                 <li><a href="<?=Url::toRoute(["/course/contents","slug"=>$course->slug])?>"><span class="count">2</span>Course Curriculum</a></li>
                                <li><a href="<?=Url::toRoute(["/course/settings","slug"=>$course->slug])?>"><span class="count">3</span>Course Settings</a></li>
                                <li><a href="<?=Url::toRoute(["/course/studentreport","slug"=>$course->slug])?>"><span class="count">4</span>Student Report</a></li>
                            <li><a href="<?=Url::toRoute(["/course/preview","slug"=>$course->slug])?>"><span class="count">5</span>Preview Course</a></li>
                            </ul>
                            <div class="support">
                                <a href="create-basic-information.html#"><i class="icon md-camera"></i>See how it work short tutorial</a>
                                <a href="create-basic-information.html#"><i class="icon md-flash"></i>Instant Support</a>
                            </div>
                        </div>
                    </div>

                <div class="col-md-9" >
                    <?php $basic =ActiveForm::begin()?>
                    <div class="create-course-content">
                      <?php if(Yii::$app->session->hasFlash("error")):?>
                       <div class="alert alert-danger">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <?= Yii::$app->session->getFlash("error")?> <strong><a href="<?=Url::toRoute('/user/premium-tutor')?>">Apply Now</a></strong>
                       </div>
                <?php endif;?>
                        <!-- Course goal-->
                        <div class="course-banner create-item">



                        <!-- EN course goals-->
                        <!-- COURSE BANNER -->
                        <div class="course-banner create-item">
                            <div class="row">
                                <div  class="col-md-3">
                                    <h4 class="err">Course Banner</h4>
                                </div>
                                <div class="col-md-9 " >
                                    <input accept="image/*" class="file-loading" id="course-image" name="course_banner" type="file"/>                       <div class="form-item"><span>maximum image size is 1MB.</span></div>
                                    <div class="form-item">
                                            <span>Required size is 480px x 270px</span>
                                        </div>
                                    <!--<div class="image-info"  style="max-height:160px">
                                        <img src="<?= Url::base()?>/images/img-upload.jpg" alt="">

                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <!-- END / COURSE BANNER -->

                        <!-- PROMO VIDEO -->
                        <div class="promo-video create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Promo Video</h4>
                                </div>
                                <div class="col-md-9">
                                   <!-- <div class="form-item">
                                        <input type="text" placeholder="Paste URL">
                                    </div>-->
                                    <div class="upload-video up-file ">
                                       <!-- or-->
                                        <input type="file" class="file-loading" id="promovideo" name="promo_video">
                                        <div class="form-item">
                                            <span>maximum video size is 50MB.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / PROMO VIDEO -->

                        <!-- DESCRIPTION -->
                        <div class=" create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="err">Description</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="">
                                       <?= $form->field($course, 'course_desc')->widget(Summernote::className(),
                                 ['clientOptions' => ["height"=>"100",'toolbar'=>[['style',['bold','italic','underline']]]]])->label(false) ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END / DESCRIPTION -->
                        <div class="row">
                                <div  class="col-md-3">
                                    <h4 class="err">Course Goals</h4>
                                </div>
                                <div class="col-md-9" >
                                    <div class="form-item">

                                    <?= $form->field($course, 'course_goal')->widget(Summernote::className(),
                                 ['clientOptions' => ["height"=>"100",'toolbar'=>[['para',['ul']],['style',['bold','italic','underline']]]]])->label(false) ?>


                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div  class="col-md-3">
                                    <h4 class="err">Target Audience</h4>
                                </div>
                                <div class="col-md-9" >
                                    <div class="form-item">
                                        <?= $form->field($course, 'target_audience')->widget(Summernote::className(),
                                 ['clientOptions' => ["height"=>"100",'toolbar'=>[["para",['ul']],['style',['bold','italic','underline']]]]])->label(false) ?>

                                    </div>

                                </div>
                            </div>
                        <!-- INSTRUCTORS
                        <div class="instructors create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Instructors</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="author">
                                        <div class="image-author">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <cite>Tyrion Lanister ( Author )</cite>
                                    </div>
                                    <div class="author">
                                        <div class="image-author">
                                            <img src="images/team-13.jpg" alt="">
                                        </div>
                                        <cite>Anna Molly</cite>
                                        <div class="remove">
                                            <a href="create-basic-information.html#"><i class="icon md-close-2"></i></a>
                                        </div>
                                    </div>
                                    <a href="create-basic-information.html#" class="add-instructor"><i class="icon md-plus"></i>Add Instructor</a>
                                </div>
                            </div>
                        </div>
                        <!-- END / INSTRUCTORS -->



                        <!-- TUITION FREE -->
                        <div id="course-fee" class="tuition-fee create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Course fee</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item form-radio radio-style">
                                <?=$form->field($course, 'course_price')->radioList(["Free"=> 'Free', "Paid" => 'Paid'],                                                ['item' => function($index, $label, $name, $checked, $value) {
                                                if($checked)
                                                    $checked="checked";
                                    $return = '<input type="radio"  '.$checked.' id=" '.$label.'" name="' .$name. '" value="'. $value.'" separator="">';
                                    $return .= '<label for=" '.$label.'"><i class="icon-radio"></i> '.$label.'</label>';
                                    return $return;
                                }])->label(false)?>
                                        <!--
                                         <div class="form-group field-course-course_price">

                                        <input type="hidden" name="Course[course_price]" value="<?=$course->course_price?>">
                            <div id="course-course_price">
                                <input type="radio" id="paid" name="Course[course_price]" value="1" separator="">
                                <label for="paid">
                                            <i class="icon-radio"></i>
                                            Paid
                                </label>
                            <input type="radio" name="Course[course_price]" value="2"  id="free" separator=""> <label for="free">
                                            <i class="icon-radio"></i>
                                            Free
                                        </label></div>

                            <div class="help-block"></div>
                            </div>
                                      -->
                                    </div>
                                    <div class="form-item" id="amount-item">
                                       <?= $form->field($course,'amount')->textInput(['id'=>'amount','placeholder'=>'amount'])->label(false)?>
                                        <span class="dl">$</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / TUITION FREE -->

                        <!-- FOR LEVEL FROM -->
                        <div class="for-level-from create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>For level from</h4>
                                </div>
                                <div class="col-md-9">
                                    <?= $form->field($course,'skill_level')->dropDownList(ArrayHelper::map(SkillLevel::find()->all(),'id','skill_level'),['class'=>'select form-control','prompt'=>'Select skill level','style'=>"max-width:60%"])->label(false) ?>
                                   <!-- <div class="form-item form-radio radio-style">
                                        <?php /*$form->field($course, 'skill_level')->radioList(ArrayHelper::map($query->select("id","skill_level")->from("skill_level")->all(),'id','skill_level'),                                                ['item' => function($index, $label, $name, $checked, $value) {
                                                if($checked)
                                                    $checked="checked";
                                    $return = '<input type="radio"  '.$checked.' id=" '.$value.'" name="' .$name. '" value="'. $value.'" separator="">';
                                    $return .= '<label for=" '.$value.'"><i class="icon-radio"></i> '.$value.'</label>';
                                    return $return;
                                }])->label(false)*/?>

                                </div>-->
                            </div>
                        </div>
                        <!-- END / FOR LEVEL FROM -->

                        <!-- CATEGORIES -->
                        <div class="categories-item create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="err">Select Category</h4>
                                    <span class="text-err">choose at least one</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-question">
                            <?= $form->field($course,'course_category')->dropDownList(CourseCategory::getCategory(),['class'=>'select form-control','id'=>'category_select','prompt'=>'Select category','style'=>"max-width:60%"])->label(false) ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="categories-item create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="err">Select Sub Category</h4>
                                    <span class="text-err">choose at least one</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-question">
                            <?= $form->field($course,'course_subcategory')->dropDownList(\yii\helpers\ArrayHelper::map(CourseCategory::getSubCat($course->course_category),"id","subcategory"),['class'=>'select form-control','id'=>'subcategory','prompt'=>'','style'=>"max-width:60%"])->label(false) ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="categories-item create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="err">Language</h4>
                                    <span class="text-err">choose at least one</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-question">
                            <?= $form->field($course,'language')->dropDownList(IsoLanguages::getISOLanguage(),['class'=>'select','multiple'=>"multiple",'id'=>'language_select','prompt'=>'Select language'])->label(false) ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- END / CATEGORIES -->

                        <!-- TOOL REQUIREMENT -->
                        <div class="tool-requirement create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4 class="">Tool requirement</h4>
                                    <span class="text-err">not filled yet</span>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <?=$form->field($course,'course_tools')->textInput(['data-role'=>'tagsinput'])->label(false)?>
                                        <span>type your tool, separated by comma or space (eg internet, adobe, notepad)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / TOOL REQUIREMENT -->

                        <!-- TAG
                        <div class="tag-item create-item">
                            <div class="row">
                                <div class="col-md-3">
                                    <h4>Tag</h4>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-item">
                                        <input type="text">
                                        <span>type your tool, separated by comma or space</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / TAG -->

                        <div class="form-action">
                            <!--<input type="submit" value="Save and Next" class="submit mc-btn-3 btn-style-1">-->
                            <input type="submit" value="Save and Next" class=" mc-btn-3 btn-style-1">
                        </div>

                    </div>
                </div>
                    <?php ActiveForm::end()?>
            </div>
        </div>
    </section>


    <!-- END / CREATE COURSE CONTENT -->

<?php

    $this->registerJsFile( Url::base().'/js/fileinput.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
$this->registerJsFile( Url::base().'/js/bootstrap-tagsinput.min.js',['depends' => [\yii\web\JqueryAsset::className()],[BootstrapAsset::className()]] );
$this->registerJsFile(Url::base()."/js/bootstrap-select.min.js",['depends'=>[\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile( Url::base().'/js/basicinfo.js',['depends' => [\yii\web\JqueryAsset::className()]] );

?>
