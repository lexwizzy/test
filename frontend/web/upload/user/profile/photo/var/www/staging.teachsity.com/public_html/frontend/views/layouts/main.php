<!DOCTYPE html>

<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use frontend\models\User;
    use frontend\assets\AppAsset;

    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
    <meta name="format-detection" content="telephone=no">
    <meta property="og:locale" content="en_US" />            <!-- Default -->
    <meta property="og:locale:alternate" content="fr_FR" />  <!-- French -->
    <meta property="og:locale:alternate" content="it_IT" />
    <meta property="og:site_name" content="Teachsity" />
    <!-- Google font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- Css -->
  <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/library/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/md-font.css">
    <!--<link rel="stylesheet" type="text/css" href="<?= Url::base()?>/css/style.css">-->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
     <?= Html::csrfMetaTags() ?>
    <!-- <title><?= Html::encode($this->title) ?></title>-->
        <title>Teachsity - Inovative learning system</title>
     <?php $this->head() ?>
    <script>var scope_array=new Array()</script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63028576-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body id="page-top" class="home">
<?php $this->beginBody() ?>
<!-- PAGE WRAP -->
<div id="page-wrap">

    <!-- PRELOADER -->
    <div id="preloader">
        <div class="pre-icon">
            <div class="pre-item pre-item-1"></div>
            <div class="pre-item pre-item-2"></div>
            <div class="pre-item pre-item-3"></div>
            <div class="pre-item pre-item-4"></div>
        </div>
    </div>
    <!-- END / PRELOADER -->

    <!-- HEADER -->
    <header id="header" class="header">
        <div class="container" >

            <!-- LOGO -->
            <div class="logo"><a href="<?= Url::home(true)?>"><img src="<?= Url::base()?>/images/logo.png" alt=""></a></div>
            <!-- END / LOGO -->

            <!-- NAVIGATION -->
            <nav class="navigation">

                <div class="open-menu">
                    <span class="item item-1"></span>
                    <span class="item item-2"></span>
                    <span class="item item-3"></span>
                </div>

                <!-- MENU -->
                <ul class="menu">

                <li class="current-menu-item"><a href="<?= Url::toRoute("/site/index")?>"><?= !Yii::$app->user->isGuest ? "Dashboard" : "Home";?></a></li>
                <?php if(Yii::$app->user->isGuest):?>

                     <li><a href="<?= Url::base()?>/login">Login</a></li>
                    <li><a href="<?= Url::base()?>/signup">SignUp</a></li>
                     <li><a href="<?= Url::toRoute('tutors/index')?>">Become a Tutor</a></li>
                <?php else:?>
                    <?php if(User::isRole(Yii::$app->user->id,1)):?>
                        <li><a href="<?= Url::toRoute('tutors/index')?>">Become a Tutor</a></li>
                    <?php endif;?>
                <?php endif;?>


                </ul>
                <!-- END / MENU -->

                <!-- SEARCH BOX -->
                <!--<div class="search-box">
                    <i class="icon md-search"></i>
                    <div class="search-inner">
                        <form>
                            <input type="text" placeholder="key words">
                        </form>
                    </div>
                </div>
                <!-- END / SEARCH BOX -->

                <!-- LIST ACCOUNT INFO -->
                <?php if(!Yii::$app->user->isGuest):?>
                <ul class="list-account-info">



                   <!-- NOTIFICATION
                    <li class="list-item notification">
                        <div class="notification-info item-click">
                            <i class="icon md-bell"></i>
                            <span class="itemnew"></span>
                        </div>
                        <div class="toggle-notification toggle-list">
                            <div class="list-profile-title">
                                <h4>Notification</h4>
                                <span class="count-value">2</span>
                            </div>

                            <ul class="list-notification">

                                <!-- LIST ITEM
                                <li class="ac-new">
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM

                                <!-- LIST ITEM
                                <li class="ac-new">
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM

                                <!-- LIST ITEM
                                <li>
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM -->

                                <!-- LIST ITEM
                                <li>
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM -->

                                <!-- LIST ITEM
                                <li>
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM -->

                                <!-- LIST ITEM
                                <li>
                                    <a href="index.html#">
                                        <div class="list-body">
                                            <div class="author">
                                                <span>Megacourse</span>
                                                <div class="div-x"></div>
                                            </div>
                                            <p>attend Salary for newbie course</p>
                                            <div class="image">
                                                <img src="<?= Url::base()?>/images/feature/img-1.jpg" alt="">
                                            </div>
                                            <div class="time">
                                                <span>5 minutes ago</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- END / LIST ITEM



                            </ul>
                        </div>
                    </li>
                    <!-- END / NOTIFICATION -->

                    <li class="list-item account">
                        <div class="account-info item-click">
                            <img class="small-profile-pix" src="<?=User::getPhoto(Yii::$app->user->id)?>" alt="your teachsity profile picture">
                        </div>
                        <div class="toggle-account toggle-list">
                            <ul class="list-account">
                                <?php if(Yii::$app->session->get("role")==1):?>
                                   <li><a href="<?=Url::base()?>/user/profile"><i class="icon fa fa-user"></i>Profile</a></li>
                                <?php elseif(Yii::$app->session->get("role")==2):?>
                                   <li><a href="<?=Url::base()?>/user/profile"><i class="icon fa fa-user"></i>Profile</a></li>
                                <?php endif;?>

                                <li><a href="<?=Url::base()?>/user/settings"><i class="icon md-config"></i>Setting</a></li>

                                <li><?=Html::a('<i class="icon md-arrow-right"></i>Sign Out',[Url::to('/site/logout')],['data-method'=>'post'])?> </li>
                            </ul>
                        </div>
                    </li>


                </ul>
                <?php endif;?>
                <!-- END / LIST ACCOUNT INFO -->

            </nav>
            <!-- END / NAVIGATION -->

        </div>
    </header>
    <!-- END / HEADER -->


    <?= $content; ?>


    <!-- FOOTER -->
    <footer id="footer" class="footer">
        <div class="first-footer">
            <div class="container">
                <div class="row">
                    <ul class="first-footer-ul">
                        <li> <a href="index.html#">About Us</a></li>
                        <li> <a href="index.html#">Contact</a></li>
                        <li> <a href="index.html#">Blog</a></li>
                        <li> <a href="index.html#">Events</a></li>
                        <li> <a href="index.html#">Teachsity for Organizations</a></li>
                        <li> <a href="index.html#">Placements</a></li>
                        <li> <a href="index.html#">Support</a></li>
                        <li> <a href="index.html#">Affliates</a></li>
                        <li> <a href="index.html#">Mobile Apps</a></li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="second-footer">
            <div class="container">
                <div class="contact">
                    <div class="email">
                        <a href="index.html#">Terms of Use</a>
                    </div>
                    <div class="email">
                        <a href="index.html#">Privacy Policy</a>
                    </div>
                    <!--<div class="phone">
                        <i class="fa fa-mobile"></i>
                        <span>+84 989 999 888</span>
                    </div>
                    <div class="address">
                        <i class="fa fa-map-marker"></i>
                        <span>Maecenas sodales, nisl eget</span>
                    </div>-->
                </div>
                <p class="copyright">Copyright © <?=date("Y") ?> Teachsity. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <!-- END / FOOTER -->





</div>
<!-- END / PAGE WRAP -->

<!-- Load jQuery -->
     <?php $this->endBody() ?>
<!--<script type="text/javascript" src="js/library/jquery-1.11.0.min.js"></script>-->

</body>

</html>
<?php $this->endPage() ?>
