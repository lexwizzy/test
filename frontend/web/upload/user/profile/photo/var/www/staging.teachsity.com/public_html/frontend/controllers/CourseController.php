<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Course;
use frontend\models\CourseInstructors;
use frontend\models\CourseParts;
use frontend\models\CourseLessons;
use frontend\models\LessonContent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use frontend\models\User;
use frontend\models\Quiz;
use frontend\models\Question;
use frontend\models\Answers;
use frontend\models\CourseCoupons;
use frontend\models\CourseSettings;
use frontend\models\CourseCategory;
use frontend\models\Utility;
use frontend\models\CourseOutlineOrder;
use Foolz\SphinxQL\SphinxQL;
use Foolz\SphinxQL\Connection;
use yii\data\Pagination;
use frontend\models\CoursesIndex;

/**
 * CourseController implements the CRUD actions for Course model.
 */

class CourseController extends Controller
{
    public $defaultAction ="checkowner";

    public function behaviors()
    {

        return [
             'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                   [
                        'actions' => ['index','checkowner','details','coursecategory','search','login','autosuggestjson'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login','search','autosuggestjson','lessoncomplete','finishquiz','saveanswers','getsavedanswers','createcourse','basicinfo','savename','savereview','flagabuse','index','subscribe','learn','getsubcategory','coursecategory','coursebanner','details','promovideo','deletebanner','contents','preview','studentreport','publish','unpublish','submitforreview','settings','saveparttitle','editpart','deletepart','newunit','editunit','deleteunit','uploadnewunitvideo','uploadnewunitpresentation','downloadables','getcontents','externallinks','textcontent','deleteunitcontent','newquiz','deletequiz','editquiz','newquestion','deletequestion','updatequestion','getquestion','deletecoupon','delete','savediscussion','getdiscussions','savereply','getlastreply','saveannouncement','classroom','studentpreview','learning','saveusernote','sectionorder','sectionitemsorder','classroomnavigation','quizintro','quizquestion','savetimer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Course models.
     * @return mixed
     */
     function checkOwner($loginuserid,$ownerid){
        if($loginuserid != $ownerid){
            return $this->redirect(Url::toRoute("/tutors/dashboard"));
        }

    }
    public function actionLogin(){
      return $this->redirect(Url::toRoute("/site/login"));
    }
    public function actionCreatecourse(){

        $course= new Course(['scenario' => 'isguest']);
        if($course->load(Yii::$app->request->post())){

            $transaction = $course->getDb()->beginTransaction();
            $course->owner = Yii::$app->user->id;
            $course->skill_level=1;

            if($course->save()){
                $instructors=  new CourseInstructors();
                $instructors->course_id=$course->getPrimaryKey();
                $instructors->user_id=Yii::$app->user->id;
                print_r($instructors);
                if($instructors->save())
                    {
                        $transaction->commit();
                        return $this->redirect(Url::toRoute(["basicinfo",'slug'=>$course->slug]));
                    }
            }

            print_r($course->getErrors());
        }



    }
    public function actionDetails($slug){
        $course=Course::find()->where(["slug"=>$slug])->one();
        $login =  new \frontend\models\LoginForm();
        Yii::$app->user->returnUrl=Url::current();
        if(Course::getPrice($course->course_id)=="Free")
            $buttontext="Start Learning Now";
        else
           $buttontext = "Take This Courses";
        /*if(!Yii::$app->request->cookies->has("tsuser")){
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name'=>"tsuser",'value'=>Yii::$app->security->generateRandomString(),'expire'=>3600 * 1000 * 24 * 365]));
        }*/
        //if it is not an instructor viewing his own course--preview
        if(Yii::$app->request->get("role")!=2 && Course::isViewed($course->course_id,Yii::$app->request->cookies->getValue("tsuser"))){
             $view = new \frontend\models\CourseViews;
                $view->courseid= $course->course_id;
                $cookie=Yii::$app->request->cookies;
                $view->cookie=$cookie->getValue("tsuser");

                $view->save();
        }

        return $this->render("coursedetails",array("course"=>$course,'login'=>$login,"buttontext"=>$buttontext,"role"=>1));


    } public function actionStudentpreview($slug){
        $course=Course::find()->where(["slug"=>$slug])->one();
        $login =  new \frontend\models\LoginForm();
        Yii::$app->user->returnUrl=Url::current();
        if(Course::getPrice($course->course_id)=="Free")
            $buttontext="Start Learning Now";
        else
           $buttontext = "Take This Courses";
        /*if(!Yii::$app->request->cookies->has("tsuser")){
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name'=>"tsuser",'value'=>Yii::$app->security->generateRandomString(),'expire'=>3600 * 1000 * 24 * 365]));
        }*/


        return $this->render("coursedetails",array("course"=>$course,'login'=>$login,"buttontext"=>$buttontext,'role'=>2));


    }


    public function actionBasicinfo($slug){
       /* if($course_id=="")
            {
                $course = new Course();
                $course->course_title="Enter course title here. Max 70 characters.";
            }
        else{*/
            $course=Course::find()->where(["slug"=>$slug])->one();
            $course_id=$course->course_id;
            //category multiselect n requires an array so...
           // $course->course_category = explode(",",$course->course_category);
            $course->language = explode(",",$course->language);
       // }

        if($course->load(Yii::$app->request->post())){
            $course->scenario="basicinfo";
            if($course->language!="")
                $course->language = implode(",",$course->language);
            if(!User::isPremium(Yii::$app->user->id) && $course->course_price=="Paid"){
                   Yii::$app->session->setFlash("error","Apply for premium tutor to start charging for your courses.");
                   return $this->redirect(array('basicinfo','slug'=>$course->slug));
            }
            if($course->validate() && $course->save()){

                return $this->redirect(array('contents','slug'=>$course->slug));
            }

        }

        return $this->render("basic-info",array("course"=>$course));
    }
     public function actionContents($slug){
            $course=Course::find()->where(["slug"=>$slug])->one();
          //  $parts_all= CourseParts::find()->where(['course_id'=>$course_id])->all();
            $part = new CourseParts;
            $lesson = new CourseLessons;
            $external= new LessonContent;
            $external->scenario="external";
            $textcontent= new LessonContent();
            $textcontent->scenario="text";

            $quiz = new Quiz();
            $question = new Question;

            return $this->render('contents',array("course"=>$course,'part'=>$part,'lesson'=>$lesson,'external'=>$external,"quiz"=>$quiz,'question'=>$question,"text"=>$textcontent));
    }
    //save section title
    public function actionSaveparttitle(){
        $part= new CourseParts();
       if($part->load(Yii::$app->request->post())){
           $transaction=$part->getDb()->beginTransaction();
           $lastcounter=CourseParts::find()->where(['course_id'=>$part->course_id])->orderBy("counter desc")->limit(1)->one();
           if($part->save() && $part->validate()){

               if($lastcounter==null){
                   $part->updateCounters(['counter' => 1]);
                   $transaction->commit();
               }
               else{
                      $part->updateCounters(['counter' => $lastcounter->counter+1]);
                      $transaction->commit();
                }


               return $this->redirect(Yii::$app->request->post('redirect'));
            }
       }
    }
    public function actionEditpart(){
        $part = CourseParts::findOne(['part_id'=>Yii::$app->request->post('sectionid')]);
        if(Yii::$app->request->post('sectiontitle') !="")
           $part->part_name=Yii::$app->request->post('sectiontitle');
         else
             $part->achievement=Yii::$app->request->post('sectionacheive');

            $part->save();
    }
    public function actionDeletepart($partid,$redirect){
        if(Yii::$app->request->isPost){
            $model=CourseParts::findOne($partid);
            //$transaction=$model->getDb()->beginTransaction();
            $model->delete();
           // CourseParts::updateAllCounters(["counter"=>-1],['>','part_id',$partid]);
           // CourseParts::updateAllCounters(["counter"=>-1],['>','part_id',$partid]);

            //if(CourseOutlineOrder::updateOutline("section",$partid))
              //  $transaction->commit();

            return $this->redirect($redirect);

        }

    }
    //Units
    public function actionNewunit($part_id){
        $model= new CourseLessons();

        if($model->load(Yii::$app->request->post())){
            $transaction=$model->getDb()->beginTransaction();
            $model->part_id=$part_id;

            $lc=CourseLessons::find()->where(['part_id'=>$part_id])->orderBy("lesson_id desc")->limit(1)->one();


            if($model->validate() && $model->save()){

                if($lc  == null)
                   $model->updateCounters(['counter'=>1]);
                else
                    $model->updateCounters(['counter'=>$lc->counter+1]);

                $lastOutlineCounter = CourseOutlineOrder::find()->where(['and',['section'=>$part_id],["courseid"=>$model->courseid]])->orderBy("id desc")->limit(1)->one();
                $outline = new CourseOutlineOrder;
                $outline->itemid=$model->getPrimaryKey();
                $outline->type="lesson";
                $outline->section=$part_id;
                $outline->courseid=$model->courseid;
                if($outline->save()){
                   if($lastOutlineCounter  == null){
                        $outline->updateCounters(['counter'=>1]);
                     }
                     else {
                         $outline->updateCounters(['counter'=>$lastOutlineCounter->counter+1]);
                                                 // $transaction->commit();
                    }
                    $model->outlineno=$outline->getPrimarykey();
                       if($model->save()){
                           $transaction->commit();
                           echo CourseLessons::getLesson($model,2,$model->outlineno);//2 id a junk value
                       }

               }


            }
            else{

                echo  "<div class='alert alert-danger'> An error occurred while performing operation. Please try again.</div>";
            }

        }


    }
    public function actionEditunit(){
        $unit_cont="";
        $unit = CourseLessons::find()->where(['lesson_id'=>Yii::$app->request->post("lesson_id")])->one();

        if($unit->load(Yii::$app->request->post()) && $unit->save()){
            $allUnit=CourseLessons::find()->where(['part_id'=>$unit->part_id])->orderBy("counter asc")->all();

               //foreach($allUnit as $row):
                    $unit_cont= CourseOutlineOrder::getSectionOutline($unit->part_id,$unit->courseid);
               //
               // endforeach;
                    echo $unit_cont;
        }

    }
    public function actionDeleteunit(){
        $unit_cont="";
        $lid=Yii::$app->request->post('lid');
        //if(Yii::$app->request->isPost){
            $delete = CourseLessons::findOne([$lid]);
            $transaction = $delete->getDb()->beginTransaction();
            $lpart =$delete->part_id;
            $courseid= $delete->courseid;
            if($delete->delete()){
                 $allUnit=CourseLessons::find()->where(['part_id'=>$lpart])->orderBy("counter asc")->all();
                 CourseLessons::updateAllCounters(["counter"=>-1],['and',['>','lesson_id',$lid],['=','part_id',$lpart]]);
                if(CourseOutlineOrder::updateOutline($delete->outlineno)){
                    $transaction->commit();
                   // foreach($allUnit as $row):
                        $unit_cont=CourseOutlineOrder::getSectionOutline($lpart,$courseid);

                   // endforeach;
                        echo $unit_cont;
                }


            }
            else
                 echo  "<div class='alert alert-danger'> An error occurred while performing operation. Please try again.</div>";
           // return $this->redirect($redirect);

        //}

    }
    //End Units

    //Start Unit contents( videos, ppt, pdf etc)


    public function actionUploadnewunitvideo(){

         $p1 = $p2 = [];
         $model=new LessonContent();
         $lesson=$_POST['lesson'];
         $thumbnail_url = Yii::getAlias("@aws_cc")."/coursevideos/thumbnails";
         $video=UploadedFile::getInstanceByName('unit_video');

            //store old banner if exist;
            if (empty($video)) {
                echo '{}';
                return;
             }

            $name=time();
            $key = $name.".".$video->extension;

            $transaction = $model->getDb()->beginTransaction();
            $model->content_editedname=$key;
            $model->content_defaultname=$video->baseName;
            $model->lesson_no =$lesson;
            $model->uploaded_by=Yii::$app->user->id;
            if(LessonContent::find()->where(['lesson_no'=>$lesson])->exists())
                $model->main="No";
            $model->content_format=$video->extension;
            $model->content_mime=$video->type;
            $model->content_basefoldername ="coursevideos";

            //$model->video_length=\frontend\models\Utility::convertToSeconds(LessonContent::getVideoLength($video->tempName));
            $model->video_length=LessonContent::getVideoLength($video->tempName);
            //check if video length is more than 20mins;

            if($model->video_length > 1260){
                echo json_encode(['error'=>"The video length is more than 20mins."]);
                Yii::$app->end();
            }

            $model->thumbnail="thumb_".$name.".png";
            $model->size=$video->size;

            if ($model->save()) {
		           $path = Yii::getAlias('@aws_cc').'/coursevideos/'.$key;

		           if($video->saveAs($path)){
                       LessonContent::createThumbnail($path,$thumbnail_url."/".$model->thumbnail);
                       $transaction->commit();
                   }
		           else{
                       echo json_encode(['error'=>"2An error occurred while uploading. Please try again."]);
                       Yii::$app->end();
                   }
		    }
		      else{
                     echo json_encode(["error"=>$model->video_length]);
                   //echo json_encode(['error'=>"1An error occurred while uploading. Please try again."]);
                   Yii::$app->end();

            }

          $p1 = "<img style='max-width:209px' src='/upload/coursevideos/thumbnails/$model->thumbnail' class='file-preview-other'>";
                //$p2 = ['caption' => "Animal-.jpg", 'width' => '120px', 'url' => $url, 'key' => $key];
            //}
            echo json_encode([
                'initialPreview' => [$p1],
                //'initialPreviewConfig' => $p2,
                'append' => true
             ]);
    }
    public function actionGetcontents(){
          echo LessonContent::getContents(Yii::$app->request->get("lessonid"));
    }
    public function actionUploadnewunitpresentation(){
         $p1 = $p2 = [];
         $model=new LessonContent();
         $lesson=$_POST['lesson'];
         $thumbnail_url='upload/coursepresentations/thumbnails/';
         $ppt=UploadedFile::getInstanceByName('powerpoint');

            //store old banner if exist;
            if (empty($ppt)) {
                echo '{}';
                return;
             }


            $key = time();
            $filename=$key.".pdf";

            $model->content_basefoldername ="coursepresentations";
            $transaction = $model->getDb()->beginTransaction();
            $model->content_editedname=$filename;
            $model->content_defaultname=$ppt->baseName;
            $model->lesson_no =$lesson;
            $model->no_of_pages=\frontend\models\Utility::getPDFPageCount($ppt->tempName);
            if(LessonContent::find()->where(['lesson_no'=>$lesson])->exists())
                $model->main="No";
            $model->content_format=$ppt->extension;
            $model->content_mime=$ppt->type;
            $model->uploaded_by=Yii::$app->user->id;
            $model->thumbnail="thumb_".$key.".jpg";
            $key=$key.".".$ppt->extension;
            $model->size=$ppt->size;

            if ($model->save()) {

		           $path = Yii::getAlias("@aws_cc").'/coursepresentations/'.$key;

		           if($ppt->saveAs($path)){

                       Utility::convert_to_pdf('upload/coursepresentations/'.$key,'upload/coursepresentations/'.$filename);
                       $model->no_of_pages=Utility::getPDFPageCount('upload/coursepresentations/'.$filename);
                       Utility::convertPdfPageToPng('upload/coursepresentations/'.$filename."[0]",$thumbnail_url.$model->thumbnail);
                        unlink('upload/coursepresentations/'.$key);
                       if ($model->save())
                            $transaction->commit();

                   }
		           else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                   }

		    }
		      else{
                  echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
         Yii::$app->end();
              }

               $p1 = "<img style='max-width:209px' src='".Yii::getAlias('@web').'/upload/coursepresentations/thumbnails/'.$model->thumbnail."' class='file-preview-other'>";
            //}
            echo json_encode([
                'initialPreview' => [$p1],
                'append' => true
             ]);
    }

    public function actionDownloadables(){
        $p1 = $p2 = [];
         $model=new LessonContent();
         $lesson=$_POST['lesson'];
         $da=UploadedFile::getInstanceByName('downloadables');

            //store old banner if exist;
            if (empty($da)) {
                echo '{}';
                return;
             }


            $key = time().".".$da->extension;

            $transaction = $model->getDb()->beginTransaction();
            $model->content_editedname=$key;
            $model->content_defaultname=$da->name;
            $model->lesson_no =$lesson;
            $model->main="downloadable";
            $model->content_format=$da->extension;
             $model->content_basefoldername ="courseotherdocs";
            $model->content_mime=$da->type;
            $model->uploaded_by=Yii::$app->user->id;
            $model->size=$da->size;
            if ($model->save()) {
		           $path = Yii::getAlias('@aws_cc').'/courseotherdocs/'.$key;
		           if($da->saveAs($path)){
                       $transaction->commit();

                   }
		           else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                   }

		    }
		      else{
                   echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);

              }

               //$p2 = ['caption' => "Animal-.jpg", 'width' => '120px', 'url' => $url, 'key' => $key];
            //}
            echo json_encode([
                //'initialPreviewConfig' => $p2,
                'append' => true
             ]);
    }
     public function actionTextcontent(){

        $model= new LessonContent;
        $model->lesson_no=Yii::$app->request->post('lesson_no');
        $model->main="text";

        $model->uploaded_by=Yii::$app->user->id;

        if($model->load(Yii::$app->request->post())){

            $model->content_format="text";
            $model->content_mime="Yes";

            if($model->save()){
                echo json_encode(array("status"=>1,"lessonid"=>$model->lesson_no));
            }
        }
    }
    public function actionExternallinks(){

        $model= new LessonContent;
        $model->lesson_no=Yii::$app->request->post('lesson_no');
        $model->main="Yes";
        $model->uploaded_by=Yii::$app->user->id;
        if($model->load(Yii::$app->request->post())){

            $model->content_format="link";
            $model->content_mime="link";
            if($model->save()){
               echo json_encode(array("status"=>1,"lessonid"=>$model->lesson_no));
            }
        }
    }

    public function actionDeleteunitcontent(){
            if(Yii::$app->request->post()){
                $delete = LessonContent::findOne(['content_no'=>Yii::$app->request->post('contentno')]);
                $transaction =$delete->getDb()->beginTransaction();
                $file =Yii::getAlias("@webroot").'/upload/'.$delete->content_basefoldername.'/'.$delete->content_editedname;
                if($delete->content_format !="text" && $delete->content_format !="link" ){
                    if(file_exists($file)){
                        unlink($file);

                        if(!empty($delete->video_thumbnail)){
                            if(file_exists(Yii::getAlias("@webroot")."/upload/coursevideos/thumbnails/".$delete->thumbnail)){
                                unlink(Yii::getAlias("@webroot")."/upload/coursevideos/thumbnails/".$delete->thumbnail);
                            }else if(file_exists(Yii::getAlias("@webroot")."/upload/coursepresentations/thumbnails/".$delete->thumbnail)){
                                unlink(Yii::getAlias("@webroot")."/upload/coursepresentations/thumbnails/".$delete->thumbnail);
                            }
                        }
                        $delete->delete();
                        $transaction->commit();
                    }
                }else{
                        $delete->delete();
                        $transaction->commit();
                }

            }
        //else

    }
    //End Unit contents

    //Begin Quuiz

    public function actionNewquiz(){
        $model= new Quiz;
        if($model->load(Yii::$app->request->post())){
            $lc=Quiz::find()->where(['partid'=>$model->partid])->orderBy("q_id desc")->limit(1)->one();
             $transaction =$model->getDb()->beginTransaction();
            if($model->save()){
                if($lc == null)
                    $model->updateCounters(['counter'=>1]);
                else
                    $model->updateCounters(['counter'=>$lc->counter+1]);

                 $lastOutlineCounter = CourseOutlineOrder::find()->where(["courseid"=>$model->courseid])->orderBy("id desc")->limit(1)->one();
                $outline = new CourseOutlineOrder;
                $outline->itemid=$model->getPrimaryKey();
                $outline->type="quiz";
                $outline->section=$model->partid;
                $outline->courseid=$model->courseid;
                if($outline->save()){
                   if($lastOutlineCounter  == null){
                        $outline->updateCounters(['counter'=>1]);
                   }
                    else {
                         $outline->updateCounters(['counter'=>$lastOutlineCounter->counter+1]);
                                                 // $transaction->commit();
                    }
                    $model->outlineno =$outline->getPrimaryKey();
                       if($model->save()){
                           $transaction->commit();
                          echo  Quiz::getQuiz($model,2,$model->outlineno);//2 id a junk value
                       }
                }

               }
            else
                echo "<div class='alert alert-danger'> An error occurred while  saving quiz. Please try again.</div>";
        }
    }

    public function actionEditquiz(){
        if(Yii::$app->request->isPost){
            $quizno= Yii::$app->request->post("quizno");
            $quizname= Yii::$app->request->post("quizname");
            $quizdesc= Yii::$app->request->post('quizdesc');
            $islimited =Yii::$app->request->post("islimited");
            $minutes =Yii::$app->request->post("minutes");
            $quizmodel= Quiz::find()->where(['q_id'=>$quizno])->one();
            $quizmodel->q_name=$quizname;
            $quizmodel->q_description=$quizdesc;
            $quizmodel->islimitedbytime=$islimited;
            $quizmodel->timelimit=$minutes;

            if($quizmodel->save()){
                echo json_encode(print_r($quizmodel));
            }
            else
                echo print_r($quizmodel->getErrors());
         }

    }
    public function actionDeletequiz(){
        if(Yii::$app->request->post()){
            $partid= Yii::$app->request->post("partid");
            $quizno=Yii::$app->request->post('quizno');
            $delete =Quiz::findOne(['q_id'=>$quizno]);
            $transaction = $delete->getDb()->beginTransaction();
            $quiz_cont='';
            $courseid=$delete->courseid;
            $outlineno= $delete->outlineno;
            if($delete->delete()){

                Quiz::updateAllCounters(["counter"=>-1],['and',['>','q_id',$quizno],['=','partid',$partid]]);
                $allQuiz=Quiz::find()->where(['partid'=>$partid])->orderBy("counter asc")->all();

                if(CourseOutlineOrder::updateOutline($outlineno)){
                    $transaction->commit();
                         $quiz_cont=CourseOutlineOrder::getSectionOutline($partid,$courseid);

                        echo $quiz_cont;
                }

            }
            else{
                echo "error";
            }

        }

    }

    public function actionNewquestion(){
    $question = new Question;
       // print_r(Yii::$app->request->post());
       // Yii::$app->end();
        if($question->load(Yii::$app->request->post())){
            $question->quiz_id=Yii::$app->request->post("quizid");
            $lc=Question::find()->where(['quiz_id'=>$question->quiz_id])->orderBy("question_id desc")->limit(1)->one();

            if($question->save()){
                if($lc == null)
                    $question->updateCounters(['counter'=>1]);
                else
                    $question->updateCounters(['counter'=>$lc->counter+1]);
                switch ($question->type){
                    case 1:    $answer = Yii::$app->request->post("answer-field-multiple");
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-multiple")[$i];
                                for($j=0; $j < count(Yii::$app->request->post("checkedanswers")); $j++){
                                    if(Yii::$app->request->post("checkedanswers")[$j] == $i){
                                        $ans->iscorrect= "true";
                                    }
                                }
                                $ans->save();
                             }
                             break;
                    case 2:  $answer = Yii::$app->request->post("answer-field-radio");
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-single")[$i];
                                if(Yii::$app->request->post("radioselected") == $i){
                                        $ans->iscorrect= "true";
                                }

                                $ans->save();
                             }
                             break;
                    case 3:  $answer = array('true','false');

                               for($i=0;$i<count($answer); $i++){

                                    $ans = new Answers;
                                    $ans->questionid = $question->getPrimaryKey();
                                    $ans->answer = $answer[$i];
                                    $ans->reason =Yii::$app->request->post("reason-truefalse")[$i];
                                    if(Yii::$app->request->post("optradio") == $answer[$i]){
                                            $ans->iscorrect= "true";
                                    }

                                    $ans->save();
                             }
                             break;
                }
                echo Question::getQuestions($question->quiz_id);
            }
            else
                echo "error";

        }
    }

    public function actionDeletequestion(){
        $question_cont="";
        $questionid=Yii::$app->request->post("questionid");
        $quizid=Yii::$app->request->post("quizid");

        if(Yii::$app->request->isPost){

            $delete = Question::findOne(['question_id'=>$questionid]);

            if($delete->delete()){

                Question::updateAllCounters(["counter"=>-1],['and',['>','question_id',$questionid],['=','quiz_id',$quizid]]);
                $allQuestion=Question::find()->where(['quiz_id'=>$quizid])->orderBy("counter asc")->all();
                foreach($allQuestion as $row):
                    $question_cont.= Question::getQuestions($row->quiz_id);

                endforeach;
                    echo $question_cont;
            }
            else
                echo "error";
        }
    }
    public function actionGetquestion($id){
        $question = \yii\helpers\ArrayHelper::toArray(Question::findOne(['question_id'=>$id]));
        $answer = \yii\helpers\ArrayHelper::toArray(Answers::find()->where(['questionid'=>$id])->all());
        $merged = \yii\helpers\ArrayHelper::merge($question,$answer);
        echo \yii\helpers\Json::encode($merged);

    }
    public function actionUpdatequestion(){
        $question = Question::find()->where(['question_id'=>Yii::$app->request->post("questionid")])->one();

        if($question->load(Yii::$app->request->post())){

            if($question->save()){

                switch ($question->type){
                    case 1:    $answer = Yii::$app->request->post("answer-field-multiple");
                               Answers::deleteAll(['questionid'=>$question->question_id]);
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-multiple")[$i];
                                for($j=0; $j < count(Yii::$app->request->post("checkedanswers")); $j++){
                                    if(Yii::$app->request->post("checkedanswers")[$j] == $i){
                                        $ans->iscorrect= "true";
                                    }
                                }
                                $ans->save();
                             }
                             break;
                    case 2:  $answer = Yii::$app->request->post("answer-field-radio");
                              Answers::deleteAll(['questionid'=>$question->question_id]);
                               for($i=0;$i<count($answer); $i++){

                                $ans = new Answers;
                                $ans->questionid = $question->getPrimaryKey();
                                $ans->answer = $answer[$i];
                                $ans->reason =Yii::$app->request->post("reason-single")[$i];
                                if(Yii::$app->request->post("radioselected") == $i){
                                        $ans->iscorrect= "true";
                                }

                                $ans->save();
                             }
                             break;
                    case 3:  $answer = array('true','false');

                                Answers::deleteAll(['questionid'=>$question->question_id]);

                               for($i=0;$i<count($answer); $i++){

                                    $ans = new Answers;
                                    $ans->questionid = $question->getPrimaryKey();
                                    $ans->answer = $answer[$i];
                                    $ans->reason =Yii::$app->request->post("reason-truefalse")[$i];
                                    if(Yii::$app->request->post("optradio") == $answer[$i]){
                                            $ans->iscorrect= "true";
                                    }

                                    $ans->save();
                             }
                             break;
                }
                echo Question::getQuestions($question->quiz_id);
            }
            else
                echo "error";
        }
    }

    //End Quiz





    public function actionDeletebanner(){
        if(Yii::$app->request->post()){
            $model= Course::find()->where(['course_id'=>$_POST['courseid']])->one();

            $path = Yii::getAlias('@aws_cc').'/coursebanners/'.$model->course_banner;
            $transaction = $model->getDb()->beginTransaction();
            $oldbanner=$model->course_banner;
            $model->course_banner="";
		           if($model->save()){
                       if(file_exists($oldbanner) && !empty($oldbanner))
                           unlink($oldbanner);
                       $transaction->commit();
                   }
		           else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                   }
            $key=122;
            // $p1 = "<img src='http://localhost/teachsity/frontend/assets/coursebanners/".$key."' class='file-preview-image'>";
             echo json_encode([
               // 'initialPreview' => [$p1],
                'append' => true
             ]);
        }
    }
    public function actionCoursebanner(){
            $p1 = $p2 = [];
            $model=Course::find()->where(['course_id'=>$_POST['courseid']])->one();

            $course_banner=UploadedFile::getInstanceByName('course_banner');
            //store old banner if exist;
            $oldbanner= $model->course_banner;
            $oldbanner_link=Yii::getAlias('@aws_cc').'/coursebanners/'.$oldbanner;
            if (empty($course_banner)) {
                echo '{}';
                return;
             }

            $key = time().".".$course_banner->extension;

               $transaction = $model->getDb()->beginTransaction();
               $model->course_banner=$key;
               if ($model->update()) {

		            $path = Yii::getAlias('@aws_cc').'/coursebanners/'.$key;

		           if($course_banner->saveAs($path)){

                       if(file_exists($oldbanner_link) && !empty($oldbanner))
                           unlink($oldbanner_link);
                       $transaction->commit();
                   }
		           else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                       Yii::$app->end();
                   }

		       }


              //  $url=Url::base()."/course/deletebanner?id=".$course_id;
               // $p1 = "<img src='http://localhost/teachsity/frontend/assets/coursebanners/".$key."' class='file-preview-image'>";
              //  $p2 = ['caption' => "Animal-.jpg", 'width' => '120px', 'url' => $url, 'key' => $key];
            //}
            echo json_encode([
               // 'initialPreview' => [$p1],
               //'initialPreviewConfig' => $p2,
                'append' => false
             ]);
    }
    public function actionPromovideo(){


            $p1 = $p2 = [];
           $model=Course::find()->where(['course_id'=>$_POST['courseid']])->one();
           $thumbnail_url=Yii::getAlias('@aws_cc').'/promovideos/thumbnails';
            $promovideo=UploadedFile::getInstanceByName('promo_video');

            //store old banner if exist;
            $oldvideo= $model->promo_video;
            $oldvideo_link=Yii::getAlias('@aws_cc').'/promovideos/'.$oldvideo;
            $oldthumbnail=$model->promo_video_thumbnail;
        $oldthumbnail_link =Yii::getAlias('@aws_cc').'/promovideos/thumbnails/'.$model->promo_video_thumbnail;
            if (empty($promovideo)) {
                echo '{}';
                return;
             }

            $key = time().".".$promovideo->extension;

               $transaction = $model->getDb()->beginTransaction();
               $model->promo_video=$key;
               $model->promo_video_mime=$promovideo->type;
               $model->promo_video_length=LessonContent::getVideoLength($promovideo->tempName);
               $model->promo_video_thumbnail= "thumb_".time().".png";
               if ($model->update()) {

		           $path = Yii::getAlias('@aws_cc').'/promovideos/'.$key;
		           if($promovideo->saveAs($path)){
                       LessonContent::createThumbnail($path,$thumbnail_url."/".$model->promo_video_thumbnail);
                      // LessonContent::createThumbnail($path,Yii::getAlias("@webroot")."/upload/teste.png");
                       if(file_exists($oldvideo_link) && !empty($oldvideo))
                           unlink($oldvideo_link);
                       if(file_exists($oldthumbnail_link) && !empty($oldthumbnail))
                           unlink($oldthumbnail_link);

                       $transaction->commit();


                   }
		           else{
                       echo json_encode(['error'=>"An error occurred while uploading. Please try again."]);
                   }

		       }


            $p1 = "<img src='".Yii::getAlias("@web")."/upload/promovideos/thumbnails/".$model->promo_video_thumbnail."' class='file-preview-other'>";
                //$p2 = ['caption' => "Animal-.jpg", 'width' => '120px', 'url' => $url, 'key' => $key];
            //}
            echo json_encode([
               'initialPreview' => [$p1],
                //'initialPreviewConfig' => $p2,
                'append' => true
             ]);
    }
    public function actionSavename($id,$newtitle){
        $course=Course::find()->where(["course_id"=>$id])->one();
        $course->course_title=$newtitle;
       /// $course->scenario="edit_title";
        if($course->save())
            return $this->redirect(array("basicinfo",'slug'=>$course->slug));
        else{
            print_r ($course->getErrors());
        }



    }
    public function actionSettings($slug){
         $course= Course::find()->where(["slug"=>$slug])->one();
         $courseid = $course->course_id;
         $setting= CourseSettings::find()->where(["courseid"=>$courseid])->one();
         if($setting == null)
             $setting= new CourseSettings;

         $coupon= new CourseCoupons;

        //saving form
       // print_r(Yii::$app->request->post());

        if($setting->load(Yii::$app->request->post())){
            $setting->courseid=$courseid;
            if($setting->save()){
                Yii::$app->session->setFlash("success","Course settings was updated successfully");
                return $this->redirect(array("settings",'slug'=>$course->slug));
            }
        }
        if($coupon->load(Yii::$app->request->post())){
            $coupon->courseid=$courseid;
            if($course->amount!=""){
                if($coupon->save()){
                    Yii::$app->session->setFlash("success","Operation was done successfully");
                    return $this->redirect(array("settings",'slug'=>$course->slug));
                }
            }
            else{
                    Yii::$app->session->setFlash("error","You cannot create coupon for a free course");
                    return $this->redirect(array("settings",'slug'=>$course->slug));
            }

        }
         return $this->render("settings",array("course"=>$course,"setting"=>$setting,"coupon"=>$coupon));
    }

    public function actionDeletecoupon($courseid,$redirect){
        if(Yii::$app->request->post()){
            if(CourseCoupons::findOne(['id'=>$courseid])->delete()){
                $this->redirect($redirect);
                Yii::$app->session->setFlash("success","Operation was done successfully");
            }

        }
    }
    public function actionPreview($slug){
        $course= Course::find()->where(["slug"=>$slug])->one();
        $course_ad=Course::getCourseAd($course->course_id);
        return $this->render("preview",array("course"=>$course,"coursead"=>$course_ad));
    }
    public function actionStudentreport($slug){
        $course= Course::find()->where(["slug"=>$slug])->one();
        return $this->render('studentreport',array("course"=>$course));
    }

    public function actionSubmitforreview(){
        if(Yii::$app->request->isPost){
             $model = Course::find()->where(['course_id'=>Yii::$app->request->post("courseid")])->one();
            $model->status=3;
            $model->save();
             return $this->render("success",array("slug"=>$model->slug));
        }

    }
    public function actionPublish($slug){
        $course= Course::find()->where(["slug"=>$slug])->one();
        if($course->status==4){
            $course->status =1;
            $course->save();
            Yii::$app->session->setFlash("success","Your course has been published");
            return $this->redirect(array("settings",'slug'=>$course->slug));
        }
        else{
            Yii::$app->session->setFlash("error","You cannot publish this course. Please submit this course for review.");
            return $this->redirect(array("settings",'slug'=>$course->slug));
        }
    }
     public function actionUnpublish($courseid){
        $course= Course::find()->where(["course_id"=>$courseid])->one();
        if($course->status==1){
            $course->status =2;
            $course->save();
            Yii::$app->session->setFlash("success","Your course has been published");
            return $this->redirect(array("settings",'slug'=>$course->slug));
        }
        else{
            Yii::$app->session->setFlash("error","An error occurred while performing the last operation.");
            return $this->redirect(array("settings",'slug'=>$course->slug));
        }
    }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Course::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSubscribe($slug){
       $course= Course::findOne(['slug'=>$slug]);
        $courseid=$course->course_id;
       if(!\frontend\models\Enrollment::find()->where(['courseid'=>$courseid,"userid"=>Yii::$app->user->id])->exists()){
           $enroll = new \frontend\models\Enrollment;
           $enroll->courseid=$courseid;
           $enroll->userid=Yii::$app->user->id;

           if($enroll->save()){
               Yii::$app->session->setFlash("success","success");
               return $this->render("subscribe",array("course"=>$course));
           }
            else{
                Yii::$app->session->setFlash("error","An error occurred while trying to enroll you for this course.");
                return $this->render("subscribe",array("course"=>$course));
            }
       }
        else{
                //Yii::$app->session->setFlash("error","You have already enrolled for this course.");
                return $this->redirect(Url::toRoute(["learn","slug"=>$course->slug]));
            }


    }

    //Course Dashboard
    public function actionLearn($slug){
        $course = Course::find()->where(["slug"=>$slug])->one();
        $courseid=$course->course_id;
        $userid=Yii::$app->user->id;
        $flag =new \frontend\models\FlagAbuse;
        $discussion= new \frontend\models\Discussions;
         if(\frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->exists())
              $rating =  \frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->one();
        else
            $rating = new \frontend\models\CourseReviews;
        $progress = Course::getCurrentProgress($course->course_id,$userid);

        return $this->render("learn",array("progress"=>$progress,"course"=>$course,'rating'=>$rating,'flag'=>$flag,'discussion'=>$discussion));
    }
    public function actionSavereview(){
        $userid= Yii::$app->request->get("CourseReviews")['userid'];
        $courseid= Yii::$app->request->get("CourseReviews")['courseid'];

        if(\frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->exists())
              $review = \frontend\models\CourseReviews::find()->where(['and',['userid'=>$userid],['courseid'=>$courseid]])->one();
        else
            $review = new \frontend\models\CourseReviews;

        if($review->load(Yii::$app->request->get())){

            if($review->save()){
                echo 1;
            }
            else
                echo 0;
        }
    }
    public function actionFlagabuse(){
        $userid= Yii::$app->request->get("FlagAbuse")['userid'];
        $courseid= Yii::$app->request->get("FlagAbuse")['courseid'];
         $flag =new \frontend\models\FlagAbuse;


         if($flag->load(Yii::$app->request->get())){
             if($flag->save())
                 echo 1;
             else
                 echo 0;
         }


    }
    public function actionSavediscussion(){
        $userid= Yii::$app->request->post("Discussions")['userid'];
        $courseid= Yii::$app->request->post("Discussions")['courseid'];

        $discussion = new \frontend\models\Discussions;

        if($discussion->load(Yii::$app->request->post())){

            if($discussion->save()){
                echo $discussion->getPrimaryKey();
            }
            else
                echo 0;
        }
    }
    public function actionGetdiscussions(){
        $d=  \frontend\models\Discussions::findOne([Yii::$app->request->get("discussionid")]);
        echo \frontend\models\Discussions::designDiscussion($d);
    }

    public function actionSavereply(){
        $reply =new  \frontend\models\DiscussionReplies;
        if($reply->load(Yii::$app->request->get())){
            if($reply->save())
                echo $reply->getPrimaryKey();
            else
                echo 0;
        }
    }
    public function actionGetlastreply(){
        $d=   \frontend\models\DiscussionReplies::findOne([Yii::$app->request->get("replyid")]);
        echo  \frontend\models\Discussions::designReply($d);
    }
    public function actionSaveannouncement(){
        $a = new \frontend\models\CourseAnnouncement;
        if($a->load(Yii::$app->request->post())){
            if($a->save()){
                Yii::$app->session->setFlash("success"," You have successfully made an announcement");
                return  $this->redirect(Yii::$app->request->post("returnUrl"));
            }
            else{
                Yii::$app->session->setFlash("error"," We were unable to complete your request at this time.");
                return  $this->redirect(Yii::$app->request->post("returnUrl"));
            }
        }
    }

    public function actionLearning($lessonid,$courseid,$isquiz=false){
        $type="video";
        $this->layout="classroom";
        $course=Course::findOne([$courseid]);
        $lesson = CourseLessons::findOne([$lessonid]);
        $content = CourseLessons::getUnitMainContent($lessonid);

        if(strpos(strtolower($content->content_mime),"video")===false){
            return $this->redirect(Yii::getAlias("@web")."/js/library/pdfjs/web/viewer.html?file=".Yii::getAlias("@web")."/upload/coursepresentations/".$content->content_editedname);
        }



        return $this->render("learning",array("course"=>$course,"lesson"=>$lesson,'content'=>$content,"type"=>$type));
    }
    public function actionClassroom($slug){

        $this->layout="classroom";
        $discussion= new \frontend\models\Discussions;
        $course=Course::find()->where(["slug"=>$slug])->one();
        $courseid = Yii::$app->request->post("courseid");

        $note = \frontend\models\UserNotebook::findOne([Yii::$app->user->id]);
        if($note==null)
            $note="Start writing here";
        else
            $note=$note->note;

        return $this->render("classroom",array("course"=>$course,'discussion'=>$discussion,'note'=>$note));
    }

    public function actionClassroomnavigation(){
        $itemid= Yii::$app->request->get("itemid");
        $type= Yii::$app->request->get("type");
        $prev_array=array();
        $next_array=array();
        if($type=="lesson"){
            $lesson = CourseLessons::findOne([$itemid]);
            $courseid=$lesson->courseid;
            $outlineno=$lesson->outlineno;

        }
        else{
            $quiz = Quiz::findOne([$itemid]);
            $courseid=$quiz->courseid;
            $outlineno=$quiz->outlineno;
        }


        $outline_model = CourseOutlineOrder::findOne([$outlineno]);

        $current['itemid'] =$itemid;
        $current['type']=$type;
        $current['section']=$outline_model->section;

        $max_outline = CourseOutlineOrder::find()->where(['and',["section"=>$outline_model->section],["courseid"=>$courseid]])->orderBy("counter desc")->one();
        $section_model= CourseParts::findOne([$outline_model->section]);
        $max_section_counter =CourseParts::find()->where(['course_id'=>$courseid])->orderBy("counter desc")->limit(1)->one();
        //checking previous

        if($outline_model->counter-1 !=0)
        {
            $prev = CourseOutlineOrder::find()->where("section=$outline_model->section and courseid =$courseid and counter < $outline_model->counter")->orderBy("counter desc")->one();
            $prev_array["itemid"]=$prev->itemid;
            $prev_array["type"]=$prev->type;
            $prev_array["section"]=$outline_model->section;
        }
        else{
            if($section_model->counter !=1){

                $s = CourseParts::find()->where("course_id =$courseid and counter < $section_model->counter")->orderBy("counter desc")->limit(1)->one();
              //  echo $s->part_id;
                $prev = CourseOutlineOrder::find()->where("section=".$s->part_id ." and courseid =$courseid ")->orderBy("counter desc")->one();
                $prev_array["itemid"]=$prev->itemid;
                $prev_array["type"]=$prev->type;
                $prev_array["section"]=$s->part_id;
            }
        }


        // Get next

        if($outline_model->counter != $max_outline->counter)
        {
            $next = CourseOutlineOrder::find()->where("section=$outline_model->section and courseid =$courseid and counter > $outline_model->counter")->orderBy("counter asc")->one();
            $next_array["itemid"]=$next->itemid;
            $next_array["type"]=$next->type;
            $next_array["section"]=$outline_model->section;
        }else{
            if($section_model->counter != $max_section_counter->counter){

                $s = CourseParts::find()->where("course_id =$courseid and counter > $section_model->counter")->orderBy("counter asc")->limit(1)->one();
                $next = CourseOutlineOrder::find()->where("section=$s->part_id and courseid =$courseid and counter = 1 ")->orderBy("counter asc")->one();
               // print_r($next);
              //  Yii::$app->end();
                $next_array["itemid"]=$next->itemid;
                $next_array["type"]=$next->type;
                $next_array["section"]=$s->part_id;
            }
        }
        $navigation["prev"]=$prev_array;
        $navigation["next"]=$next_array;
        $navigation["current"]=$current;
        echo json_encode($navigation);

    }
    public function actionSaveusernote(){
        $note = new \frontend\models\UserNotebook;
        $note->note = Yii::$app->request->get("note");
        $note->userid= Yii::$app->request->get("userid");
        if($note->save())
            echo 1;
        else
            echo 0;
    }
    public function actionQuizintro(){

        $this->layout="classroom";

        $quiz = Quiz::findOne([Yii::$app->request->get("quiz")]);
        $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quiz->q_id,"userid"=>Yii::$app->user->id]);
        if($timer->exists())
            $timelimit = $timer->one()->time;
        else{
            $timer =new \frontend\models\Quiztimer;
            $timer->quizid=$quiz->q_id;
            $timer->userid =Yii::$app->user->id;
            $timer->time=$quiz->timelimit;
            $timelimit=$quiz->timelimit;
            $timer->save();
        }
  return $this->render("quizintro",array("quiz"=>$quiz,"timelimit"=>$timelimit));
    }
    public function actionSavetimer(){
        $seconds = Yii::$app->request->get("seconds");
        $minutes = Yii::$app->request->get("minutes");
        $quizid = Yii::$app->request->get("quizid");
        $userid = Yii::$app->request->get("userid");
        $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quizid,"userid"=>$userid])->one();

        if(Yii::$app->request->get("delete")=="true"){
            if($timer->delete())
                echo 1;
        }
        else{
            $time=($minutes * 60) + $seconds;
            $timer->time=$time;
            $timer->save();
            echo $time;
        }

    }
    public function actionQuizquestion(){
        $question_array=array();
        $answer_array=array();
        $quizid=Yii::$app->request->get("quizid");
        $counter = Yii::$app->request->get("counter");
        if(Yii::$app->request->get("resume")==true){
              if(Yii::$app->request->cookies->has("quizid")){
                   $cookies=  Yii::$app->request->cookies;
                   $quizid= $cookies->getValue("quizid");
                   $counter = $cookies->getValue("counter");

              }else{
                echo 0;
                Yii::$app->end();
              }

         }
        $quiz = Quiz::findOne([$quizid]);
        $question= Question::find()->where(['quiz_id'=>$quiz->q_id,'counter'=>$counter])->orderBy("counter asc")->limit(1)->one();
        $question_array['description']=$question->description;
        $question_array["questionid"]=$question->question_id;
        $question_array['title']=$question->title;
        $question_array['type']=$question->type;
        if($question->type==1){
             $question_array["multiple"]="multiple";
        }
        else if($question->type==2){
             $question_array["single"]="single";
        }
        else {
              $question_array["truefalse"]="truefalse";
        }

        $question_array['counter']=$question->counter;
        $a =Answers::find()->where(["questionid"=>$question->question_id])->all();
        if($a!=null){
                    foreach($a as $answer):
                       $a_array["id"]=$answer->id;
                       $a_array["reason"]=$answer->reason;
                       $a_array["answer"]=$answer->answer;
                       $answer_array[]=$a_array;
                   endforeach;
        }
         //select the last question;
        $lq = Question::find()->where(['quiz_id'=>$quiz->q_id])->orderBy("counter desc")->limit(1)->one();
        $lastquestion =$lq->counter;
        //check if question is the last.
        if($counter == $lastquestion)
          $question_array['last']=true;
        $question_array['answers']=$answer_array;

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            "name"=>"quizid",
            "value"=>$quiz->q_id
          ]));
        $cookies->add(new \yii\web\Cookie([
              "name"=>"counter",
              "value"=>$question->counter
            ]));
        $cookies->add(new \yii\web\Cookie([
                "name"=>"question",
                "value"=>$question->question_id
        ]));

        echo json_encode($question_array);


    }

    public function actionSaveanswers(){
      Quiz::setSavedAnswers(Yii::$app->request,Yii::$app->request->get("userid"));
       //echo $answers;
    }
    public function actionGetsavedanswers($quizid){
       $answers = Quiz::getSavedAnswers(Yii::$app->request->get("userid"),$quizid);
       echo $answers;

    }
    public function actionFinishquiz(){
      $correctans =0;
      $flag=false;
      $failed =0;
      $quizid = Yii::$app->request->get("quiz");
      $userid = Yii::$app->request->get("userid");
      $answers = Quiz::getSavedAnswers($userid,$quizid);
      $totalquestion = Question::find()->where(["quiz_id"=>$quizid])->orderBy("counter desc")->limit(1)->one();
      $quizmodel=Quiz::findOne([$quizid]);
      $answers= json_decode($answers,true);

      if(count($answers["answers"])!=0){
        foreach($answers["answers"] as $p=>$q):
        // echo json_encode($q);
        //print_r($q);
          $question= Question::findOne([$q["question"]]);
          $r["question"]=$question->title;
          $r["counter"]=$question->counter;
          if($q['type']==1){
            //check is the users answer is more than thec correct ans
            if( !$a=Answers::find()->where(["and",['questionid'=>$q["question"]],["iscorrect"=>"true"]])->count() < count($q['answer'])){
              $a=Answers::find()->where(["and",['questionid'=>$q["question"]],["iscorrect"=>"true"]])->all();
              //check if the amount of checked answered matches with the user selection;
              foreach ($a as $key) {
                if(!in_array($key->id,$q['answer'])){
                  $r["iscorrect"]="false";
                  $r['class']="icon-err md-close-2";
                  break;
                }
                else{
                  $r["iscorrect"]="true";
                  $r['class']="icon-val md-check-2";
                  $flag=true;
                }
              }
              if($flag)
                ++$correctans;
              else
                ++$failed;
            }else{
              $r["iscorrect"]="false";
              $r['class']="icon-err md-close-2";
              ++$failed;
            }
          }
          else{
            $a=Answers::find()->where(["and",["id"=>$q['answer'][0]],["iscorrect"=>"true"]])->one();
          //  echo $a->answer."<br/>";
            if($a!=null){
                  $r["iscorrect"]="true";
                  $r['class']="icon-val md-check-2";
                  ++$correctans;
              }
              else{
                $r["iscorrect"]="false";
                $r['class']="icon-err md-close-2";
                ++$failed;
              }
          }
          $result["answers"][]=$r;
        endforeach;
      }else {
        $failedquestion = Question::find()->where(["quiz_id"=>$quizid])->all();
        foreach($failedquestion as $qu):
          $r["question"]=$qu->title;
          $r["counter"]=$qu->counter;
          $r["iscorrect"]="false";
          $r['class']="icon-err md-close-2";
          $result["answers"][]=$r;
        endforeach;
        $correctans=0;
        $failed=0;


      }

      $result["correct"]=$correctans;
      $result["failed"]=$failed;
      $result["total"]=$totalquestion->counter;
      echo json_encode($result);

      ///Do some house cleaning
      $cookies =Yii::$app->response->cookies;
      $cookies->remove("quizid");
      $cookies->remove("counter");
      $cookies->remove("question");
      if(!\frontend\models\CompletedQuiz::find()->where(["userid"=>$userid,"quizid"=>$quizid])->exists()){
        $complete= new \frontend\models\CompletedQuiz;
        $transaction = $complete->getDb()->beginTransaction();
        $complete->userid=$userid;
        $complete->quizid =$quizid;
        $complete->score=$correctans;
        $complete->save();
      }

      if(!\frontend\models\CourseLearnTracking::find()->where(["userid"=>$userid,"courseid"=>$quizmodel->courseid,"section"=>$quizmodel->partid,"lesson"=>$quizid,"lesson_type"=>"quiz"])->exists()){
        $track =new \frontend\models\CourseLearnTracking;
        $track->userid=$userid;
        $track->courseid=$quizmodel->courseid;
        $track->section=$quizmodel->partid;
        $track->lesson=$quizid;
        $track->lesson_type="quiz";
        $track->status="Completed";
        if($track->save())
                $transaction->commit();
      }


      if( \frontend\models\QuizselectedAns::find()->where(["and",["quizid"=>$quizid],["userid"=>$userid]])->exists())
          \frontend\models\QuizselectedAns::find()->where(["and",["quizid"=>$quizid],["userid"=>$userid]])->one()->delete();
      $timer =\frontend\models\Quiztimer::find()->where(["quizid"=>$quizid,"userid"=>$userid]);
      if($timer->exists()){
        $timer->one()->delete();
      }

    }

    public function actionLessoncomplete(){
      $userid=Yii::$app->request->get("userid");
      $courseid=Yii::$app->request->get("courseid");
      $section=Yii::$app->request->get("sectionid");
      $lesson =Yii::$app->request->get("lessonid");
      $type=Yii::$app->request->get("type");
      $status =Yii::$app->request->get("status");

      if(!\frontend\models\CourseLearnTracking::find()->where(["userid"=>$userid,"courseid"=>$courseid,"section"=>$section,"lesson"=>$lesson,"lesson_type"=>$type])->exists()){
        $track =new \frontend\models\CourseLearnTracking;
        $track->userid=$userid;
        $track->courseid=$courseid;
        $track->section=$section;
        $track->lesson=$lesson;
        $track->lesson_type=$type;
        $track->status=$status;
        if($track->save()){
          echo 1;
        }
        else {
            echo 0;
        }
      }
      else {
          echo "already exist";
        }
    }
    /**
     * Displays a single Course model.
     * @param string $id
     * @return mixed
     */
    public function actionSectionorder(){
        if(Yii::$app->request->isPost){
            $counter=0;
           foreach(Yii::$app->request->post("neworder") as $order):
                $s= CourseParts::findOne([$order['sectionid']]);
                $s->counter=++$counter;
                $s->save();

            endforeach;
        }

    }
    public function actionSectionitemsorder(){
        if(Yii::$app->request->isPost){
            $counter=0;
           foreach(Yii::$app->request->post("neworder") as $order):
                $s= CourseOutlineOrder::findOne([$order['outlineno']]);
               // $transaction = $s->getDb()->beginTransaction();
                $s->counter=++$counter;
                if(Yii::$app->request->post('swapsection')==1)
                    $s->section = Yii::$app->request->post('sectionid');
                $s->save();
                if($s->type=="lesson"){
                    $l=CourseLessons::findOne([$s->itemid]);
                   $l->part_id=Yii::$app->request->post('sectionid');
                    $l->save();
                         // $transaction.commit();

                }
                /*else{
                    $q=Quiz::findOne([$s->itemid]);
                    $q->partid=$order['sectionid'];
                    if($q->save())
                        $transaction->commit();
                }*/
            endforeach;
        }
    }
    public function actionCoursecategory($category){
      $category =str_replace("-"," ",str_replace("&","and",strtolower($category)));
      $category = Utility::cleanString(\yii\helpers\HtmlPurifier::process($category));

        if(trim($category)=="")
            $category ="Teachnology";
      $category = CourseCategory::find()->where(["category"=>$category])->one()->id;
      $result = Course::doSearch("category",$category);
      $model = $result["model"];
      $search_term =$result["search_term"];
      $pages =$result["pages"];

      $savequery = new \frontend\models\TcSearchqueries;
      $savequery->query =$category;
      $savequery->page =Yii::$app->request->get("page");
      $savequery->save();

      return $this->render("search",["rows"=>$model,"search_term"=>$search_term,"pages"=>$pages]);

        //echo Url::toRoute(["/course/coursecategory","slug"=>"adobe-photoshop-tool"]);
    }
    public function actionSearch(){

        $search_query = \yii\helpers\HtmlPurifier::process(Yii::$app->request->get("q"));

        $search_query = Utility::cleanString($search_query);

        if(trim($search_query)=="")
            $search_query="web";

        $result = Course::doSearch("search",$search_query);
        $model = $result["model"];
        $search_term =$result["search_term"];
        $pages =$result["pages"];

        $savequery = new \frontend\models\TcSearchqueries;
        $savequery->query =$search_query;
        $savequery->page =Yii::$app->request->get("page");
        $savequery->save();

         return $this->render("search",["rows"=>$model,"search_term"=>$search_term,"pages"=>$pages]);
    }
    public function actionAutosuggestjson($q){


        $query = new \yii\sphinx\Query;
        $query->from('searchqueries')
        ->match(new \yii\db\Expression(':match', ['match' => '@(query) ' . Yii::$app->sphinx->escapeMatchValue($q)]))
        ->limit(10);
       // build and execute the query
        $command = $query->createCommand();
        // $command->sql returns the actual SQL
        $data = $command->queryAll();
        /*$query = new  \yii\db\Query();

        $query->select('query')
            ->from('tc_searchqueries')
            ->where(['LIKE',"query",$q])
            ->orderBy('query');
            $command = $query->createCommand();
        $data = $command->queryAll();*/
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['query']];
        }
        echo \yii\helpers\Json::encode($out);
    }
    public function actionGetsubcategory($category){
        $sub=CourseCategory::getSubCat($category);
        echo \yii\helpers\Json::encode($sub);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->course_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            if($this->findModel($id)->delete())
                Yii::$app->session->setFlash("success","Operation was performed successfully");
            else
                Yii::$app->session->setFlash("error","An error occurred while performing the operation. Please try again");

            return $this->redirect(Url::base().'/tutors/dashboard');


    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
