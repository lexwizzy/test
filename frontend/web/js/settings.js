$("#create-coupon-btn").click(function(){
    $("#couponform").removeClass("hidden").addClass("show")
})

$("#privacy").change(function(){
    if($(this).val()=="public"){
        $("#pwddiv").removeClass("show").addClass("hidden")
        $("#pwdlbl").removeClass("show").addClass("hidden")
    }   
    else
        {
            $("#pwddiv").removeClass("hidden").addClass("show")
            $("#pwdlbl").removeClass("hidden").addClass("show")
        }
})

$(document).ready(function(){
    $("#privacy").trigger("change")
})


$("#delete-course").click(function(){
    answer = confirm("Are you sure you want to delete thise course?")
    
    if(answer==1){
        courseid = courseid;
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        $.ajax({
            url:'delete?id='+courseid,
            type:'post',
            data:{"_csrf": yii.getCsrfToken()},
            success:function(){
                
            }
        })
    }
})
if ($('.popup-with-zoom-anim').length) {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',

            fixedContentPos: false,
            fixedBgPos: true,

            overflowY: 'auto',

            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });
        $('.design-course-popup').delegate('.cancel', 'click', function(evt) {
            evt.preventDefault();
            $('.mfp-close').trigger('click');
        });
    }