

//edit course title pencil icon
jQuery("#course-name").click(function(){
    jQuery("#course-name-div").removeClass("hidden").addClass("show");
    jQuery("#course-title").addClass("hidden")
})
//edit course title cancel button
jQuery("#cancel-course-name").click(function(){
    jQuery("#course-name-div").removeClass("show").addClass("hidden");
    jQuery("#course-title").removeClass("hidden").addClass("show")
})


function savecoursetitle(button,title,courseid){
  $.ajax({
    url:"/course/savename",
    data:{"id":courseid,'newtitle':title},
    beforeSend:function(){
        //button.parents("#course-name-div").find(".fa-circle-o-notch").css("display","block");
       // button.parents("#course-name-div").find(".fa-circle-o-notch").addClass("fa-spin");
    },
    success:function(data){
        //button.parents("#course-name-div").find(".fa-circle-o-notch").removeClass("fa-spin");
       // button.parents("#course-name-div").find(".fa-circle-o-notch").css("display","none");
        if(data=="success"){
            $("#title_label").text(title);
            button.parents("#course-name-div").find(".help-block").text("Course title was updated successfully.").delay(1800).fadeOut("slow",function(){
                  jQuery("#course-name-div").removeClass("show").addClass("hidden")
                  jQuery("#course-title").removeClass("hidden").addClass("show");
            });

        }

        else
            button.parents("#course-name-div").find(".help-block").html(data).delay(1800).fadeOut("slow");;
    }
  })
  return false;
}

$("#save-course-name").click(function(){
    title = $("#course-course_title").val();
    savecoursetitle($(this),title,courseid);
})


$(document).ready(function() {
    $('.bootstrap-tagsinput input').css({width: ''})
    $('.bootstrap-tagsinput input').css({width: '8em !important'});

    $("#course-image").fileinput({
        previewFileType:"image",
        showCaption:false,
        minImageWidth: 480,
        maxImageWidth: 480,
        maxImageHeight: 270,
        minImageHeight: 270,
       /* layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\'>\n" +
            "       {browse}\n" +
            "       {upload}\n" +
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>",
            progress: '<div class="progressbar">\n' +
        '    <div class="up-progress text-center" role="progressbar" aria-valuenow="{percent}" aria-valuemin="0" aria-valuemax="100" style="left: 40%;width:{percent}%;">\n' +
        '        {percent}%\n' +
        '     </div>\n' +
        '</div>',
             main2: '{preview}\n<div class="kv-upload-progress up-progress show"></div>\n{remove}\n{cancel}\n{upload}\n{browse}\n',
        },*/
        browseLabel: "Pick Image",
        browseIcon: "<i class=\"fa fa-picture-o\"></i> ",
        uploadUrl: baseUrl+"/course/coursebanner", // server upload action,
        deleteUrl:baseUrl+'/course/deletebanner',
        deleteExtraData: function() {
            var obj = {};

              obj['courseid']=courseid;
            return obj;
        },
        uploadExtraData:function() {
            var obj = {};

              obj['courseid']=courseid;
            return obj;
        },

       /* minImageWidth: 270,
        maxImageWidth: 270,
        maxImageHeight: 160,
        minImageHeight: 160,*/
        dropZoneTitle:"<img src='"+coursebanner+"'/>",
        allowedFileExtensions:['jpg','png'],



        removeClass: "btn btn-danger",
        removeLabel: " Delete",
        removeIcon: "<i class=\"fa fa-trash\"></i>",
        uploadClass: "btn btn-info",
        uploadLabel: "Upload",
        uploadIcon: "<i class=\"icon md-upload\"></i>",

    });

$("#promovideo").fileinput({
        previewFileType:"any",
        showCaption:false,
        overwriteInitial:true,
       // minImageWidth: 270,
      //  maxImageWidth: 270,
      //  maxImageHeight: 160,
       // minImageHeight: 160,
        maxFileSize:9000000,
        dropZoneTitle:promo_video_thumbnail,
        allowedFileExtensions:['mp4','flv','wmv','avi'],
        uploadUrl: baseUrl+"/course/promovideo", // server upload action
        browseLabel: " Select Video",
        browseIcon: "<i class='fa fa-file-video-o'></i>",
        removeClass: "btn btn-danger",
        removeLabel: " Delete",
        removeIcon: "<i class=\"fa fa-trash\"></i>",
        uploadClass: "btn btn-info",
        uploadLabel: "Upload",
        uploadIcon: "<i class=\"icon md-upload\"></i>",
        uploadExtraData:function() {
            var obj = {};

              obj['courseid']=courseid;
            return obj;
        }
    });

});


 //$('#category_select').selectpicker();


/*
var $input = $('#tools');
$input.typeahead({source:[{id: "someId1", name: "Display name 1"},
            {id: "someId2", name: "Display name 2"}],
            autoSelect: true});

*/
