scope_array=['https://www.googleapis.com/auth/drive'];





$("body").on("change","#user-photo",function(){
    //$("#photoform").click();
     $(".progress").removeClass("hidden").addClass("show");
    $(".progress-bar").html(0)
       $(".progress-bar").width("0%");
    var formData = new FormData();
    formData.append( 'User[photo]', $("#user-photo")[0].files[0] );

         $.ajax({
            url:'photo',
            type:"post",
            data:formData,
            contentType: false,
            processData: false,
             xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress',progressHandlingFunction, false);

                   }
                   return myXhr;
                },
            success:function(data){
                data=$.parseJSON(data)
                console.log(data);
              //  console.log(data.partialpath)
                if(data.status==1){
                    $("#profile-pix").attr("src",data.path);
                    $("#path").val(data.partialpath);
                    $("#filename").val(data.filename);
                    $("#savephoto").prop("disabled" ,false)
                    jQuery(function($) {
                        $('#profile-pix').Jcrop({
                            aspectRatio:1,
                            //trueSize:[413,531],
                             trueSize:[data.width,data.height],
                            // onSelect: function (coords) {
                            //   console.log($('#profile-pix').get(0).naturalWidth;)
                            //     console.log($('#profile-pix')[0].naturalWidth)
                            //     // fix crop size: find ratio dividing current per real size
                            //     var ratioW = $('#profile-pix')[0].naturalWidth / $('#profile-pix').width();
                            //     var ratioH = $('#profile-pix')[0].naturalHeight / $('#profile-pix').height();
                            //     var currentRatio = Math.min(ratioW, ratioH);
                            //     $('#x').val(Math.round(coords.x * currentRatio));
                            //     $('#y').val(Math.round(coords.y * currentRatio));
                            //     $('#w').val(Math.round(coords.w * currentRatio));
                            //     $('#h').val(Math.round(coords.h * currentRatio));
                            // }
                             //boxWidth: 413,

                            setSelect:   [ 200, 200, 50, 50 ],
					                  onSelect: updateCoords
                            });
                    });
                }
            }
        })



})
function updateCoords(c)
{
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};
function progressHandlingFunction(e)
{
    if(e.lengthComputable){
        var percentage = Math.floor((e.loaded / e.total) * 100);
        //update progressbar percent complete
       $(".progress-bar").html(percentage+"%")
       $(".progress-bar").width(percentage+"%");
      //  console.log("Value = "+e.loaded +" :: Max ="+e.total);
    }
}

function checkCoords()
{
	if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press save.');
		return false;
};
function createPicker() {
        if (pickerApiLoaded && oauthToken) {
          var picker = new google.picker.PickerBuilder().
              addView(google.picker.ViewId.DOCS_IMAGES).
              setOAuthToken(oauthToken).
              setDeveloperKey(developerKey).
              setCallback(pickerCallback).
              //enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
              build();
          picker.setVisible(true);
        }
}
function getDownloadUrl(fileId) {

            var request =
                gapi.client.request({
                    'path': '/drive/v2/files/' + fileId,
                    'params': { 'maxResults': '1000' },
                    callback: function (responsejs, responsetxt) {
                            var fileDownloadUrl = responsejs.downloadUrl; //using this downloadUrl you will be able to download Drive File Successfully
                           console.log(fileDownloadUrl);
                            $.ajax({
                                url:'user/savephoto',
                                data:{'url':fileDownloadUrl,'id':userid,'access_token':gapi.auth.getToken().access_token,'fileid':fileId},
                                beforeSend:function(){

                                },
                                success:function(data){
                                    //console.log(data);
                                    if(data==1){
                                        //$("#profile-pix").attr("src",url)
                                        $(".alert-success").html("Your profile picture has been updated")
                                        $(".alert-success").removeClass("hidden").addClass('show')
                                    }
                                    else{
                                         $(".alert-danger").html("An error occurred while uploading your profile picture")
                                         $(".alert-danger").removeClass("hidden").addClass('show')
                                    }

                                }
                            })
                    }
                });
 }


function pickerCallback(data) {
        var url = 'nothing';
        if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
          var doc = data.docs[0];
          //url = doc.url;
         var fileId = data.docs[0].id;
          getDownloadUrl(fileId);
         }
          else if (data[google.picker.Response.ACTION] == google.picker.Action.CANCELED){
              var message = 'You canceled' + url;alert(message);
          }

        //document.getElementById('result').innerHTML = message;

}
$("#google-drive").click(function(){
    onApiLoad();
})
