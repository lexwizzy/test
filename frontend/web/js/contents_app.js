(function(){

 			var coursecontent_app = angular.module(name,requires);
      coursecontent_app.run( function run($http){
          //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
          $http.defaults.headers.post['X-CSRF-Token'] = yii.getCsrfToken();//$('meta[name="csrf-token"]').attr("content");
      });
      coursecontent_app.factory("loadContent",function($rootScope,$http,$q){
        return {
          getOutline:function(url){
            var deferred = $q.defer();
            $http.get(url)
              .success(function(data){
                  deferred.resolve(data)
                  $rootScope.$broadcast("test")
                //  doSort();
              })
              .error(function(error){
                console.log("Something went wrong",error)
                deferred.eject(error)
              })
              return deferred.promise
            }

          }
        })
        //coursecontent_app.factory("loadLibrary",function)
            coursecontent_app.controller("content-ctrler",function($scope,$http,$timeout,loadContent,$parse,$compile){
                $scope.contents =[];
                $scope.c_thumbnail="";
                $scope.c_file_name="";
                $scope.c_uploaded_date="";
                $scope.c_size="";
                $scope.c_dimension="";
                $scope.content_id="";
                $scope.current_lesson="";
                $scope.library=[];
                $scope.l_section="";//this stores the current section when adding from library
                $scope.l_lessonindex="";//this stores the current lesson index when adding from library
                $scope.c_index="";
                $scope.sectionsLessonArray=[]
               	$scope.toggleNewUnit =true;
                $scope.show_file_detail=false;
               	$scope.newUnitFormSrc ="";
               	$scope.url;
                $scope.dataLoading = true;

                $scope.loadLibrary=function($event){

                   $http({
                     url:baseUrl+"/user/load-library?userid="+userid,
                     method:'get',
                   })
                   .success(function(data){
                     $scope.library = data
                     $("#library").modal("show");
                     $scope.current_lesson=angular.element($event.currentTarget).data("lesson-id");
                     $scope.l_lessonindex=angular.element($event.currentTarget).data("lesson-index");
                     $scope.l_section=angular.element($event.currentTarget).data("section");
                     console.log(data)

                   })
                   .error(function(){
                     alert("An error occurred while loading library");
                   })
                }
                $scope.useThisItem=function(id,lessonid,l_section,l_lessonindex){
                  console.log(l_section+" "+l_lessonindex);
                  $http({
                    url:baseUrl+"/course/content-from-library?lessonid="+lessonid+"&contentid="+id,
                    method:"post",
                //    data:$.param({lessonid:lessonid,contentid:id}),

                  })
                  .success(function(data){
                      $scope.sectionsLessonArray[l_section][l_lessonindex].content[0].lesson_content=data.content
                      $scope.sectionsLessonArray[l_section][l_lessonindex].content[0].content_dropdown=data.dropdown
                      //$scope.$apply();
                  })
                  .error(function(){
                    alert("An error occurred while adding this file to the lesson");
                  })

                }
                $scope.showFileDetails=function(index){
                  $scope.show_file_detail=true;
                  var lib = $scope.library[index];
                  $scope.c_thumbnail=lib["thumbnail"];
                  $scope.c_file_name=lib["filename"];
                  $scope.c_index=index;
                  $scope.c_uploaded_date=lib["uploaded_date"];
                  $scope.c_size=lib["size"];
                  $scope.c_dimension=lib["dimension"];
                  $scope.content_id=lib["id"];
                }
                $scope.deleteContent=function(id,index){
                  if(confirm("Are you sure you want to delete this file?")){
                    $http({
                      url:baseUrl+"/user/delete-content?id="+id,
                      method:"post",
                    })
                    .success(function(){
                        $scope.library.splice(index,1);
                    })
                    .error(function(){
                      alert("An error occurred while deleting content");
                    });
                  }
                }
               	$scope.showNewUnitForm =function(){
               		$scope.toggleNewUnit =false;
               	}
                //this function will update a lesson after editing
                $scope.updateLesson=function(sectionid,index,title,description){
                   $scope.sectionsLessonArray[sectionid][index].content[0].lesson_title=title;
                   $scope.sectionsLessonArray[sectionid][index].content[0].lesson_description=description;
                   $scope.$digest();
                }

                //'application/x-www-form-urlencoded'
                $scope.deleteLesson=function($event,index,sectionid){
                  var option = confirm("Are you sure you want to delete this lesson?");
                  if(option==1){
                    var lessonid= angular.element($event.currentTarget).data("lessonid");
                    var url =baseUrl+'/course/deleteunit';
                    $http({
                      url:url,
                      method:"post",
                      data:$.param({lid:lessonid}),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    })
                      .success(function(data){
                        //console.log(data)
                        $scope.sectionsLessonArray[sectionid].splice(index,1);
                        updateCounter(eval($("#section-"+index).find(".lesson-li")),".lesson_counter");
                      })
                      .error(function(error){
                        alert("Something went wrong",error)
                      })

                  }
                }
                $scope.deleteQuiz=function($event,index,sectionid){
                  var option = confirm("Are you sure you want to delete this quiz?");
                  if(option==1){
                    var quizid= angular.element($event.currentTarget).data("quizno");
                    var url =baseUrl+'/course/deletequiz';
                    $http({
                      url:url,
                      method:"post",
                      data:$.param({quizno:quizid,partid:sectionid}),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    })
                      .success(function(data){
                        console.log(data)
                        $scope.sectionsLessonArray[sectionid].splice(index,1);
                        updateCounter(eval($("#section-"+index).find(".lesson-li")),".quiz_counter");
                      })
                      .error(function(error){
                        alert("Something went wrong",error)
                      })

                  }
                }
                $scope.questionModal=function($event,scenario){
                  if(scenario=="edit"){
                    var section =$($event.currentTarget).data("section");
                    var parentindex=$($event.currentTarget).data("quiz-index");
                    var index =$($event.currentTarget).data("index");//question index
                    var questions =$scope.sectionsLessonArray[section][parentindex].content[0].questions[index];
                  }
                  $http({
                    url:baseUrl+"/course/question-modal",
                    method:"get"
                  })
                  .success(function(data){
                    var compiledhtml = $compile($(data))($scope);
                    $("#addquestion .modal-content").html(compiledhtml)
                    $("#addquestion").modal();
                    $("#quizid").val($($event.currentTarget).data("quizno"))
                    $("#quiz-index").val($($event.currentTarget).data("quiz-index"))
                    $("#quiz-sectionid").val($($event.currentTarget).data("section"))

                    if(scenario=="edit"){
                        $("#addquestion .modal-title").text("Edit Question");
                        $("#addquestion #savequestion").attr("data-scenario","edit")
                        $("#addquestion #quizid").val(questions.quiz_id);
                        $("#addquestion #questionid").val(questions.id);
                        $("#addquestion #question-title").val(questions.title);
                        $("#addquestion #question-type").val(questions.type)
                        $("#addquestion .question-type").trigger("change");
                        $("#addquestion #question-description").val(questions.description);
                        if(questions.type==1){
                             $("#addquestion .checkbox-answer-div").html("")
                                    for(i=0;i<questions.answers.length;i++){
                                       data=questions.answers;
                                         if(data[i]['iscorrect']=="true")
                                            $checked="checked"
                                        else
                                             $checked="";
                                        checked_answer='<div class="answer-row" > <div class="form-item-wrap" style="width:80%"> <div class="input-group"> <span class="input-group-addon"> <input type="checkbox" name="checkedanswers[]" '+$checked+' class="checkbox_ans" value="'+i+'" aria-label="..." style=""> </span> <input type="text" class="form-control answer-field" name="answer-field-multiple[]" aria-label="..." value="'+data[i]['answer']+'"> <span class="input-group-addon"><div class="course-region-tool"> <a style="cursor" class="delete" title="delete"><i class="fa fa-trash"></i></a> </div></span> </div></div><div class="form-item"> <input type="text" class="reason-multiple form-control reason" name="reason-multiple[]" value="'+data[i]['reason']+'" style="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer"> </div></div>'
                                        $("#addquestion .checkbox-answer-div").append(checked_answer)

                              }
                        }
                       else if(questions.type==2){
                                   $("#addquestion .radio-answer-div").html("")
                                   for(i=0;i<questions.answers.length;i++){
                                      data=questions.answers;
                                         if(data[i]['iscorrect']=="true")
                                            $checked="checked"
                                        else
                                             $checked=""
                                         radioans='<div class="answer-row main"> <div class="form-item-wrap" style="width:80%"> <div class="input-group"> <span class="input-group-addon"> <input type="radio" aria-label="..." name="radioselected" class="radio-ans" '+$checked+' value="'+i+'" style=""> </span> <input type="text" class="form-control answer-field" name="answer-field-radio[]" aria-label="..." value="'+data[i]['answer']+'"> <span class="input-group-addon"><div class="course-region-tool"> <a style="cursor" class="delete" title="delete"><i class="fa fa-trash"></i></a> </div></span> </div></div><div class="form-item"> <input type="text" class="form-control reason" name="reason-single[]" value="'+data[i]['reason']+'" style="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer"> </div></div>';
                                        $("#addquestion .radio-answer-div").append(radioans)

                                    }
                       }
                       else{
                               data=questions.answers;
                               console.log(data[0].iscorrect);

                                 if(data[0].iscorrect)
                                       $("#addquestion .truefalse-answer-div .true").prop("checked",true);
                                 else
                                        $("#addquestion .truefalse-answer-div .false").prop("checked",true);
                                        $("#addquestion .truefalse-answer-div .true-reason").val(data[0].reason);
                                        $("#addquestion .truefalse-answer-div .false-reason").val(data[1].reason);

                          }
                    }
                  })

                }
                $scope.saveQuestion = function($event){
                  var section =$("#quiz-sectionid").val();
                  var index =$("#quiz-index").val();
                  var url =baseUrl+"/course/newquestion";
                  if($($event.currentTarget).data("scenario")=="edit")
                    url =baseUrl+"/course/updatequestion"
                  $($event.currentTarget).prop("disabled",true);
                  $http({
                    url:url,
                    method:"post",
                    data:$("#questionform").serialize(),
                    headers:{'Content-Type':'application/x-www-form-urlencoded'}
                  })
                  .success(function(data){
                     $($event.currentTarget).prop("disabled",false);
                      if(data=="error")
                          $("#question-error").html("An error occurred while performing the operation").fadeIn().delay(3000).fadeOut();
                      else{
                          $scope.sectionsLessonArray[section][index].content[0].questions=data
                          if($($event.currentTarget).data("scenario")=="edit")
                              $("#question-success").html("Question was updated successfully.").fadeIn().delay(3000).fadeOut();
                          else
                              $("#question-success").html("A new question was created successfully.").fadeIn().delay(3000).fadeOut();


                      }
                  })
                  .error(function(error){
                    alert("Something went wrong",error)
                  })
                }

                $scope.appendJson=function(sectionid,data){
                  console.log(data);
                    $scope.sectionsLessonArray[sectionid].push(angular.fromJson(data))
                    $scope.$digest();
                }
                $scope.test=function(data,$event,section,index){
                  $scope.sectionsLessonArray[section][index].content[0].name=$event.currentTarget.innerText;
                  console.log(data)
                  console.log($event.currentTarget.innerText)
                }
               	$scope.loadOutline = function(url){
                    $scope.contents=loadContent.getOutline(url).then(success,error);
                    function success(data){``
                      console.log(data)
                      $scope.contents=data
                      $scope.dataLoading = false;

                      //push lessons
                      angular.forEach($scope.contents,function(item){
                        $scope.sectionsLessonArray[item.part_id]=item.contents;
                        //$scope.sectionsLessonArray[item.part_id][0];//.push(item.quiz);
                      })
                    }
                    function error(error){
                      console.log(error)
                    }

               	}
               	// $scope.hideNewUnitForm =function(){
               	// 	$scope.toggleNewUnit =false;
               	// }
            }).directive('sortables', ['$timeout', function ($timeout) {
                return {
                  restrict:'AEC',
                  link: function (scope, element, attrs) {
                    doSort();
                  }
                };
              }]);
            // coursecontent_app.directive('sortables', ['$timeout', function ($timeout) {
            //     return {
            //       link: function ($scope, element, attrs) {
            //         $scope.$on('dataloaded', function () {
            //           $timeout(function () {
            //           console.log(element)
            //           }, 0, false);
            //         })
            //       }
            //     };
            //   }]);


}())
