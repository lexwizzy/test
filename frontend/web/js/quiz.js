$(document).ready(function(){
  //$("html").hide();
    $.get(baseUrl+'/course/quizquestion',{'resume':true},function(data){
      // console.log(data)
      $('#clock').countdown("resume")
      if(data != 0){

         source   = $("#questions-template").html();
         var template = Handlebars.compile(source);
         //console.log(1234);
         data=JSON.parse(data);
         var html    = template(data);
         $(".question-content").html(html);
         $("html").show();
        // getAnswered(quizid,data.questionid,data.type);
      //  $('#clock').countdown("resume")
      }
      else {
        $.get(baseUrl+'/course/quizquestion',{'quizid':quizid,"counter":1},function(data){

            source   = $("#questions-template").html();
            var template = Handlebars.compile(source);
            data=JSON.parse(data);
            var html    = template(data);
            $(".question-content").html(html);
            // $('#clock').countdown("resume")

        })
          $("html").show();
      }
    });
 })

 $("#clock").countdown(
   {
     until:"+"+timer_counter+"s",
     layout:"{hn}:{mn}:{sn}",
     tickInterval:5,
     onExpiry:timeElapsed,
     onTick:saveTime,//the countdown passes the periods array to the saveTime function
   })
   function saveTime(periods){
     //
     hour=periods[4];
     minutes=periods[5];
     seconds=periods[6];
     console.log(hour+":"+minutes+":"+seconds);
     $.get(baseUrl+"/course/savetimer",{"seconds":seconds,"minutes":minutes,"hour":hour,"quizid":quizid,"userid":userid},function(data){
        console.log(data)
      })
   }
   function timeElapsed(){
        $.get(baseUrl+"/course/savetimer",{"delete":"true","quizid":quizid,"userid":userid},function(data){
                 if(data==1){
                    window.location.href=baseUrl+"/course/finish-quiz?quiz="+quizid+"&userid="+userid;
                  //  $.get(baseUrl+'/course/finishquiz',{"quiz":quizid,"userid":userid},function(data){
                  //    source   = $("#result-template").html();
                  //    var template = Handlebars.compile(source);
                  //    data=JSON.parse(data);
                  //    console.log(data);
                  //    var html    = template(data);
                  //    $(".question-content").html(html);
                  //    //console.log(data)
                  //    //getAnswered(quizid,data.questionid,data.type);
                  //  })
                 }
      })
   }
// $('#clock').countdown(timer_counter, function(event) {
//     $(this).html(event.strftime('%H:%M:%S'));
//     $("#clock").text(event.strftime('%H:%M:%S'))
//     // $.get(baseUrl+"/course/savetimer",{"seconds":event.offset.seconds,"minutes":event.offset.minutes,"quizid":quizid,"userid":userid},function(data){
//     //   //console.log(data)
//     // })
//     // if (event.elapsed){
//     //      $.get(baseUrl+"/course/savetimer",{"delete":"true","quizid":quizid,"userid":userid},function(data){
//     //         if(data==1){
//     //           $.get(baseUrl+'/course/finishquiz',{"quiz":quizid,"userid":userid},function(data){
//     //             source   = $("#result-template").html();
//     //             var template = Handlebars.compile(source);
//     //             data=JSON.parse(data);
//     //             console.log(data);
//     //             var html    = template(data);
//     //             $(".question-content").html(html);
//     //             //console.log(data)
//     //             //getAnswered(quizid,data.questionid,data.type);
//     //           })
//     //         }
//     //     })
//     // }
//
//  });
// if($("#quizheader").is(":visible")){
//     $('#clock').countdown("pause")
// }
// // $("#start-quiz").click(function(){
// //   window.location.href=baseUrl+"/course/questions-display?quizid="+quizid+"&counter="+counter;
// //     // quizid=$(this).attr("data-quizid");
// //     // $.get(baseUrl+'/course/quizquestion',{'quizid':quizid,"counter":1},function(data){
// //     //     source   = $("#questions-template").html();
// //     //     var template = Handlebars.compile(source);
// //     //     data=JSON.parse(data);
// //     //     var html    = template(data);
// //     //     $(".question-content").html(html);
// //     //      $('#clock').countdown("resume")
// //     //
// //     // })
// //
// // })
//
$(document).on("click","#next",function(){
  var formData = $("#ans-form").serializeArray();
  console.log(formData)
  $(this).prop("disable",true);
  formData.push({name:"quizid",value:$(this).attr("data-quizid")});
  formData.push({name:"questionid",value:$(this).attr("data-questionid")});
  formData.push({name:"type",value:$(this).attr("data-questiontype")});
  formData.push({name:"userid",value:userid});
  $.get("saveanswers",formData,function(data){
    // console.log(data);
  })
    quizid=$(this).attr("data-quizid");
    counter =parseInt($(this).attr("data-counter"));
    $.get(baseUrl+'/course/quizquestion',{'quizid':quizid,"counter":counter+1},function(data){
        source   = $("#questions-template").html();
        $(this).prop("disable",false);
        var template = Handlebars.compile(source);
        data=JSON.parse(data);
        var html    = template(data);
        $(".question-content").html(html);
        //console.log(data)
        getAnswered(quizid,data.questionid,data.type);
    })
})

//
$(document).on("click","#previous",function(){
  quizid=$(this).attr("data-quizid");
  var data="";
  $(this).prop("disable",true);

  if(counter =$(this).attr("data-counter")==1){
    $(this).prop("disable",true);
  }else{

    counter =parseInt($(this).attr("data-counter"));
    $.get(baseUrl+'/course/quizquestion',{'quizid':quizid,"counter":counter-1,"previous":true},function(data){
        source   = $("#questions-template").html();
        var template = Handlebars.compile(source);
        $(this).prop("disable",false);
        data=JSON.parse(data);
        var html    = template(data);
        $(".question-content").html(html);
        //console.log(data)
        getAnswered(quizid,data.questionid,data.type);
    })

  }

})
//
$(document).on("click","#finish",function(){
  var formData = $("#ans-form").serializeArray();
  //console.log(formData)
  formData.push({name:"quizid",value:$(this).attr("data-quizid")});
  formData.push({name:"questionid",value:$(this).attr("data-questionid")});
  formData.push({name:"userid",value:userid});
  formData.push({name:"type",value:$(this).attr("data-questiontype")});
  $.get(baseUrl+"/course/saveanswers",formData,function(data){
     //console.log(data);
     window.location.href=baseUrl+"/course/finish-quiz?quiz="+quizid+"&userid="+userid;
  })


    // quizid=$(this).attr("data-quizid");
    // counter =parseInt($(this).attr("data-counter"));
    // $.get(baseUrl+'/course/finishquiz',{"quiz":quizid,"userid":userid},function(data){
    //   source   = $("#result-template").html();
    //   var template = Handlebars.compile(source);
    //   data=JSON.parse(data);
    //   console.log(data);
    //   var html    = template(data);
    //   $(".question-content").html(html);
    //   $('#clock').countdown("pause")
    //   //console.log(data)
    //   //getAnswered(quizid,data.questionid,data.type);
    // })

})
function getAnswered(quizid,question,type){
  $.get(baseUrl+"/course/getsavedanswers",{"quizid":quizid,"userid":userid},function(data){
    //console.log(data);
    data= JSON.parse(data);
    if(data.answers.length!=0){
        $.each(data.answers,function(index,value){
         if(value.question==question && value.type==1){
              $.each(value.answer,function(key){
                $("#checkbox-"+value.answer[key]).prop("checked",true)

             })
         }else{
            //
             $.each(value.answer,function(key){
                $("#radio-"+value.answer[key]).prop("checked",true)
             })

         }
      })
    }

  //console.log(data);
  })
}
