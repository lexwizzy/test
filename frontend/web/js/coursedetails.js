// $("#fb-share").click(function(){
//     FB.ui({
//       method: 'share',
//       href: current,
//     }, function(response){});
// })
// var $el, $ps, $up, totalHeight;
$(document).ready(function(){
	$(".text-box").each(function(){
		$(this).attr("data-height",$(this).height())
	})
	$(".text-box").each(function(){
		$(this).css({"max-height":200})
	})
})

$(".read-more-btn").click(function() {
      
  totalHeight = 0


  $el = $(this);
  $p  = $el.parent();
  $up = $p.parent();
  $ps = $up.find("p:not('.read-more')");

  
  // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
  $ps.each(function() {
    totalHeight += $(this).outerHeight();
  });
        
  $up
    .css({
      // Set height to prevent instant jumpdown when max height is removed
      "height": $up.height(),
      "max-height": 9999
    })
    .animate({
      "height": $(this).parents(".text-box").attr("data-height")
    });
  
  // fade out read-more
  $p.fadeOut();
  
  // prevent jump-down
  return false;
    
});