var newunitform="";
var addcontenttable="";
var addquestion ="";



function updateCounter(selector,counter_class){
   var counter=0
   selector.each(function(){
       $(this).find(counter_class).text(++counter)
   })
}

$(document).on("click",".delete-section",function(event){
 // event.preventDefault();
 var option = confirm("Do you want to delete this section?");
 if(option == 1){
   console.log(1)
     $.post(baseUrl+"/course/deletepart",{"partid":$(this).data("sectionid")},function(){
       console.log(12)
     })
 }


})
function toggleSection(p){
 if(p.parents(".dc-section-info").find(".dc-section-body").is(":visible")){
       p.parents(".dc-section-info").find(".dc-section-body").removeClass("show").addClass("hidden");
       p.find(".section-toggle").removeClass("fa-caret-right").addClass("fa-caret-left")
   }
   else{
       p.parents(".dc-section-info").find(".dc-section-body").removeClass("hidden").addClass("show");
       p.find(".section-toggle").removeClass("fa-caret-left").addClass("fa-caret-right")
   }
}

if ($('.popup-with-zoom-anim').length) {
       $('.popup-with-zoom-anim').magnificPopup({
           type: 'inline',

           fixedContentPos: false,
           fixedBgPos: true,

           overflowY: 'auto',

           closeBtnInside: true,
           preloader: false,

           midClick: true,
           removalDelay: 300,
           mainClass: 'my-mfp-zoom-in'
       });
       $('.design-course-popup').delegate('.cancel', 'click', function(evt) {
           evt.preventDefault();
           $('.mfp-close').trigger('click');
       });
   }


$(document).on("focus",'.editsection_title,.section_achiev',function(){
   $(this).css('border','1px solid #fff').css('padding','2px');
})
$(document).on("click",".edit-section-icon",function(){
   $(this).parents(".title-section").find('.editsection_title').focus();
})
$(document).on("focusout",'.editsection_title,.editsection,.section_achiev',function(){
   $(this).css('border','none').css('padding','0');
})
$(document).on("blur",'.editsection_title,.section_achiev',function(){
   sectionid=$(this).data('part-id');
   if($(this).hasClass('editsection_title')){
       if($(this).text()!=""){
           $.ajax({
               url:baseUrl+'/course/editpart',
               type:'post',
               data:{'sectiontitle':$(this).text(),'sectionid':sectionid},
               success:function(data){
                   console.log(data);
               }

           })

       }
   }else if($(this).hasClass('section_achiev')){
       $.ajax({
               url:baseUrl+'/course/editpart',
               type:'post',
               data:{'sectionacheive':$(this).text(),'sectionid':sectionid},
               beforeSend:function(){
                 console.log(sectionid)
               },
               success:function(data){
                   console.log(data);
               }
           })

   }

});

$("body").delegate(".toggle-section","click",function(){

 toggleSection($(this))
})
$(document).ready(function(){
  // newunitform =$("#newunit_div").clone().find("form");
   //addcontenttable =$("#add-content-table").clone()

})

//Unit section
$("body").on("click",'.add-unit',function(){
    newunitform =$("#newunit_div").clone().find("form");
//$(this).parents(".dc-section-info").find(".dc-section-body").removeClass("hidden").addClass("show").find(".newunit-form-hidden").removeClass("hide").addClass("show");
 $(this).parents(".dc-section-info").find(".unit-body").removeClass("hidden").addClass("show");
   $(this).parents(".dc-section-info").find(".newunit-form-hidden").removeClass("hide").addClass("show");
    $(this).parents(".dc-section-info").find(".newunit-form-hidden").html(newunitform.addClass("newunitform-cloned"))



   //ad validations
 jQuery('.newunitform-cloned').yiiActiveForm([{"id":"courselessons-lesson_title","name":"lesson_title","container":".field-courselessons-lesson_title","input":"#courselessons-lesson_title","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Lesson Title cannot be blank."});yii.validation.string(value, messages, {"message":"Lesson Title must be a string.","max":70,"tooLong":"Lesson Title should contain at most 70 characters.","skipOnEmpty":1});}},{"id":"courselessons-lesson_description","name":"lesson_description","container":".field-courselessons-lesson_description","input":"#courselessons-lesson_description","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Lesson Description cannot be blank."});yii.validation.string(value, messages, {"message":"Lesson Description must be a string.","max":200,"tooLong":"Lesson Description should contain at most 200 characters.","skipOnEmpty":1});}}], []);
})

$(document).on("click",".edit-unit",function(){
     newunitform =$("#newunit_div").clone().find("form");
    $(this).parents(".unit-title-edit-div").find(".hold-edit-form").removeClass("hide").addClass("show");
    $(this).parents(".unit-title-edit-div").find(".hold-edit-form").html(newunitform.addClass("editunitform-cloned"));
    editform= $(this).parents(".unit-title-edit-div").find(".hold-edit-form").find("form");
    editform.find("button[type='submit'], input[type='submit']").removeClass("save-unit").addClass("edit-unit-btn").val("Edit Unit");
    var lessondetails = angular.element("#create-course-section").scope().sectionsLessonArray[$(this).parents(".lesson-li").data("section-no")];
    var index = $(this).parents(".lesson-li").data("index");
    editform.find("#courselessons-lesson_title").val(lessondetails[index].content[0].lesson_title);
    editform.find("#courselessons-lesson_description").val(lessondetails[index].content[0].lesson_description);
    editform.find(".lesson_id_hidden").val($(this).data("lesson-id"))

    jQuery('.editunitform-cloned').yiiActiveForm([{"id":"courselessons-lesson_title","name":"lesson_title","container":".field-courselessons-lesson_title","input":"#courselessons-lesson_title","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Lesson Title cannot be blank."});yii.validation.string(value, messages, {"message":"Lesson Title must be a string.","max":70,"tooLong":"Lesson Title should contain at most 70 characters.","skipOnEmpty":1});}},{"id":"courselessons-lesson_description","name":"lesson_description","container":".field-courselessons-lesson_description","input":"#courselessons-lesson_description","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Lesson Description cannot be blank."});yii.validation.string(value, messages, {"message":"Lesson Description must be a string.","max":200,"tooLong":"Lesson Description should contain at most 200 characters.","skipOnEmpty":1});}}], []);
})

$(document).on("click",".save-unit",function(){
   button =$(this);
   $.ajax({
       url:baseUrl+"/course/newunit?part_id="+button.parents(".section-li").data("part-id"),
       type:'post',
       data:button.parents(".section-li").find(".newunitform-cloned").serialize(),
       beforeSend:function(){
           button.prop("disable",true);
       },
       success:function(data){
           console.log(data)
           angular.element("#create-course-section").scope().appendJson(button.parents(".section-li").data("part-id"),data)
          // // button.parents(".section-li").find(".section-item-ul").append(data);
          //  button.prop("disable",false);
          updateCounter(button.parents(".section-li").find(".lesson-li"),".lesson_counter");
          toggleSection($(this))
           button.parents(".section-li").find(".unit-body").removeClass("show").addClass("hide");
           button.parents(".section-li").find(".newunitform-cloned").remove();

           // var scope=angular.element(save-unit).scope();
           // console.log(scope);
           // scope.$apply(function(){
           //   scope.loadOutline();
           // })
           //toggleSection(button)
       }

   })

})

$(document).on("click",".edit-unit-btn",function(){
   button =$(this);
   $.ajax({
       url:baseUrl+"/course/editunit",
       type:'post',
       data:button.parents(".dc-section-body").find(".editunitform-cloned").serialize(),
       beforeSend:function(){
           button.prop("disable",false);
       },
       success:function(data){
         //  console.log(data);
           var index= button.parents(".lesson-li").data("index");
           var title = button.parent(".editunitform-cloned").find("#courselessons-lesson_title").val();
           var description = button.parent(".editunitform-cloned").find("#courselessons-lesson_description").val();
           angular.element("#create-course-section").scope().updateLesson(button.parents(".section-li").data("part-id"),index,title,description)
           button.parents(".hold-edit-form").removeClass("show").addClass("hide")
           updateCounter(button.parents(".section-li").find(".lesson-li"),".lesson_counter");
           button.prop("disable",false);

       }

   })
})
$(document).on("click",".cancel-unit",function(){
   $(this).parents(".dc-section-info").find(".unit-body").removeClass("show").addClass("hide");
})

$(document).on("click",".hold-edit-form .cancel-unit",function(){
   $(this).parents(".hold-edit-form").removeClass("show").addClass("hide")
})

//moved to angularjs
// $(document).on("click",'.delete-unita',function(){
//
//     var option = confirm("Are you sure you want to delete this lesson?");
//     if(option){
//          link =$(this);
//         lessonid=link.data('lessonid');
//
//
//         $.ajax({
//             url:baseUrl+'/course/deleteunit',
//             type:'post',
//             data:{'lid':lessonid},
//             success:function(data){
//                  link.parents(".lesson-li").remove();
//                 // angular.element("#create-course-section").scope().appendJson("section"+link.parents(".section-li").data("part-id"),data,"new")
//                 // link.parents(".section-li").find(".section-item-ul").html(data);
//             }
//         })
//     }
//
//
// })
//End Unit section
function disableSubmit(element){
   $("."+element).attr('disabled','disabled');
}


//Remove Content Plugin

$(document).on("click",".fileinput-remove",function(){
   $(".unit-content-div").html("");
})
//Add content section
$(document).on("click",'.dropdown-menu .dropdown-item',function(){
   $addcontenttable = $("#add-content-table").clone()
   var section = $(this).data("section")
   var lesson_index = $(this).data("lesson-index")
   var lessonid=$(this).data("lesson-id");
   li=$(this)
   var $container=$(this).parents(".unit-title-edit-div")
   .find(".unit-content-div")
   .html("<div class='alert alert-danger hide'></div>")
   .append($("#add-content-table").html())
   .removeClass("hide")
   .addClass("show")
   if($(this).data('type')=='video'){
     //  if( $(this).parents(".unit-title-edit-div").find(".video-content").is(":visible")){
       //  }else{

       $container.find(".video-content")
         .addClass("show").removeClass('hide');

        $(this).parents(".unit-title-edit-div")
        .find(".video-content")
        .find(".video-upload")
        .html('<input data-lesson-id="'+lessonid+'" multiple=true type="file" class="unit_video" id="" name="TcUploadedContent[single_upload]"><div class="form-item"><span>maximum video size is 5GB.</span><span> <a href="http://support.teachsity.com">See instructions on how to make a good course video</a</span></div>');

        $(".unit_video").fileinput({
            showCaption:true,
            showPreview :true,// I hide this div 'file-preview'
            showRemove : true,
            showUpload: true,
            overwriteInitial:true,
            maxFileSize:625000,
            allowedFileExtensions:['mp4','avi'],
            dropZoneTitle:"Drag and drop video",
            uploadUrl: baseUrl+"/course/uploadnewunitvideo", // server upload action
            browseLabel: " Select Video",
            browseClass: "btn btn-danger",
            browseIcon: "<i class='fa fa-file-video-o'></i>",
            removeClass: "btn btn-danger",
            removeLabel: " Delete",
            removeIcon: "<i class=\"fa fa-trash\"></i>",
            uploadClass: "btn btn-info",
            uploadLabel: "Upload",
            uploadIcon: "<i class=\"icon md-upload\"></i>",
            uploadExtraData:function() {
                var obj = {};
                  obj['courseid']=courseid;
                  obj['lesson']=lessonid;

                return obj;
            }
        });
            $(".unit_video").on('fileuploaded', function(event, data, previewId, index) {
                $(this).parents(".unit-content-div").find(".fileinput-remove-button").html('<i class="fa fa-times"></i> Close');
               $.ajax({
                   url:baseUrl+"/course/getcontents",
                   type:'get',
                   data:{'type':'main','lessonid':lessonid},
                   success:function(data){

                     angular.element("#create-course-section").scope().$apply(function(scope){
                       console.log(data);
                      angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].lesson_content=$.parseJSON(data).content
                      angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].content_dropdown=$.parseJSON(data).dropdown
                     })


                      //  li.parents(".unit-title-edit-div").find(".unit-contents").html(data);
                   }
               })
            });
           $(document).on('fileuploaderror',".unit_video" ,function(event, data,previewId,index) {
                 var error = data.response.error
                 if(error==undefined)
                   error="Please ensure you are uploading a file not above 5GB. The file must be an mp4 or avi file.";
                   console.log(error)
                 var b = $(this)
                 $(this).parents(".unit-content-div").find(".alert-danger").html(error).addClass("show").removeClass("hide").delay(5000).fadeOut("slow",function(){
                     b.parents(".unit-content-div").find(".alert-danger").removeClass("show").addClass("hide")
                 })
             });
             $(document).on('fileclear', ".unit_video", function(event) {
                 $(this).parents(".unit-content-div").addClass("hide").removeClass("show")
             });


       //}

   }

   else if($(this).data('type')=='ppt'){

     $container.find(".ppt-content")
       .addClass("show").removeClass('hide');
       //  $(this).parents(".unit-title-edit-div").find(".unit-content-div").html(addcontenttable.addClass(" add-content-table").removeClass("hide").addClass("show").find(".ppt-content").addClass("show").removeClass('hide'));
         $(this).parents(".unit-title-edit-div")
         .find(".ppt-content")
         .find(".ppt-upload")
         .html('<input data-lesson-id="'+lessonid+'"multiple=true type="file" class="powerpoint file-loading" id="" name="TcUploadedContent[single_upload]"><div class="form-item"><span>maximum file size is 50MB (ppt,pptx,pdf,gslides,docx,doc)</span></div>');
         $(".powerpoint").fileinput({
             previewFileType:"any",
             showCaption:true,
             showPreview :true,// I hide this div 'file-preview'
             showRemove : true,
             showUpload: true,
             overwriteInitial:true,
             browseClass: "btn btn-danger",
             uploadAsync:true,
             maxFileSize:50000,
             msgSizeTooLarge:"Your file cannot be more than 50GB",
             uploadUrl: baseUrl+"/course/uploadnewunitpresentation",
             dropZoneTitle:"Drag and drop presentation file",
             allowedFileExtensions:['ppt','pptx','pdf','gslides','docx','doc'],
            // uploadUrl: "promovideo", // server upload action
             browseLabel: " Select file",
             browseIcon: "<i class='fa fa-file-powerpoint-o'></i>",
             removeClass: "btn btn-danger",
             removeLabel: " Delete",
             removeIcon: "<i class=\"fa fa-trash\"></i>",
             uploadClass: "btn btn-info",
             uploadLabel: "Upload",
             uploadIcon: "<i class=\"icon md-upload\"></i>",
             uploadExtraData:function() {
                 var obj = {};
                    obj['courseid']=courseid;
                    obj["type"]="main"
                    obj['lesson']=lessonid
                 return obj;
             }
         });
         $(".powerpoint").on('fileuploaded', function(event, data, previewId, index) {
           $(this).parents(".unit-content-div").find(".fileinput-remove-button").html('<i class="fa fa-times"></i> Close');
                $.ajax({
                    url:baseUrl+"/course/getcontents",
                    type:'get',
                    data:{'type':'main','lessonid':lessonid},
                    success:function(data){
                      console.log(data);
                      angular.element("#create-course-section").scope().$apply(function(scope){
                        angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].lesson_content=$.parseJSON(data).content
                        angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].content_dropdown=$.parseJSON(data).dropdown
                     })
                   }
                })
             });
         $(document).on('fileuploaderror',".powerpoint" ,function(event, data,previewId,index) {

                 var error = data.response.error
                 if(error==undefined)
                   error="Please ensure you are uploading a file not above 50MB. File types include ppt,pptx,pdf,gslides,docx,doc.";
                   console.log(error)
                 var b = $(this)
                 $(this).parents(".unit-content-div").find(".alert-danger").html(error).addClass("show").removeClass("hide").delay(5000).fadeOut("slow",function(){
                     b.parents(".unit-content-div").find(".alert-danger").removeClass("show").addClass("hide")
                 })
           });
           $(document).on('fileclear', ".powerpoint", function(event) {
                   $(this).parents(".unit-content-div").addClass("hide").removeClass("show")
             });
     //  }


   }

   else if($(this).attr('data-type')=='downloadables'){
     $container.find(".reference-content")
      .addClass("show").removeClass('hide');
      //  $(this).parents(".unit-title-edit-div").find(".unit-content-div").html(addcontenttable.addClass(" add-content-table").removeClass("hide").addClass("show").find(".ppt-content").addClass("show").removeClass('hide'));
      $(this).parents(".unit-title-edit-div")
      .find(".reference-content")
      .find(".downloadable-upload")
      .html('<input data-lesson-id="'+lessonid+'"multiple=true type="file" class="documents file-loading" id="" name="TcUploadedContent[single_upload]"><div class="form-item"><span>maximum file size is 50MB </span></div>');

      $(".documents").fileinput({
          previewFileType:"any",
          showCaption:true,
          browseClass: "btn btn-danger",
          showPreview :true,// I hide this div 'file-preview'
          showRemove : true,
          showUpload: true,
          overwriteInitial:true,
          maxFileSize:50000,
          uploadUrl: baseUrl+"/course/downloadables",
        //  dropZoneTitle:"Drag and drop file",
          //allowedFileTypes:['image', 'html', 'text', 'video', 'audio', 'flash', 'object'],
         // uploadUrl: "promovideo", // server upload action
          browseLabel: " Select file",
          browseIcon: "<i class='fa fa-file-video-o'></i>",
          removeClass: "btn btn-danger",
          removeLabel: " Delete",
          removeIcon: "<i class=\"fa fa-trash\"></i>",
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: "<i class=\"icon md-upload\"></i>",
          uploadExtraData:function() {
              var obj = {};
                obj['courseid']=courseid;
                obj['lesson']=lessonid;
              return obj;
          }
      });
       $(".documents").on('fileuploaded', function(event, data, previewId, index) {
          $(this).parents(".unit-content-div").find(".fileinput-remove-button").html('<i class="fa fa-times"></i> Close');
         $.ajax({
             url:baseUrl+"/course/getcontents",
             type:'get',
             data:{'type':'other','lessonid':lessonid},
             success:function(data){
               angular.element("#create-course-section").scope().$apply(function(scope){
                // angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].othercontent=$.parseJSON(data)
                angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].lesson_content=$.parseJSON(data).content
                angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].content_dropdown=$.parseJSON(data).dropdown
              })
            }
         })
      });
           $(document).on('fileuploaderror',".documents" ,function(event, data,previewId,index) {
                 var error = data.response.error
                 if(error==undefined)
                   error="Please ensure you are uploading a file not above 50MB.";
                   console.log(error)
                 var b = $(this)
                 $(this).parents(".unit-content-div").find(".alert-danger").html(error).addClass("show").removeClass("hide").delay(5000).fadeOut("slow",function(){
                     b.parents(".unit-content-div").find(".alert-danger").removeClass("show").addClass("hide")
                 })
             });
             $(document).on('fileclear', ".unit_video", function(event) {
                   $(this).parents(".unit-content-div").addClass("hide").removeClass("show")
             });
       //}
   }
   else if($(this).attr('data-type')=='text'){
     text_content=$container.find(".text-content")
     .addClass("show").removeClass('hide');
     text_content.find("form").find(".add-textcontent").attr("data-section",section);
     text_content.find("form").find(".add-textcontent").attr("data-lesson-index",lesson_index);
     text_content.find("form").find(".add-textcontent").attr("data-lesson-id",lessonid);

     text_content.find("form").find("#lesson_no").val(lessonid)
     text_content.find("form").yiiActiveForm([{"id":"lessoncontent-content_defaultname","name":"content_defaultname","container":".field-lessoncontent-content_defaultname","input":"#lessoncontent-content_defaultname","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Text title cannot be blank."});yii.validation.string(value, messages, {"message":"Text title must be a string.","max":200,"tooLong":"Text title should contain at most 200 characters.","skipOnEmpty":1});}},{"id":"lessoncontent-text_content","name":"text_content","container":".field-lessoncontent-text_content","input":"#lessoncontent-text_content","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Text Content cannot be blank."});yii.validation.string(value, messages, {"message":"Text Content must be a string.","skipOnEmpty":1});}}], []);
   }

   // else{
   //      externel_container=addcontenttable.addClass(" add-content-table").removeClass("hide").addClass("show").find(".ext-content")
   //         $(this).parents(".unit-title-edit-div").find(".unit-content-div").html( externel_container.addClass("show").removeClass('hide'));
   //
   //     externel_container.find("form").find("#content_id_hidden").val(li.parents(".course-region-tool").find("a:first").attr("data-lesson-id"))
   //      externel_container.find("form").yiiActiveForm([{"id":"lessoncontent-content_defaultname","name":"content_defaultname","container":".field-lessoncontent-content_defaultname","input":"#lessoncontent-content_defaultname","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Content Defaultname cannot be blank."});yii.validation.string(value, messages, {"message":"Content Defaultname must be a string.","max":200,"tooLong":"Content Defaultname should contain at most 200 characters.","skipOnEmpty":1});}},{"id":"lessoncontent-content_link","name":"content_link","container":".field-lessoncontent-content_link","input":"#lessoncontent-content_link","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Content Link cannot be blank."});yii.validation.string(value, messages, {"message":"Content Link must be a string.","max":1000,"tooLong":"Content Link should contain at most 1000 characters.","skipOnEmpty":1});yii.validation.url(value, messages, {"pattern":/^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i,"message":"Content Link is not a valid URL.","enableIDN":false,"skipOnEmpty":1});}}], []);
   // }
           ;
   //$(this).parents(".dc-section-body").find(".dc-item-body").addClass("show").removeClass("hide")

})

$(document).on("click",".cancel-textcontent",function(){
   $(this).parents(".text-content").addClass("hide").removeClass("show");
   $(".unit-content-div").addClass("hide").removeClass("show")
})
$(document).on("click",'.add-textcontent',function(){
   button = $(this)
   var section = $(this).data("section")
   var lesson_index = $(this).data("lesson-index")
   var lessonid=$(this).data("lesson-id");
   $.ajax({
       url:baseUrl+'/course/textcontent',
       type:'post',
       data:button.parents('form').serialize(),
       beforeSend:function(){
           button.prop("disable",true)
           console.log("about to send")
       },
       success:function(data){
           data=$.parseJSON(data)
           console.log(data)
           button.prop("disable",false)
           $.ajax({
                  url:baseUrl+"/course/getcontents",
                  type:'get',
                  data:{'type':'main','lessonid':data.lessonid},
                  success:function(data){
                    console.log($.parseJSON(data))
                    angular.element("#create-course-section").scope().$apply(function(scope){
                      angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].main_content=$.parseJSON(data).content
                      angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].content_dropdown=$.parseJSON(data).dropdown

                   })
                 }
           })
       }
   })
});
$(document).on("click",".cancel-link",function(){
   $(this).parents(".ext-content").addClass("hide").removeClass("show");
   $(".unit-content-div").addClass("hide").removeClass("show")
})

$(document).on("click",'.add-link',function(){
   button = $(this)
   $.ajax({
       url:'externallinks',
       type:'post',
       data:button.parents('form').serialize(),
       beforeSend:function(){
           button.prop("disable",true)
           console.log("about to send")
       },
       success:function(data){
            data=$.parseJSON(data)
           console.log(data)
           button.prop("disable",false)
           $.ajax({
                  url:"getcontents",
                  type:'get',
                  data:{'lessonid':data.lessonid},
                  success:function(data){
                      //console.log(data)
                      li.parents(".unit-title-edit-div").find(".unit-contents").html(data);
                  }
           })
       }
   })
});

$(document).on('click','.delete-content',function(){
   var section = $(this).data("section")
   var lesson_index = $(this).data("lesson-index")
   var content_index =$(this).data("content-index")
   var content_type = $(this).data("content-type");
   var option = confirm("Are you sure you want to delete this content?");
   var contentno= $(this).data('contentno');
   if(option){

       link = $(this);
       $.ajax({
           url:baseUrl+'/course/deleteunitcontent',
           data:{'contentno':contentno},
           type:'post',
           beforeSend:function(){

           },
           success:function(data){
             console.log(data)
             angular.element("#create-course-section").scope().$apply(function(scope){
              angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0]['lesson_content'].splice(content_index,1)
              angular.element("#create-course-section").scope().sectionsLessonArray[section][lesson_index].content[0].content_dropdown=$.parseJSON(data).dropdown

             })
           }
       })
   }

})

//End Add content

// Begin Quiz

$(document).on('click','.add-quiz',function(){

   newquizform = $("#newquiz-div").clone();

   $(this).parents(".section-li").find(".newquiz").removeClass("hidden").addClass("show").html(newquizform.find("form").addClass("newquizform"));

   jQuery('.newquizform').yiiActiveForm([{"id":"quiz-q_name","name":"q_name","container":".field-quiz-q_name","input":"#quiz-q_name","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Quiz name cannot be blank."});yii.validation.string(value, messages, {"message":"Q Name must be a string.","max":200,"tooLong":"Q Name should contain at most 200 characters.","skipOnEmpty":1});}},{"id":"quiz-q_description","name":"q_description","container":".field-quiz-q_description","input":"#quiz-q_description","validate":function (attribute, value, messages, deferred, $form) {yii.validation.string(value, messages, {"message":"Q Description must be a string.","max":200,"tooLong":"Q Description should contain at most 200 characters.","skipOnEmpty":1});}}], []);
})
$(document).on("click",".cancel-quiz",function(){
   $(this).parents(".section-li").find(".newquiz").removeClass("show").addClass("hidden");
})

$(document).on('click','.save-quiz',function(){

   button = $(this)
   button.parents(".newquizform").find(".quiz_part_id").val(button.parents(".section-li").attr("data-part-id"))
   $.ajax({
       url:baseUrl+'/course/newquiz',
       type:'post',
       data:button.parents(".newquizform").serialize(),
       beforeSend:function(){
           button.attr('disable','disabled');
       },
       success:function(data){
           console.log(data)
           toggleSection($(this));
           angular.element("#create-course-section").scope().appendJson(button.parents(".section-li").data("part-id"),data)
           button.parents(".section-li").find(".newquiz").removeClass("show").addClass("hidden");
           //button.parents(".section-li").find(".section-item-ul").append(data);
           updateCounter(button.parents(".section-li").find(".quiz-li"),".quiz_counter");


       }
   })
})
//moved to angularjs
// $(document).on("click",'.delete-quiz',function(){
//     var option = confirm("Are you sure you want to delete this quiz?");
//     if(option){
//         link =$(this);
//         partid=link.attr('data-partid');
//         quizno=link.attr('data-quizno');
//
//         $.ajax({
//             url:'deletequiz',
//             type:'post',
//             data:{'quizno':quizno,'partid':partid},
//             success:function(data){
//                link.parents(".section-li").find(".section-item-ul").html(data)
//             }
//         })
//     }
//
//
// })

///save quix icon click
$(document).delegate('.save-quiz-icon',"click",function(){

   var section =$(this).data("section");
   var index = $(this).data("index");
   var ang =angular.element("#create-course-section").scope();
   quizname=ang.sectionsLessonArray[section][index].content[0].name;
   quizdesc =ang.sectionsLessonArray[section][index].content[0].description;
   minutes=ang.sectionsLessonArray[section][index].content[0].timelimit;
   quizno =$(this).data('quizno');
   button =$(this)
   if($(this).parents(".dc-quizz-info").find(".limittime").is(":checked") && (minutes=="" || minutes ==0)){
       $(this).parents(".dc-quizz-info").find(".error").addClass("alert alert-danger").html("Please enter minutes").delay(5000).fadeOut("slow",function(){
           button.parents(".dc-quizz-info").find(".error").removeClass("alert alert-danger")
       })
   }
   else{
       $(this).parents(".dc-quizz-info").find(".loading").removeClass("hide").addClass("show")
        if($(this).parents(".dc-quizz-info").find(".limittime").is(":checked"))
            islimited="checked";
       else
           islimited="No";
        $.ajax({
           url:baseUrl+'/course/editquiz',
           type:'post',
           data:{'quizno':quizno,'quizname':quizname,'quizdesc':quizdesc,'islimited':islimited,'minutes':minutes},
           success:function(data){
               console.log(data);
               button.parents(".dc-quizz-info").find(".loading").removeClass("show").addClass("hide")
           }
       })
   }





})
$(document).on("focus",'.quiz-title',function(){
   $(this).css('border','1px solid #ccc').css('padding','5px').css("width","600px");
})
$(document).on("focusout",'.quiz-title',function(){
   $(this).css('border','none').css('padding','0');
})
$(document).on("click",".quiz-edit",function(){
   $(this).parents(".dc-content-title").find('.quiz-title').focus();
})

$(document).delegate(".question-type","change",function(){
   if($(this).val()==1){
      $(".answer").removeClass("show").addClass("hidden");
      $(".checkbox-answer-div").removeClass("hidden").addClass("show");
   }
   else if($(this).val()==2){
       $(".answer").removeClass("show").addClass("hidden");
       $(".radio-answer-div").removeClass("hidden").addClass("show");
   }
   else{
       $(".answer").removeClass("show").addClass("hidden");
       $(".truefalse-answer-div").removeClass("hidden").addClass("show");
   }

})
$(document).delegate("#addquestion .checkbox-answer-div .answer-field:last","focus",function(){
  $(this).parents(".answer").append( $(this).parents(".answer-row").clone().removeClass("main"))
})
$(document).delegate("#editquestion .checkbox-answer-div .answer-field:last","focus",function(){
  $(this).parents(".answer").append( $(this).parents(".answer-row").clone().removeClass("main"))
})
$(document).delegate(".checkbox-answer-div .answer-field","keyup",function(){
  $(this).prev().find(".checkbox_ans").val($(".checkbox-answer-div .answer-field").index($(this)))

})
$(document).delegate(".radio-answer-div .answer-field","keyup",function(){
  $(this).prev().find(".radio-ans").val($(".radio-answer-div .answer-field").index($(this)))

})
$(document).delegate(".radio-answer-div .answer-field:last","focus",function(){
  $(this).parents(".answer").append( $(this).parents(".answer-row").clone().removeClass("main"));

})

$(document).delegate(".answer .fa-trash","click",function(){
   if(!$(this).parents(".answer-row").hasClass("main"))
     $(this).parents(".answer-row").remove();
      $(".checkbox-answer-div .answer-field").each(function(index){


           $(this).prev().find(".checkbox_ans").val(index)

       })
       $(".radio-answer-div .answer-field").each(function(index){
           $(this).prev().find(".radio-ans").val(index)

       })
})
// $(document).delegate(".add-question-link",'click',function(){
//
//     //  console.log($(this).data("quizno"))
//     //  addquestion =$(this)
// })
// $("#savequestion").click(function(){
//       button = $(this);
//       button.prop("disabled",true);
//       $.ajax({
//           url:baseUrl+'/course/newquestion',
//           type:"post",
//           data:$("#questionform").serialize(),
//           beforeSend:function(){
//
//
//           },
//           success:function(data){
//               console.log(data)
//                button.prop("disabled",false);
//               if(data=="error")
//                   $("#question-error").html("An error occurred while performing the operation").fadeIn().delay(3000).fadeOut();
//               else{
//                   $("#question-success").html("A new question was created successfully").fadeIn().delay(3000).fadeOut();
//                   //addquestion.parents(".dc-quizz-info").find(".hold-quiz").html(data)
//               }
//
//           }
//       })
// })


$(document).on('click','.delete-question',function(){
var option = confirm("Are you sure you want to delete this question?")
if(option ==1){
  var questionid= $(this).data('questionid');
  var quizid= $(this).data('quizid');
  var section =$(this).data("section");
  var parentindex= $(this).data("parentindex");
  var index =$(this).data("index");
  link = $(this);
  $(this).parents(".dc-quizz-info").find(".loading").removeClass("hide").addClass("show")
  $.ajax({
      url:baseUrl+'/course/deletequestion',
      data:{'quizid':quizid,'questionid':questionid},
      type:'post',
      beforeSend:function(){

      },
      success:function(data){
          //console.log(parentindex)
          link.parents(".dc-quizz-info").find(".loading").removeClass("show").addClass("hide")
          angular.element("#create-course-section").scope().$apply(function(scope){
            angular.element("#create-course-section").scope().sectionsLessonArray[section][parentindex].content[0].questions.splice(index,1)
          })
          link.parents(".form-items").remove();
      }
  })
}

})
//edit quiz popup

// $(document).delegate('.edit-question','click',function(){
//       var section =$(this).data("section");
//       var parentindex=$(this).data("parentindex");
//       var index =$(this).data("index");
//       $("#addquestion .modal-title").text("Edit Question");
//       var questions=angular.element("#create-course-section").scope().sectionsLessonArray[section][parentindex].content[0].questions[index];
//       $("#addquestion #quizid").val(questions.quiz_id);
//       $("#addquestion #questionid").val(questions.id);
//       $("#addquestion #question-title").val(questions.title);
//       $("#addquestion #question-type").val(questions.type)
//       $("#addquestion .question-type").trigger("change");
//       $("#addquestion #question-description").val(questions.description);
//        if(questions.type==1){
//             $("#addquestion .checkbox-answer-div").html("")
//                    for(i=0;i<questions.answers.length;i++){
//                       data=questions.answers;
//                         if(data[i]['iscorrect']=="true")
//                            $checked="checked"
//                        else
//                             $checked="";
//                        checked_answer='<div class="answer-row" > <div class="form-item-wrap" style="width:80%"> <div class="input-group"> <span class="input-group-addon"> <input type="checkbox" name="checkedanswers[]" '+$checked+' class="checkbox_ans" value="'+i+'" aria-label="..." style=""> </span> <input type="text" class="form-control answer-field" name="answer-field-multiple[]" aria-label="..." value="'+data[i]['answer']+'"> <span class="input-group-addon"><div class="course-region-tool"> <a style="cursor" class="delete" title="delete"><i class="fa fa-trash"></i></a> </div></span> </div></div><div class="form-item"> <input type="text" class="reason-multiple form-control reason" name="reason-multiple[]" value="'+data[i]['reason']+'" style="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer"> </div></div>'
//                        $("#addquestion .checkbox-answer-div").append(checked_answer)
//
//              }
//        }
//       else if(questions.type==2){
//                   $("#addquestion .radio-answer-div").html("")
//                   for(i=0;i<questions.answers.length;i++){
//                      data=questions.answers;
//                         if(data[i]['iscorrect']=="true")
//                            $checked="checked"
//                        else
//                             $checked=""
//                         radioans='<div class="answer-row main"> <div class="form-item-wrap" style="width:80%"> <div class="input-group"> <span class="input-group-addon"> <input type="radio" aria-label="..." name="radioselected" class="radio-ans" '+$checked+' value="'+i+'" style=""> </span> <input type="text" class="form-control answer-field" name="answer-field-radio[]" aria-label="..." value="'+data[i]['answer']+'"> <span class="input-group-addon"><div class="course-region-tool"> <a style="cursor" class="delete" title="delete"><i class="fa fa-trash"></i></a> </div></span> </div></div><div class="form-item"> <input type="text" class="form-control reason" name="reason-single[]" value="'+data[i]['reason']+'" style="height:40px; width:66%; margin-left:1em" placeholder="Enter why this is not or is the correct answer"> </div></div>';
//                        $("#addquestion .radio-answer-div").append(radioans)
//
//                    }
//       }
//       else{
//               data=questions.answers;
//               console.log(data[0].iscorrect);
//
//                 if(data[0].iscorrect)
//                       $("#addquestion .truefalse-answer-div .true").prop("checked",true);
//                 else
//                        $("#addquestion .truefalse-answer-div .false").prop("checked",true);
//                        $("#addquestion .truefalse-answer-div .true-reason").val(data[0].reason);
//                        $("#addquestion .truefalse-answer-div .false-reason").val(data[1].reason);
//
//          }
//
// })
// $(document).on("click","#editquestion-btn",function(){
//     button = $(this);
//     button.prop("disabled",true);
//     var section =$("#quiz-sectionid").val();
//     var index =$("#quiz-index").val()
//     $.ajax({
//         url:baseUrl+'/course/updatequestion',
//         type:"post",
//         data:$("#questionform").serialize(),
//         beforeSend:function(){
//
//         },
//         success:function(data){
//             console.log(data)
//             button.prop("disabled",false);
//             if(data=="error")
//                 $("#question-error").html("An error occurred while performing the operation").fadeIn().delay(3000).fadeOut();
//             else{
//                 angular.element("#create-course-section").scope().$apply(function(scope){
//                     angular.element("#create-course-section").scope().sectionsLessonArray[section][index].content[0].questions=data;
//                 })
//                 $("#question-success").html("Question was updated successfully").fadeIn().delay(3000).fadeOut();
//                 //addquestion.parents(".dc-quizz-info").find(".hold-quiz").html(data)
//             }
//
//         }
//     })
// })

function doSort(){

 $( ".outline-ul" ).sortable(
     {
         placeholder: "ui-state-highlight",
         connectWith: ".section-quiz-ul",
         handle: ".section-drag",
         start:function(event,ui){
             $(".ui-state-highlight").height(ui.item.height())
         },
         update:function(event,ui){
              var neworder = new Array();

                 $('.outline-ul .section-li').each(function() {
                     var counter  = $(this).data("counter-no");
                     var sectionid =$(this).data("part-id");

                    // var id  = $(this).attr("id");
                     //create an object
                     var obj = {};
                     //insert the id into the object
                     obj["counter"] = counter;
                     obj["sectionid"] = sectionid;

                     //push the object into the array
                     neworder.push(obj);
                   });
                   console.log(neworder)
                     $.post(baseUrl+"/course/sectionorder",{"neworder":neworder},function(data){
                       //  console.log(data)
                         updateCounter($(".outline-ul .section-li"),".section_counter");
                     })
         }

 });

 //$(document).on("click",".section-item-drag",function(){
     $( ".section-item-ul" ).sortable({
      connectWith: ".section-item-ul",
      placeholder: "ui-state-highlight",
      handle: ".section-item-drag",
      start:function(event,ui){
             $(".ui-state-highlight").height(ui.item.height())
      },
      update:function(event, ui){
          if (ui.sender) {
                 var neworder = new Array();
              $(this).find('.section-items').each(function() {
                         //var counter  = $(this).attr("data--no");
                         var outlineid =$(this).attr("data-outlineno");

                        // var id  = $(this).attr("id");
                         //create an object
                         var obj = {};
                         //insert the id into the object

                         obj["outlineno"] = outlineid;

                        //console.log($(this).attr("data-outlineno"))
                         //push the object into the array
                        neworder.push(obj);


              });
              //console.log(neworder)
              sectionid=ui.item.parents(".section-li").data("part-id")
               updateCounter($(this).find('.lesson-li'),".lesson_counter");
               updateCounter($(this).find(".quiz-li"),".quiz_counter");
              $.post(baseUrl+"/course/sectionitemsorder",{"neworder":neworder,"sectionid":sectionid,"swapsection":"1"},function(data){
                  console.log(data)
                  //updateCounter(ui.item.parents(".section-item-ul").find(".lesson_li"),".lesson_counter");
                  //
               })
         }else{
             var neworder = new Array();
             $(this).find('.section-items').each(function() {
                         //var counter  = $(this).attr("data--no");
                         var outlineid =$(this).attr("data-outlineno");

                        // var id  = $(this).attr("id");
                         //create an object
                         var obj = {};
                         //insert the id into the object

                         obj["outlineno"] = outlineid;

                        //console.log($(this).attr("data-outlineno"))
                         //push the object into the array
                        neworder.push(obj);


              });
              updateCounter($(this).find('.lesson-li'),".lesson_counter");
               updateCounter($(this).find(".quiz-li"),".quiz_counter");
               updateCounter($(this).find('.lesson-li'),".lesson_counter");
               updateCounter($(this).find(".quiz-li"),".quiz_counter");
              $.post(baseUrl+"/course/sectionitemsorder",{"neworder":neworder,"swapsection":"0"},function(data){
                  console.log(data)
                  //updateCounter(ui.item.parents(".section-item-ul").find(".lesson_li"),".lesson_counter");
                  //
               })
         }

      }
 });
}
/*$('.popup-with-zoom-anim').magnificPopup({
               type: 'inline',

               fixedContentPos: false,
               fixedBgPos: true,

               overflowY: 'auto',

               closeBtnInside: true,
               preloader: false,

               midClick: true,
               removalDelay: 300,
               mainClass: 'my-mfp-zoom-in'
           });
           $('.design-course-popup').delegate('.cancel', 'click', function(evt) {
               evt.preventDefault();
               $('.mfp-close').trigger('click');
           });*/
//End Quiz

//Drag and Drop
$(document).on("click",".section-drag",function(){

})
$(document).ready(function(){
 //doSort();
})
