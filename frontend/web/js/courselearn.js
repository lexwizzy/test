

$(document).ready(function(){

    $('#camo-rating').on('rating.change', function(event, value, caption) {

       // jQuery('#coursereviews-rating').rating('update',value);
        //$('#reviews').modal('show')
    });
    $('#coursereviews-rating').on('rating.change', function(event, value, caption) {

        jQuery('#camo-rating').rating('update',value);

    });
    $("#courseinfo").click(function(){
        $("#coursedesc").modal('show');
    });
  //  Ladda.bind(".save-reply, #save-discussion,#save-review");
})

$("#save-review").click(function(){

  //  var sr = Ladda.create( document.querySelector( '#save-review' ) )
    b=$(this);
    $.ajax({
        url:baseUrl+'/course/savereview',
        type:'get',
        data:$("#ratingform").serialize(),
        beforeSend:function(){
          //  sr.start();
            $("#save-review").prop("disable",true)
        },
        success:function(data){
          //  sr.stop();
            if(data==1){

                $("#alert-rating-success").html("Your rating was saved successfully");
                $("#alert-rating-success").fadeIn("slow").delay(1800).fadeOut("slow",function(){
                    b.prop("disable",false)
                })

            }
            else{
                $("#alert-rating-error").html("Sorry we are on unable to complete your request at this time.");
                 $("#alert-rating-error").fadeIn("slow").delay(1800).fadeOut("slow",function(){
                     b.prop("disable",false)
                })
            }

        }
    })
    return false;
})

$("#save-discussion").click(function(){
    //var sd = Ladda.create( document.querySelector( '#save-discussion' ))

    b=$(this);
    courseid=$("#discussions-courseid").val();
    $.ajax({
        url:baseUrl+'/course/savediscussion',
        type:'post',
        data:$("#discussionform").serialize(),
        beforeSend:function(){
            // sd.start();
            $("#save-discussion").prop("disable",true)
        },
        success:function(data){
             //sd.stop();
            if(data!=0){
                $.get(baseUrl+"/course/getdiscussions",{"discussionid":data},function(data){
                    $(".post-dicuss-status").append(data);
                })

            }
            else{

            }

        }
    })
    return false;
})

$(".save-reply").click(function(){
    //var sr = Ladda.create( document.querySelector( '.save-reply' ) )
    b=$(this);
    b.parents(".replyform").find(".userid").val(userid);
    $.ajax({
        url:baseUrl+'/course/savereply',
        type:'get',
        data:b.parents(".replyform").serialize(),
        beforeSend:function(){
          //  sr.start()
            $(".save-reply").prop("disable",true)
        },
        success:function(data){
            //sr.stop()
            if(data!=0){
                $.get(baseUrl+"/course/getlastreply",{"replyid":data},function(data){
                    b.parents(".replies").prepend(data);
                })

            }
            else{
               console.log(data);
            }

        }
    })
    return false;
})
$("#saveflag").click(function(){
    b=$(this);
    $.ajax({
        url:baseUrl+'/course/flagabuse',
        type:'get',
        data:$("#flagabuseform").serialize(),
        beforeSend:function(){
            $("#saveflag").prop("disable",true)
        },
        success:function(data){
            if(data==1){
                 $("#alert-flag-success").html("You have successfully received your complain. Our review team will get back to you after reviewing your complain.");
                $("#alert-flag-success").fadeIn("slow").delay(1800).fadeOut("slow",function(){
                    b.prop("disable",false)
                })

               // $('#flagabuse').modal('hide')
            }
            else{
                $("#alert-flag-error").html("Sorry we are on unable to submit your complain at this time.");
                 $("#alert-flag-error").fadeIn("slow").delay(1800).fadeOut("slow",function(){
                     b.prop("disable",false)
                })

                //alert("Sorry we are on unable to save your review at this time.")
            }

        }
    })
    return false;
})
$(".count-reply").click(function(){
    if($(this).parents(".discuss-section").find(".replies").is(":visible")){
        $(this).parents(".discuss-section").find(".replies").fadeOut("fast")
    }
    else
        $(this).parents(".discuss-section").find(".replies").fadeIn("slow");
})

/*$(document).on("click",".count-reply",function(){
    if($(this).parents(".list-li").find(".replies").is(":visible")){
        $(this).parents(".list-li").find(".replies").fadeOut("fast")
    }
    else
        $(this).parents(".list-li").find(".replies").fadeIn("slow");
})

$(".o-view").click(function(){
    alert()
    $.post("classroom",{"courseid":course_id});
})*/
