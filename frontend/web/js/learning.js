//
//$("#learning_video").bind('ended', function(){
//        $.get("lessoncomplete",{"userid":window.parent.userid,"courseid":window.parent.courseid,"lessonid":window.parent.current_lesson,"type":"lesson","status":"Completed","sectionid":window.parent.current_section},function(data){
//            console.log(data)
//        })
//  });


 var autoplayToggleNamespace = 'videojs-plugin-integration';
 var autoplayFromStorage = videojs.autoplaySettingFromStorage({
     namespace: autoplayToggleNamespace
 });

// initialize video
 var player = videojs('learning_video',{autoplay: autoplayFromStorage,plugins: {
        speed: [
          { text: '1배속', rate: 1, selected: true },
          { text: '2배속', rate: 2 },
          { text: '4배속', rate: 4 },
          { text: '8배속', rate: 8 }
        ]
      }});

// setup autoplay toggle plugin
player.autoplayToggle({
  namespace: autoplayToggleNamespace
});
player.persistvolume();
// 
// player.overlay({
//  content: '<strong><img src="'+baseUrl+'/images/logo.png"/></strong>',
//  overlays: [{
//    start: 'playing',
//    end: 'pause'
//  }]
// });
