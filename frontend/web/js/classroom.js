 var hash=""
 var current_section="";
 var current_lesson="";
 var current_lesson_type="";
$(document).ready(function(){
   hash =window.location.hash.split("/");
    if(hash[1]=="lesson")
        setandreload(baseUrl+"/course/learning?lessonid="+hash[2]+"&courseid="+courseid);
    else
        setandreload(baseUrl+"/course/quizintro?quiz="+hash[2]+"&courseid="+courseid);

    $(".section-item").click(function(){
        $(this).removeClass("active-lesson");
        $(this).addClass("active-lesson")
        itemid= $(this).attr("data-itemid");
         type= $(this).attr("data-type");
        itemid =$(this).attr("data-itemid");
        if(type=="lesson")
            setandreload(baseUrl+"/course/learning?lessonid="+itemid+"&courseid="+courseid);
        else
            setandreload(baseUrl+"/course/quizintro?quiz="+itemid+"&courseid="+courseid);
        window.location.hash="#/"+type+"/"+itemid
        getNavigation(type,itemid)
    });

    getNavigation(hash[1],hash[2]);

    $("#next").click(function(){
        type= $(this).attr("data-type");
        itemid =$(this).attr("data-itemid");
        console.log(type+" "+itemid)
        console.log($(this))
        //if($.trim(type)!="" && $.trim(itemid)!=""){
          if(type=="lesson")
              setandreload(baseUrl+"/course/learning?lessonid="+itemid+"&courseid="+courseid);
          else
              setandreload(baseUrl+"/course/quizintro?quiz="+itemid+"&courseid="+courseid);
          window.location.hash="#/"+type+"/"+itemid
          getNavigation(type,itemid)
        //}
        // else{
        //   //alert("Thats all folks");
        // }

    })
      $("#previous").click(function(){
        type= $(this).attr("data-type");
        itemid =$(this).attr("data-itemid");
        if($.trim(type)!="" && $.trim(itemid)!=""){
          if(type=="lesson")
              setandreload(baseUrl+"/course/learning?lessonid="+itemid+"&courseid="+courseid);
          else
              setandreload(baseUrl+"/course/quizintro?quiz="+itemid+"&courseid="+courseid);
            window.location.hash="#/"+type+"/"+itemid;
            getNavigation(type,itemid)
        }
    })
})

function getNavigation(type,itemid){
    $.get(baseUrl+"/course/classroomnavigation",{"itemid":itemid,"type":type},function(data){
        console.log(data)
         data =$.parseJSON(data)

        $("#previous").attr("data-itemid",data.prev.itemid);
        $("#previous").attr("data-type",data.prev.type);
        $("#next").attr("data-itemid",data.next.itemid);
        $("#next").attr("data-type",data.next.type);
        current_section=data.current.section;
        current_lesson=data.current.itemid;
        current_lesson_type=data.current.type;
        console.log(data.current.section,data.current.itemid,data.current.type);
        makeActive(data.current.section,data.current.itemid,data.current.type)
    })
}

function makeActive(section,itemid,type){
    $(".section-item").removeClass("active-lesson");
    $("li[data-type='"+type+"'][data-itemid='"+itemid+"'][data-section='"+section+"']").find(".section-item").addClass("active-lesson")
}
$(function () {
    setIFrameSize();
    $(window).resize(function () {
        setIFrameSize();
    });
});

$(".note-body").on("focusout",function(){
   savenote();
})
//
// $(document).on("click","#savenote",function(){
//   alert()
//     savenote();
// })
$("#savenote").click(function(){
  savenote();
})

$("#previous").click(function(){
   // window.history.back();
   // iframe.contentWindow.history.back();
   // lessonid = window.location.hash.replace("#","");
   // setandreload(baseUrl+"/course/learning?lessonid="+lessonid+"&courseid="+courseid);
})
function savenote(){

    //$(".note-body").on("focusout",function(){)
        $.get(baseUrl+"/course/saveusernote",{"note":$(".note-body").text(),"userid":userid},function(data){})
    //})
}

function setIFrameSize() {
    /*var parentDivWidth = $("#displayboard").parent().width();
    var parentDivHeight = $("#displayboard").parent().height();
    $("#displayboard")[0].setAttribute("width", parentDivWidth);
    $("#displayboard")[0].setAttribute("height", parentDivHeight);
    console.log(parentDivHeight)
    console.log(parentDivHeight)*/
}
function setandreload(src){
    $('iframe').attr('src', src);
}

/// track when pdf page is finished
pageNo = setInterval(function(){

lastpage =parseInt($("#displayboard").contents().find("#numPages").text().replace(/[^0-9]/g,""));
currentpage=parseInt($("#displayboard").contents().find("#pageNumber").val());
if(currentpage==lastpage)
    {
      $.get(baseUrl+"/course/lessoncomplete",{"userid":userid,"courseid":courseid,"lessonid":current_lesson,"type":"lesson","status":"Completed","sectionid":current_section},function(data){
          console.log(data)
      })
      clearInterval(pageNo)
    }
},10);

/// track when video is finihed
$("#displayboard").contents().find("#learning_video").bind('ended', function(){
        $.get("/course/lessoncomplete",{"userid":window.parent.userid,"courseid":courseid,"lessonid":current_lesson,"type":"lesson","status":"Completed","sectionid":current_section},function(data){
            console.log(data)
        })

  });
