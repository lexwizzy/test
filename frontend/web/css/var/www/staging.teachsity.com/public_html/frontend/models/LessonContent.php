<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;
use frontend\models\CourseLessons;
use frontend\models\User;
use yii\i18n\Formatter;

/**
 * This is the model class for table "lesson_content".
 *
 * @property string $content_no
 * @property string $lesson_no
 * @property string $content_defaultname
 * @property string $content_editedname
 * @property string $content_format
 * @property string $content_mime
 * @property string $content_link
 * @property string $content_basefoldername
 * @property string $date
 * @property string $uploaded_by
 * @property string $main
 */
class LessonContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_no', 'content_format',], 'required'],
            [['content_link', 'content_defaultname',], 'required','on'=>'external'],
            [['content_defaultname', 'text_content'], 'required','on'=>'text','message'=>'This field is required'],
            [['lesson_no', 'uploaded_by','video_length','no_of_pages'], 'integer'],
            [['date'], 'safe'],
            [['thumbnail'],'string'],
            [['size'],'integer'],
            [['content_defaultname'], 'string', 'max' => 200],
            [['content_editedname'], 'string', 'max' => 400],
            [['content_format'], 'string', 'max' => 100],
            [['content_link'], 'string', 'max' => 1000],
            [['content_link'],'url'],
            [['content_mime'],'string','max'=>400],
            [['text_content'],'string'],
            [['content_basefoldername'],'string','max'=>1000],
            [['main'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'content_no' => 'Content No',
            'lesson_no' => 'Lesson No',
            'content_defaultname' => 'Content Defaultname',
            'content_editedname' => 'Content Editedname',
            'content_format' => 'Content Format',
            'content_link' => 'Content Link',
            'date' => 'Date',
            'uploaded_by' => 'Uploaded By',
            'main' => 'Main',
        ];
    }
    public function getContents($lesson_no){
        $content="";
        $content.=LessonContent::getMainContent($lesson_no);
        $content.=LessonContent::getOtherContent($lesson_no);
        return $content;
    }
    public function getMainContent($lesson_no){
        $content="";
        $lesson = LessonContent::find()->where(['and',['=','lesson_no',$lesson_no],['=','main','Yes']])->one();
        if($lesson !=null){
             $content= '<!--Begin Contents info div--><div class="content-item-info">
                                                        <div class="upload-thumb">';

            if(!empty($lesson->video_length))
               $content.='<img src="'.Url::base().'/upload/coursevideos/thumbnails/'.$lesson->thumbnail.'" alt="">';
            else
                   $content.='<img src="'.Url::base().'/upload/coursepresentations/thumbnails/'.$lesson->thumbnail.'" alt="">';
                                                $content.='  </div>
                                                        <div class="upload-cnt">
                                                            <div class="title-file">
                                                                <span class="name-file">'.$lesson->content_defaultname.'</span>

                                                            </div>
                                                            <div class="upload-meta">
                                                                <span>Uploaded by '.User::findByUserId($lesson->uploaded_by)->firstname.' . '.Yii::$app->formatter->asDatetime($lesson->date, "php:d, M,Y").'</span>
                                                            </div>';
            if(!empty($lesson->video_length))
                                $content.='<div class="upload-meta"><i class="fa fa-clock-o icon">  '. gmdate("H:i:s",$lesson->video_length).'</i> </div>';
            else
                 $content.='<div class="upload-meta"><i class="fa icon fa-file-text-o"></i>  '.$lesson->no_of_pages.' Pages</i>
                                                    </div>';

                                                    $content.='

                                                    <div class="upload-footer">
                                                               <!-- <a href="#">Edit</a>
                                                                <a href="#">Preview</a>-->
                                                                <a style="cursor:pointer" class="delete-content" data-contentno="'.$lesson->content_no.'">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div><!--End Contents info div-->';
            return $content;

        }

    }
     public function getOtherContent($lesson_no){
         $others="";
        $others_content = LessonContent::find()->where(['and',['=','lesson_no',$lesson_no],['!=','main','Yes']])->all();
        if($others_content !=null){
            foreach($others_content as $lesson):
                $others .='<!--Begin Contents info div--><div class="content-item-info">
                                                        <div class="upload-thumb">';
             if(!empty($lesson->video_thumbnail))
               $others.='<img src="'.Url::base().'/upload/coursevideos/thumbnails/'.$lesson->video_thumbnail.'" alt="">';

                            $others.='</div>
                                                        <div class="upload-cnt">
                                                            <div class="title-file">
                                                                <span class="name-file">'.$lesson->content_defaultname.'</span>

                                                            </div>
                                                            <div class="upload-meta">
                                                                <span>Uploaded by '.User::findByUserId($lesson->uploaded_by)->firstname.' . '.Yii::$app->formatter->asDatetime($lesson->date, "php:d, M,Y").'</span>
                                                            </div>
                                                            <div class="upload-meta"><i class="fa fa-file icon">  '.LessonContent::formatBytes($lesson->size).'</i>
                                                    </div>
                                                            <div class="upload-footer">
                                                               <!-- <a href="#">Edit</a>-->
                                                                <!--<a href="#">Preview</a>-->
                                                                <a style="cursor:pointer" class="delete-content" data-contentno="'.$lesson->content_no.'">Delete</a>
                                                            </div>
                                                        </div>
                                                    </div><!--End Contents info div-->';
            endforeach;
            return $others;

        }

    }
    public function getContentThumbanail($content_id){

    }

    public function getVideoLength($file){
        //$ffmpeg_path = Yii::getAlias("@vendor").'/ffmpeg/bin/';
        $xyz = shell_exec("/home/ebuka/bin/ffmpeg -i \"{$file}\" 2>&1");
        $search='/Duration: (.*?),/';

        preg_match($search, $xyz, $matches);
        $explode = explode(':', $matches[1]);
        return round($explode[2]);
    }
    // public function convertToSeconds($time){
    //     $seconds="";
    //     $time =explode(":",$time);
    //     $hh=$time[0];
    //     $mm =$time[1];
    //     $ss = $time[2];
    //     return $seconds = 3600 * $hh + 60 * $mm + $ss;
    // }
    public function convertSeconds($second){
       return gmdate("H:i:s",$second);
    }

    public function createThumbnail($videofile,$destination){
         //$destination = Yii::getAlias("@webroot")."/upload/tdseste.png";
        // $ffmpeg_path = Yii::getAlias("@vendor").'/ffmpeg/bin/';
        return  shell_exec("/home/ebuka/bin/ffmpeg -i \"{$videofile}\" -ss 00:00:3.435 -vframes 1 ".$destination);
    }
    public function formatBytes($bytes, $decimals = 2) {
      $sz = 'BKMGTP';
      $factor = floor((strlen($bytes) - 1) / 3);
      return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
}
